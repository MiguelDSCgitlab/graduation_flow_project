import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;

import 'infrastructure/api_config.dart';
import 'infrastructure/component_diagram_get_dao.dart';
import 'infrastructure/container_diagram_get_dao.dart';
import 'infrastructure/context_diagram_get_dao.dart';
import 'infrastructure/view_context_diagram_repository.dart';

final apiViewDiagram = GetIt.instance;

void setupApiViewDiagram() {
  if (!GetIt.I.isRegistered<SVGAPIConfig>()) {
    apiViewDiagram.registerSingleton<SVGAPIConfig>(SVGAPIConfig());
  }

  if (!GetIt.I.isRegistered<http.Client>()) {
    apiViewDiagram.registerSingleton<http.Client>(http.Client());
  }

  if (!GetIt.I.isRegistered<ContextDiagramGetDao>()) {
    apiViewDiagram
        .registerSingleton<ContextDiagramGetDao>(ContextDiagramGetDaoServer());
  }

  if (!GetIt.I.isRegistered<ContainerDiagramGetDao>()) {
    apiViewDiagram.registerSingleton<ContainerDiagramGetDao>(
        ContainerDiagramGetDaoServer());
  }

  if (!GetIt.I.isRegistered<ComponentDiagramGetDao>()) {
    apiViewDiagram.registerSingleton<ComponentDiagramGetDao>(
        ComponentDiagramGetDaoServer());
  }

  if (!GetIt.I.isRegistered<ViewDiagramsRepository>()) {
    apiViewDiagram
        .registerSingleton<ViewDiagramsRepository>(ViewDiagramsRepository());
  }
}
