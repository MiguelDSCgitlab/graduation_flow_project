/*
  APIConfig is used to get endpoint for the api server.
  When running the app from terminal (using 'flutter run'), Flutter provides the
  option to pass environment properties. This can be used to inject compile/ build
  time variables. This is done by the 'dart-define' command, followed by a certain
  key/value pair. To change the default url settings use the following command when
  running the app:
  flutter run --dart-define=API_CONFIG=PROD
 */
class SVGAPIConfig {
  String apiConfig = "";

  // String c4Scheme = "http";
  // String c4BaseUrl = "10.0.2.2";
  String c4Scheme = "https";
  String c4BaseUrl = "c4enhanced.nl/orchestrator";

  // int c4Port = 8082;

  final String contextsEndpoint = "svg/contexts";

  // final String contextsEndpoint = "/context";

  final String containersEndpoint = "containers";

  // final String containersEndpoint = "container";
  final String componentsEndpoint = "components";

  SVGAPIConfig({String? anApiConfig}) {
    apiConfig = anApiConfig ?? const String.fromEnvironment("API_CONFIG");
  }

  Uri constructContextsUri(String contextId) {
    // print('inside constructContextsUri');
    Map<String, String> queryParams = {};
    // queryParams['contextId'] = contextId;
    queryParams['includeRelationShips'] = 'true';

    Uri uri = Uri(
      scheme: c4Scheme,
      host: c4BaseUrl,
      // port: c4Port,
      // path: "$contextsEndpoint/$contextId?includeRelationShips=true",
      path: "$contextsEndpoint/$contextId",
      // queryParameters: queryParams
    );
    // print(uri);
    return uri;
  }

  Uri constructContainersUri(String contextId, String containerId) {
    Map<String, String> queryParams = {};
    queryParams['contextId'] = contextId;

    Uri uri = Uri(
        scheme: c4Scheme,
        host: c4BaseUrl,
        // port: c4Port,
        // path: "$contextsEndpoint/$contextId/$containersEndpoint/$containerId?includeRelationShips=true",
        path: containersEndpoint,
        queryParameters: queryParams);
    return uri;
  }

  Uri constructComponentsUri(
      String contextId, String containerId, componentId) {
    Uri uri = Uri(
      scheme: c4Scheme,
      host: c4BaseUrl,
      // port: c4Port,
      path:
          "$contextsEndpoint/$contextId/$containersEndpoint/$containerId/$componentsEndpoint/$componentId?includeRelationShips=true",
    );

    return uri;
  }

  Map<String, String> getHeaders() {
    Map<String, String> headers = {};
    headers['Content-Type'] = 'application/json';
    return headers;
  }
}
