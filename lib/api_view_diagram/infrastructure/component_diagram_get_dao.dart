import 'dart:convert';

import 'package:http/http.dart' as http;

import '../../api_raw_data/infrastructure/api_c4_containers/dependency_injection.dart';
import '../../api_raw_data/infrastructure/api_c4_error/api_exception.dart';
import '../../api_raw_data/infrastructure/api_c4_error/error_dto.dart';
import 'api_config.dart';
import 'svg_output_dto.dart';

/*
Responsible for api call GET to /contexts
 */
abstract class ComponentDiagramGetDao {
  Future<SvgOutputDto> getComponentDiagram(
    String contextId,
    String containerId,
    String? componentId,
  );
}

/*
Retrieves contexts from API service.
Endpoint: /contexts?contextProperties=name,description
*/
class ComponentDiagramGetDaoServer implements ComponentDiagramGetDao {
  late SVGAPIConfig apiConfig;
  late http.Client client;

  ComponentDiagramGetDaoServer({
    http.Client? aClient,
    SVGAPIConfig? anApiConfig,
  }) {
    client = aClient ?? getIt<http.Client>();
    apiConfig = anApiConfig ?? getIt<SVGAPIConfig>();
  }

  @override
  Future<SvgOutputDto> getComponentDiagram(
    String contextId,
    String containerId,
    String? componentId,
  ) async {
    try {
      SvgOutputDto outputDto = SvgOutputDto();
      Map<String, String> queryParams = {
        'contextId': contextId,
        'containerId': containerId,
        'componentId': componentId ?? '',
      };
      Uri uri =
          apiConfig.constructComponentsUri(contextId, containerId, queryParams);
      // print(uri);
      var response = await client.get(uri);

      if (response.statusCode == 200) {
        outputDto.svg = response.body;
      } else if (response.statusCode == 400 || response.statusCode == 500) {
        var errorDto = ErrorDto.fromJson(json.decode(response.body));
        outputDto.errorDto = errorDto;
      } else {
        throw ApiException();
      }
      return outputDto;
    } catch (err) {
      rethrow;
    }
  }
}
