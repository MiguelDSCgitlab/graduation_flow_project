import 'dart:convert';

import 'package:http/http.dart' as http;

import '../../api_raw_data/infrastructure/api_c4_error/api_exception.dart';
import '../../api_raw_data/infrastructure/api_c4_error/error_dto.dart';
import '../di.dart';
import 'api_config.dart';
import 'svg_output_dto.dart';

/*
Responsible for api call GET to /contexts
 */
abstract class ContextDiagramGetDao {
  Future<SvgOutputDto> getContextDiagram(String? contextId);
}

/*
Retrieves contexts from API service.
Endpoint: /contexts?contextProperties=name,description
*/
class ContextDiagramGetDaoServer implements ContextDiagramGetDao {
  late SVGAPIConfig apiConfig;
  late http.Client client;

  ContextDiagramGetDaoServer({
    http.Client? aClient,
    SVGAPIConfig? anApiConfig,
  }) {
    client = aClient ?? apiViewDiagram<http.Client>();
    apiConfig = anApiConfig ?? apiViewDiagram<SVGAPIConfig>();
  }

  @override
  Future<SvgOutputDto> getContextDiagram(String? contextId) async {
    try {
      SvgOutputDto outputDto = SvgOutputDto();
      String aContextId = contextId ?? '';
      Uri uri = apiConfig.constructContextsUri(aContextId);
      // print(uri);
      var response = await client.get(uri);

      if (response.statusCode == 200) {
        // print(response.body);
        outputDto.svg = response.body;
      } else if (response.statusCode == 400 || response.statusCode == 500) {
        var errorDto = ErrorDto.fromJson(json.decode(response.body));
        outputDto.errorDto = errorDto;
      } else {
        throw ApiException();
      }
      return outputDto;
    } catch (err) {
      rethrow;
    }
  }
}
