import '../di.dart';
import 'component_diagram_get_dao.dart';
import 'container_diagram_get_dao.dart';
import 'context_diagram_get_dao.dart';
import 'svg_output_dto.dart';

class ViewDiagramsRepository {
  late ContextDiagramGetDao contextDiagramGetDao;
  late ContainerDiagramGetDao containerDiagramGetDao;
  late ComponentDiagramGetDao componentDiagramGetDao;

  ViewDiagramsRepository({
    ContextDiagramGetDao? aContextDiagramGetDao,
    ContainerDiagramGetDao? aContainerDiagramGetDao,
    ComponentDiagramGetDao? aComponentDiagramGetDao,
  }) {
    contextDiagramGetDao =
        aContextDiagramGetDao ?? apiViewDiagram<ContextDiagramGetDao>();
    containerDiagramGetDao =
        aContainerDiagramGetDao ?? apiViewDiagram<ContainerDiagramGetDao>();
    componentDiagramGetDao =
        aComponentDiagramGetDao ?? apiViewDiagram<ComponentDiagramGetDao>();
  }

  Future<SvgOutputDto> getContextDiagram({
    required String contextId,
  }) async {
    SvgOutputDto svgOutputDto =
        await contextDiagramGetDao.getContextDiagram(contextId);
    return svgOutputDto;
  }

  Future<SvgOutputDto> getContainerDiagram({
    required String contextId,
    String? containerId,
  }) async {
    return await containerDiagramGetDao.getContainerDiagram(
        contextId, containerId);
  }

  Future<SvgOutputDto> getComponentDiagram({
    required String contextId,
    required String containerId,
    String? componentId,
  }) async {
    return await componentDiagramGetDao.getComponentDiagram(
        contextId, containerId, componentId);
  }
}
