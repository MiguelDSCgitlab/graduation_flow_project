import '../../api_raw_data/infrastructure/api_c4_error/error_dto.dart';

class SvgOutputDto {
  String? svg;
  ErrorDto? errorDto;
}
