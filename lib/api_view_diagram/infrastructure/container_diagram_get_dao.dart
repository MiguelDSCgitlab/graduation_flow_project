import 'dart:convert';

import 'package:http/http.dart' as http;

import '../../api_raw_data/infrastructure/api_c4_containers/dependency_injection.dart';
import '../../api_raw_data/infrastructure/api_c4_error/api_exception.dart';
import '../../api_raw_data/infrastructure/api_c4_error/error_dto.dart';
import 'api_config.dart';
import 'svg_output_dto.dart';

/*
Responsible for api call GET to /contexts
 */
abstract class ContainerDiagramGetDao {
  Future<SvgOutputDto> getContainerDiagram(
      String contextId, String? containerId);
}

/*
Retrieves contexts from API service.
Endpoint: /contexts?contextProperties=name,description
*/
class ContainerDiagramGetDaoServer implements ContainerDiagramGetDao {
  late SVGAPIConfig apiConfig;
  late http.Client client;

  ContainerDiagramGetDaoServer({
    http.Client? aClient,
    SVGAPIConfig? anApiConfig,
  }) {
    client = aClient ?? getIt<http.Client>();
    apiConfig = anApiConfig ?? getIt<SVGAPIConfig>();
  }

  @override
  Future<SvgOutputDto> getContainerDiagram(
    String contextId,
    String? containerId,
  ) async {
    try {
      SvgOutputDto outputDto = SvgOutputDto();
      String aContainerId = containerId ?? '';
      Uri uri = apiConfig.constructContainersUri(contextId, aContainerId);
      // print(uri);
      var response = await client.get(uri);

      if (response.statusCode == 200) {
        outputDto.svg = response.body;
      } else if (response.statusCode == 400 || response.statusCode == 500) {
        var errorDto = ErrorDto.fromJson(json.decode(response.body));
        outputDto.errorDto = errorDto;
      } else {
        throw ApiException();
      }
      return outputDto;
    } catch (err) {
      rethrow;
    }
  }
}
