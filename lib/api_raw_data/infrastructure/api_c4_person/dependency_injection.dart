import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;

import '../api_c4_shared/api_config.dart';
import 'domain/add_person_usecase.dart';
import 'domain/delete_person_usecase.dart';
import 'domain/update_person_usecase.dart';
import 'infrastructure/api/person_delete_dao.dart';
import 'infrastructure/api/person_get_dao.dart';
import 'infrastructure/api/person_post.dao.dart';
import 'infrastructure/api/person_put_dao.dart';
import 'infrastructure/person_repository.dart';

final getIt = GetIt.instance;

void setupApiC4Person() {
  if (!GetIt.I.isRegistered<APIConfig>()) {
    getIt.registerSingleton<APIConfig>(APIConfig());
  }

  if (!GetIt.I.isRegistered<http.Client>()) {
    getIt.registerSingleton<http.Client>(http.Client());
  }

  if (!GetIt.I.isRegistered<DeletePersonDao>()) {
    getIt.registerSingleton<DeletePersonDao>(DeletePersonDaoApiServer());
  }

  if (!GetIt.I.isRegistered<GetPersonDao>()) {
    getIt.registerSingleton<GetPersonDao>(GetPersonDaoApiServer());
  }

  if (!GetIt.I.isRegistered<UpdatePersonDao>()) {
    getIt.registerSingleton<UpdatePersonDao>(UpdatePersonDaoApiServer());
  }

  if (!GetIt.I.isRegistered<PostPersonDao>()) {
    getIt.registerSingleton<PostPersonDao>(PostPersonDaoApiServer());
  }

  if (!GetIt.I.isRegistered<PersonRepository>()) {
    getIt.registerSingleton<PersonRepository>(PersonRepository());
  }

  if (!GetIt.I.isRegistered<AddPersonUseCase>()) {
    getIt.registerSingleton<AddPersonUseCase>(AddPersonUseCase());
  }

  if (!GetIt.I.isRegistered<DeletePersonUseCase>()) {
    getIt.registerSingleton<DeletePersonUseCase>(DeletePersonUseCase());
  }

  if (!GetIt.I.isRegistered<UpdatePersonUseCase>()) {
    getIt.registerSingleton<UpdatePersonUseCase>(UpdatePersonUseCase());
  }
}
