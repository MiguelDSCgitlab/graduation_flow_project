import 'dart:convert';

import 'package:http/http.dart' as http;

import '../../../api_c4_error/api_exception.dart';
import '../../../api_c4_error/error_dto.dart';
import '../../../api_c4_shared/api_config.dart';
import '../../../api_c4_shared/data/person_dto.dart';
import '../../dependency_injection.dart';
import '../data/person_output_dto.dart';

/*
Responsible for getting person
 */
abstract class GetPersonDao {
  Future<PersonOutputDto> getPerson(String id);
}

/*
Responsible for getting person
 */
class GetPersonDaoApiServer implements GetPersonDao {
  late APIConfig apiConfig;
  late http.Client client;

  GetPersonDaoApiServer({
    http.Client? aClient,
    APIConfig? anApiConfig,
  }) {
    client = aClient ?? getIt<http.Client>();
    apiConfig = anApiConfig ?? getIt<APIConfig>();
  }

  @override
  Future<PersonOutputDto> getPerson(String id) async {
    try {
      PersonOutputDto outputDto = PersonOutputDto();
      Uri uri = apiConfig.constructPersonUri(id);

      var response = await client.get(
        uri,
        headers: apiConfig.getHeaders(),
      );

      if (response.statusCode == 200) {
        Map<String, dynamic> json = jsonDecode(response.body);
        PersonDto personDto = PersonDto.fromJson(json);
        outputDto.personDto = personDto;
        return outputDto;
      } else if (response.statusCode == 400 || response.statusCode == 500) {
        var errorDto = ErrorDto.fromJson(json.decode(response.body));
        outputDto.errorDto = errorDto;
        return outputDto;
      } else {
        throw ApiException();
      }
    } catch (err) {
      rethrow;
    }
  }
}
