import 'dart:convert';

import 'package:http/http.dart' as http;

import '../../../api_c4_error/api_exception.dart';
import '../../../api_c4_error/error_dto.dart';
import '../../../api_c4_shared/api_config.dart';
import '../../../api_c4_shared/data/deleted_dto.dart';
import '../../dependency_injection.dart';
import '../data/person_output_dto.dart';

/*
Responsible for deleting person
 */
abstract class DeletePersonDao {
  Future<PersonOutputDto> deletePerson(String id);
}

/*
Responsible for deleting person
 */
class DeletePersonDaoApiServer implements DeletePersonDao {
  late APIConfig apiConfig;
  late http.Client client;

  DeletePersonDaoApiServer({
    http.Client? aClient,
    APIConfig? anApiConfig,
  }) {
    client = aClient ?? getIt<http.Client>();
    apiConfig = anApiConfig ?? getIt<APIConfig>();
  }

  @override
  Future<PersonOutputDto> deletePerson(String id) async {
    try {
      PersonOutputDto outputDto = PersonOutputDto();
      Uri uri = apiConfig.constructPersonUri(id);

      var response = await client.delete(
        uri,
        headers: apiConfig.getHeaders(),
      );

      if (response.statusCode == 200) {
        Map<String, dynamic> json = jsonDecode(response.body);
        DeletedDto deletedDto = DeletedDto.fromJson(json);
        outputDto.deletedDto = deletedDto;
        return outputDto;
      } else if (response.statusCode == 400 || response.statusCode == 500) {
        var errorDto = ErrorDto.fromJson(json.decode(response.body));
        outputDto.errorDto = errorDto;
        return outputDto;
      } else {
        throw ApiException();
      }
    } catch (err) {
      rethrow;
    }
  }
}
