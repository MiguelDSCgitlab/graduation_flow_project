import 'dart:convert';

import 'package:http/http.dart' as http;

import '../../../api_c4_error/api_exception.dart';
import '../../../api_c4_error/error_dto.dart';
import '../../../api_c4_shared/api_config.dart';
import '../../../api_c4_shared/data/person_dto.dart';
import '../../../api_c4_shared/data/person_input.dto.dart';
import '../../dependency_injection.dart';
import '../data/person_output_dto.dart';

/*
Responsible for updating person
 */
abstract class UpdatePersonDao {
  Future<PersonOutputDto> updatePerson(
      String contextId, PersonInputDto personInputDto);
}

/*
Responsible for updating person
 */
class UpdatePersonDaoApiServer implements UpdatePersonDao {
  late APIConfig apiConfig;
  late http.Client client;

  UpdatePersonDaoApiServer({
    http.Client? aClient,
    APIConfig? anApiConfig,
  }) {
    client = aClient ?? getIt<http.Client>();
    apiConfig = anApiConfig ?? getIt<APIConfig>();
  }

  @override
  Future<PersonOutputDto> updatePerson(
      String contextId, PersonInputDto personInputDto) async {
    try {
      PersonOutputDto outputDto = PersonOutputDto();
      Uri uri = apiConfig.constructPersonUri(contextId);

      var response = await client.put(
        uri,
        body: jsonEncode(
          personInputDto.toJson(),
        ),
        headers: apiConfig.getHeaders(),
      );

      if (response.statusCode == 200) {
        Map<String, dynamic> json = jsonDecode(response.body);
        PersonDto personDto = PersonDto.fromJson(json);
        outputDto.personDto = personDto;
        return outputDto;
      } else if (response.statusCode == 400 || response.statusCode == 500) {
        var errorDto = ErrorDto.fromJson(json.decode(response.body));
        outputDto.errorDto = errorDto;
        return outputDto;
      } else {
        throw ApiException();
      }
    } catch (err) {
      rethrow;
    }
  }
}
