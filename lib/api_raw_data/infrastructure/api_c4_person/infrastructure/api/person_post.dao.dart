import 'dart:convert';

import 'package:http/http.dart' as http;

import '../../../api_c4_error/api_exception.dart';
import '../../../api_c4_error/error_dto.dart';
import '../../../api_c4_error/required_property_exception.dart';
import '../../../api_c4_shared/api_config.dart';
import '../../../api_c4_shared/data/person_input.dto.dart';
import '../../dependency_injection.dart';
import '../data/person_output_dto.dart';

/*
Responsible for creating person
 */
abstract class PostPersonDao {
  Future<PersonOutputDto> postPerson(String contextId, PersonInputDto inputDto);
}

/*
Responsible for creating person
 */
class PostPersonDaoApiServer implements PostPersonDao {
  late APIConfig apiConfig;
  late http.Client client;

  PostPersonDaoApiServer({
    http.Client? aClient,
    APIConfig? anApiConfig,
  }) {
    client = aClient ?? getIt<http.Client>();
    apiConfig = anApiConfig ?? getIt<APIConfig>();
  }

  @override
  Future<PersonOutputDto> postPerson(
      String contextId, PersonInputDto inputDto) async {
    try {
      PersonOutputDto outputDto = PersonOutputDto();
      Uri uri = apiConfig.constructPersonUri(contextId);
      var response = await client.post(uri,
          body: jsonEncode(
            inputDto.toJson(),
          ),
          headers: apiConfig.getHeaders());
      if (response.statusCode == 200) {
        Map json = jsonDecode(response.body);
        if (json.containsKey('id')) {
          outputDto.id = json['id'];
          return outputDto;
        } else {
          throw RequiredPropertyMissingException(
            propertyName: 'id',
            className: 'PostPersonDaoApiServer',
          );
        }
      } else if (response.statusCode == 400 || response.statusCode == 500) {
        var errorDto = ErrorDto.fromJson(json.decode(response.body));
        outputDto.errorDto = errorDto;
        return outputDto;
      } else {
        throw ApiException();
      }
    } catch (err) {
      rethrow;
    }
  }
}
