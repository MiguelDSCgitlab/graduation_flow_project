import '../../../api_c4_error/error_dto.dart';
import '../../../api_c4_shared/data/deleted_dto.dart';
import '../../../api_c4_shared/data/person_dto.dart';

class PersonOutputDto {
  ErrorDto? errorDto;
  PersonDto? personDto;
  DeletedDto? deletedDto;
  String? id;
}
