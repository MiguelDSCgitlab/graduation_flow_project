import '../../api_c4_shared/data/person_input.dto.dart';
import '../dependency_injection.dart';
import 'api/person_delete_dao.dart';
import 'api/person_get_dao.dart';
import 'api/person_post.dao.dart';
import 'api/person_put_dao.dart';
import 'data/person_output_dto.dart';

class PersonRepository {
  late PostPersonDao postDao;
  late GetPersonDao getDao;
  late UpdatePersonDao updateDao;
  late DeletePersonDao deleteDao;

  PersonRepository(
      {PostPersonDao? postPersonDao,
      GetPersonDao? getPersonDao,
      UpdatePersonDao? updatePersonDao,
      DeletePersonDao? deletePersonDao}) {
    postDao = postPersonDao ?? getIt<PostPersonDao>();
    getDao = getPersonDao ?? getIt<GetPersonDao>();
    updateDao = updatePersonDao ?? getIt<UpdatePersonDao>();
    deleteDao = deletePersonDao ?? getIt<DeletePersonDao>();
  }

  Future<PersonOutputDto> postPerson(
      String contextId, PersonInputDto personInputDto) async {
    personInputDto.location ??= "Internal";
    personInputDto.description ??= "Uses";
    return await postDao.postPerson(contextId, personInputDto);
  }

  Future<PersonOutputDto> getPerson(String id) async {
    return await getDao.getPerson(id);
  }

  Future<PersonOutputDto> putPerson(
      String contextId, PersonInputDto personInputDto) async {
    return await updateDao.updatePerson(contextId, personInputDto);
  }

  Future<PersonOutputDto> deletePerson(String id) async {
    return await deleteDao.deletePerson(id);
  }
}
