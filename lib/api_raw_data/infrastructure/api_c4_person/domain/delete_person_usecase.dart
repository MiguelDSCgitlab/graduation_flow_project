import '../dependency_injection.dart';
import '../infrastructure/person_repository.dart';

class DeletePersonUseCase {
  late PersonRepository repository;

  DeletePersonUseCase({PersonRepository? personRepository}) {
    repository = personRepository ?? getIt<PersonRepository>();
  }

  call({required String contextId}) async {
    return await repository.deletePerson(contextId);
  }
}
