import '../../api_c4_shared/data/person_input.dto.dart';
import '../dependency_injection.dart';
import '../infrastructure/data/person_output_dto.dart';
import '../infrastructure/person_repository.dart';

class AddPersonUseCase {
  late PersonRepository repository;

  AddPersonUseCase({PersonRepository? personRepository}) {
    repository = personRepository ?? getIt<PersonRepository>();
  }

  Future<PersonOutputDto> call({
    required String contextId,
    required PersonInputDto personInputDto,
  }) async {
    return await repository.postPerson(contextId, personInputDto);
  }
}
