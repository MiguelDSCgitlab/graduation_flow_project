import '../../api_c4_shared/data/person_input.dto.dart';
import '../dependency_injection.dart';
import '../infrastructure/person_repository.dart';

class UpdatePersonUseCase {
  late PersonRepository repository;

  UpdatePersonUseCase({PersonRepository? personRepository}) {
    repository = personRepository ?? getIt<PersonRepository>();
  }

  call(
      {required String contextId,
      required PersonInputDto personInputDto}) async {
    return await repository.putPerson(contextId, personInputDto);
  }
}
