import '../api_c4_error/error_dto.dart';
import 'container_dto.dart';

class ContainersOutputDto {
  List<MinContainerDto>? containers;
  ErrorDto? errorDto;
}
