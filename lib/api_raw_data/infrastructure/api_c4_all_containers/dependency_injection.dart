import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;

import '../api_c4_shared/api_config.dart';
import 'all_containers_get_dao.dart';
import 'all_containers_repository.dart';

final getIt = GetIt.instance;

void setupApiC4AllContainers() {
  if (!GetIt.I.isRegistered<APIConfig>()) {
    getIt.registerSingleton<APIConfig>(APIConfig());
  }

  if (!GetIt.I.isRegistered<http.Client>()) {
    getIt.registerSingleton<http.Client>(http.Client());
  }

  if (!GetIt.I.isRegistered<AllContainersGetDao>()) {
    getIt
        .registerSingleton<AllContainersGetDao>(AllContainersGetDaoApiServer());
  }

  if (!GetIt.I.isRegistered<AllContainersRepository>()) {
    getIt.registerSingleton<AllContainersRepository>(AllContainersRepository());
  }
}
