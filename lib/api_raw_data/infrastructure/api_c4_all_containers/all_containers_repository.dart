import 'all_containers_get_dao.dart';
import 'containers_output_dto.dart';
import 'dependency_injection.dart';

class AllContainersRepository {
  late AllContainersGetDao allContainersGetDao;

  AllContainersRepository({
    AllContainersGetDao? anAllContainersGetDao,
  }) {
    allContainersGetDao = anAllContainersGetDao ?? getIt<AllContainersGetDao>();
  }

  Future<ContainersOutputDto> getContainers() async {
    return await allContainersGetDao.getContainers();
  }
}
