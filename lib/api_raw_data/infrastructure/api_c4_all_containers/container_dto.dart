import '../api_c4_error/required_property_exception.dart';

class MinContainerDto {
  String componentId;
  String containerId;
  String contextId;
  String name;
  String? description;
  String contextName;
  String containerName;

  MinContainerDto(
      {required this.componentId,
      required this.containerId,
      required this.contextId,
      required this.name,
      this.description,
      required this.contextName,
      required this.containerName});

  factory MinContainerDto.fromJson(Map<String, dynamic> data) {
    try {
      final componentId = data.containsKey('componentId')
          ? data['componentId']
          : throw RequiredPropertyMissingException(
              propertyName: 'componentId', className: 'ContainerDto');

      final containerId = data.containsKey('containerId')
          ? data['containerId']
          : throw RequiredPropertyMissingException(
              propertyName: 'containerId', className: 'ContainerDto');

      final contextId = data.containsKey('contextId')
          ? data['contextId']
          : throw RequiredPropertyMissingException(
              propertyName: 'contextId', className: 'ContainerDto');

      final name = data.containsKey('name')
          ? data['name']
          : throw RequiredPropertyMissingException(
              propertyName: 'name', className: 'ContainerDto');

      final description =
          data.containsKey('description') ? data['description'] : null;

      final contextName = data.containsKey('contextName')
          ? data['contextName']
          : throw RequiredPropertyMissingException(
              propertyName: 'contextName', className: 'ContainerDto');

      final containerName = data.containsKey('containerName')
          ? data['containerName']
          : throw RequiredPropertyMissingException(
              propertyName: 'containerName', className: 'ContainerDto');

      return MinContainerDto(
        componentId: componentId,
        containerId: containerId,
        contextId: contextId,
        name: name,
        description: description,
        contextName: contextName,
        containerName: containerName,
      );
    } catch (err) {
      rethrow;
    }
  }
}
