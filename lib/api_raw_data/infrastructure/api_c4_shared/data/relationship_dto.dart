import '../../api_c4_error/required_property_exception.dart';

class RelationshipDto {
  String id;
  String relationshipId;
  String toContext;
  String? toContainer;
  String? toComponent;
  String? toName;
  String fromContext;
  String? fromContainer;
  String? fromComponent;
  String? fromName;
  String? description;
  List<String>? technology;
  String? communicationMechanism;
  String? communicationStyle;
  String? type;
  bool? outgoing;

  RelationshipDto({
    required this.toContext,
    this.toContainer,
    this.toComponent,
    required this.toName,
    required this.fromContext,
    this.fromContainer,
    this.fromComponent,
    required this.fromName,
    required this.id,
    required this.relationshipId,
    this.description,
    this.technology,
    this.communicationMechanism,
    this.communicationStyle,
    this.type,
    this.outgoing,
  });

  factory RelationshipDto.fromJson(Map<String, dynamic> data) {
    final toContext = data.containsKey('toContext')
        ? data['toContext']
        : throw RequiredPropertyMissingException(
            propertyName: 'toContext', className: 'RelationshipDto');

    final toContainer =
        data.containsKey('toContainer') ? data['toContainer'] : null;

    final toComponent =
        data.containsKey('toComponent') ? data['toComponent'] : null;

    final toName = data.containsKey('toName') ? data['toName'] : null;

    final fromContext = data.containsKey('fromContext')
        ? data['fromContext']
        : throw RequiredPropertyMissingException(
            propertyName: 'fromContext', className: 'RelationshipDto');

    final fromContainer =
        data.containsKey('fromContainer') ? data['fromContainer'] : null;

    final fromComponent =
        data.containsKey('fromComponent') ? data['fromComponent'] : null;

    final fromName = data.containsKey('fromName') ? data['fromName'] : null;

    final id = data.containsKey('id')
        ? data['id']
        : throw RequiredPropertyMissingException(
            propertyName: 'id', className: 'RelationshipDto');

    final relationshipId = data.containsKey('relationshipId')
        ? data['relationshipId']
        : throw RequiredPropertyMissingException(
            propertyName: 'relationshipId', className: 'RelationshipDto');

    final description =
        data.containsKey('description') ? data['description'] : null;

    final List<String> technologyList = data.containsKey('technology')
        ? data['technology'] != null
            ? List<String>.from(data['technology'])
            : List.empty()
        : List.empty();

    final communicationMechanism = data.containsKey('communicationMechanism')
        ? data['communicationMechanism']
        : null;

    final communicationStyle = data.containsKey('communicationStyle')
        ? data['communicationStyle']
        : null;

    final type = data.containsKey('type') ? data['type'] : null;
    final outgoing = data.containsKey('outgoing') ? data['outgoing'] : null;

    return RelationshipDto(
      fromContext: fromContext,
      fromContainer: fromContainer,
      fromComponent: fromComponent,
      fromName: fromName,
      toContext: toContext,
      toContainer: toContainer,
      toComponent: toComponent,
      toName: toName,
      id: id,
      relationshipId: relationshipId,
      description: description,
      technology: technologyList,
      communicationMechanism: communicationMechanism,
      communicationStyle: communicationStyle,
      type: type,
      outgoing: outgoing,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'toContext': toContext,
      'toContainer': toContainer,
      'toComponent': toComponent,
      'toName': toName,
      'fromContext': fromContext,
      'fromContainer': fromContainer,
      'fromComponent': fromComponent,
      'fromName': fromName,
      'id': id,
      'relationshipId': relationshipId,
      'description': description,
      'technology': technology,
      'communicationMechanism': communicationMechanism,
      'communicationStyle': communicationStyle,
      'type': type,
      'outgoing': outgoing,
    };
  }
}
