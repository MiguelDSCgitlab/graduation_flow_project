class PersonInputDto {
  String name;
  String? description;
  String? location;

  PersonInputDto({
    required this.name,
    this.description,
    this.location,
  });

  factory PersonInputDto.fromJson(Map<String, dynamic> json) {
    return PersonInputDto(
      name: json['name'] as String,
      description: json['description'] as String?,
      location: json['location'] as String?,
    );
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> values = {};
    values['name'] = name;

    if (description != null) {
      values['description'] = description;
    }
    if (location != null) {
      values['location'] = location;
    }

    return values;
  }
}
