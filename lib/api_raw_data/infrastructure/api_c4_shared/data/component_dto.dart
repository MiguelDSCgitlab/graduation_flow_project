import '../../api_c4_error/required_property_exception.dart';
import 'relationship_dto.dart';

class ComponentDto {
  String componentId;
  String id;
  String name;
  String? description;
  String? type;
  String? subType;
  List<String>? technology;
  String? location;
  String createdAt;
  String updatedAt;
  List<RelationshipDto>? relationships = [];

  ComponentDto({
    required this.componentId,
    required this.id,
    required this.name,
    this.description,
    this.type,
    this.subType,
    this.location,
    this.createdAt = "",
    this.updatedAt = "",
    this.technology,
    this.relationships,
  });

  factory ComponentDto.fromJson(Map<String, dynamic> data) {
    try {
      final id = data.containsKey('id')
          ? data['id']
          : throw RequiredPropertyMissingException(
              propertyName: 'id', className: 'ComponentDto');

      final componentId = data.containsKey('componentId')
          ? data['componentId']
          : throw RequiredPropertyMissingException(
              propertyName: 'componentId', className: 'ComponentDto');

      final name = data.containsKey('name')
          ? data['name']
          : throw RequiredPropertyMissingException(
              propertyName: 'name', className: 'ComponentDto');

      final description =
          data.containsKey('description') ? data['description'] : null;
      final location =
          data.containsKey('location') ? data['location'] : 'Internal';
      final createdAt = data.containsKey('createdAt') ? data['createdAt'] : "";
      final updatedAt = data.containsKey('updatedAt') ? data['updatedAt'] : "";
      final type = data.containsKey('type') ? data['type'] : null;
      final subType = data.containsKey('subType') ? data['subType'] : null;
      final List<String> technologyList = data.containsKey('technology')
          ? data['technology'] != null
              ? List<String>.from(data['technology'])
              : List.empty()
          : List.empty();
      final List<RelationshipDto> relationshipList = data
              .containsKey('relationShips')
          ? List<RelationshipDto>.from(data['relationShips']
              .map<RelationshipDto>(
                  (e) => RelationshipDto.fromJson(e as Map<String, dynamic>)))
          : List.empty();

      return ComponentDto(
        id: id,
        componentId: componentId,
        name: name,
        description: description,
        location: location,
        createdAt: createdAt,
        updatedAt: updatedAt,
        type: type,
        subType: subType,
        technology: technologyList,
        relationships: relationshipList,
      );
    } catch (err) {
      rethrow;
    }
  }

  Map<String, dynamic> toJson() {
    return {
      'componentId': componentId,
      'id': id,
      'name': name,
      'description': description,
      'type': type,
      'subType': subType,
      'technology': technology,
      'location': location,
      'createdAt': createdAt,
      'updatedAt': updatedAt,
      'relationships': relationships?.map((e) => e.toJson()).toList(),
    };
  }
}
