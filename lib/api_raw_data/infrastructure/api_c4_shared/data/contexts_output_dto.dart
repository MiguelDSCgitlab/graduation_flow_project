import '../../api_c4_error/error_dto.dart';
import 'context_dto.dart';

class ContextsOutputDto {
  List<ContextDto>? contexts;
  ErrorDto? errorDto;
}
