import '../../api_c4_error/required_property_exception.dart';
import 'component_dto.dart';
import 'relationship_dto.dart';

class ContainerDto {
  String containerId;
  String id;
  String name;
  String? description;
  String? type;
  String? subType;
  List<String>? technology;
  String? location;
  String createdAt;
  String updatedAt;
  List<RelationshipDto>? relationships = [];
  List<ComponentDto>? elements = [];

  ContainerDto(
      {required this.containerId,
      required this.id,
      required this.name,
      this.description,
      this.type,
      this.subType,
      this.location = "Internal",
      this.createdAt = "",
      this.updatedAt = "",
      this.technology,
      this.relationships,
      this.elements});

  factory ContainerDto.fromJson(Map<String, dynamic> data) {
    try {
      final id = data.containsKey('id')
          ? data['id']
          : throw RequiredPropertyMissingException(
              propertyName: 'id', className: 'ContainerDto');

      final containerId =
          data.containsKey('containerId') ? data['containerId'] : "";

      final name = data.containsKey('name')
          ? data['name']
          : throw RequiredPropertyMissingException(
              propertyName: 'name', className: 'ContainerDto');

      final description =
          data.containsKey('description') ? data['description'] : null;
      final location =
          data.containsKey('location') ? data['location'] : 'Internal';
      final createdAt = data.containsKey('createdAt') ? data['createdAt'] : '';
      final updatedAt = data.containsKey('updatedAt') ? data['updatedAt'] : '';
      final type = data.containsKey('type') ? data['type'] : '';
      final subType = data.containsKey('subType') ? data['subType'] : '';
      final List<String> technologyList = data.containsKey('technology')
          ? data['technology'] != null
              ? List<String>.from(data['technology'])
              : List.empty()
          : List.empty();
      final List<RelationshipDto> relationshipList = data
              .containsKey('relationShips')
          ? List<RelationshipDto>.from(data['relationShips']
              .map<RelationshipDto>(
                  (e) => RelationshipDto.fromJson(e as Map<String, dynamic>)))
          : List.empty();
      final List<ComponentDto> componentList = data.containsKey('elements')
          ? List<ComponentDto>.from(data['elements'].map<ComponentDto>(
              (e) => ComponentDto.fromJson(e as Map<String, dynamic>)))
          : List.empty();

      return ContainerDto(
          id: id,
          containerId: containerId,
          name: name,
          description: description,
          location: location,
          createdAt: createdAt,
          updatedAt: updatedAt,
          type: type,
          subType: subType,
          technology: technologyList,
          relationships: relationshipList,
          elements: componentList);
    } catch (err) {
      rethrow;
    }
  }

  Map<String, dynamic> toJson() {
    return {
      'containerId': containerId,
      'id': id,
      'name': name,
      'description': description,
      'type': type,
      'subType': subType,
      'technology': technology,
      'location': location,
      'createdAt': createdAt,
      'updatedAt': updatedAt,
      'relationships': relationships?.map((e) => e.toJson()).toList(),
      'elements': elements?.map((e) => e.toJson()).toList(),
    };
  }
}
