import '../../api_c4_error/required_property_exception.dart';

class DeletedDto {
  String? type;
  bool? acknowledged;
  int deletedCount;

  DeletedDto({
    this.type,
    this.acknowledged,
    required this.deletedCount,
  });

  factory DeletedDto.fromJson(Map<String, dynamic> data) {
    try {
      final type = data.containsKey('deleted') ? data['deleted'] : null;

      final acknowledged =
          data.containsKey('acknowledged') ? data['acknowledged'] : null;

      final deletedCount = data.containsKey('deletedCount')
          ? data['deletedCount']
          : throw RequiredPropertyMissingException(
              propertyName: 'deletedCount', className: 'DeletedDto');

      return DeletedDto(
          acknowledged: acknowledged, deletedCount: deletedCount, type: type);
    } catch (err) {
      rethrow;
    }
  }

  Map<String, dynamic> toJson() {
    return {
      'deleted': type,
      'acknowledged': acknowledged,
      'deletedCount': deletedCount,
    };
  }
}
