import '../../api_c4_error/required_property_exception.dart';
import 'container_dto.dart';
import 'person_dto.dart';
import 'relationship_dto.dart';

class ContextDto {
  String contextId;
  String id;
  String name;
  String? description;
  String? type;
  String? location;
  String? createdAt;
  String? updatedAt;
  PersonDto? person;
  List<RelationshipDto>? relationships = List.empty();
  List<ContainerDto>? containers = List.empty();
  Map<String, dynamic> values = {};

  ContextDto(
      {required this.contextId,
      required this.id,
      required this.name,
      this.description,
      this.type = "Context",
      this.location = "Internal",
      this.createdAt = "",
      this.updatedAt = "",
      this.person,
      this.relationships,
      this.containers});

  factory ContextDto.fromJson(Map<String, dynamic> data) {
    try {
      final id = data.containsKey('id')
          ? data['id']
          : throw RequiredPropertyMissingException(
              propertyName: 'id', className: 'ContextDto');
      final name = data.containsKey('name')
          ? data['name']
          : throw RequiredPropertyMissingException(
              propertyName: 'name', className: 'ContextDto');
      final description =
          data.containsKey('description') ? data['description'] : null;
      final location = data.containsKey('location') ? data['location'] : null;
      final createdAt =
          data.containsKey('createdAt') ? data['createdAt'] : null;
      final updatedAt =
          data.containsKey('updatedAt') ? data['updatedAt'] : null;
      final type = data.containsKey('type') ? data['type'] : null;
      final personData = data.containsKey('person') ? data['person'] : null;
      PersonDto? personDto;
      if (personData != null) {
        personDto = PersonDto.fromJson(data['person']);
      }
      final List<RelationshipDto> relationshipList = data
              .containsKey('relationShips')
          ? List<RelationshipDto>.from(data['relationShips']
              .map<RelationshipDto>(
                  (e) => RelationshipDto.fromJson(e as Map<String, dynamic>)))
          : List.empty();

      final List<ContainerDto> containerList = data.containsKey('elements')
          ? List<ContainerDto>.from(data['elements'].map<ContainerDto>(
              (e) => ContainerDto.fromJson(e as Map<String, dynamic>)))
          : List.empty();

      ContextDto contextDto = ContextDto(
          id: id,
          contextId: id,
          name: name,
          description: description,
          location: location,
          createdAt: createdAt,
          updatedAt: updatedAt,
          type: type,
          person: personDto,
          relationships: relationshipList,
          containers: containerList);
      return contextDto;
    } catch (err) {
      rethrow;
    }
  }

  Map<String, dynamic> toJson() {
    values = {};
    values['id'] = id;
    values['contextId'] = contextId;
    values['name'] = name;
    values['description'] = description;
    if (location != null) {
      values['location'] = location;
    }
    if (createdAt != null) {
      values['createdAt'] = createdAt;
    }
    if (updatedAt != null) {
      values['updatedAt'] = updatedAt;
    }
    if (type != null) {
      values['type'] = type;
    }
    if (person != null) {
      values['person'] = person;
    }
    if (relationships != null) {
      values['relationships'] = relationships;
    }
    if (containers != null) {
      values['containers'] = containers;
    }
    return values;
  }

  @override
  String toString() {
    return toJson().toString();
  }
}
