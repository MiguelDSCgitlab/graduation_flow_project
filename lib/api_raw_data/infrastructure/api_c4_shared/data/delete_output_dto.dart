import '../../api_c4_error/error_dto.dart';
import 'deleted_dto.dart';

class DeleteOutputDto {
  DeletedDto? deletedDto;
  ErrorDto? errorDto;
}
