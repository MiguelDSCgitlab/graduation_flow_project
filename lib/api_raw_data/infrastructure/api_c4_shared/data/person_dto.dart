import '../../api_c4_error/required_property_exception.dart';
import 'relationship_dto.dart';

class PersonDto {
  String id;
  String name;
  String? description;
  String type;
  String? location;
  List<RelationshipDto>? relationships = [];

  PersonDto({
    required this.id,
    required this.name,
    required this.type,
    this.description,
    this.location,
    this.relationships,
  });

  factory PersonDto.fromJson(Map<String, dynamic> data) {
    try {
      final id = data.containsKey('id')
          ? data['id']
          : throw RequiredPropertyMissingException(
              propertyName: 'id', className: 'PersonDto');
      final name = data.containsKey('name')
          ? data['name']
          : throw RequiredPropertyMissingException(
              propertyName: 'name', className: 'PersonDto');
      final description =
          data.containsKey('description') ? data['description'] : null;
      final location = data.containsKey('location') ? data['location'] : null;
      final type = data.containsKey('type')
          ? data['type']
          : throw RequiredPropertyMissingException(
              propertyName: 'type', className: 'PersonDto');

      List<RelationshipDto> relationshipList = data.containsKey('relationShips')
          ? List<RelationshipDto>.from(
              data['relationShips'].map<RelationshipDto>(
                (dynamic i) => RelationshipDto.fromJson(i),
              ),
            )
          : List.empty();

      return PersonDto(
        id: id,
        name: name,
        type: type,
        description: description,
        location: location,
        relationships: relationshipList,
      );
    } catch (err) {
      rethrow;
    }
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
      'description': description,
      'type': type,
      'location': location,
      'relationships': relationships?.map((e) => e.toJson()).toList(),
    };
  }
}
