/*
  APIConfig is used to get endpoint for the api server.
  When running the app from terminal (using 'flutter run'), Flutter provides the
  option to pass environment properties. This can be used to inject compile/ build
  time variables. This is done by the 'dart-define' command, followed by a certain
  key/value pair. To change the default url settings use the following command when
  running the app:
  flutter run --dart-define=API_CONFIG=PROD
 */
class APIConfig {
  String apiConfig = "";

  // String c4Scheme = "http";
  // String c4BaseUrl = "136.144.189.154";
  String c4Scheme = "https";
  String c4BaseUrl = "c4enhanced.nl";

  // int c4Port = 8000;
  final String contextsEndpoint = "raw/contexts";
  final String contextsSearchEndpoint = "search";
  final String containersEndpoint = "containers";
  final String componentsEndpoint = "components";
  final String personEndpoint = "person";
  final String relationshipsEndpoint = "relationships";

  APIConfig({String? anApiConfig}) {
    apiConfig = anApiConfig ?? const String.fromEnvironment("API_CONFIG");
  }

  Uri constructContextsUri({Map<String, String>? queryParams}) {
    Map<String, String>? queryParameters = queryParams;
    Uri uri = Uri(
        scheme: c4Scheme,
        host: c4BaseUrl,
        // port: c4Port,
        path: contextsEndpoint,
        queryParameters: queryParameters);
    return uri;
  }

  Uri constructContextsFromNameUri(String contextName,
      {Map<String, String>? queryParams}) {
    Map<String, String>? queryParameters = queryParams;
    Uri uri = Uri(
        scheme: c4Scheme,
        host: c4BaseUrl,
        // port: c4Port,
        path: "$contextsEndpoint/$contextsSearchEndpoint/$contextName",
        queryParameters: queryParameters);
    return uri;
  }

  Uri constructContextUri(String contextId,
      {Map<String, String>? queryParams}) {
    Uri uri;
    if (queryParams != null && queryParams.isNotEmpty) {
      uri = Uri(
          scheme: c4Scheme,
          host: c4BaseUrl,
          // port: c4Port,
          path: "$contextsEndpoint/$contextId",
          queryParameters: queryParams);
    } else {
      uri = Uri(
        scheme: c4Scheme,
        host: c4BaseUrl,
        // port: c4Port,
        path: "$contextsEndpoint/$contextId",
      );
    }
    return uri;
  }

  Uri constructContextByNameUri(String name) {
    Uri uri = Uri(
      scheme: c4Scheme,
      host: c4BaseUrl,
      // port: c4Port,
      path: "$contextsEndpoint/search/$name",
    );
    return uri;
  }

  Uri constructContextRelationshipUri(
      String contextId, String? relationshipId) {
    String path = relationshipId == null
        ? "$contextsEndpoint/$contextId/$relationshipsEndpoint"
        : "$contextsEndpoint/$contextId/$relationshipsEndpoint/$relationshipId";
    Uri uri = Uri(
      scheme: c4Scheme,
      host: c4BaseUrl,
      // port: c4Port,
      path: path,
    );
    return uri;
  }

  Uri constructPersonUri(String contextId) {
    Uri uri = Uri(
      scheme: c4Scheme,
      host: c4BaseUrl,
      // port: c4Port,
      path: "$contextsEndpoint/$contextId/$personEndpoint",
    );
    return uri;
  }

  Uri constructPersonRelationshipUri(String contextId, String? relationshipId) {
    String path = relationshipId == null
        ? "$contextsEndpoint/$contextId/$personEndpoint/$relationshipsEndpoint"
        : "$contextsEndpoint/$contextId/$personEndpoint/$relationshipsEndpoint/$relationshipId";

    Uri uri = Uri(
      scheme: c4Scheme,
      host: c4BaseUrl,
      // port: c4Port,
      path: path,
    );
    return uri;
  }

  Uri constructContainersUri(
      String contextId, Map<String, dynamic>? queryParams) {
    Uri uri;
    queryParams == null
        ? uri = Uri(
            scheme: c4Scheme,
            host: c4BaseUrl,
            // port: c4Port,
            path: "$contextsEndpoint/$contextId/$containersEndpoint",
          )
        : uri = Uri(
            scheme: c4Scheme,
            host: c4BaseUrl,
            // port: c4Port,
            path: "$contextsEndpoint/$contextId/$containersEndpoint",
            queryParameters: queryParams,
          );
    return uri;
  }

  Uri constructAllContainersUri() {
    Uri uri = Uri(
      scheme: c4Scheme,
      host: c4BaseUrl,
      // port: c4Port,
      path: 'raw/containers',
    );

    return uri;
  }

  Uri constructAllComponentsUri() {
    Uri uri = Uri(
      scheme: c4Scheme,
      host: c4BaseUrl,
      // port: c4Port,
      path: 'raw/components',
    );

    return uri;
  }

  Uri constructContainerUri(
      String contextId, String containerId, Map<String, dynamic>? queryParams) {
    Uri uri;
    queryParams == null
        ? uri = Uri(
            scheme: c4Scheme,
            host: c4BaseUrl,
            // port: c4Port,
            path:
                "$contextsEndpoint/$contextId/$containersEndpoint/$containerId",
          )
        : uri = Uri(
            scheme: c4Scheme,
            host: c4BaseUrl,
            // port: c4Port,
            path:
                "$contextsEndpoint/$contextId/$containersEndpoint/$containerId",
            queryParameters: queryParams,
          );

    return uri;
  }

  Uri constructContainerRelationshipUri(
    String contextId,
    String containerId,
    String? relationshipId,
  ) {
    String path = relationshipId == null
        ? "$contextsEndpoint/$contextId/$containersEndpoint/$containerId/$relationshipsEndpoint"
        : "$contextsEndpoint/$contextId/$containersEndpoint/$containerId/$relationshipsEndpoint/$relationshipId";

    Uri uri = Uri(
      scheme: c4Scheme,
      host: c4BaseUrl,
      // port: c4Port,
      path: path,
    );
    return uri;
  }

  Uri constructComponentsUri(
      String contextId, String containerId, Map<String, dynamic>? queryParams) {
    Uri uri;
    queryParams == null
        ? uri = Uri(
            scheme: c4Scheme,
            host: c4BaseUrl,
            // port: c4Port,
            path:
                "$contextsEndpoint/$contextId/$containersEndpoint/$containerId/$componentsEndpoint",
          )
        : uri = Uri(
            scheme: c4Scheme,
            host: c4BaseUrl,
            // port: c4Port,
            path:
                "$contextsEndpoint/$contextId/$containersEndpoint/$containerId/$componentsEndpoint",
            queryParameters: queryParams,
          );

    return uri;
  }

  Uri constructComponentUri(String contextId, String containerId,
      String componentId, Map<String, dynamic>? queryParams) {
    Uri uri = Uri(
      scheme: c4Scheme,
      host: c4BaseUrl,
      // port: c4Port,
      path:
          "$contextsEndpoint/$contextId/$containersEndpoint/$containerId/$componentsEndpoint/$componentId",
    );
    return uri;
  }

  Uri constructComponentRelationshipsUri(String contextId, String containerId,
      String componentId, String? relationshipId) {
    String path = relationshipId == null
        ? "$contextsEndpoint/$contextId/$containersEndpoint/$containerId/$componentsEndpoint/$componentId/$relationshipsEndpoint"
        : "$contextsEndpoint/$contextId/$containersEndpoint/$containerId/$componentsEndpoint/$componentId/$relationshipsEndpoint/$relationshipId";

    Uri uri = Uri(
      scheme: c4Scheme,
      host: c4BaseUrl,
      // port: c4Port,
      path: path,
    );
    return uri;
  }

  Map<String, String> getHeaders() {
    Map<String, String> headers = {};
    headers['Content-Type'] = 'application/json';
    return headers;
  }
}
