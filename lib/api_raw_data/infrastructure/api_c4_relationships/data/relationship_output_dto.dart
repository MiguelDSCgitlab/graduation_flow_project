import '../../api_c4_error/error_dto.dart';
import '../../api_c4_shared/data/deleted_dto.dart';
import '../../api_c4_shared/data/relationship_dto.dart';

class RelationshipOutputDto {
  ErrorDto? errorDto;
  DeletedDto? deletedDto;
  String? id;
  RelationshipDto? relationshipDto;
  List<RelationshipDto>? relationships;

  RelationshipOutputDto({
    this.errorDto,
    this.deletedDto,
    this.id,
    this.relationshipDto,
    this.relationships,
  });
}
