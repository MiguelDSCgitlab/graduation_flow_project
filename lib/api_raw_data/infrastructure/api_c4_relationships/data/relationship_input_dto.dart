class RelationshipInputDto {
  String? relationshipId;
  String? toContext;
  String? toContainer;
  String? toComponent;
  String fromContext;
  String? fromContainer;
  String? fromComponent;
  String fromName;
  String? toName;
  String? description;
  List<String>? technology;
  String? communicationMechanism;
  String? communicationStyle;
  String type;
  bool outgoing = true;

  RelationshipInputDto({
    this.relationshipId,
    required this.fromContext,
    required this.toContext,
    required this.fromName,
    required this.toName,
    this.fromContainer,
    this.fromComponent,
    this.toContainer,
    this.toComponent,
    this.description,
    required this.type,
    this.technology,
    this.communicationStyle,
    this.communicationMechanism,
  });

  Map<String, dynamic> toJson() {
    Map<String, dynamic> map = {};
    map['fromContext'] = fromContext;
    map['fromContainer'] = fromContainer;
    map['fromComponent'] = fromComponent;
    map['toContext'] = toContext;
    map['toContainer'] = toContainer;
    map['toComponent'] = toComponent;
    map['fromName'] = fromName;
    map['toName'] = toName;
    map['type'] = type;
    if (description != null) {
      map['description'] = description;
    }
    if (technology != null) {
      map['technology'] = technology;
    }
    if (communicationStyle != null) {
      map['communicationStyle'] = communicationStyle;
    }
    if (communicationMechanism != null) {
      map['communicationMechanism'] = communicationMechanism;
    }

    return map;
  }
}
