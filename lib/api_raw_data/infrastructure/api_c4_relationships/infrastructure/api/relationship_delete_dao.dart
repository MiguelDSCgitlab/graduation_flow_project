import 'dart:convert';

import 'package:c4_frontend_data_entry_flutter/api_raw_data/infrastructure/api_c4_relationships/data/relationship_input_dto.dart';
import 'package:http/http.dart' as http;

import '../../../api_c4_error/api_exception.dart';
import '../../../api_c4_error/error_dto.dart';
import '../../../api_c4_shared/api_config.dart';
import '../../../api_c4_shared/data/deleted_dto.dart';
import '../../data/relationship_output_dto.dart';
import '../../dependency_injection.dart';

/*
Responsible for creating person
 */
abstract class DeleteRelationshipDao {
  Future<RelationshipOutputDto> deleteRelationship(
      Uri uri, RelationshipInputDto inputDto);
}

/*
Responsible for creating person
 */
class DeleteRelationshipDaoApiServer implements DeleteRelationshipDao {
  late http.Client client;
  late APIConfig apiConfig;

  DeleteRelationshipDaoApiServer({
    http.Client? aClient,
    APIConfig? anApiConfig,
  }) {
    client = aClient ?? getIt<http.Client>();
    apiConfig = anApiConfig ?? getIt<APIConfig>();
  }

  @override
  Future<RelationshipOutputDto> deleteRelationship(
      Uri uri, RelationshipInputDto inputDto) async {
    try {
      RelationshipOutputDto outputDto = RelationshipOutputDto();
      String jsonEncodeString = jsonEncode(
        inputDto.toJson(),
      );
      var response = await client.delete(uri,
          body: jsonEncodeString, headers: apiConfig.getHeaders());

      if (response.statusCode == 200) {
        DeletedDto deletedDto = DeletedDto.fromJson(jsonDecode(response.body));
        outputDto.deletedDto = deletedDto;
        return outputDto;
      }

      if (response.statusCode == 400 || response.statusCode == 500) {
        var errorDto = ErrorDto.fromJson(jsonDecode(response.body));
        outputDto.errorDto = errorDto;
        return outputDto;
      }

      throw ApiException();
    } catch (err) {
      rethrow;
    }
  }
}
