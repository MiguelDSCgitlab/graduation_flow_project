import 'dart:convert';

import 'package:http/http.dart' as http;

import '../../../api_c4_error/api_exception.dart';
import '../../../api_c4_error/error_dto.dart';
import '../../../api_c4_shared/api_config.dart';
import '../../../api_c4_shared/data/relationship_dto.dart';
import '../../data/relationship_output_dto.dart';
import '../../dependency_injection.dart';

/*
Responsible for creating person
 */
abstract class GetRelationshipDao {
  Future<RelationshipOutputDto> getRelationship(Uri uri);
}

/*
Responsible for creating person
 */
class GetRelationshipDaoApiServer implements GetRelationshipDao {
  late http.Client client;
  late APIConfig apiConfig;

  GetRelationshipDaoApiServer({
    http.Client? aClient,
    APIConfig? anApiConfig,
  }) {
    client = aClient ?? getIt<http.Client>();
    apiConfig = anApiConfig ?? getIt<APIConfig>();
  }

  @override
  Future<RelationshipOutputDto> getRelationship(Uri uri) async {
    try {
      RelationshipOutputDto outputDto = RelationshipOutputDto();
      var response = await client.get(uri, headers: apiConfig.getHeaders());

      if (response.statusCode == 200) {
        RelationshipDto relationshipDto =
            RelationshipDto.fromJson(jsonDecode(response.body));
        outputDto.relationshipDto = relationshipDto;
        return outputDto;
      }

      if (response.statusCode == 400 || response.statusCode == 500) {
        var errorDto = ErrorDto.fromJson(jsonDecode(response.body));
        outputDto.errorDto = errorDto;
        return outputDto;
      }

      throw ApiException();
    } catch (err) {
      rethrow;
    }
  }
}
