import 'dart:convert';

import 'package:http/http.dart' as http;

import '../../../api_c4_error/api_exception.dart';
import '../../../api_c4_error/error_dto.dart';
import '../../../api_c4_shared/api_config.dart';
import '../../data/relationship_input_dto.dart';
import '../../data/relationship_output_dto.dart';
import '../../dependency_injection.dart';

/*
Responsible for creating person
 */
abstract class PostRelationshipDao {
  Future<RelationshipOutputDto> postRelationship(
      Uri uri, RelationshipInputDto inputDto);
}

/*
Responsible for creating person
 */
class PostRelationshipDaoApiServer implements PostRelationshipDao {
  late http.Client client;
  late APIConfig apiConfig;

  PostRelationshipDaoApiServer({
    http.Client? aClient,
    APIConfig? anApiConfig,
  }) {
    client = aClient ?? getIt<http.Client>();
    apiConfig = anApiConfig ?? getIt<APIConfig>();
  }

  @override
  Future<RelationshipOutputDto> postRelationship(
      Uri uri, RelationshipInputDto inputDto) async {
    try {
      RelationshipOutputDto outputDto = RelationshipOutputDto();
      var response = await client.post(uri,
          body: jsonEncode(
            inputDto.toJson(),
          ),
          headers: apiConfig.getHeaders());

      if (response.statusCode == 200) {
        Map json = jsonDecode(response.body);
        if (json.containsKey('id')) {
          outputDto.id = json['id'];
          return outputDto;
        }
      }
      if (response.statusCode == 400 || response.statusCode == 500) {
        var errorDto = ErrorDto.fromJson(jsonDecode(response.body));
        outputDto.errorDto = errorDto;
        return outputDto;
      }

      throw ApiException();
    } catch (err) {
      rethrow;
    }
  }
}
