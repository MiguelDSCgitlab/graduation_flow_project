import 'dart:convert';

import 'package:http/http.dart' as http;

import '../../../api_c4_error/api_exception.dart';
import '../../../api_c4_error/error_dto.dart';
import '../../../api_c4_shared/api_config.dart';
import '../../../api_c4_shared/data/relationship_dto.dart';
import '../../data/relationship_output_dto.dart';
import '../../dependency_injection.dart';

abstract class GetAllRelationshipsDao {
  Future<RelationshipOutputDto> getRelationships(Uri uri);
}

/*
Responsible for get relationships
 */
class GetAllRelationshipsDaoApiServer implements GetAllRelationshipsDao {
  late http.Client client;
  late APIConfig apiConfig;

  GetAllRelationshipsDaoApiServer({
    http.Client? aClient,
    APIConfig? anApiConfig,
  }) {
    client = aClient ?? getIt<http.Client>();
    apiConfig = anApiConfig ?? getIt<APIConfig>();
  }

  @override
  Future<RelationshipOutputDto> getRelationships(Uri uri) async {
    try {
      RelationshipOutputDto outputDto = RelationshipOutputDto();
      var response = await client.get(uri, headers: apiConfig.getHeaders());

      if (response.statusCode == 200) {
        List<RelationshipDto> relationships = [];
        var data = jsonDecode(response.body) as List;
        for (var element in data) {
          relationships.add(RelationshipDto.fromJson(element));
        }
        outputDto.relationships = relationships;
        return outputDto;
      }

      if (response.statusCode == 400 || response.statusCode == 500) {
        var errorDto = ErrorDto.fromJson(jsonDecode(response.body));
        outputDto.errorDto = errorDto;
        return outputDto;
      }

      throw ApiException();
    } catch (err) {
      rethrow;
    }
  }
}
