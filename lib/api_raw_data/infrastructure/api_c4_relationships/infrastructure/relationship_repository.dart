import 'package:http/http.dart' as http;

import '../../api_c4_shared/api_config.dart';
import '../data/relationship_input_dto.dart';
import '../data/relationship_output_dto.dart';
import '../dependency_injection.dart';
import 'api/all_relationships_get_dao.dart';
import 'api/relationship_delete_dao.dart';
import 'api/relationship_get_dao.dart';
import 'api/relationship_post_dao.dart';
import 'api/relationship_put_dao.dart';

class RelationshipRepository {
  late http.Client client;
  late APIConfig apiConfig;
  late PostRelationshipDao postRelationshipDao;
  late DeleteRelationshipDao deleteRelationshipDao;
  late UpdateRelationshipDao updateRelationshipDao;
  late GetRelationshipDao getRelationshipDao;
  late GetAllRelationshipsDao getAllRelationshipsDao;

  RelationshipRepository(
      {http.Client? aClient,
      APIConfig? anApiConfig,
      PostRelationshipDao? aPostRelationshipDao,
      DeleteRelationshipDao? aDeleteRelationshipDao,
      UpdateRelationshipDao? anUpdateRelationshipDao,
      GetRelationshipDao? aGetRelationshipDao,
      GetAllRelationshipsDao? aGetAllRelationshipsDao}) {
    client = aClient ?? getIt<http.Client>();
    apiConfig = anApiConfig ?? getIt<APIConfig>();
    postRelationshipDao = aPostRelationshipDao ?? getIt<PostRelationshipDao>();
    deleteRelationshipDao =
        aDeleteRelationshipDao ?? getIt<DeleteRelationshipDao>();
    updateRelationshipDao =
        anUpdateRelationshipDao ?? getIt<UpdateRelationshipDao>();
    getRelationshipDao = aGetRelationshipDao ?? getIt<GetRelationshipDao>();
    getAllRelationshipsDao =
        aGetAllRelationshipsDao ?? getIt<GetAllRelationshipsDao>();
  }

  Future<RelationshipOutputDto> addContextRelationship(
      String contextId, RelationshipInputDto inputDto) async {
    Uri uri = apiConfig.constructContextRelationshipUri(contextId, null);
    return await postRelationshipDao.postRelationship(uri, inputDto);
  }

  Future<RelationshipOutputDto> deleteContextRelationship(String contextId,
      String relationshipId, RelationshipInputDto inputDto) async {
    Uri uri =
        apiConfig.constructContextRelationshipUri(contextId, relationshipId);
    return await deleteRelationshipDao.deleteRelationship(uri, inputDto);
  }

  Future<RelationshipOutputDto> updateContextRelationship(String contextId,
      String relationshipId, RelationshipInputDto inputDto) async {
    Uri uri =
        apiConfig.constructContextRelationshipUri(contextId, relationshipId);
    return await updateRelationshipDao.updateRelationship(uri, inputDto);
  }

  Future<RelationshipOutputDto> getContextRelationship(
      String contextId, String relationshipId) async {
    Uri uri =
        apiConfig.constructContextRelationshipUri(contextId, relationshipId);
    return await getRelationshipDao.getRelationship(uri);
  }

  Future<RelationshipOutputDto> getAllContextRelationships(
      String contextId) async {
    Uri uri = apiConfig.constructContextRelationshipUri(contextId, null);
    return await getAllRelationshipsDao.getRelationships(uri);
  }

  Future<RelationshipOutputDto> addPersonRelationship(
      String contextId, RelationshipInputDto inputDto) async {
    Uri uri = apiConfig.constructPersonRelationshipUri(contextId, null);
    return await postRelationshipDao.postRelationship(uri, inputDto);
  }

  Future<RelationshipOutputDto> deletePersonRelationship(String contextId,
      String relationshipId, RelationshipInputDto inputDto) async {
    Uri uri =
        apiConfig.constructPersonRelationshipUri(contextId, relationshipId);
    return await deleteRelationshipDao.deleteRelationship(uri, inputDto);
  }

  Future<RelationshipOutputDto> updatePersonRelationship(String contextId,
      String relationshipId, RelationshipInputDto inputDto) async {
    Uri uri =
        apiConfig.constructPersonRelationshipUri(contextId, relationshipId);
    return await updateRelationshipDao.updateRelationship(uri, inputDto);
  }

  Future<RelationshipOutputDto> getPersonRelationship(
      String contextId, String relationshipId) async {
    Uri uri =
        apiConfig.constructPersonRelationshipUri(contextId, relationshipId);
    return await getRelationshipDao.getRelationship(uri);
  }

  Future<RelationshipOutputDto> getAllPersonRelationships(
      String contextId) async {
    Uri uri = apiConfig.constructPersonRelationshipUri(contextId, null);
    return await getAllRelationshipsDao.getRelationships(uri);
  }

  Future<RelationshipOutputDto> addContainerRelationship(String contextId,
      String containerId, RelationshipInputDto inputDto) async {
    Uri uri = apiConfig.constructContainerRelationshipUri(
        contextId, containerId, null);
    return await postRelationshipDao.postRelationship(uri, inputDto);
  }

  Future<RelationshipOutputDto> deleteContainerRelationship(
      String contextId,
      String containerId,
      String relationshipId,
      RelationshipInputDto inputDto) async {
    Uri uri = apiConfig.constructContainerRelationshipUri(
        contextId, containerId, relationshipId);
    return await deleteRelationshipDao.deleteRelationship(uri, inputDto);
  }

  Future<RelationshipOutputDto> updateContainerRelationship(
      String contextId,
      String containerId,
      String relationshipId,
      RelationshipInputDto inputDto) async {
    Uri uri = apiConfig.constructContainerRelationshipUri(
        contextId, containerId, relationshipId);
    return await updateRelationshipDao.updateRelationship(uri, inputDto);
  }

  Future<RelationshipOutputDto> getContainerRelationship(
      String contextId, String containerId, String relationshipId) async {
    Uri uri = apiConfig.constructContainerRelationshipUri(
        contextId, containerId, relationshipId);
    return await getRelationshipDao.getRelationship(uri);
  }

  Future<RelationshipOutputDto> getAllContainerRelationships(
      String contextId, String containerId) async {
    Uri uri = apiConfig.constructContainerRelationshipUri(
        contextId, containerId, null);
    return await getAllRelationshipsDao.getRelationships(uri);
  }

  Future<RelationshipOutputDto> addComponentRelationship(
      String contextId,
      String containerId,
      String componentId,
      RelationshipInputDto inputDto) async {
    Uri uri = apiConfig.constructComponentRelationshipsUri(
        contextId, containerId, componentId, null);
    return await postRelationshipDao.postRelationship(uri, inputDto);
  }

  Future<RelationshipOutputDto> deleteComponentRelationship(
    String contextId,
    String containerId,
    String componentId,
    String relationshipId,
    RelationshipInputDto inputDto,
  ) async {
    Uri uri = apiConfig.constructComponentRelationshipsUri(
        contextId, containerId, componentId, relationshipId);
    return await deleteRelationshipDao.deleteRelationship(uri, inputDto);
  }

  Future<RelationshipOutputDto> updateComponentRelationship(
      String contextId,
      String containerId,
      String componentId,
      String relationshipId,
      RelationshipInputDto inputDto) async {
    Uri uri = apiConfig.constructComponentRelationshipsUri(
        contextId, containerId, componentId, relationshipId);
    return await updateRelationshipDao.updateRelationship(uri, inputDto);
  }

  Future<RelationshipOutputDto> getComponentRelationship(String contextId,
      String containerId, String componentId, String relationshipId) async {
    Uri uri = apiConfig.constructComponentRelationshipsUri(
        contextId, containerId, componentId, relationshipId);
    return await getRelationshipDao.getRelationship(uri);
  }

  Future<RelationshipOutputDto> getComponentRelationships(String contextId,
      String containerId, String componentId, String relationshipId) async {
    Uri uri = apiConfig.constructComponentRelationshipsUri(
        contextId, containerId, componentId, relationshipId);
    return await getAllRelationshipsDao.getRelationships(uri);
  }
}
