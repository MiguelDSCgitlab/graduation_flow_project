import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;

import '../api_c4_shared/api_config.dart';
import 'infrastructure/api/all_relationships_get_dao.dart';
import 'infrastructure/api/relationship_delete_dao.dart';
import 'infrastructure/api/relationship_get_dao.dart';
import 'infrastructure/api/relationship_post_dao.dart';
import 'infrastructure/api/relationship_put_dao.dart';
import 'infrastructure/relationship_repository.dart';

final getIt = GetIt.instance;

void setupApiC4Relationships() {
  if (!GetIt.I.isRegistered<APIConfig>()) {
    getIt.registerSingleton<APIConfig>(APIConfig());
  }

  if (!GetIt.I.isRegistered<http.Client>()) {
    getIt.registerSingleton<http.Client>(http.Client());
  }

  if (!GetIt.I.isRegistered<GetAllRelationshipsDao>()) {
    getIt.registerSingleton<GetAllRelationshipsDao>(
        GetAllRelationshipsDaoApiServer());
  }

  if (!GetIt.I.isRegistered<PostRelationshipDao>()) {
    getIt
        .registerSingleton<PostRelationshipDao>(PostRelationshipDaoApiServer());
  }

  if (!GetIt.I.isRegistered<DeleteRelationshipDao>()) {
    getIt.registerSingleton<DeleteRelationshipDao>(
        DeleteRelationshipDaoApiServer());
  }

  if (!GetIt.I.isRegistered<UpdateRelationshipDao>()) {
    getIt.registerSingleton<UpdateRelationshipDao>(
        UpdateRelationshipDaoApiServer());
  }

  if (!GetIt.I.isRegistered<GetRelationshipDao>()) {
    getIt.registerSingleton<GetRelationshipDao>(GetRelationshipDaoApiServer());
  }

  if (!GetIt.I.isRegistered<RelationshipRepository>()) {
    getIt.registerSingleton<RelationshipRepository>(RelationshipRepository());
  }
}
