import 'dart:convert';

import 'package:http/http.dart' as http;

import '../../../di/di.dart';
import '../api_c4_error/api_exception.dart';
import '../api_c4_error/error_dto.dart';
import '../api_c4_shared/api_config.dart';
import '../api_c4_shared/data/container_dto.dart';
import 'containers_output_dto.dart';

abstract class SingleContainerGetDao {
  Future<ContainersOutputDto> getContainer(
      String contextId, String containerId);
}

class SingleContainerGetDaoApiServer extends SingleContainerGetDao {
  late APIConfig apiConfig;
  late http.Client client;

  SingleContainerGetDaoApiServer({
    http.Client? aClient,
    APIConfig? anApiConfig,
  }) {
    client = aClient ?? getIt<http.Client>();
    apiConfig = anApiConfig ?? getIt<APIConfig>();
  }

  @override
  Future<ContainersOutputDto> getContainer(
      String contextId, String containerId) async {
    try {
      ContainersOutputDto outputDto = ContainersOutputDto();
      Map<String, dynamic> queryParams = {'includeRelationShips': true};

      Uri uri =
          apiConfig.constructContainerUri(contextId, containerId, queryParams);

      List<ContainerDto> containers = [];
      var response = await client.get(
        uri,
        headers: apiConfig.getHeaders(),
      );
      if (response.statusCode == 200) {
        var data = jsonDecode(response.body) as List;
        for (var element in data) {
          containers.add(ContainerDto.fromJson(element));
        }
        outputDto.containers = containers;
        return outputDto;
      }

      if (response.statusCode == 400 || response.statusCode == 500) {
        var errorDto = ErrorDto.fromJson(json.decode(response.body));
        outputDto.errorDto = errorDto;
        return outputDto;
      }

      throw ApiException();
    } catch (err) {
      rethrow;
    }
  }
}
