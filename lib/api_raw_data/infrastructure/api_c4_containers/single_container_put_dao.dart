import 'dart:convert';

import 'package:http/http.dart' as http;

import '../../../di/di.dart';
import '../api_c4_error/api_exception.dart';
import '../api_c4_error/error_dto.dart';
import '../api_c4_shared/api_config.dart';
import '../api_c4_shared/data/container_dto.dart';
import 'container_input_dto.dart';
import 'containers_output_dto.dart';

abstract class PutSingleContainerDao {
  Future<ContainersOutputDto> updateContainer(
      String contextId, String containerId, ContainerInputDto inputDto);
}

class PutSingleContainerDaoApiServer extends PutSingleContainerDao {
  late APIConfig apiConfig;
  late http.Client client;

  PutSingleContainerDaoApiServer({
    http.Client? aClient,
    APIConfig? anApiConfig,
  }) {
    client = aClient ?? getIt<http.Client>();
    apiConfig = anApiConfig ?? getIt<APIConfig>();
  }

  @override
  Future<ContainersOutputDto> updateContainer(
      String contextId, String containerId, ContainerInputDto inputDto) async {
    try {
      ContainersOutputDto outputDto = ContainersOutputDto();
      Uri uri = apiConfig.constructContainerUri(contextId, containerId, null);
      var response = await client.put(uri,
          body: jsonEncode(
            inputDto.toJson(),
          ),
          headers: apiConfig.getHeaders());
      if (response.statusCode == 200) {
        ContainerDto containerDto = ContainerDto.fromJson(
          jsonDecode(response.body),
        );
        outputDto.containerDto = containerDto;
        return outputDto;
      }

      if (response.statusCode == 400 || response.statusCode == 500) {
        final errorDto = ErrorDto.fromJson(json.decode(response.body));
        outputDto.errorDto = errorDto;
        return outputDto;
      }

      throw (ApiException());
    } catch (err) {
      rethrow;
    }
  }
}
