import '../api_c4_error/error_dto.dart';
import '../api_c4_shared/data/container_dto.dart';
import '../api_c4_shared/data/deleted_dto.dart';

class ContainersOutputDto {
  List<ContainerDto>? containers;
  ContainerDto? containerDto;
  ErrorDto? errorDto;
  DeletedDto? deletedDto;
  String? id;
}
