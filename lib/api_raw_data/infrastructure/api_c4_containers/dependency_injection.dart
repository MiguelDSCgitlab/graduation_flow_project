import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;

import '../../../api_view_diagram/infrastructure/container_diagram_get_dao.dart';
import '../api_c4_shared/api_config.dart';
import 'all_containers_get_dao.dart';
import 'containers_repository.dart';
import 'single_container_delete_dao.dart';
import 'single_container_get_dao.dart';
import 'single_container_post_dao.dart';
import 'single_container_put_dao.dart';

final getIt = GetIt.instance;

void setupApiC4Containers() {
  if (!GetIt.I.isRegistered<APIConfig>()) {
    getIt.registerSingleton<APIConfig>(APIConfig());
  }

  if (!GetIt.I.isRegistered<http.Client>()) {
    getIt.registerSingleton<http.Client>(http.Client());
  }

  if (!GetIt.I.isRegistered<AllContainersGetDao>()) {
    getIt
        .registerSingleton<AllContainersGetDao>(AllContainersGetDaoApiServer());
  }

  if (!GetIt.I.isRegistered<SingleContainerDeleteDao>()) {
    getIt.registerSingleton<SingleContainerDeleteDao>(
        SingleContainerDeleteDaoApiServer());
  }

  if (!GetIt.I.isRegistered<ContainerDiagramGetDao>()) {
    getIt.registerSingleton<ContainerDiagramGetDao>(
        ContainerDiagramGetDaoServer());
  }

  if (!GetIt.I.isRegistered<SingleContainerGetDao>()) {
    getIt.registerSingleton<SingleContainerGetDao>(
        SingleContainerGetDaoApiServer());
  }

  if (!GetIt.I.isRegistered<PostSingleContainerDao>()) {
    getIt.registerSingleton<PostSingleContainerDao>(
        PostSingleContainerDaoApiServer());
  }

  if (!GetIt.I.isRegistered<PutSingleContainerDao>()) {
    getIt.registerSingleton<PutSingleContainerDao>(
        PutSingleContainerDaoApiServer());
  }

  if (!GetIt.I.isRegistered<ContainersRepository>()) {
    getIt.registerSingleton<ContainersRepository>(ContainersRepository());
  }
}
