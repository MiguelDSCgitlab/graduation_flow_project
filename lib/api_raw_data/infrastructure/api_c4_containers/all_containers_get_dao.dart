import 'dart:convert';

import 'package:http/http.dart' as http;

import '../api_c4_error/api_exception.dart';
import '../api_c4_error/error_dto.dart';
import '../api_c4_shared/api_config.dart';
import '../api_c4_shared/data/container_dto.dart';
import 'containers_output_dto.dart';
import 'dependency_injection.dart';

/*
Responsible for api call GET to /contexts
 */
abstract class AllContainersGetDao {
  Future<ContainersOutputDto> getContainers(String contextId);
}

/*
Retrieves contexts from API service.
Endpoint: /contexts?contextProperties=name,description
*/
class AllContainersGetDaoApiServer implements AllContainersGetDao {
  late APIConfig apiConfig;
  late http.Client client;

  AllContainersGetDaoApiServer({
    http.Client? aClient,
    APIConfig? anApiConfig,
  }) {
    client = aClient ?? getIt<http.Client>();
    apiConfig = anApiConfig ?? getIt<APIConfig>();
  }

  @override
  Future<ContainersOutputDto> getContainers(String contextId) async {
    try {
      ContainersOutputDto outputDto = ContainersOutputDto();
      Map<String, String> queryParams = {'containerProperties': 'name'};
      Uri uri = apiConfig.constructContainersUri(contextId, queryParams);

      List<ContainerDto> containers = [];
      var response = await client.get(
        uri,
        headers: apiConfig.getHeaders(),
      );

      if (response.statusCode == 200) {
        var data = jsonDecode(response.body) as List;
        for (var element in data) {
          containers.add(ContainerDto.fromJson(element));
        }
        outputDto.containers = containers;
        return outputDto;
      }

      if (response.statusCode == 400 || response.statusCode == 500) {
        var errorDto = ErrorDto.fromJson(json.decode(response.body));
        outputDto.errorDto = errorDto;
        return outputDto;
      }
      throw ApiException();
    } catch (err) {
      rethrow;
    }
  }
}
