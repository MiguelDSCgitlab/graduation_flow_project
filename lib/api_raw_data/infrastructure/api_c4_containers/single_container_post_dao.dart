import 'dart:convert';

import 'package:http/http.dart' as http;

import '../api_c4_error/api_exception.dart';
import '../api_c4_error/error_dto.dart';
import '../api_c4_error/required_property_exception.dart';
import '../api_c4_shared/api_config.dart';
import 'container_input_dto.dart';
import 'containers_output_dto.dart';
import 'dependency_injection.dart';

/*
Responsible for api call to GET /contexts/:contextId
 */
abstract class PostSingleContainerDao {
  Future<ContainersOutputDto> postContainer(
      String contextId, ContainerInputDto containerInputDto);
}

/*
  Retrieves single context from API service.
  Endpoint: /contexts/:contextId?contextProperties=name,description
 */
class PostSingleContainerDaoApiServer implements PostSingleContainerDao {
  late APIConfig apiConfig;
  late http.Client client;

  PostSingleContainerDaoApiServer({
    http.Client? aClient,
    APIConfig? anApiConfig,
  }) {
    client = aClient ?? getIt<http.Client>();
    apiConfig = anApiConfig ?? getIt<APIConfig>();
  }

  @override
  Future<ContainersOutputDto> postContainer(
      String contextId, ContainerInputDto inputDto) async {
    try {
      ContainersOutputDto outputDto = ContainersOutputDto();
      Uri uri = apiConfig.constructContainersUri(contextId, null);
      var response = await client.post(uri,
          body: jsonEncode(inputDto.toJson()), headers: apiConfig.getHeaders());

      if (response.statusCode == 200) {
        Map json = jsonDecode(response.body);
        if (json.containsKey('id')) {
          outputDto.id = json['id'];
          return outputDto;
        } else {
          throw RequiredPropertyMissingException(
            propertyName: 'id',
            className: 'PostSingleContainerDaoApiServer',
          );
        }
      }

      if (response.statusCode == 400 || response.statusCode == 500) {
        var errorDto = ErrorDto.fromJson(json.decode(response.body));
        outputDto.errorDto = errorDto;
        return outputDto;
      }

      throw ApiException();
    } catch (err) {
      rethrow;
    }
  }
}
