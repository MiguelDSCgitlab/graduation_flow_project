import 'dart:convert';

import 'package:http/http.dart' as http;

import '../../../di/di.dart';
import '../api_c4_error/api_exception.dart';
import '../api_c4_error/error_dto.dart';
import '../api_c4_shared/api_config.dart';
import '../api_c4_shared/data/deleted_dto.dart';
import 'containers_output_dto.dart';

abstract class SingleContainerDeleteDao {
  Future<ContainersOutputDto> deleteContainer(
      String contextId, String containerId);
}

class SingleContainerDeleteDaoApiServer extends SingleContainerDeleteDao {
  late APIConfig apiConfig;
  late http.Client client;

  SingleContainerDeleteDaoApiServer({
    http.Client? aClient,
    APIConfig? anApiConfig,
  }) {
    client = aClient ?? getIt<http.Client>();
    apiConfig = anApiConfig ?? getIt<APIConfig>();
  }

  @override
  Future<ContainersOutputDto> deleteContainer(
      String contextId, String containerId) async {
    try {
      ContainersOutputDto outputDto = ContainersOutputDto();

      Uri uri = apiConfig.constructContainerUri(contextId, containerId, null);

      var response = await client.delete(
        uri,
        headers: apiConfig.getHeaders(),
      );

      if (response.statusCode == 200) {
        DeletedDto deletedDto = DeletedDto.fromJson(jsonDecode(response.body));
        outputDto.deletedDto = deletedDto;
        return outputDto;
      }

      if (response.statusCode == 400 || response.statusCode == 500) {
        var errorDto = ErrorDto.fromJson(json.decode(response.body));
        outputDto.errorDto = errorDto;
        return outputDto;
      }

      throw ApiException();
    } catch (err) {
      rethrow;
    }
  }
}
