import 'all_containers_get_dao.dart';
import 'container_input_dto.dart';
import 'containers_output_dto.dart';
import 'dependency_injection.dart';
import 'single_container_delete_dao.dart';
import 'single_container_get_dao.dart';
import 'single_container_post_dao.dart';
import 'single_container_put_dao.dart';

class ContainersRepository {
  late AllContainersGetDao allContainersGetDao;
  late SingleContainerGetDao singleContainerGetDao;
  late PostSingleContainerDao postSingleContainerDao;
  late SingleContainerDeleteDao singleContainerDeleteDao;
  late PutSingleContainerDao putSingleContainerDao;

  ContainersRepository({
    AllContainersGetDao? allContainersGetDao,
    SingleContainerGetDao? singleContainerGetDao,
    PostSingleContainerDao? postSingleContainerDao,
    SingleContainerDeleteDao? singleContainerDeleteDao,
    PutSingleContainerDao? putSingleContainerDao,
  }) {
    this.allContainersGetDao =
        allContainersGetDao ?? getIt<AllContainersGetDao>();
    this.singleContainerGetDao =
        singleContainerGetDao ?? getIt<SingleContainerGetDao>();
    this.postSingleContainerDao =
        postSingleContainerDao ?? getIt<PostSingleContainerDao>();
    this.singleContainerDeleteDao =
        singleContainerDeleteDao ?? getIt<SingleContainerDeleteDao>();
    this.putSingleContainerDao =
        putSingleContainerDao ?? getIt<PutSingleContainerDao>();
  }

  Future<ContainersOutputDto> getContainers(String contextId) async {
    return await allContainersGetDao.getContainers(contextId);
  }

  Future<ContainersOutputDto> getSingleContainer(
      {required String contextId, required String containerId}) async {
    return await singleContainerGetDao.getContainer(contextId, containerId);
  }

  Future<ContainersOutputDto> postSingleContainer(
      {required String contextId,
      required ContainerInputDto containerInputDto}) async {
    return await postSingleContainerDao.postContainer(
        contextId, containerInputDto);
  }

  Future<ContainersOutputDto> deleteSingleContainer(
      {required String contextId, required String containerId}) async {
    return await singleContainerDeleteDao.deleteContainer(
        contextId, containerId);
  }

  Future<ContainersOutputDto> updateSingleContainer(
      {required String contextId,
      required String containerId,
      required ContainerInputDto containerInputDto}) async {
    return await putSingleContainerDao.updateContainer(
        contextId, containerId, containerInputDto);
  }
}
