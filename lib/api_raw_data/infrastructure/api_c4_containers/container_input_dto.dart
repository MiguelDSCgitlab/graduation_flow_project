class ContainerInputDto {
  String? contextId;
  String? containerId;
  String? name;
  String? description;
  String? location;
  String? subType;
  List<String>? technology;

  ContainerInputDto({
    this.name,
    this.contextId,
    this.containerId,
    this.description,
    this.location,
    this.subType,
    this.technology,
  });

  Map<String, dynamic> toJson() {
    Map<String, dynamic> map = {};
    map['name'] = name;

    if (description != null) {
      map['description'] = description;
    }
    if (location != null) {
      map['location'] = location;
    }
    if (subType != null) {
      map['subType'] = subType;
    }
    if (technology != null) {
      map['technology'] = technology;
    }
    return map;
  }

  @override
  String toString() {
    return 'name: $name, description: $description, location: $location, subType: $subType, technology: $technology';
  }
}
