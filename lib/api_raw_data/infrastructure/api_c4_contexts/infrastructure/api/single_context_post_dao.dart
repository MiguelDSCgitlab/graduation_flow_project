import 'dart:convert';

import 'package:http/http.dart' as http;

import '../../../api_c4_error/api_exception.dart';
import '../../../api_c4_error/error_dto.dart';
import '../../../api_c4_error/required_property_exception.dart';
import '../../../api_c4_shared/api_config.dart';
import '../../dependency_injection.dart';
import '../data/context_input.dto.dart';
import '../data/store_context_output_dto.dart';

/*
Responsible for api call to GET /contexts/:contextId
 */
abstract class PostSingleContextDao {
  Future<StoreContextOutputDto> postContext(
    String name,
    String? description,
    String? location,
  );
}

/*
  Retrieves single context from API service.
  Endpoint: /contexts/:contextId?contextProperties=name,description
 */
class PostSingleContextDaoApiServer implements PostSingleContextDao {
  late APIConfig apiConfig;
  late http.Client client;

  PostSingleContextDaoApiServer({
    http.Client? aClient,
    APIConfig? anApiConfig,
  }) {
    client = aClient ?? apiC4Contexts<http.Client>();
    apiConfig = anApiConfig ?? apiC4Contexts<APIConfig>();
  }

  @override
  Future<StoreContextOutputDto> postContext(
      String name, String? description, String? location) async {
    try {
      StoreContextOutputDto outputDto = StoreContextOutputDto();
      ContextInputDto contextDto = ContextInputDto(
        name: name,
        description: description,
        location: location,
      );
      Uri uri = apiConfig.constructContextsUri();
      var response = await client.post(uri,
          body: jsonEncode(
            contextDto.toJson(),
          ),
          headers: apiConfig.getHeaders());
      if (response.statusCode == 200) {
        Map json = jsonDecode(response.body);
        if (json.containsKey('id')) {
          outputDto.id = json['id'];
          return outputDto;
        } else {
          throw RequiredPropertyMissingException(
            propertyName: 'id',
            className: 'PostSingleContextDaoApiServer',
          );
        }
      } else if (response.statusCode == 400 || response.statusCode == 500) {
        var errorDto = ErrorDto.fromJson(json.decode(response.body));
        outputDto.errorDto = errorDto;
        return outputDto;
      } else {
        throw ApiException();
      }
    } catch (err) {
      rethrow;
    }
  }
}
