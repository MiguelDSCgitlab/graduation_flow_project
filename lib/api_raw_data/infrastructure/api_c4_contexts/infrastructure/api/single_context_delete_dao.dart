import 'dart:convert';
import 'dart:developer';

import 'package:http/http.dart' as http;

import '../../../api_c4_error/api_exception.dart';
import '../../../api_c4_error/error_dto.dart';
import '../../../api_c4_shared/api_config.dart';
import '../../../api_c4_shared/data/delete_output_dto.dart';
import '../../../api_c4_shared/data/deleted_dto.dart';
import '../../dependency_injection.dart';

abstract class DeleteSingleContextDao {
  Future<DeleteOutputDto> deleteContext(String id);
}

class DeleteSingleContextDaoApiServer extends DeleteSingleContextDao {
  late APIConfig apiConfig;
  late http.Client client;

  DeleteSingleContextDaoApiServer({
    http.Client? aClient,
    APIConfig? anApiConfig,
  }) {
    client = aClient ?? apiC4Contexts<http.Client>();
    apiConfig = anApiConfig ?? apiC4Contexts<APIConfig>();
  }

  @override
  Future<DeleteOutputDto> deleteContext(String id) async {
    try {
      Uri uri = apiConfig.constructContextUri(id);
      var response = await client.delete(
        uri,
        headers: apiConfig.getHeaders(),
      );

      if (response.statusCode == 200) {
        DeleteOutputDto outputDto = DeleteOutputDto();
        DeletedDto deletedDto = DeletedDto.fromJson(json.decode(response.body));
        outputDto.deletedDto = deletedDto;
        return outputDto;
      }

      if (response.statusCode == 400 || response.statusCode == 500) {
        DeleteOutputDto outputDto = DeleteOutputDto();
        final errorDto = ErrorDto.fromJson(json.decode(response.body));
        outputDto.errorDto = errorDto;
        return outputDto;
      }

      throw (ApiException());
    } catch (err) {
      log(err.toString());
      rethrow;
    }
  }
}
