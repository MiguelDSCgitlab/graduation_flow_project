import 'dart:convert';

import 'package:http/http.dart' as http;

import '../../../api_c4_error/api_exception.dart';
import '../../../api_c4_error/error_dto.dart';
import '../../../api_c4_shared/api_config.dart';
import '../../../api_c4_shared/data/context_dto.dart';
import '../../../api_c4_shared/data/contexts_output_dto.dart';
import '../../dependency_injection.dart';

/*
Responsible for api call to GET /contexts/:contextId
 */
abstract class AllContextGetByNameDao {
  Future<ContextsOutputDto> getContextsByName(String name);
}

/*
Retrieves contexts from API service.
Endpoint: /contexts?contextProperties=name,description
*/
class ContextsGetByNameDaoApiServer implements AllContextGetByNameDao {
  late APIConfig apiConfig;
  late http.Client client;

  ContextsGetByNameDaoApiServer({
    http.Client? aClient,
    APIConfig? anApiConfig,
  }) {
    client = aClient ?? apiC4Contexts<http.Client>();
    apiConfig = anApiConfig ?? apiC4Contexts<APIConfig>();
  }

  @override
  Future<ContextsOutputDto> getContextsByName(String name) async {
    try {
      ContextsOutputDto outputDto = ContextsOutputDto();
      Uri uri = apiConfig.constructContextByNameUri(name);

      List<ContextDto> contexts = [];
      var response = await client.get(
        uri,
        headers: apiConfig.getHeaders(),
      );

      if (response.statusCode == 200) {
        var data = jsonDecode(response.body) as List;
        for (var element in data) {
          contexts.add(ContextDto.fromJson(element));
        }
        outputDto.contexts = contexts;
      } else if (response.statusCode == 400 || response.statusCode == 500) {
        var errorDto = ErrorDto.fromJson(json.decode(response.body));
        outputDto.errorDto = errorDto;
      } else {
        throw ApiException();
      }
      return outputDto;
    } catch (err) {
      rethrow;
    }
  }
}
