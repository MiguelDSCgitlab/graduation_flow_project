import 'dart:convert';
import 'dart:developer';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

import '../../../api_c4_error/api_exception.dart';
import '../../../api_c4_error/error_dto.dart';
import '../../../api_c4_shared/api_config.dart';
import '../../../api_c4_shared/data/context_dto.dart';
import '../../dependency_injection.dart';
import '../data/context_output_dto.dart';

/*
Responsible for api call to GET /contexts/:contextId
 */
abstract class SingleContextGetDao {
  Future<ContextOutputDto> getContext(String contextId);
}

/*
  Retrieves single context from API service.
  Endpoint: /contexts/:contextId?contextProperties=name,description
 */
class SingleContextGetDaoApiServer implements SingleContextGetDao {
  late APIConfig apiConfig;
  late http.Client client;

  SingleContextGetDaoApiServer({
    http.Client? aClient,
    APIConfig? anApiConfig,
  }) {
    client = aClient ?? apiC4Contexts<http.Client>();
    apiConfig = anApiConfig ?? apiC4Contexts<APIConfig>();
  }

  @override
  Future<ContextOutputDto> getContext(String contextId) async {
    try {
      ContextOutputDto contextOutputDto = ContextOutputDto();
      Map<String, String>? queryParams = {};
      queryParams['includeRelationShips'] = 'false';
      Uri uri =
          apiConfig.constructContextUri(contextId, queryParams: queryParams);
      var response = await client.get(
        uri,
        headers: apiConfig.getHeaders(),
      );
      if (response.statusCode == 200) {
        List<ContextDto> contexts = [];
        if (kDebugMode) {
          // print(response.body);
        }
        var data = jsonDecode(response.body) as List;
        for (var element in data) {
          contexts.add(ContextDto.fromJson(element));
        }
        if (contexts.isNotEmpty) {
          contextOutputDto.contextDto = contexts[0];
        }
        return contextOutputDto;
      }

      if (response.statusCode == 400 || response.statusCode == 500) {
        final errorDto = ErrorDto.fromJson(json.decode(response.body));
        contextOutputDto.errorDto = errorDto;
        return contextOutputDto;
      }

      throw (ApiException());
    } catch (err) {
      log(err.toString());
      rethrow;
    }
  }
}
