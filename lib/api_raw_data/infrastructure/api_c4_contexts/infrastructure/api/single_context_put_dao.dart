import 'dart:convert';
import 'dart:developer';

import 'package:http/http.dart' as http;

import '../../../api_c4_error/api_exception.dart';
import '../../../api_c4_error/error_dto.dart';
import '../../../api_c4_shared/api_config.dart';
import '../../../api_c4_shared/data/context_dto.dart';
import '../../dependency_injection.dart';
import '../data/context_input.dto.dart';
import '../data/context_output_dto.dart';

abstract class PutSingleContextDao {
  Future<ContextOutputDto> updateContext(
    String id,
    String name,
    String? description,
    String? location,
  );
}

class PutSingleContextDaoApiServer extends PutSingleContextDao {
  late APIConfig apiConfig;
  late http.Client client;

  PutSingleContextDaoApiServer({
    http.Client? aClient,
    APIConfig? anApiConfig,
  }) {
    client = aClient ?? apiC4Contexts<http.Client>();
    apiConfig = anApiConfig ?? apiC4Contexts<APIConfig>();
  }

  @override
  Future<ContextOutputDto> updateContext(
      String id, String name, String? description, String? location) async {
    try {
      ContextInputDto inputDto = ContextInputDto(
        name: name,
        description: description,
        location: location,
      );

      Uri uri = apiConfig.constructContextUri(id);
      var response = await client.put(uri,
          body: jsonEncode(
            inputDto.toJson(),
          ),
          headers: apiConfig.getHeaders());
      if (response.statusCode == 200) {
        ContextDto contextDto = ContextDto.fromJson(json.decode(response.body));
        ContextOutputDto outputDto = ContextOutputDto();
        outputDto.contextDto = contextDto;
        return outputDto;
      }

      if (response.statusCode == 400 || response.statusCode == 500) {
        final errorDto = ErrorDto.fromJson(json.decode(response.body));
        ContextOutputDto outputDto = ContextOutputDto();
        outputDto.errorDto = errorDto;
        return outputDto;
      }

      throw (ApiException());
    } catch (err) {
      log(err.toString());
      rethrow;
    }
  }
}
