class ContextInputDto {
  String? id;
  String name;
  String? description;
  String? location;

  ContextInputDto({
    this.id,
    required this.name,
    this.description,
    this.location,
  });

  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'description': description,
      'location': location,
    };
  }
}
