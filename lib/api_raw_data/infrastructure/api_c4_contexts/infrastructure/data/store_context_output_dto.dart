import '../../../api_c4_error/error_dto.dart';

class StoreContextOutputDto {
  ErrorDto? errorDto;
  String? id;
}
