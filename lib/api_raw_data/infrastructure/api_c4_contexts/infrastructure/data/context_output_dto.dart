import '../../../api_c4_error/error_dto.dart';
import '../../../api_c4_shared/data/context_dto.dart';

class ContextOutputDto {
  ContextDto? contextDto;
  ErrorDto? errorDto;
}
