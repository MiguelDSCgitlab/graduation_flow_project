import '../../api_c4_shared/data/contexts_output_dto.dart';
import '../../api_c4_shared/data/delete_output_dto.dart';
import '../dependency_injection.dart';
import 'api/all_contexts_get_dao.dart';
import 'api/search_contexts_by_id_dao.dart';
import 'api/single_context_delete_dao.dart';
import 'api/single_context_get_dao.dart';
import 'api/single_context_get_name_dao.dart';
import 'api/single_context_post_dao.dart';
import 'api/single_context_put_dao.dart';
import 'data/context_output_dto.dart';
import 'data/store_context_output_dto.dart';

class ContextsRepository {
  late AllContextsGetDao allContextsGetDao;
  late SingleContextGetDao singleContextGetDao;
  late AllContextGetByNameDao allContextGetByNameDao;
  late PostSingleContextDao postSingleContextDao;
  late PutSingleContextDao putSingleContextDao;
  late DeleteSingleContextDao deleteSingleContextDao;
  late SearchContextsByIdDao searchContextsByIdDao;

  ContextsRepository({
    AllContextsGetDao? allContextsGetDao,
    SingleContextGetDao? singleContextGetDao,
    AllContextGetByNameDao? allContextGetByNameDao,
    PostSingleContextDao? postSingleContextDao,
    PutSingleContextDao? putSingleContextDao,
    DeleteSingleContextDao? deleteSingleContextDao,
    SearchContextsByIdDao? searchContextsByIdDao,
  }) {
    this.allContextsGetDao =
        allContextsGetDao ?? apiC4Contexts<AllContextsGetDao>();
    this.singleContextGetDao =
        singleContextGetDao ?? apiC4Contexts<SingleContextGetDao>();
    this.postSingleContextDao =
        postSingleContextDao ?? apiC4Contexts<PostSingleContextDao>();
    this.putSingleContextDao =
        putSingleContextDao ?? apiC4Contexts<PutSingleContextDao>();
    this.deleteSingleContextDao =
        deleteSingleContextDao ?? apiC4Contexts<DeleteSingleContextDao>();
    this.searchContextsByIdDao =
        searchContextsByIdDao ?? apiC4Contexts<SearchContextsByIdDao>();
  }

  Future<ContextsOutputDto> getContexts() async {
    return await allContextsGetDao.getContexts();
  }

  Future<ContextOutputDto> getSingleContext({required String id}) async {
    return await singleContextGetDao.getContext(id);
  }

  Future<ContextsOutputDto> getContextsByName({required String name}) async {
    return await allContextGetByNameDao.getContextsByName(name);
  }

  Future<StoreContextOutputDto> storeContext(
      String name, String? description, String? location) async {
    return await postSingleContextDao.postContext(name, description, location);
  }

  Future<ContextOutputDto> updateContext(
      String id, String name, String? description, String? location) async {
    return await putSingleContextDao.updateContext(
        id, name, description, location);
  }

  Future<DeleteOutputDto> deleteContext(String id) async {
    return await deleteSingleContextDao.deleteContext(id);
  }

  searchContextsById({required String id}) async {
    return await searchContextsByIdDao.searchContextsById(id);
  }
}
