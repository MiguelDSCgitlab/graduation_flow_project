import '../dependency_injection.dart';
import '../infrastructure/contexts_repository.dart';
import '../infrastructure/data/context_output_dto.dart';

class GetSingleContextUseCase {
  late ContextsRepository contextsRepository;

  GetSingleContextUseCase({ContextsRepository? aContextsRepository}) {
    contextsRepository =
        aContextsRepository ?? apiC4Contexts<ContextsRepository>();
  }

  Future<ContextOutputDto> call(String id) async {
    return await contextsRepository.getSingleContext(id: id);
  }
}
