import '../dependency_injection.dart';
import '../infrastructure/contexts_repository.dart';
import '../infrastructure/data/store_context_output_dto.dart';

class NewContextUseCase {
  late ContextsRepository contextsRepository;

  NewContextUseCase({ContextsRepository? aContextsRepository}) {
    contextsRepository =
        aContextsRepository ?? apiC4Contexts<ContextsRepository>();
  }

  Future<StoreContextOutputDto> call(
      {required String name,
      required String location,
      required String description}) async {
    return await contextsRepository.storeContext(name, description, location);
  }
}
