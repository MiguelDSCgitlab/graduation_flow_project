import '../../api_c4_components/dependency_injection.dart';
import '../../api_c4_shared/data/contexts_output_dto.dart';
import '../infrastructure/contexts_repository.dart';

class GetContextsByNameUseCase {
  late ContextsRepository contextsRepository;

  GetContextsByNameUseCase({ContextsRepository? aContextsRepository}) {
    contextsRepository = aContextsRepository ?? getIt<ContextsRepository>();
  }

  Future<ContextsOutputDto> call(String name) async {
    return await contextsRepository.getContextsByName(name: name);
  }
}
