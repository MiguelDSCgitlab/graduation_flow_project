import '../../api_c4_shared/data/contexts_output_dto.dart';
import '../dependency_injection.dart';
import '../infrastructure/contexts_repository.dart';

class SearchContextsByIdUseCase {
  late ContextsRepository contextsRepository;

  SearchContextsByIdUseCase({ContextsRepository? aContextsRepository}) {
    contextsRepository =
        aContextsRepository ?? apiC4Contexts<ContextsRepository>();
  }

  Future<ContextsOutputDto> call(String id) async {
    return await contextsRepository.searchContextsById(id: id);
  }
}
