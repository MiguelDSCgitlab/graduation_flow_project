import '../../api_c4_shared/data/context_dto.dart';
import '../../api_c4_shared/data/contexts_output_dto.dart';
import '../dependency_injection.dart';
import '../infrastructure/contexts_repository.dart';

class GetContextsUseCase {
  late ContextsRepository contextsRepository;

  GetContextsUseCase({ContextsRepository? aContextsRepository}) {
    contextsRepository =
        aContextsRepository ?? apiC4Contexts<ContextsRepository>();
  }

  Future<ContextsOutputDto> call() async {
    ContextsOutputDto contextsOutputDto =
        await contextsRepository.getContexts();
    List<ContextDto>? contexts = contextsOutputDto.contexts;
    if (contexts != null) {
      contexts.sort((a, b) => a.name.toLowerCase().compareTo(
            b.name.toLowerCase(),
          ));
      return ContextsOutputDto()..contexts = contexts;
    }
    return contextsOutputDto;
  }
}
