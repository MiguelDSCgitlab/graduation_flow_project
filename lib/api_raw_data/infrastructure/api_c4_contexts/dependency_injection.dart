import 'package:c4_frontend_data_entry_flutter/api_raw_data/infrastructure/api_c4_contexts/domain/search_contexts_by_id_usecase.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;

import '../../../api_view_diagram/infrastructure/context_diagram_get_dao.dart';
import '../api_c4_shared/api_config.dart';
import 'domain/get_contexts_by_name_usecase.dart';
import 'domain/get_contexts_usecase.dart';
import 'domain/get_single_context_usecase.dart';
import 'domain/new_context_usecase.dart';
import 'infrastructure/api/all_contexts_get_dao.dart';
import 'infrastructure/api/search_contexts_by_id_dao.dart';
import 'infrastructure/api/single_context_delete_dao.dart';
import 'infrastructure/api/single_context_get_dao.dart';
import 'infrastructure/api/single_context_get_name_dao.dart';
import 'infrastructure/api/single_context_post_dao.dart';
import 'infrastructure/api/single_context_put_dao.dart';
import 'infrastructure/contexts_repository.dart';

final apiC4Contexts = GetIt.instance;

void setupApiC4Contexts() {
  if (!GetIt.I.isRegistered<APIConfig>()) {
    apiC4Contexts.registerSingleton<APIConfig>(APIConfig());
  }

  if (!GetIt.I.isRegistered<http.Client>()) {
    apiC4Contexts.registerSingleton<http.Client>(http.Client());
  }

  if (!GetIt.I.isRegistered<AllContextsGetDao>()) {
    apiC4Contexts
        .registerSingleton<AllContextsGetDao>(AllContextsGetDaoApiServer());
  }

  if (!GetIt.I.isRegistered<AllContextGetByNameDao>()) {
    apiC4Contexts.registerSingleton<AllContextGetByNameDao>(
        ContextsGetByNameDaoApiServer());
  }

  if (!GetIt.I.isRegistered<DeleteSingleContextDao>()) {
    apiC4Contexts.registerSingleton<DeleteSingleContextDao>(
        DeleteSingleContextDaoApiServer());
  }

  if (!GetIt.I.isRegistered<SingleContextGetDao>()) {
    apiC4Contexts
        .registerSingleton<SingleContextGetDao>(SingleContextGetDaoApiServer());
  }
  if (!GetIt.I.isRegistered<ContextDiagramGetDao>()) {
    apiC4Contexts
        .registerSingleton<ContextDiagramGetDao>(ContextDiagramGetDaoServer());
  }

  if (!GetIt.I.isRegistered<PostSingleContextDao>()) {
    apiC4Contexts.registerSingleton<PostSingleContextDao>(
        PostSingleContextDaoApiServer());
  }

  if (!GetIt.I.isRegistered<PutSingleContextDao>()) {
    apiC4Contexts
        .registerSingleton<PutSingleContextDao>(PutSingleContextDaoApiServer());
  }

  if (!GetIt.I.isRegistered<SearchContextsByIdDao>()) {
    apiC4Contexts.registerSingleton<SearchContextsByIdDao>(
        SearchContextsByIdDaoApiServer());
  }

  if (!GetIt.I.isRegistered<ContextsRepository>()) {
    apiC4Contexts.registerSingleton<ContextsRepository>(ContextsRepository());
  }

  if (!GetIt.I.isRegistered<GetContextsUseCase>()) {
    apiC4Contexts.registerSingleton<GetContextsUseCase>(GetContextsUseCase());
  }

  if (!GetIt.I.isRegistered<GetSingleContextUseCase>()) {
    apiC4Contexts
        .registerSingleton<GetSingleContextUseCase>(GetSingleContextUseCase());
  }

  if (!GetIt.I.isRegistered<SearchContextsByIdUseCase>()) {
    apiC4Contexts.registerSingleton<SearchContextsByIdUseCase>(
        SearchContextsByIdUseCase());
  }

  if (!GetIt.I.isRegistered<NewContextUseCase>()) {
    apiC4Contexts.registerSingleton<NewContextUseCase>(NewContextUseCase());
  }

  if (!GetIt.I.isRegistered<GetContextsByNameUseCase>()) {
    apiC4Contexts.registerSingleton<GetContextsByNameUseCase>(
        GetContextsByNameUseCase());
  }
}
