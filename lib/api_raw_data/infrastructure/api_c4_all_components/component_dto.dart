import 'dart:convert';

import '../api_c4_error/required_property_exception.dart';

class MinComponentDto {
  String componentId;
  String containerId;
  String contextId;
  String name;
  String componentName;
  String containerName;
  String contextName;
  String? description;

  MinComponentDto({
    required this.componentId,
    required this.containerId,
    required this.contextId,
    required this.name,
    required this.componentName,
    required this.containerName,
    required this.contextName,
    this.description,
  });

  factory MinComponentDto.fromJson(Map<String, dynamic> data) {
    try {
      final componentId = data.containsKey('componentId')
          ? data['componentId']
          : throw RequiredPropertyMissingException(
              propertyName: 'componentId', className: 'ContainerDto');
      final containerId = data.containsKey('containerId')
          ? data['containerId']
          : throw RequiredPropertyMissingException(
              propertyName: 'containerId', className: 'ContainerDto');
      final contextId = data.containsKey('contextId')
          ? data['contextId']
          : throw RequiredPropertyMissingException(
              propertyName: 'contextId', className: 'ContainerDto');
      final name = data.containsKey('name')
          ? data['name']
          : throw RequiredPropertyMissingException(
              propertyName: 'name', className: 'ContainerDto');
      final contextName = data.containsKey('contextName')
          ? data['contextName']
          : throw RequiredPropertyMissingException(
              propertyName: 'contextName', className: 'ContainerDto');
      final containerName = data.containsKey('containerName')
          ? data['containerName']
          : throw RequiredPropertyMissingException(
              propertyName: 'containerName', className: 'ContainerDto');
      final componentName = data.containsKey('componentName')
          ? data['componentName']
          : throw RequiredPropertyMissingException(
              propertyName: 'componentName', className: 'ContainerDto');

      final description =
          data.containsKey('description') ? data['description'] : null;

      return MinComponentDto(
        componentId: componentId,
        containerId: containerId,
        contextId: contextId,
        name: name,
        contextName: contextName,
        containerName: containerName,
        componentName: componentName,
        description: description,
      );
    } catch (err) {
      rethrow;
    }
  }

  String toJson() {
    return jsonEncode({
      "contextId": contextId,
      "containerId": containerId,
      "componentId": componentId,
      "name": name,
      "description": description,
    });
  }
}
