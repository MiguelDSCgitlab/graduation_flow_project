import 'all_components_get_dao.dart';
import 'components_output_dto.dart';
import 'dependency_injection.dart';

class AllComponentsRepository {
  Future<ComponentsOutputDto> getComponents() async {
    return await getIt<AllComponentsGetDao>().getComponents();
  }
}
