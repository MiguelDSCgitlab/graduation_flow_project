import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;

import '../api_c4_shared/api_config.dart';
import 'all_components_get_dao.dart';
import 'all_components_repository.dart';

final getIt = GetIt.instance;

void setupApiC4AllComponents() {
  if (!GetIt.I.isRegistered<APIConfig>()) {
    getIt.registerSingleton<APIConfig>(APIConfig());
  }

  if (!GetIt.I.isRegistered<http.Client>()) {
    getIt.registerSingleton<http.Client>(http.Client());
  }

  if (!GetIt.I.isRegistered<AllComponentsGetDao>()) {
    getIt
        .registerSingleton<AllComponentsGetDao>(AllComponentsGetDaoApiServer());
  }

  if (!GetIt.I.isRegistered<AllComponentsRepository>()) {
    getIt.registerSingleton<AllComponentsRepository>(AllComponentsRepository());
  }
}
