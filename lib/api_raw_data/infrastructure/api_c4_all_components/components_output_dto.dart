import '../api_c4_error/error_dto.dart';
import 'component_dto.dart';

class ComponentsOutputDto {
  List<MinComponentDto>? components;
  ErrorDto? errorDto;
}
