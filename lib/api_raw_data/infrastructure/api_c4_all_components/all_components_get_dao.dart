import 'dart:convert';

import 'package:http/http.dart' as http;

import '../api_c4_error/api_exception.dart';
import '../api_c4_error/error_dto.dart';
import '../api_c4_shared/api_config.dart';
import 'component_dto.dart';
import 'components_output_dto.dart';
import 'dependency_injection.dart';

/*
Responsible for api call GET to /contexts
 */
abstract class AllComponentsGetDao {
  Future<ComponentsOutputDto> getComponents();
}

/*
Retrieves contexts from API service.
Endpoint: /contexts?contextProperties=name,description
*/
class AllComponentsGetDaoApiServer implements AllComponentsGetDao {
  late APIConfig apiConfig;
  late http.Client client;

  AllComponentsGetDaoApiServer({
    http.Client? aClient,
    APIConfig? anApiConfig,
  }) {
    client = aClient ?? getIt<http.Client>();
    apiConfig = anApiConfig ?? getIt<APIConfig>();
  }

  @override
  Future<ComponentsOutputDto> getComponents() async {
    try {
      ComponentsOutputDto outputDto = ComponentsOutputDto();
      Uri uri = apiConfig.constructAllComponentsUri();

      List<MinComponentDto> components = [];
      var response = await client.get(
        uri,
        headers: apiConfig.getHeaders(),
      );

      if (response.statusCode == 200) {
        var data = jsonDecode(response.body) as List;
        for (var element in data) {
          components.add(MinComponentDto.fromJson(element));
        }
        outputDto.components = components;
        return outputDto;
      }

      if (response.statusCode == 400 || response.statusCode == 500) {
        var errorDto = ErrorDto.fromJson(json.decode(response.body));
        outputDto.errorDto = errorDto;
        return outputDto;
      }
      throw ApiException();
    } catch (err) {
      rethrow;
    }
  }
}
