import 'dart:convert';

import 'package:http/http.dart' as http;

import '../api_c4_error/api_exception.dart';
import '../api_c4_error/error_dto.dart';
import '../api_c4_shared/api_config.dart';
import '../api_c4_shared/data/component_dto.dart';
import 'component_output_dto.dart';
import 'dependency_injection.dart';

abstract class SingleComponentGetDao {
  Future<ComponentsOutputDto> getComponent(
      String contextId, String containerId, String componentId);
}

class SingleComponentGetDaoApiServer extends SingleComponentGetDao {
  late APIConfig apiConfig;
  late http.Client client;

  SingleComponentGetDaoApiServer({
    http.Client? aClient,
    APIConfig? anApiConfig,
  }) {
    client = aClient ?? getIt<http.Client>();
    apiConfig = anApiConfig ?? getIt<APIConfig>();
  }

  @override
  Future<ComponentsOutputDto> getComponent(
      String contextId, String containerId, String componentId) async {
    try {
      ComponentsOutputDto outputDto = ComponentsOutputDto();
      Map<String, dynamic> queryParams = {'includeRelationShips': true};

      Uri uri = apiConfig.constructComponentUri(
          contextId, containerId, componentId, queryParams);

      List<ComponentDto> components = [];
      var response = await client.get(
        uri,
        headers: apiConfig.getHeaders(),
      );
      if (response.statusCode == 200) {
        var data = jsonDecode(response.body) as List;
        for (var element in data) {
          components.add(ComponentDto.fromJson(element));
        }
        outputDto.components = components;
        return outputDto;
      }

      if (response.statusCode == 400 || response.statusCode == 500) {
        var errorDto = ErrorDto.fromJson(json.decode(response.body));
        outputDto.errorDto = errorDto;
        return outputDto;
      }

      throw ApiException();
    } catch (err) {
      rethrow;
    }
  }
}
