import 'dart:convert';
import 'dart:developer';

import 'package:http/http.dart' as http;

import '../api_c4_error/api_exception.dart';
import '../api_c4_error/error_dto.dart';
import '../api_c4_shared/api_config.dart';
import '../api_c4_shared/data/component_dto.dart';
import 'component_input_dto.dart';
import 'component_output_dto.dart';
import 'dependency_injection.dart';

abstract class PutSingleComponentDao {
  Future<ComponentsOutputDto> updateComponent(String contextId,
      String containerId, String componentId, ComponentInputDto inputDto);
}

class PutSingleComponentDaoDaoApiServer extends PutSingleComponentDao {
  late APIConfig apiConfig;
  late http.Client client;

  PutSingleComponentDaoDaoApiServer({
    http.Client? aClient,
    APIConfig? anApiConfig,
  }) {
    client = aClient ?? getIt<http.Client>();
    apiConfig = anApiConfig ?? getIt<APIConfig>();
  }

  @override
  Future<ComponentsOutputDto> updateComponent(
      String contextId,
      String containerId,
      String componentId,
      ComponentInputDto inputDto) async {
    try {
      ComponentsOutputDto outputDto = ComponentsOutputDto();

      Uri uri = apiConfig.constructComponentUri(
          contextId, containerId, componentId, null);
      var response = await client.put(uri,
          body: jsonEncode(
            inputDto.toJson(),
          ),
          headers: apiConfig.getHeaders());
      if (response.statusCode == 200) {
        ComponentDto componentDto =
            ComponentDto.fromJson(jsonDecode(response.body));
        outputDto.componentDto = componentDto;
        return outputDto;
      }

      if (response.statusCode == 400 || response.statusCode == 500) {
        final errorDto = ErrorDto.fromJson(json.decode(response.body));
        outputDto.errorDto = errorDto;
        return outputDto;
      }

      throw (ApiException());
    } catch (err) {
      log(err.toString());
      rethrow;
    }
  }
}
