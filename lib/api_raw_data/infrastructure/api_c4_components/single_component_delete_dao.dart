import 'dart:convert';

import 'package:http/http.dart' as http;

import '../api_c4_error/api_exception.dart';
import '../api_c4_error/error_dto.dart';
import '../api_c4_shared/api_config.dart';
import '../api_c4_shared/data/deleted_dto.dart';
import 'component_output_dto.dart';
import 'dependency_injection.dart';

abstract class SingleComponentDeleteDao {
  Future<ComponentsOutputDto> deleteComponent(
      String contextId, String containerId, String componentId);
}

class SingleComponentDeleteDaoApiServer extends SingleComponentDeleteDao {
  late APIConfig apiConfig;
  late http.Client client;

  SingleComponentDeleteDaoApiServer({
    http.Client? aClient,
    APIConfig? anApiConfig,
  }) {
    client = aClient ?? getIt<http.Client>();
    apiConfig = anApiConfig ?? getIt<APIConfig>();
  }

  @override
  Future<ComponentsOutputDto> deleteComponent(
      String contextId, String containerId, String componentId) async {
    try {
      ComponentsOutputDto outputDto = ComponentsOutputDto();

      Uri uri = apiConfig.constructComponentUri(
          contextId, containerId, componentId, null);

      var response = await client.delete(
        uri,
        headers: apiConfig.getHeaders(),
      );
      if (response.statusCode == 200) {
        DeletedDto deletedDto = DeletedDto.fromJson(jsonDecode(response.body));
        outputDto.deletedDto = deletedDto;
        return outputDto;
      }

      if (response.statusCode == 400 || response.statusCode == 500) {
        var errorDto = ErrorDto.fromJson(json.decode(response.body));
        outputDto.errorDto = errorDto;
        return outputDto;
      }

      throw ApiException();
    } catch (err) {
      rethrow;
    }
  }
}
