import 'all_components_get_dao.dart';
import 'component_input_dto.dart';
import 'component_output_dto.dart';
import 'dependency_injection.dart';
import 'single_component_delete_dao.dart';
import 'single_component_get_dao.dart';
import 'single_component_post_dao.dart';
import 'single_component_put_dao.dart';

class ComponentsRepository {
  Future<ComponentsOutputDto> getComponents(
      String contextId, String containerId) async {
    return await getIt<AllComponentsGetDao>()
        .getComponents(contextId, containerId);
  }

  Future<ComponentsOutputDto> getSingleComponent(
      {required String contextId,
      required String containerId,
      required String componentId}) async {
    return await getIt<SingleComponentGetDao>()
        .getComponent(contextId, containerId, componentId);
  }

  Future<ComponentsOutputDto> postSingleComponent(
      {required String contextId,
      required String containerId,
      required ComponentInputDto componentInputDto}) async {
    return await getIt<PostSingleComponentDao>()
        .postComponent(contextId, containerId, componentInputDto);
  }

  Future<ComponentsOutputDto> deleteSingleComponent(
      {required String contextId,
      required String containerId,
      required String componentId}) async {
    return await getIt<SingleComponentDeleteDao>()
        .deleteComponent(contextId, containerId, componentId);
  }

  Future<ComponentsOutputDto> updateSingleComponent(
      {required String contextId,
      required String containerId,
      required String componentId,
      required ComponentInputDto componentInputDto}) async {
    return await getIt<PutSingleComponentDao>().updateComponent(
        contextId, containerId, componentId, componentInputDto);
  }
}
