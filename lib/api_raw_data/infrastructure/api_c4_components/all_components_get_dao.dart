import 'dart:convert';

import 'package:http/http.dart' as http;

import '../api_c4_error/api_exception.dart';
import '../api_c4_error/error_dto.dart';
import '../api_c4_shared/api_config.dart';
import '../api_c4_shared/data/component_dto.dart';
import 'component_output_dto.dart';
import 'dependency_injection.dart';

/*
Responsible for api call GET to /contexts
 */
abstract class AllComponentsGetDao {
  Future<ComponentsOutputDto> getComponents(
      String contextId, String containerId);
}

/*
Retrieves contexts from API service.
Endpoint: /contexts?contextProperties=name,description
*/
class AllComponentsGetDaoApiServer implements AllComponentsGetDao {
  late APIConfig apiConfig;
  late http.Client client;

  AllComponentsGetDaoApiServer({
    http.Client? aClient,
    APIConfig? anApiConfig,
  }) {
    client = aClient ?? getIt<http.Client>();
    apiConfig = anApiConfig ?? getIt<APIConfig>();
  }

  @override
  Future<ComponentsOutputDto> getComponents(
      String contextId, String containerId) async {
    try {
      ComponentsOutputDto outputDto = ComponentsOutputDto();
      Map<String, String> queryParams = {'componentProperties': 'name'};
      Uri uri =
          apiConfig.constructComponentsUri(contextId, containerId, queryParams);
      List<ComponentDto> components = [];
      var response = await client.get(
        uri,
        headers: apiConfig.getHeaders(),
      );

      if (response.statusCode == 200) {
        var data = jsonDecode(response.body) as List;
        for (var element in data) {
          components.add(ComponentDto.fromJson(element));
        }
        outputDto.components = components;
        return outputDto;
      }

      if (response.statusCode == 400 || response.statusCode == 500) {
        var errorDto = ErrorDto.fromJson(json.decode(response.body));
        outputDto.errorDto = errorDto;
        return outputDto;
      }
      throw ApiException();
    } catch (err) {
      rethrow;
    }
  }
}
