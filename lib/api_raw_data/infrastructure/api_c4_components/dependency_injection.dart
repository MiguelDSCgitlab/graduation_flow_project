import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;

import '../../../api_view_diagram/infrastructure/component_diagram_get_dao.dart';
import '../api_c4_shared/api_config.dart';
import 'all_components_get_dao.dart';
import 'components_repository.dart';
import 'single_component_delete_dao.dart';
import 'single_component_post_dao.dart';
import 'single_component_put_dao.dart';

final getIt = GetIt.instance;

void setupApiC4Components() {
  if (!GetIt.I.isRegistered<APIConfig>()) {
    getIt.registerSingleton<APIConfig>(APIConfig());
  }

  if (!GetIt.I.isRegistered<http.Client>()) {
    getIt.registerSingleton<http.Client>(http.Client());
  }

  if (!GetIt.I.isRegistered<AllComponentsGetDao>()) {
    getIt
        .registerSingleton<AllComponentsGetDao>(AllComponentsGetDaoApiServer());
  }

  if (!GetIt.I.isRegistered<SingleComponentDeleteDao>()) {
    getIt.registerSingleton<SingleComponentDeleteDao>(
        SingleComponentDeleteDaoApiServer());
  }

  if (!GetIt.I.isRegistered<PutSingleComponentDao>()) {
    getIt.registerSingleton<PutSingleComponentDao>(
        PutSingleComponentDaoDaoApiServer());
  }

  if (!GetIt.I.isRegistered<ComponentDiagramGetDao>()) {
    getIt.registerSingleton<ComponentDiagramGetDao>(
        ComponentDiagramGetDaoServer());
  }

  if (!GetIt.I.isRegistered<PostSingleComponentDao>()) {
    getIt.registerSingleton<PostSingleComponentDao>(
        PostSingleComponentDaoApiServer());
  }

  if (!GetIt.I.isRegistered<ComponentsRepository>()) {
    getIt.registerSingleton<ComponentsRepository>(ComponentsRepository());
  }
}
