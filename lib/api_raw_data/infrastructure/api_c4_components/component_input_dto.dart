class ComponentInputDto {
  String name;
  String? description;
  String? location;
  String? subType;
  List<String>? technology;

  ComponentInputDto(
      {required this.name,
      this.description,
      this.location,
      this.subType,
      this.technology});

  Map<String, dynamic> toJson() {
    Map<String, dynamic> map = {};
    map['name'] = name;

    if (description != null) {
      map['description'] = description;
    }
    if (location != null) {
      map['location'] = location;
    }
    if (subType != null) {
      map['subType'] = subType;
    }
    if (technology != null) {
      map['technology'] = technology;
    }
    return map;
  }
}
