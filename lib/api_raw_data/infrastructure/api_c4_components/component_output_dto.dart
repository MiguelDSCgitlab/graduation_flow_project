import '../api_c4_error/error_dto.dart';
import '../api_c4_shared/data/component_dto.dart';
import '../api_c4_shared/data/deleted_dto.dart';

class ComponentsOutputDto {
  List<ComponentDto>? components;
  ComponentDto? componentDto;
  ErrorDto? errorDto;
  DeletedDto? deletedDto;
  String? id;
}
