class RequiredPropertyMissingException implements Exception {
  String propertyName;
  String className;

  RequiredPropertyMissingException(
      {required this.propertyName, required this.className});
}
