import 'required_property_exception.dart';

class ErrorListDto {
  List<ErrorDto> errors;

  ErrorListDto({
    required this.errors,
  });

  factory ErrorListDto.fromJson(Map<String, dynamic> json) {
    try {
      final list =
          json.containsKey('errors') ? json['errors'] as List : List.empty();
      List<ErrorDto> errorsList =
          list.map((i) => ErrorDto.fromJson(i)).toList();
      return ErrorListDto(
        errors: errorsList,
      );
    } catch (err) {
      rethrow;
    }
  }

  Map<String, dynamic> toJson() {
    return {
      'errors': errors.map((e) => e.toJson()).toList(),
    };
  }
}

class ErrorDto {
  String error;

  ErrorDto({
    required this.error,
  });

  factory ErrorDto.fromJson(Map<String, dynamic> json) {
    try {
      final error = json.containsKey('error')
          ? json['error']
          : throw RequiredPropertyMissingException(
              propertyName: 'error',
              className: 'ErrorDto',
            );
      return ErrorDto(
        error: error,
      );
    } catch (err) {
      rethrow;
    }
  }

  Map<String, dynamic> toJson() {
    return {
      'error': error,
    };
  }
}
