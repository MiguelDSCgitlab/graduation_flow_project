import 'package:get_it/get_it.dart';

import '../api_cdn/dependency_injection.dart';
import '../api_raw_data/infrastructure/api_c4_all_components/dependency_injection.dart';
import '../api_raw_data/infrastructure/api_c4_all_containers/dependency_injection.dart';
import '../api_raw_data/infrastructure/api_c4_components/dependency_injection.dart';
import '../api_raw_data/infrastructure/api_c4_containers/dependency_injection.dart';
import '../api_raw_data/infrastructure/api_c4_contexts/dependency_injection.dart';
import '../api_raw_data/infrastructure/api_c4_person/dependency_injection.dart';
import '../api_raw_data/infrastructure/api_c4_relationships/dependency_injection.dart';
import '../api_view_diagram/di.dart';

final getIt = GetIt.instance;

void setup() {
  setupApiViewDiagram();
  setupApiC4Contexts();
  setupApiC4Containers();
  setupApiC4Components();
  setupApiC4Person();
  setupApiC4Relationships();
  setupApiC4AllContainers();
  setupApiC4AllComponents();
  setupApiCDN();
}
