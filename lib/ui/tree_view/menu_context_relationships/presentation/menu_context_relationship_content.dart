import 'package:c4_frontend_data_entry_flutter/ui/tree_view/constants.dart';
import 'package:flutter/material.dart';

import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/relationship_dto.dart';

class MenuContextRelationshipContent extends StatelessWidget {
  final ContextDto contextDto;
  final RelationshipDto relationshipDto;

  const MenuContextRelationshipContent({
    super.key,
    required this.contextDto,
    required this.relationshipDto,
  });

  @override
  Widget build(BuildContext context) {
    bool incoming = false;
    if (relationshipDto.toContext == contextDto.contextId) {
      incoming = true;
    }

    return Column(
      children: [
        Row(
          children: [
            const Icon(
              Icons.chevron_right,
              color: Colors.transparent,
            ),
            Padding(
              padding: EdgeInsets.only(left: tabSize),
            ),
            const Icon(
              Icons.chevron_right,
              color: Colors.transparent,
            ),
            Padding(
              padding: EdgeInsets.only(left: tabSize),
            ),
            (incoming)
                ? Expanded(
                    child: Row(
                      children: [
                        Expanded(
                          child: Text(
                            '(I) ${relationshipDto.fromName}',
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ],
                    ),
                  )
                : Expanded(
                    child: Text(
                      '(O) ${relationshipDto.toName}',
                      overflow: TextOverflow.ellipsis,
                    ),
                  )
          ],
        ),
      ],
    );
  }
}
