import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import 'menu_context_relationship.dart';

class MenuRelationshipsContext extends ConsumerStatefulWidget {
  final ContextDto contextDto;

  const MenuRelationshipsContext({
    Key? key,
    required this.contextDto,
  }) : super(key: key);

  @override
  ConsumerState createState() => _MenuContextRelationshipsState();
}

class _MenuContextRelationshipsState
    extends ConsumerState<MenuRelationshipsContext> {
  final GlobalKey<AnimatedListState> _listKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    int length = 0;
    var relationships = widget.contextDto.relationships;
    if (relationships != null) {
      length = relationships.length;
    }

    if (length == 0) {
      return Container();
    } else {
      return ScrollConfiguration(
        behavior: ScrollConfiguration.of(context).copyWith(
          dragDevices: {
            PointerDeviceKind.touch,
            PointerDeviceKind.mouse,
          },
        ),
        child: ListView.builder(
          key: _listKey,
          shrinkWrap: true,
          physics: const AlwaysScrollableScrollPhysics(),
          itemCount: length,
          itemBuilder: (context, index) => Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              MenuContextRelationship(
                contextDto: widget.contextDto,
                relationshipDto: widget.contextDto.relationships![index],
              ),
            ],
          ),
        ),
      );
    }
  }
}
