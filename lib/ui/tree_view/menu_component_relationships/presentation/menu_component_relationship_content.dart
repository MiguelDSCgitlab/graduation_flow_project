import 'package:c4_frontend_data_entry_flutter/api_raw_data/infrastructure/api_c4_shared/data/relationship_dto.dart';
import 'package:c4_frontend_data_entry_flutter/ui/tree_view/constants.dart';
import 'package:flutter/material.dart';

import '../../../../api_raw_data/infrastructure/api_c4_shared/data/component_dto.dart';

class MenuComponentRelationshipContent extends StatelessWidget {
  final RelationshipDto relationshipDto;
  final ComponentDto componentDto;

  const MenuComponentRelationshipContent({
    super.key,
    required this.componentDto,
    required this.relationshipDto,
  });

  @override
  Widget build(BuildContext context) {
    bool outgoing = relationshipDto.outgoing ?? true;
    return Row(
      children: [
        const Icon(
          Icons.chevron_right,
          color: Colors.transparent,
        ),
        Padding(
          padding: EdgeInsets.only(left: tabSize),
        ),
        const Icon(
          Icons.chevron_right,
          color: Colors.transparent,
        ),
        Padding(
          padding: EdgeInsets.only(left: tabSize),
        ),
        const Icon(
          Icons.chevron_right,
          color: Colors.transparent,
        ),
        Padding(
          padding: EdgeInsets.only(left: tabSize),
        ),
        const Icon(
          Icons.chevron_right,
          color: Colors.transparent,
        ),
        Padding(
          padding: EdgeInsets.only(left: tabSize),
        ),
        const Icon(
          Icons.chevron_right,
          color: Colors.transparent,
        ),
        Padding(
          padding: EdgeInsets.only(left: tabSize),
        ),
        const Icon(
          Icons.chevron_right,
          color: Colors.transparent,
        ),
        Padding(
          padding: EdgeInsets.only(left: tabSize),
        ),
        (outgoing)
            ? Expanded(
                child: Text(
                  '(O) ${relationshipDto.toName}',
                  overflow: TextOverflow.ellipsis,
                  style: Theme.of(context).textTheme.bodySmall,
                ),
                // child: Text(
                //   '(O) ${relationshipDto.fromName} => ${relationshipDto.toName}',
                //   overflow: TextOverflow.ellipsis,
                // ),
              )
            : Expanded(
                child: Text(
                  '(I) ${relationshipDto.fromName}',
                  overflow: TextOverflow.ellipsis,
                ),
                // child: Text(
                //   '(I) ${relationshipDto.fromName} => ${relationshipDto.toName}',
                //   overflow: TextOverflow.ellipsis,
                // ),
              ),
      ],
    );
  }
}
