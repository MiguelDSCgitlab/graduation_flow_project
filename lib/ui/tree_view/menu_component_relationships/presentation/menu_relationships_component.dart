import 'dart:ui';

import 'package:c4_frontend_data_entry_flutter/api_raw_data/infrastructure/api_c4_shared/data/container_dto.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_shared/data/component_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import 'menu_component_relationship.dart';

class MenuRelationshipsComponent extends ConsumerStatefulWidget {
  final ContextDto contextDto;
  final ContainerDto containerDto;
  final ComponentDto componentDto;

  const MenuRelationshipsComponent({
    Key? key,
    required this.contextDto,
    required this.containerDto,
    required this.componentDto,
  }) : super(key: key);

  @override
  ConsumerState createState() => _MenuRelationshipsComponentState();
}

class _MenuRelationshipsComponentState
    extends ConsumerState<MenuRelationshipsComponent> {
  final GlobalKey<AnimatedListState> _listKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    int length = 0;
    var relationships = widget.componentDto.relationships;
    if (relationships != null) {
      length = relationships.length;
    }

    if (length == 0) {
      return Container();
    } else {
      return ScrollConfiguration(
        behavior: ScrollConfiguration.of(context).copyWith(
          dragDevices: {
            PointerDeviceKind.touch,
            PointerDeviceKind.mouse,
          },
        ),
        child: ListView.builder(
          key: _listKey,
          shrinkWrap: true,
          physics: const AlwaysScrollableScrollPhysics(),
          itemCount: length,
          itemBuilder: (context, index) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                MenuComponentRelationship(
                  contextDto: widget.contextDto,
                  containerDto: widget.containerDto,
                  componentDto: widget.componentDto,
                  relationshipDto: widget.componentDto.relationships![index],
                ),
              ],
            );
          },
        ),
      );
    }
  }
}
