import 'package:c4_frontend_data_entry_flutter/api_raw_data/infrastructure/api_c4_shared/data/relationship_dto.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_shared/data/container_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import '../../../../app/foundation_widgets_styled/app_container_selected.dart';
import '../../../edit_raw_data/dialog/application/form_provider.dart';
import '../../constants.dart';
import '../../generic/presentation/popup_menu_widget.dart';
import 'menu_container_relationship_content.dart';

class MenuContainerRelationship extends ConsumerStatefulWidget {
  final ContextDto contextDto;
  final ContainerDto containerDto;
  final RelationshipDto relationshipDto;

  const MenuContainerRelationship({
    Key? key,
    required this.contextDto,
    required this.containerDto,
    required this.relationshipDto,
  }) : super(key: key);

  @override
  ConsumerState createState() => _MenuContainerRelationshipState();
}

class _MenuContainerRelationshipState
    extends ConsumerState<MenuContainerRelationship> {
  late FocusNode focusNode;
  bool _focused = false;

  @override
  void initState() {
    super.initState();
    focusNode = FocusNode();
    focusNode.addListener(_handleFocusChange);
  }

  @override
  void dispose() {
    focusNode.removeListener(_handleFocusChange);
    focusNode.dispose();
    super.dispose();
  }

  void _handleFocusChange() {
    if (focusNode.hasFocus != _focused) {
      setState(() {
        _focused = focusNode.hasFocus;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onLongPress: () {
        onLongPress(ref, context);
      },
      child: Column(
        children: [
          Focus(
            focusNode: focusNode,
            child: focusNode.hasFocus
                ? AppContainerSelected(
                    child: MenuContainerRelationshipContent(
                      relationshipDto: widget.relationshipDto,
                      containerDto: widget.containerDto,
                    ),
                  )
                : MenuContainerRelationshipContent(
                    relationshipDto: widget.relationshipDto,
                    containerDto: widget.containerDto,
                  ),
          ),
        ],
      ),
    );
  }

  void onLongPress(WidgetRef ref, BuildContext context) {
    focusNode.requestFocus();
    var renderObject = context.findRenderObject();
    var translation = renderObject?.getTransformTo(null).getTranslation();
    var position = Offset(translation?.x ?? 0, translation?.y ?? 0);
    showMenu(
      context: context,
      position: RelativeRect.fromLTRB(position.dx, position.dy, position.dx, 0),
      items: getMenuItems(ref),
    );
  }

  getMenuItems(WidgetRef ref) {
    bool outgoing = widget.relationshipDto.outgoing ?? true;
    String menuTitle = outgoing
        ? 'Outgoing from ${widget.relationshipDto.fromName} to ${widget.relationshipDto.toName}'
        : 'Incoming into ${widget.relationshipDto.toName} from ${widget.relationshipDto.fromName}';
    String menuText = outgoing
        ? 'Edit container relationship'
        : 'View container relationship';
    return <PopupMenuEntry<dynamic>>[
      PopupMenuWidget(
          child: Row(
        children: [
          const Icon(Icons.chevron_right, color: Colors.transparent),
          Padding(padding: EdgeInsets.only(left: tabSize)),
          Text(menuTitle),
        ],
      )),
      const PopupMenuDivider(),
      PopupMenuItem<Text>(
        value: const Text('editContainerRelationship'),
        child: Row(
          children: [
            const Icon(Icons.edit),
            const SizedBox(width: 16),
            Text(menuText)
          ],
        ),
        onTap: () {
          ref.read(formProvider.notifier).editContainerRelationship(
                contextDto: widget.contextDto,
                containerDto: widget.containerDto,
                relationshipDto: widget.relationshipDto,
              );
        },
      ),
    ];
  }
}
