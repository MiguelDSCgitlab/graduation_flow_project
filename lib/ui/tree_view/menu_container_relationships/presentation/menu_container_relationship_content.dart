import 'package:flutter/material.dart';

import '../../../../api_raw_data/infrastructure/api_c4_shared/data/container_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/relationship_dto.dart';
import '../../constants.dart';

class MenuContainerRelationshipContent extends StatelessWidget {
  final ContainerDto containerDto;
  final RelationshipDto relationshipDto;

  const MenuContainerRelationshipContent({
    super.key,
    required this.relationshipDto,
    required this.containerDto,
  });

  @override
  Widget build(BuildContext context) {
    bool incoming = false;
    if (relationshipDto.toContainer == containerDto.id) {
      incoming = true;
    }

    return Row(
      children: [
        const Icon(
          Icons.chevron_right,
          color: Colors.transparent,
        ),
        Padding(
          padding: EdgeInsets.only(left: tabSize),
        ),
        const Icon(
          Icons.chevron_right,
          color: Colors.transparent,
        ),
        Padding(
          padding: EdgeInsets.only(left: tabSize),
        ),
        const Icon(
          Icons.chevron_right,
          color: Colors.transparent,
        ),
        Padding(
          padding: EdgeInsets.only(left: tabSize),
        ),
        const Icon(
          Icons.chevron_right,
          color: Colors.transparent,
        ),
        Padding(
          padding: EdgeInsets.only(left: tabSize),
        ),
        (incoming)
            ? Expanded(
                child: Text(
                  '(I) ${relationshipDto.fromName}',
                  overflow: TextOverflow.ellipsis,
                ),
              )
            : Expanded(
                child: Text(
                  '(O) ${relationshipDto.toName}',
                  overflow: TextOverflow.ellipsis,
                ),
              ),
      ],
    );
  }
}
