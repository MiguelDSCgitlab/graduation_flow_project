import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

// import 'contexts_list_provider.dart';

class ContextsRefresh extends ConsumerWidget {
  const ContextsRefresh({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return IconButton(
      onPressed: () async {
        // ref.read(contextsListProvider.notifier).refresh();
      },
      icon: const Icon(Icons.refresh),
    );
  }
}
