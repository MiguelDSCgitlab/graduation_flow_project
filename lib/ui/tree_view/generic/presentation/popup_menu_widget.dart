import 'package:flutter/material.dart';

class PopupMenuWidget extends PopupMenuEntry<Never> {
  final Widget child;

  const PopupMenuWidget({super.key, this.height = 4.0, required this.child});

  @override
  final double height;

  @override
  bool represents(void value) => false;

  @override
  State<PopupMenuWidget> createState() => _PopupMenuTextState();
}

class _PopupMenuTextState extends State<PopupMenuWidget> {
  @override
  Widget build(BuildContext context) => widget.child;
}
