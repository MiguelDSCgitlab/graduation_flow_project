import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class AppC4Element extends ConsumerStatefulWidget {
  final void Function() onLongPress;
  final void Function() onTap;
  final String chevronId;
  final Widget child;

  const AppC4Element({
    super.key,
    required this.onLongPress,
    required this.onTap,
    required this.chevronId,
    required this.child,
  });

  @override
  ConsumerState createState() => _AppC4ElementState();
}

class _AppC4ElementState extends ConsumerState<AppC4Element> {
  late FocusNode focusNode;
  bool _focused = false;

  @override
  void initState() {
    super.initState();
    focusNode = FocusNode();
    focusNode.addListener(_handleFocusChange);
  }

  @override
  void dispose() {
    focusNode.removeListener(_handleFocusChange);
    focusNode.dispose();
    super.dispose();
  }

  void _handleFocusChange() {
    if (focusNode.hasFocus != _focused) {
      setState(() {
        _focused = focusNode.hasFocus;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: widget.onTap,
      onLongPress: widget.onLongPress,
      child: Focus(
        focusNode: focusNode,
        child: widget.child,
      ),
    );
  }
}
