import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../application/menu_contexts_state_notifier_provider.dart';
import 'menu_context.dart';

class MenuContexts extends ConsumerStatefulWidget {
  const MenuContexts({
    Key? key,
  }) : super(key: key);

  @override
  ConsumerState createState() => _MenuContextsState();
}

class _MenuContextsState extends ConsumerState<MenuContexts> {
  final GlobalKey<AnimatedListState> _listKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    var items = ref.read(menuContextsStateNotifierProvider.notifier).contexts;
    return ScrollConfiguration(
      behavior: ScrollConfiguration.of(context).copyWith(
        dragDevices: {
          PointerDeviceKind.touch,
          PointerDeviceKind.mouse,
        },
      ),
      child: ListView.builder(
        key: _listKey,
        shrinkWrap: true,
        physics: const AlwaysScrollableScrollPhysics(),
        itemCount: items.length,
        itemBuilder: (context, index) => Column(
          children: [
            MenuContext(
              index: index,
            ),
          ],
        ),
      ),
    );
  }
}
