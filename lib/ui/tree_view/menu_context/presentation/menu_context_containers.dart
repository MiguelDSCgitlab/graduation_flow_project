import 'package:c4_frontend_data_entry_flutter/ui/tree_view/generic/presentation/popup_menu_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_svg/svg.dart';

import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import '../../../../app/foundation_widgets_styled/app_container_selected.dart';
import '../../../browser/browser_body/diagram/application/diagram_path.dart';
import '../../../browser/browser_tabs/application/providers.dart';
import '../../../edit_raw_data/dialog/application/form_provider.dart';
import '../../chevron/application/chevron_animation_provider.dart';
import '../../constants.dart';
import '../../menu_container/presentation/menu_containers.dart';
import '../application/menu_context_state_notifier_provider.dart';
import 'menu_context_containers_content.dart';

class MenuContextContainers extends ConsumerStatefulWidget {
  final ContextDto contextDto;

  const MenuContextContainers({
    Key? key,
    required this.contextDto,
  }) : super(key: key);

  @override
  ConsumerState createState() => _MenuContextContainersState();
}

class _MenuContextContainersState extends ConsumerState<MenuContextContainers> {
  late FocusNode focusNode;
  bool _focused = false;

  @override
  void initState() {
    super.initState();
    focusNode = FocusNode();
    focusNode.addListener(_handleFocusChange);
  }

  @override
  void dispose() {
    focusNode.removeListener(_handleFocusChange);
    focusNode.dispose();
    super.dispose();
  }

  void _handleFocusChange() {
    if (focusNode.hasFocus != _focused) {
      setState(() {
        _focused = focusNode.hasFocus;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final contextDto = ref
        .read(menuContextStateNotifierProvider(widget.contextDto.contextId)
            .notifier)
        .outputContextDto;
    var state =
        ref.read(chevronAnimationProvider('${contextDto.name}_containers'));

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        InkWell(
          onLongPress: () {
            onLongPress(contextDto, ref, context);
          },
          onTap: () {
            onTap(contextDto, ref);
          },
          child: Focus(
            focusNode: focusNode,
            child: focusNode.hasFocus
                ? AppContainerSelected(
                    child: MenuContextContainersContent(
                      providerId: '${contextDto.name}_containers',
                    ),
                  )
                : MenuContextContainersContent(
                    providerId: '${contextDto.name}_containers',
                  ),
          ),
        ),
        (state is StartAnimatingState)
            ? MenuContainers(
                contextDto: widget.contextDto,
              )
            : const SizedBox.shrink(),
      ],
    );
  }

  void onTap(ContextDto contextDto, WidgetRef ref) {
    var state =
        ref.read(chevronAnimationProvider('${contextDto.name}_containers'));

    focusNode.requestFocus();
    setState(() {
      (state is InitialAnimatingState || state is ReverseAnimatingState)
          ? ref
              .read(chevronAnimationProvider('${contextDto.name}_containers')
                  .notifier)
              .start()
          : ref
              .read(chevronAnimationProvider('${contextDto.name}_containers')
                  .notifier)
              .stop();
    });
  }

  void onLongPress(ContextDto contextDto, WidgetRef ref, BuildContext context) {
    focusNode.requestFocus();
    var renderObject = context.findRenderObject();
    var translation = renderObject?.getTransformTo(null).getTranslation();
    var position = Offset(translation?.x ?? 0, translation?.y ?? 0);
    showMenu(
      context: context,
      position: RelativeRect.fromLTRB(position.dx, position.dy, position.dx, 0),
      items: getMenuItems(contextDto, ref),
    );
  }

  void handleShowDiagram(contextDto, ref) {
    if (!ref.watch(addTabProvider)) {
      ref
          .read(tabsListProvider.notifier)
          .navigate(AllContainersDiagramPath(contextDto.contextId));
    } else {
      ref.read(tabsListProvider.notifier).addTab(
          AllContainersDiagramPath(contextDto.contextId),
          selectIt: true);
    }
  }

  getMenuItems(ContextDto contextDto, WidgetRef ref) {
    return <PopupMenuEntry<dynamic>>[
      PopupMenuWidget(
          child: Row(
        children: [
          const Icon(
            Icons.chevron_right,
            color: Colors.transparent,
          ),
          Padding(
            padding: EdgeInsets.only(left: tabSize),
          ),
          const Text("containers"),
        ],
      )),
      const PopupMenuDivider(),
      PopupMenuItem<Text>(
        value: const Text('showDiagram'),
        child: Row(
          children: [
            SvgPicture.asset(
              'assets/icons/c2.svg',
              width: 20,
              height: 20,
            ),
            const SizedBox(
              width: 16,
            ),
            const Text(
              "Containers diagram",
            ),
          ],
        ),
        onTap: () {
          handleShowDiagram(contextDto, ref);
        },
      ),
      PopupMenuItem<Text>(
        value: const Text('newContainer'),
        child: const Row(
          children: [
            Icon(Icons.add),
            SizedBox(
              width: 16,
            ),
            Text(
              "New container",
            ),
          ],
        ),
        onTap: () {
          ref.read(formProvider.notifier).newContainer(contextDto: contextDto);
        },
      ),
    ];
  }
}
