import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import '../application/menu_context_future_provider.dart';
import '../application/menu_context_state_notifier_provider.dart';
import 'menu_context_containers.dart';
import 'menu_context_relationships.dart';

class MenuContextApi extends ConsumerWidget {
  final ContextDto contextDto;

  const MenuContextApi({
    Key? key,
    required this.contextDto,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final result = ref.watch(menuContextFutureProvider(contextDto.contextId));
    return result.when(
      data: (data) {
        final contexts = data.contexts;
        if (contexts != null) {
          ref
              .read(menuContextStateNotifierProvider(contextDto.contextId)
                  .notifier)
              .setOutputContext(contexts[0]);
          return Column(
            children: [
              // MenuContextAttributes(
              //   contextDto: contexts[0],
              // ),
              MenuContextContainers(
                contextDto: contexts[0],
              ),
              MenuContextRelationships(
                contextDto: contexts[0],
              ),
            ],
          );
        } else {
          return const SizedBox.shrink();
        }
      },
      error: (err, stacktrace) {
        return const Text('Something went wrong');
      },
      loading: () {
        return const Text('Loading');
      },
    );
  }
}
