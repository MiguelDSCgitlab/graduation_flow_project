import 'package:c4_frontend_data_entry_flutter/ui/browser/browser_body/diagram/application/diagram_path.dart';
import 'package:c4_frontend_data_entry_flutter/ui/tree_view/generic/presentation/popup_menu_widget.dart';
import 'package:c4_frontend_data_entry_flutter/ui/tree_view/menu_context/presentation/menu_context_content_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_svg/svg.dart';

import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import '../../../../app/foundation_widgets_styled/app_container_selected.dart';
import '../../../browser/browser_tabs/application/providers.dart';
import '../../../edit_raw_data/dialog/application/form_provider.dart';
import '../../chevron/application/chevron_animation_provider.dart';
import '../../constants.dart';
import '../application/menu_context_state_notifier_provider.dart';
import '../application/menu_contexts_state_notifier_provider.dart';
import 'menu_context_api.dart';

class MenuContext extends ConsumerStatefulWidget {
  final int index;

  const MenuContext({
    Key? key,
    required this.index,
  }) : super(key: key);

  @override
  ConsumerState createState() => _ContextWidgetState();
}

class _ContextWidgetState extends ConsumerState<MenuContext> {
  late FocusNode focusNode;
  bool _focused = false;
  GlobalKey globalKey = GlobalKey();

  @override
  void initState() {
    super.initState();
    focusNode = FocusNode();
    focusNode.addListener(_handleFocusChange);
  }

  @override
  void dispose() {
    focusNode.removeListener(_handleFocusChange);
    focusNode.dispose();
    super.dispose();
  }

  void _handleFocusChange() {
    if (focusNode.hasFocus != _focused) {
      setState(() {
        _focused = focusNode.hasFocus;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    ContextDto contextDto = ref
        .read(menuContextsStateNotifierProvider.notifier)
        .contexts[widget.index];
    final state = ref.read(chevronAnimationProvider(contextDto.name));
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        InkWell(
          key: globalKey,
          onLongPress: () {
            onLongPress(contextDto, ref, context);
          },
          onTap: () {
            onTap(contextDto, ref);
          },
          child: Focus(
            focusNode: focusNode,
            child: focusNode.hasFocus
                ? AppContainerSelected(
                    child: MenuContextContentWidget(
                      name: contextDto.name,
                    ),
                  )
                : MenuContextContentWidget(
                    name: contextDto.name,
                  ),
          ),
        ),
        (state is StartAnimatingState)
            ? MenuContextApi(
                contextDto: contextDto,
              )
            : const SizedBox.shrink(),
      ],
    );
  }

  void handleShowDiagram(contextDto, ref) {
    if (!ref.watch(addTabProvider)) {
      ref
          .read(tabsListProvider.notifier)
          .navigate(ContextDiagramPath(contextDto.contextId));
    } else {
      ref
          .read(tabsListProvider.notifier)
          .addTab(ContextDiagramPath(contextDto.contextId), selectIt: true);
    }
  }

  void onLongPress(ContextDto contextDto, WidgetRef ref, BuildContext context) {
    focusNode.requestFocus();
    var renderObject = context.findRenderObject();
    var translation = renderObject?.getTransformTo(null).getTranslation();
    var position = Offset(translation?.x ?? 0, translation?.y ?? 0);
    showMenu(
      context: context,
      position: RelativeRect.fromLTRB(position.dx, position.dy, position.dx, 0),
      items: getMenuItems(contextDto, ref),
    );
  }

  void onTap(contextDto, ref) {
    final state = ref.read(chevronAnimationProvider(contextDto.name));
    focusNode.requestFocus();
    ref
        .read(menuContextStateNotifierProvider(contextDto.contextId).notifier)
        .setInputContext(contextDto);
    setState(() {
      (state is InitialAnimatingState || state is ReverseAnimatingState)
          ? {
              ref
                  .read(chevronAnimationProvider(contextDto.name).notifier)
                  .start(),
            }
          : {
              ref
                  .read(chevronAnimationProvider(contextDto.name).notifier)
                  .stop(),
            };
    });
  }

  getMenuItems(ContextDto contextDto, WidgetRef ref) {
    return <PopupMenuEntry<dynamic>>[
      PopupMenuWidget(
          child: Row(
        children: [
          const Icon(
            Icons.chevron_right,
            color: Colors.transparent,
          ),
          Padding(
            padding: EdgeInsets.only(left: tabSize),
          ),
          Text(contextDto.name),
        ],
      )),
      const PopupMenuDivider(),
      // PopupMenuWidget(child: child)
      PopupMenuItem<Text>(
        value: const Text('showDiagram'),
        child: Row(
          children: [
            SvgPicture.asset(
              'assets/icons/c1.svg',
              width: 20,
              height: 20,
            ),
            const SizedBox(
              width: 16,
            ),
            const Text(
              "Context diagram",
            ),
          ],
        ),
        onTap: () {
          handleShowDiagram(contextDto, ref);
        },
      ),
      PopupMenuItem<Text>(
        value: const Text('editContext'),
        child: const Row(
          children: [
            Icon(Icons.edit),
            SizedBox(
              width: 16,
            ),
            Text(
              "Edit context",
            ),
          ],
        ),
        onTap: () {
          ref.read(formProvider.notifier).editContext(contextDto: contextDto);
        },
      ),
    ];
  }
}
