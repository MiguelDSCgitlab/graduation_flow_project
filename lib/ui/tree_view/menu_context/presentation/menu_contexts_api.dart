import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../application/menu_contexts_future_provider.dart';
import '../application/menu_contexts_state_notifier_provider.dart';
import 'menu_contexts.dart';

class GetContextsApi extends ConsumerWidget {
  const GetContextsApi({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final result = ref.watch(futureProviderGetContextsNew);
    return result.when(data: (data) {
      if (data.contexts != null) {
        var contexts = data.contexts;
        if (contexts != null) {
          ref.read(menuContextsStateNotifierProvider.notifier).contexts =
              contexts;
        }
      }
      return const MenuContexts();
    }, error: (err, trace) {
      print(err);
      print(trace);
      return const Text('Something went wrong');
    }, loading: () {
      return const Text('Loading');
    });
  }
}
