import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../chevron/presentation/chevron_animation.dart';
import '../../constants.dart';

class MenuContextContentWidget extends ConsumerStatefulWidget {
  final String name;

  const MenuContextContentWidget({
    super.key,
    required this.name,
  });

  @override
  ConsumerState createState() => _MenuContextContentWidgetState();
}

class _MenuContextContentWidgetState
    extends ConsumerState<MenuContextContentWidget> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        ChevronAnimation(
          providerId: widget.name,
        ),
        Padding(
          padding: EdgeInsets.only(left: tabSize),
        ),
        Expanded(
          child: Text(
            widget.name,
            overflow: TextOverflow.ellipsis,
          ),
        ),
      ],
    );
  }
}
