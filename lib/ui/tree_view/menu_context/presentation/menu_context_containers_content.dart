import 'package:c4_frontend_data_entry_flutter/ui/tree_view/constants.dart';
import 'package:flutter/material.dart';

import '../../chevron/presentation/chevron_animation.dart';

class MenuContextContainersContent extends StatelessWidget {
  final String providerId;

  const MenuContextContainersContent({
    super.key,
    required this.providerId,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        const Icon(
          Icons.chevron_right,
          color: Colors.transparent,
        ),
        Padding(
          padding: EdgeInsets.only(left: tabSize),
        ),
        ChevronAnimation(providerId: providerId),
        Padding(
          padding: EdgeInsets.only(left: tabSize),
        ),
        const Expanded(
          child: Text(
            'containers',
            overflow: TextOverflow.ellipsis,
          ),
        ),
      ],
    );
  }
}
