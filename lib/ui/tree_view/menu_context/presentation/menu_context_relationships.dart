import 'package:c4_frontend_data_entry_flutter/ui/tree_view/menu_context/presentation/menu_context_relationships_content.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import '../../../../app/foundation_widgets_styled/app_container_selected.dart';
import '../../../edit_raw_data/dialog/application/form_provider.dart';
import '../../chevron/application/chevron_animation_provider.dart';
import '../../constants.dart';
import '../../generic/presentation/popup_menu_widget.dart';
import '../../menu_context_relationships/presentation/menu_relationships_context.dart';
import '../application/menu_context_state_notifier_provider.dart';

class MenuContextRelationships extends ConsumerStatefulWidget {
  final ContextDto contextDto;

  const MenuContextRelationships({
    Key? key,
    required this.contextDto,
  }) : super(key: key);

  @override
  ConsumerState createState() => _ContextRelationshipsMenuState();
}

class _ContextRelationshipsMenuState
    extends ConsumerState<MenuContextRelationships> {
  late FocusNode focusNode;
  bool _focused = false;

  @override
  void initState() {
    super.initState();
    focusNode = FocusNode();
    focusNode.addListener(_handleFocusChange);
  }

  @override
  void dispose() {
    focusNode.removeListener(_handleFocusChange);
    focusNode.dispose();
    super.dispose();
  }

  void _handleFocusChange() {
    if (focusNode.hasFocus != _focused) {
      setState(() {
        _focused = focusNode.hasFocus;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final contextDto = ref
        .read(menuContextStateNotifierProvider(widget.contextDto.contextId)
            .notifier)
        .outputContextDto;
    var state =
        ref.read(chevronAnimationProvider('${contextDto.name}_relationships'));
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        InkWell(
          onLongPress: () {
            onLongPress(contextDto, ref, context);
          },
          onTap: () {
            onTap(contextDto, ref);
          },
          child: Focus(
            focusNode: focusNode,
            child: focusNode.hasFocus
                ? AppContainerSelected(
                    child: MenuContextRelationshipsContent(
                      providerId: '${contextDto.name}_relationships',
                    ),
                  )
                : MenuContextRelationshipsContent(
                    providerId: '${contextDto.name}_relationships',
                  ),
          ),
        ),
        (state is StartAnimatingState)
            ? MenuRelationshipsContext(
                contextDto: widget.contextDto,
              )
            : const SizedBox.shrink(),
      ],
    );
  }

  void onTap(ContextDto contextDto, WidgetRef ref) {
    var state =
        ref.read(chevronAnimationProvider('${contextDto.name}_relationships'));
    focusNode.requestFocus();
    setState(() {
      (state is InitialAnimatingState || state is ReverseAnimatingState)
          ? ref
              .read(chevronAnimationProvider('${contextDto.name}_relationships')
                  .notifier)
              .start()
          : ref
              .read(chevronAnimationProvider('${contextDto.name}_relationships')
                  .notifier)
              .stop();
    });
  }

  void onLongPress(ContextDto contextDto, WidgetRef ref, BuildContext context) {
    focusNode.requestFocus();
    var renderObject = context.findRenderObject();
    var translation = renderObject?.getTransformTo(null).getTranslation();
    var position = Offset(translation?.x ?? 0, translation?.y ?? 0);
    showMenu(
      context: context,
      position: RelativeRect.fromLTRB(position.dx, position.dy, position.dx, 0),
      items: getMenuItems(contextDto, ref),
    );
  }

  getMenuItems(ContextDto contextDto, WidgetRef ref) {
    return <PopupMenuEntry<dynamic>>[
      PopupMenuWidget(
          child: Row(
        children: [
          const Icon(
            Icons.chevron_right,
            color: Colors.transparent,
          ),
          Padding(
            padding: EdgeInsets.only(left: tabSize),
          ),
          const Text("relationships"),
        ],
      )),
      const PopupMenuDivider(),
      PopupMenuItem<Text>(
        value: const Text('newContextRelationship'),
        child: const Row(
          children: [
            Icon(Icons.add),
            SizedBox(
              width: 16,
            ),
            Text(
              "New context relationship",
            ),
          ],
        ),
        onTap: () {
          ref
              .read(formProvider.notifier)
              .newContextRelationship(contextDto: contextDto);
        },
      ),
    ];
  }
}
