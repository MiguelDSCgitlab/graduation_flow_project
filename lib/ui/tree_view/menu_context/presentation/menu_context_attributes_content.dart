import 'package:flutter/material.dart';

import '../../constants.dart';

class MenuContextAttributesContent extends StatelessWidget {
  const MenuContextAttributesContent({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        const Icon(
          Icons.chevron_right,
          color: Colors.transparent,
        ),
        Padding(
          padding: EdgeInsets.only(left: tabSize),
        ),
        const Icon(
          Icons.edit,
        ),
        Padding(
          padding: EdgeInsets.only(left: tabSize),
        ),
        const Expanded(
          child: Text(
            'attributes',
            overflow: TextOverflow.ellipsis,
          ),
        ),
      ],
    );
  }
}
