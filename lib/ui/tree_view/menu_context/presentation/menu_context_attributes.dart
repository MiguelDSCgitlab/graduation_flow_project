import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import '../../../../app/foundation_widgets_styled/app_container_selected.dart';
import '../../../edit_raw_data/dialog/application/form_provider.dart';
import 'menu_context_attributes_content.dart';

class MenuContextAttributes extends ConsumerStatefulWidget {
  final ContextDto contextDto;

  const MenuContextAttributes({
    Key? key,
    required this.contextDto,
  }) : super(key: key);

  @override
  ConsumerState createState() => _MenuContextAttributesState();
}

class _MenuContextAttributesState extends ConsumerState<MenuContextAttributes> {
  late FocusNode focusNode;
  bool _focused = false;

  @override
  void initState() {
    super.initState();
    focusNode = FocusNode();
    focusNode.addListener(_handleFocusChange);
  }

  @override
  void dispose() {
    focusNode.removeListener(_handleFocusChange);
    focusNode.dispose();
    super.dispose();
  }

  void _handleFocusChange() {
    if (focusNode.hasFocus != _focused) {
      setState(() {
        _focused = focusNode.hasFocus;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        focusNode.requestFocus();
        setState(() {
          ref
              .read(formProvider.notifier)
              .editContext(contextDto: widget.contextDto);
        });
      },
      child: Focus(
        focusNode: focusNode,
        child: focusNode.hasFocus
            ? const AppContainerSelected(
                child: MenuContextAttributesContent(),
              )
            : const MenuContextAttributesContent(),
      ),
    );
  }
}
