import 'package:c4_frontend_data_entry_flutter/ui/tree_view/menu_context/application/menu_contexts_refresh_provider.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_contexts/domain/search_contexts_by_id_usecase.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/contexts_output_dto.dart';
import 'menu_context_state_notifier_provider.dart';

final menuContextFutureProvider =
    FutureProvider.family<ContextsOutputDto, String>((ref, id) async {
  ref.watch(refreshContextsProvider);
  final inputDto =
      ref.read(menuContextStateNotifierProvider(id).notifier).inputContextDto;
  SearchContextsByIdUseCase getContextUseCase = SearchContextsByIdUseCase();
  return await getContextUseCase.call(inputDto.contextId);
});
