import 'package:diffutil_dart/diffutil.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';

final menuContextsStateNotifierProvider =
    StateNotifierProvider<ContextsListNotifier, FormHandlerState>((ref) {
  return ContextsListNotifier(ref);
});

class ContextsListNotifier extends StateNotifier<FormHandlerState> {
  final Ref ref;
  List<ContextDto> contexts = [];
  List<ContextDto> oldList = [];
  ContextDto? newContextDto;
  ContextDto? selectedContextDto;

  ContextsListNotifier(this.ref) : super(InitialState());

  void refresh() async {
    // ref.read(diagramProvider.notifier).resetForm();
    // ref.read(searchContextProvider.notifier).reset();
    // ContextsOutputDto contexts = await GetContextsUseCase().call();
    // List<ContextDto>? newContexts = contexts.contexts;
    // if (newContexts != null) {
    //   convertContexts(newContexts);
    //   state = RefreshState();
    // }
  }

  void addContexts(List<ContextDto> newContexts) {
    // convertContexts(newContexts);
    // oldList = List.from(contexts);
  }

  void search(String value) {
    // state = SearchState();
  }

  void addContext(ContextDto contextDto) {
    // newContextDto = ContextDtoUi(
    //   contextId: contextDto.contextId,
    //   id: contextDto.id,
    //   name: contextDto.name,
    // )..selected = true;
    //
    // state = AddContextState();
  }

  void init() {
    // state = InitialState();
  }

  // Iterable<DataDiffUpdate<ContextDtoUi>> applySearch() {
  // String filterString = ref.read(searchContextProvider.notifier).filterString;
  // return FilterContextsUseCase().call(
  //   contexts,
  //   oldList,
  //   filterString,
  // );
  // }

  Iterable<DataDiffUpdate<ContextDto>> applyAdd() {
    // List<ContextDtoUi> newList = [];
    // final contextDtoUi = newContextDto;
    // if (contextDtoUi != null) {
    //   contexts.add(contextDtoUi);
    //   contexts
    //       .sort((a, b) => a.name.toLowerCase().compareTo(b.name.toLowerCase()));
    //   newList.add(contextDtoUi);
    //   DetermineDifferences determineDifferences = DetermineDifferences();
    //   return determineDifferences.call(oldList, newList);
    // }
    return [];
  }

  void convertContexts(List<ContextDto> newContexts) {
    // List<ContextDtoUi> contextsUi = [];
    // for (int i = 0; i < newContexts.length; i++) {
    //   ContextDtoUi contextDtoUi = ContextDtoUi(
    //     contextId: newContexts[i].contextId,
    //     id: newContexts[i].id,
    //     name: newContexts[i].name,
    //     description: newContexts[i].description,
    //     type: newContexts[i].type,
    //     location: newContexts[i].location,
    //     createdAt: newContexts[i].createdAt,
    //     updatedAt: newContexts[i].updatedAt,
    //     person: newContexts[i].person,
    //     relationships: newContexts[i].relationships,
    //     containers: newContexts[i].containers,
    //   );
    // contextDtoUi.values = contextDtoUi.toJson();
    // contextDtoUi.values['expanded'] = false;

    // var containers = contextDtoUi.containers;
    // if (containers != null) {
    //   for (var container in containers) {
    //     var components = container.elements;
    //     if (components != null) {
    //       for (var component in components) {
    //         print(component.name);
    //       }
    //     }
    //   }
    // }

    // contextsUi.add(contextDtoUi);
    // }
    // contexts = contextsUi;
  }

  applyRefresh() {
    // DetermineDifferences determineDifferences = DetermineDifferences();
    // return determineDifferences.call(oldList, contexts);
  }

  void getContext(ContextDto contextDto) async {
    // SearchContextsByIdUseCase getContextUseCase = SearchContextsByIdUseCase();
    // final result = await getContextUseCase.call(contextDtoUi.contextId);
    // List<ContextDto>? newContexts = result.contexts;
    // if (newContexts != null && newContexts.isNotEmpty) {
    //   List<ContextDto>? filteredContexts =
    //       RemoveIncomingContextRelationshipsUseCase().call(newContexts);
    //   convertContexts(filteredContexts);
    //   setSelected(contexts, contextDtoUi);
    //   state = RefreshState();
    // }
  }

  setSelected(List<ContextDto> contexts, ContextDto contextDtoUi) {
    // for (ContextDtoUi newContext in contexts) {
    //   if (newContext.compare(contextDtoUi)) {
    //     newContext.selected = true;
    //     selectedContextDto = newContext;
    //   }
    // }
  }
}

abstract class FormHandlerState {}

class InitialState extends FormHandlerState {}

class RefreshState extends FormHandlerState {}

class AddContextState extends FormHandlerState {}

class SearchState extends FormHandlerState {}
