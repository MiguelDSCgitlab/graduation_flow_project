import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_all_components/dependency_injection.dart';
import '../../../../api_raw_data/infrastructure/api_c4_contexts/domain/get_contexts_usecase.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/contexts_output_dto.dart';
import 'menu_contexts_refresh_provider.dart';

final futureProviderGetContextsNew =
    FutureProvider<ContextsOutputDto>((ref) async {
  ref.watch(refreshContextsProvider);
  return getIt<GetContextsUseCase>().call();
});
