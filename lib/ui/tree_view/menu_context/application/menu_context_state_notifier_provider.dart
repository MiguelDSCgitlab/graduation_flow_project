import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';

final menuContextStateNotifierProvider = StateNotifierProvider.family<
    MenuContextStateNotifier, MenuContextState, String>((ref, contextId) {
  return MenuContextStateNotifier(ref);
});

class MenuContextStateNotifier extends StateNotifier<MenuContextState> {
  final Ref ref;
  late ContextDto inputContextDto;
  late ContextDto outputContextDto;

  MenuContextStateNotifier(this.ref) : super(InitialState());

  void setInputContext(ContextDto iContextDto) {
    inputContextDto = iContextDto;
  }

  void setOutputContext(ContextDto oContextDto) {
    outputContextDto = oContextDto;
  }
}

abstract class MenuContextState {}

class InitialState extends MenuContextState {}
