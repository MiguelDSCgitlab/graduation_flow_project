import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_shared/data/container_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import 'menu_component.dart';

class MenuComponents extends ConsumerStatefulWidget {
  final ContextDto contextDto;
  final ContainerDto containerDto;

  const MenuComponents({
    Key? key,
    required this.contextDto,
    required this.containerDto,
  }) : super(key: key);

  @override
  ConsumerState createState() => _MenuContainersState();
}

class _MenuContainersState extends ConsumerState<MenuComponents> {
  final GlobalKey<AnimatedListState> _listKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    int length = 0;
    var components = widget.containerDto.elements;
    if (components != null) {
      length = components.length;
    }

    return ScrollConfiguration(
      behavior: ScrollConfiguration.of(context).copyWith(
        dragDevices: {
          PointerDeviceKind.touch,
          PointerDeviceKind.mouse,
        },
      ),
      child: ListView.builder(
        key: _listKey,
        shrinkWrap: true,
        physics: const AlwaysScrollableScrollPhysics(),
        itemCount: length,
        itemBuilder: (context, index) => Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            MenuComponent(
              contextDto: widget.contextDto,
              containerDto: widget.containerDto,
              componentDto: widget.containerDto.elements![index],
            ),
          ],
        ),
      ),
    );
  }
}
