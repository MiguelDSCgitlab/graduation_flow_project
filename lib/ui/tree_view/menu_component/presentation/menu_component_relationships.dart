import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_shared/data/component_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/container_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import '../../../../app/foundation_widgets_styled/app_container_selected.dart';
import '../../../browser/browser_tabs/application/providers.dart';
import '../../../edit_raw_data/dialog/application/form_provider.dart';
import '../../chevron/application/chevron_animation_provider.dart';
import '../../constants.dart';
import '../../generic/presentation/popup_menu_widget.dart';
import '../../menu_component_relationships/presentation/menu_relationships_component.dart';
import 'menu_component_relationships_content.dart';

class MenuComponentRelationships extends ConsumerStatefulWidget {
  final ContextDto contextDto;
  final ContainerDto containerDto;
  final ComponentDto componentDto;

  const MenuComponentRelationships({
    Key? key,
    required this.contextDto,
    required this.containerDto,
    required this.componentDto,
  }) : super(key: key);

  @override
  ConsumerState createState() => _MenuContainerRelationshipsState();
}

class _MenuContainerRelationshipsState
    extends ConsumerState<MenuComponentRelationships> {
  late FocusNode focusNode;
  bool _focused = false;

  @override
  void initState() {
    super.initState();
    focusNode = FocusNode();
    focusNode.addListener(_handleFocusChange);
  }

  @override
  void dispose() {
    focusNode.removeListener(_handleFocusChange);
    focusNode.dispose();
    super.dispose();
  }

  void _handleFocusChange() {
    if (focusNode.hasFocus != _focused) {
      setState(() {
        _focused = focusNode.hasFocus;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final state = ref.read(
        chevronAnimationProvider('${widget.componentDto.name}_relationships'));
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        InkWell(
          onLongPress: () {
            if (!ref.watch(addTabProvider)) {
              onLongPress(widget.contextDto, widget.containerDto,
                  widget.componentDto, ref, context);
            }
          },
          onTap: () {
            onTap(widget.contextDto, widget.containerDto, widget.componentDto,
                ref);
            // final state = ref.read(
            //     chevronAnimationProvider('${widget.componentDto.name}_relationships'));
            //
            // focusNode.requestFocus();
            // setState(() {
            //   (state is InitialAnimatingState || state is ReverseAnimatingState)
            //       ? ref
            //           .read(chevronAnimationProvider(
            //                   '${widget.componentDto.name}_relationships')
            //               .notifier)
            //           .start()
            //       : ref
            //           .read(chevronAnimationProvider(
            //                   '${widget.componentDto.name}_relationships')
            //               .notifier)
            //           .stop();
            // });
          },
          child: Focus(
            focusNode: focusNode,
            child: focusNode.hasFocus
                ? AppContainerSelected(
                    child: MenuComponentRelationshipContent(
                      providerId: '${widget.componentDto.name}_relationships',
                    ),
                  )
                : MenuComponentRelationshipContent(
                    providerId: '${widget.componentDto.name}_relationships',
                  ),
          ),
        ),
        (state is StartAnimatingState)
            ? MenuRelationshipsComponent(
                contextDto: widget.contextDto,
                containerDto: widget.containerDto,
                componentDto: widget.componentDto,
              )
            : const SizedBox.shrink(),
      ],
    );
  }

  void onTap(ContextDto contextDto, ContainerDto containerDto,
      ComponentDto componentDto, WidgetRef ref) {
    final state = ref.read(
        chevronAnimationProvider('${widget.componentDto.name}_relationships'));

    focusNode.requestFocus();
    setState(() {
      (state is InitialAnimatingState || state is ReverseAnimatingState)
          ? ref
              .read(chevronAnimationProvider(
                      '${widget.componentDto.name}_relationships')
                  .notifier)
              .start()
          : ref
              .read(chevronAnimationProvider(
                      '${widget.componentDto.name}_relationships')
                  .notifier)
              .stop();
    });
  }

  void onLongPress(ContextDto contextDto, ContainerDto containerDto,
      ComponentDto componentDto, WidgetRef ref, BuildContext context) {
    focusNode.requestFocus();
    var renderObject = context.findRenderObject();
    var translation = renderObject?.getTransformTo(null).getTranslation();
    var position = Offset(translation?.x ?? 0, translation?.y ?? 0);
    showMenu(
      context: context,
      position: RelativeRect.fromLTRB(position.dx, position.dy, position.dx, 0),
      items: getMenuItems(contextDto, containerDto, componentDto, ref),
    );
  }

  getMenuItems(ContextDto contextDto, ContainerDto containerDto,
      ComponentDto componentDto, WidgetRef ref) {
    return <PopupMenuEntry<dynamic>>[
      PopupMenuWidget(
          child: Row(
        children: [
          const Icon(
            Icons.chevron_right,
            color: Colors.transparent,
          ),
          Padding(
            padding: EdgeInsets.only(left: tabSize),
          ),
          const Text("relationships"),
        ],
      )),
      const PopupMenuDivider(),
      PopupMenuItem<Text>(
        value: const Text('newComponentRelationship'),
        child: const Row(
          children: [
            Icon(Icons.add),
            SizedBox(
              width: 16,
            ),
            Text(
              "New component relationship",
            ),
          ],
        ),
        onTap: () {
          ref.read(formProvider.notifier).newComponentRelationship(
              contextDto: contextDto,
              containerDto: containerDto,
              componentDto: componentDto);
        },
      ),
    ];
  }
}
