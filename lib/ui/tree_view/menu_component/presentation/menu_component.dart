import 'package:c4_frontend_data_entry_flutter/ui/tree_view/menu_component/presentation/menu_component_content.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_svg/svg.dart';

import '../../../../api_raw_data/infrastructure/api_c4_shared/data/component_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/container_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import '../../../../app/foundation_widgets_styled/app_container_selected.dart';
import '../../../browser/browser_body/diagram/application/diagram_path.dart';
import '../../../browser/browser_tabs/application/providers.dart';
import '../../../edit_raw_data/dialog/application/form_provider.dart';
import '../../../explorer/explorer_finder/application/state_notifier_provider_new_c4_element.dart';
import '../../chevron/application/chevron_animation_provider.dart';
import '../../constants.dart';
import '../../generic/presentation/popup_menu_widget.dart';
import 'menu_component_api.dart';

class MenuComponent extends ConsumerStatefulWidget {
  final ContextDto contextDto;
  final ContainerDto containerDto;
  final ComponentDto componentDto;

  const MenuComponent({
    Key? key,
    required this.contextDto,
    required this.containerDto,
    required this.componentDto,
  }) : super(key: key);

  @override
  ConsumerState createState() => _MenuComponentState();
}

class _MenuComponentState extends ConsumerState<MenuComponent> {
  late FocusNode focusNode;
  bool _focused = false;

  @override
  void initState() {
    super.initState();
    focusNode = FocusNode();
    focusNode.addListener(_handleFocusChange);
  }

  @override
  void dispose() {
    focusNode.removeListener(_handleFocusChange);
    focusNode.dispose();
    super.dispose();
  }

  void _handleFocusChange() {
    if (focusNode.hasFocus != _focused) {
      setState(() {
        _focused = focusNode.hasFocus;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final state = ref.read(chevronAnimationProvider(widget.componentDto.name));
    return InkWell(
      onLongPress: () {
        onLongPress(widget.contextDto, widget.containerDto, widget.componentDto,
            ref, context);
      },
      onTap: () {
        onTap(widget.contextDto, widget.containerDto, widget.componentDto, ref);
      },
      child: Column(
        children: [
          Focus(
            focusNode: focusNode,
            child: focusNode.hasFocus
                ? AppContainerSelected(
                    child: MenuComponentContent(
                      name: widget.componentDto.name,
                    ),
                  )
                : MenuComponentContent(
                    name: widget.componentDto.name,
                  ),
          ),
          (state is StartAnimatingState)
              ? MenuComponentApi(
                  contextDto: widget.contextDto,
                  containerDto: widget.containerDto,
                  componentDto: widget.componentDto,
                )
              : const SizedBox.shrink(),
        ],
      ),
    );
  }

  void handleShowDiagram(ContextDto contextDto, ContainerDto containerDto,
      ComponentDto componentDto, WidgetRef ref) {
    if (!ref.watch(addTabProvider)) {
      ref.read(tabsListProvider.notifier).navigate(ComponentDiagramPath(
          widget.contextDto.contextId,
          widget.containerDto.id,
          widget.componentDto.componentId));
    } else {
      ref.read(tabsListProvider.notifier).addTab(
          ComponentDiagramPath(widget.contextDto.contextId,
              widget.containerDto.id, widget.componentDto.componentId),
          selectIt: true);
    }
  }

  void onLongPress(ContextDto contextDto, ContainerDto containerDto,
      ComponentDto componentDto, WidgetRef ref, BuildContext context) {
    focusNode.requestFocus();
    var renderObject = context.findRenderObject();
    var translation = renderObject?.getTransformTo(null).getTranslation();
    var position = Offset(translation?.x ?? 0, translation?.y ?? 0);
    showMenu(
      context: context,
      position: RelativeRect.fromLTRB(position.dx, position.dy, position.dx, 0),
      items: getMenuItems(contextDto, containerDto, componentDto, ref),
    );
  }

  void onTap(ContextDto contextDto, ContainerDto containerDto,
      ComponentDto componentDto, WidgetRef ref) {
    final state = ref.read(chevronAnimationProvider(widget.componentDto.name));
    focusNode.requestFocus();
    setState(() {
      (state is InitialAnimatingState || state is ReverseAnimatingState)
          ? {
              ref
                  .read(chevronAnimationProvider(widget.componentDto.name)
                      .notifier)
                  .start(),
              ref.read(newC4ElementProvider.notifier).componentState(
                  widget.contextDto, widget.containerDto, widget.componentDto),
            }
          : {
              ref
                  .read(chevronAnimationProvider(widget.componentDto.name)
                      .notifier)
                  .stop(),
              ref.read(newC4ElementProvider.notifier).contextsState(),
            };
    });
  }

  getMenuItems(ContextDto contextDto, ContainerDto containerDto,
      ComponentDto componentDto, WidgetRef ref) {
    return <PopupMenuEntry<dynamic>>[
      PopupMenuWidget(
          child: Row(
        children: [
          const Icon(
            Icons.chevron_right,
            color: Colors.transparent,
          ),
          Padding(
            padding: EdgeInsets.only(left: tabSize),
          ),
          Text(componentDto.name),
        ],
      )),
      const PopupMenuDivider(),
      // PopupMenuWidget(child: child)
      PopupMenuItem<Text>(
        value: const Text('showDiagram'),
        child: Row(
          children: [
            SvgPicture.asset(
              'assets/icons/c3.svg',
              width: 20,
              height: 20,
            ),
            const SizedBox(
              width: 16,
            ),
            const Text(
              "Component diagram",
            ),
          ],
        ),
        onTap: () {
          handleShowDiagram(contextDto, containerDto, componentDto, ref);
        },
      ),
      PopupMenuItem<Text>(
        value: const Text('editComponent'),
        child: const Row(
          children: [
            Icon(Icons.edit),
            SizedBox(
              width: 16,
            ),
            Text(
              "Edit component",
            ),
          ],
        ),
        onTap: () {
          ref.read(formProvider.notifier).editComponent(
              contextDto: contextDto,
              containerDto: containerDto,
              componentDto: componentDto);
        },
      ),
    ];
  }
}
