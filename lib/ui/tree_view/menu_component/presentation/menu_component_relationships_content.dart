import 'package:flutter/material.dart';

import '../../chevron/presentation/chevron_animation.dart';
import '../../constants.dart';

class MenuComponentRelationshipContent extends StatelessWidget {
  final String providerId;

  const MenuComponentRelationshipContent({
    super.key,
    required this.providerId,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        const Icon(
          Icons.chevron_right,
          color: Colors.transparent,
        ),
        Padding(
          padding: EdgeInsets.only(left: tabSize),
        ),
        const Icon(
          Icons.chevron_right,
          color: Colors.transparent,
        ),
        Padding(
          padding: EdgeInsets.only(left: tabSize),
        ),
        const Icon(
          Icons.chevron_right,
          color: Colors.transparent,
        ),
        Padding(
          padding: EdgeInsets.only(left: tabSize),
        ),
        const Icon(
          Icons.chevron_right,
          color: Colors.transparent,
        ),
        Padding(
          padding: EdgeInsets.only(left: tabSize),
        ),
        const Icon(
          Icons.chevron_right,
          color: Colors.transparent,
        ),
        Padding(
          padding: EdgeInsets.only(left: tabSize),
        ),
        ChevronAnimation(providerId: providerId),
        Padding(
          padding: EdgeInsets.only(left: tabSize),
        ),
        const Expanded(
          child: Text(
            'relations',
            overflow: TextOverflow.ellipsis,
          ),
        ),
      ],
    );
  }
}
