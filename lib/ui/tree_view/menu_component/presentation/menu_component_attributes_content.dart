import 'package:c4_frontend_data_entry_flutter/ui/tree_view/constants.dart';
import 'package:flutter/material.dart';

class MenuComponentAttributesContent extends StatelessWidget {
  const MenuComponentAttributesContent({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        const Icon(
          Icons.chevron_right,
          color: Colors.transparent,
        ),
        Padding(
          padding: EdgeInsets.only(left: tabSize),
        ),
        const Icon(
          Icons.chevron_right,
          color: Colors.transparent,
        ),
        Padding(
          padding: EdgeInsets.only(left: tabSize),
        ),
        const Icon(
          Icons.chevron_right,
          color: Colors.transparent,
        ),
        Padding(
          padding: EdgeInsets.only(left: tabSize),
        ),
        const Icon(
          Icons.chevron_right,
          color: Colors.transparent,
        ),
        Padding(
          padding: EdgeInsets.only(left: tabSize),
        ),
        const Icon(
          Icons.chevron_right,
          color: Colors.transparent,
        ),
        Padding(
          padding: EdgeInsets.only(left: tabSize),
        ),
        const Icon(
          Icons.edit,
        ),
        Padding(
          padding: EdgeInsets.only(left: tabSize),
        ),
        const Expanded(
          child: Text(
            'attributes',
            overflow: TextOverflow.ellipsis,
          ),
        ),
      ],
    );
  }
}
