import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_shared/data/component_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/container_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import 'menu_component_relationships.dart';

class MenuComponentApi extends ConsumerWidget {
  final ContextDto contextDto;
  final ContainerDto containerDto;
  final ComponentDto componentDto;

  const MenuComponentApi({
    Key? key,
    required this.contextDto,
    required this.containerDto,
    required this.componentDto,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Column(
      children: [
        // MenuComponentAttributes(
        //     contextDto: contextDto,
        //     containerDto: containerDto,
        //     componentDto: componentDto),
        MenuComponentRelationships(
            contextDto: contextDto,
            containerDto: containerDto,
            componentDto: componentDto),
      ],
    );
  }
}
