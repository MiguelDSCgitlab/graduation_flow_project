import 'package:flutter_riverpod/flutter_riverpod.dart';

final chevronAnimationProvider = StateNotifierProvider.autoDispose
    .family<AnimationNotifier, AnimationState, String>((ref, id) {
  return AnimationNotifier(ref);
});

class AnimationNotifier extends StateNotifier<AnimationState> {
  final Ref ref;

  AnimationNotifier(this.ref) : super(InitialAnimatingState());

  void start() async {
    state = StartAnimatingState();
  }

  void stop() async {
    state = ReverseAnimatingState();
  }
}

abstract class AnimationState {}

class InitialAnimatingState extends AnimationState {}

class StartAnimatingState extends AnimationState {}

class ReverseAnimatingState extends AnimationState {}
