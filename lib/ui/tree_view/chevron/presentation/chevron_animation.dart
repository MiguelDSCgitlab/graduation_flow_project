import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../application/chevron_animation_provider.dart';

class ChevronAnimation extends ConsumerStatefulWidget {
  final String providerId;

  const ChevronAnimation({
    Key? key,
    required this.providerId,
  }) : super(key: key);

  @override
  ConsumerState createState() => _ChevronAnimationState();
}

class _ChevronAnimationState extends ConsumerState<ChevronAnimation>
    with SingleTickerProviderStateMixin {
  late final AnimationController _controller;
  late final Animation<double> _animation;

  @override
  void initState() {
    super.initState();

    _controller = AnimationController(
        duration: const Duration(milliseconds: 50), vsync: this);
    _animation = Tween(begin: 0.0, end: .25).animate(
      CurvedAnimation(parent: _controller, curve: Curves.easeOut),
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final state = ref.watch(chevronAnimationProvider(widget.providerId));
    if (state is StartAnimatingState) {
      _controller.forward();
    }
    if (state is ReverseAnimatingState) {
      _controller.reverse();
    }
    return RotationTransition(
      turns: _animation,
      child: const Icon(Icons.chevron_right),
    );
  }
}
