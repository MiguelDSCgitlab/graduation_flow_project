import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_shared/data/container_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import 'menu_container_components.dart';
import 'menu_container_relationships.dart';

class MenuContainerApi extends ConsumerWidget {
  final ContextDto contextDto;
  final ContainerDto containerDto;

  const MenuContainerApi({
    Key? key,
    required this.contextDto,
    required this.containerDto,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Column(
      children: [
        // MenuContainerAttributes(
        //   contextDto: contextDto,
        //   containerDto: containerDto,
        // ),
        MenuContainerComponents(
          contextDto: contextDto,
          containerDto: containerDto,
        ),
        MenuContainerRelationships(
          contextDto: contextDto,
          containerDto: containerDto,
        )
      ],
    );
  }
}
