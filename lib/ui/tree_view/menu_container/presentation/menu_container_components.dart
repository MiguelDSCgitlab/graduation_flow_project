import 'package:c4_frontend_data_entry_flutter/api_raw_data/infrastructure/api_c4_shared/data/container_dto.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_svg/svg.dart';

import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import '../../../../app/foundation_widgets_styled/app_container_selected.dart';
import '../../../browser/browser_body/diagram/application/diagram_path.dart';
import '../../../browser/browser_tabs/application/providers.dart';
import '../../../edit_raw_data/dialog/application/form_provider.dart';
import '../../chevron/application/chevron_animation_provider.dart';
import '../../constants.dart';
import '../../generic/presentation/popup_menu_widget.dart';
import '../../menu_component/presentation/menu_components.dart';
import 'menu_container_components_content.dart';

class MenuContainerComponents extends ConsumerStatefulWidget {
  final ContextDto contextDto;
  final ContainerDto containerDto;

  const MenuContainerComponents({
    Key? key,
    required this.contextDto,
    required this.containerDto,
  }) : super(key: key);

  @override
  ConsumerState createState() => _MenuContainerComponentsState();
}

class _MenuContainerComponentsState
    extends ConsumerState<MenuContainerComponents> {
  late FocusNode focusNode;
  bool _focused = false;

  @override
  void initState() {
    super.initState();
    focusNode = FocusNode();
    focusNode.addListener(_handleFocusChange);
  }

  @override
  void dispose() {
    focusNode.removeListener(_handleFocusChange);
    focusNode.dispose();
    super.dispose();
  }

  void _handleFocusChange() {
    if (focusNode.hasFocus != _focused) {
      setState(() {
        _focused = focusNode.hasFocus;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    var state = ref.read(
        chevronAnimationProvider('${widget.containerDto.name}_components'));

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        InkWell(
          onLongPress: () {
            onLongPress(widget.contextDto, widget.containerDto, ref, context);
          },
          onTap: () {
            onTap(widget.contextDto, widget.containerDto, ref);
          },
          child: Focus(
            focusNode: focusNode,
            child: focusNode.hasFocus
                ? AppContainerSelected(
                    child: MenuContainerComponentsContent(
                      providerId: '${widget.containerDto.name}_components',
                    ),
                  )
                : MenuContainerComponentsContent(
                    providerId: '${widget.containerDto.name}_components',
                  ),
          ),
        ),
        (state is StartAnimatingState)
            ? MenuComponents(
                contextDto: widget.contextDto,
                containerDto: widget.containerDto,
              )
            : const SizedBox.shrink(),
      ],
    );
  }

  void handleShowDiagram(
      ContextDto contextDto, ContainerDto containerDto, WidgetRef ref) {
    if (!ref.watch(addTabProvider)) {
      ref.read(tabsListProvider.notifier).navigate(AllComponentsDiagramPath(
          widget.contextDto.contextId, widget.containerDto.id));
    } else {
      ref.read(tabsListProvider.notifier).addTab(
          AllComponentsDiagramPath(
              widget.contextDto.contextId, widget.containerDto.id),
          selectIt: true);
    }
  }

  void onLongPress(ContextDto contextDto, ContainerDto containerDto,
      WidgetRef ref, BuildContext context) {
    focusNode.requestFocus();
    var renderObject = context.findRenderObject();
    var translation = renderObject?.getTransformTo(null).getTranslation();
    var position = Offset(translation?.x ?? 0, translation?.y ?? 0);
    showMenu(
      context: context,
      position: RelativeRect.fromLTRB(position.dx, position.dy, position.dx, 0),
      items: getMenuItems(contextDto, containerDto, ref),
    );
  }

  void onTap(ContextDto contextDto, ContainerDto containerDto, WidgetRef ref) {
    var state = ref.read(
        chevronAnimationProvider('${widget.containerDto.name}_components'));
    focusNode.requestFocus();
    setState(() {
      (state is InitialAnimatingState || state is ReverseAnimatingState)
          ? ref
              .read(chevronAnimationProvider(
                      '${widget.containerDto.name}_components')
                  .notifier)
              .start()
          : ref
              .read(chevronAnimationProvider(
                      '${widget.containerDto.name}_components')
                  .notifier)
              .stop();
    });
  }

  getMenuItems(
      ContextDto contextDto, ContainerDto containerDto, WidgetRef ref) {
    return <PopupMenuEntry<dynamic>>[
      PopupMenuWidget(
          child: Row(
        children: [
          const Icon(
            Icons.chevron_right,
            color: Colors.transparent,
          ),
          Padding(
            padding: EdgeInsets.only(left: tabSize),
          ),
          const Text("components"),
        ],
      )),
      const PopupMenuDivider(),
      // PopupMenuWidget(child: child)
      PopupMenuItem<Text>(
        value: const Text('showDiagram'),
        child: Row(
          children: [
            SvgPicture.asset(
              'assets/icons/c3.svg',
              width: 20,
              height: 20,
            ),
            const SizedBox(
              width: 16,
            ),
            const Text(
              "Components diagram",
            ),
          ],
        ),
        onTap: () {
          handleShowDiagram(contextDto, containerDto, ref);
        },
      ),
      PopupMenuItem<Text>(
        value: const Text('newComponent'),
        child: const Row(
          children: [
            Icon(Icons.add),
            SizedBox(
              width: 16,
            ),
            Text(
              "New component",
            ),
          ],
        ),
        onTap: () {
          ref
              .read(formProvider.notifier)
              .newComponent(contextDto: contextDto, containerDto: containerDto);
        },
      ),
    ];
  }
}
