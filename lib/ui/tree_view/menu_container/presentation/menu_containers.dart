import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import 'menu_container.dart';

class MenuContainers extends ConsumerStatefulWidget {
  final ContextDto contextDto;

  const MenuContainers({
    Key? key,
    required this.contextDto,
  }) : super(key: key);

  @override
  ConsumerState createState() => _MenuContainersState();
}

class _MenuContainersState extends ConsumerState<MenuContainers> {
  final GlobalKey<AnimatedListState> _listKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    int length = 0;
    var containers = widget.contextDto.containers;
    if (containers != null) {
      length = containers.length;
    }

    return ScrollConfiguration(
      behavior: ScrollConfiguration.of(context).copyWith(
        dragDevices: {
          PointerDeviceKind.touch,
          PointerDeviceKind.mouse,
        },
      ),
      child: ListView.builder(
        key: _listKey,
        shrinkWrap: true,
        physics: const AlwaysScrollableScrollPhysics(),
        itemCount: length,
        itemBuilder: (context, index) => Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            MenuContainer(
              contextDto: widget.contextDto,
              containerDto: widget.contextDto.containers![index],
            ),
          ],
        ),
      ),
    );
  }
}
