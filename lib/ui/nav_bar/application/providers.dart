import 'package:flutter_riverpod/flutter_riverpod.dart';

enum NavPage { HOME, ARCHIVE, RECENT, PRESENTATION, FILES }

final currentPageProvider = StateProvider<NavPage>((ref) => NavPage.HOME);
