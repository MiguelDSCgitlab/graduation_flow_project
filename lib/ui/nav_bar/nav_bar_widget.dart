import 'package:c4_frontend_data_entry_flutter/ui/color_scheme/colors.dart';
import 'package:c4_frontend_data_entry_flutter/ui/nav_bar/presentation/nav_bar_buttons.dart';
import 'package:flutter/cupertino.dart';

class NavBarWidget extends StatelessWidget {
  const NavBarWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: darkColor,
      width: 70,
      child: Column(
        children: [
          const SizedBox(height: 10),
          logo,
          const SizedBox(height: 10),
          ...navBarItems,
          const Spacer(),
          settingsButton,
          const SizedBox(height: 20),
        ],
      ),
    );
  }
}
