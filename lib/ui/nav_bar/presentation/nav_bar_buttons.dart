import 'package:c4_frontend_data_entry_flutter/ui/nav_bar/application/providers.dart';
import 'package:c4_frontend_data_entry_flutter/ui/nav_bar/presentation/nav_bar_icon.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/svg.dart';

const _basePath = "assets/icons/ui/";

final navBarItems = [
  const NavBarIcon(iconPath: "${_basePath}nav_home.svg", page: NavPage.HOME),
  // const NavBarIcon(
  //     iconPath: "${_basePath}nav_archive.svg", page: NavPage.ARCHIVE),
  const NavBarIcon(iconPath: "${_basePath}nav_files.svg", page: NavPage.FILES),
  // const NavBarIcon(
  //     iconPath: "${_basePath}nav_recent.svg", page: NavPage.RECENT),
  const NavBarIcon(
      iconPath: "${_basePath}nav_presentation.svg", page: NavPage.PRESENTATION),
];

final logo = SizedBox(
  width: 50,
  height: 50,
  child: Center(
    child: SvgPicture.asset("${_basePath}logo.svg", width: 45, height: 45),
  ),
);

final settingsButton = GestureDetector(
  onTap: () {},
  child: SizedBox(
    width: 45,
    height: 45,
    child: Center(
      child: SvgPicture.asset(
        "${_basePath}nav_settings.svg",
        width: 30,
        height: 30,
      ),
    ),
  ),
);
