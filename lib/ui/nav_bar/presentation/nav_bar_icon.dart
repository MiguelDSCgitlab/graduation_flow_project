import 'package:c4_frontend_data_entry_flutter/ui/browser/browser_body/diagram/application/diagram_path.dart';
import 'package:c4_frontend_data_entry_flutter/ui/browser/browser_body/diagram/application/providers.dart';
import 'package:c4_frontend_data_entry_flutter/ui/color_scheme/colors.dart';
import 'package:c4_frontend_data_entry_flutter/ui/nav_bar/application/providers.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_svg/svg.dart';

import '../../browser/browser_tabs/application/providers.dart';

class NavBarIcon extends ConsumerWidget {
  final String iconPath;
  final NavPage page;

  const NavBarIcon({
    Key? key,
    required this.iconPath,
    required this.page,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    var isSelected = ref.watch(currentPageProvider) == page;
    return GestureDetector(
      onTap: isSelected
          ? null
          : () {
              ref.read(currentPageProvider.notifier).state = page;
              _performAction(ref, page);
            },
      child: Container(
        height: 60,
        color: isSelected ? blackColor : null,
        child: Center(
          child: SvgPicture.asset(
            iconPath,
            width: 30,
            height: 30,
          ),
        ),
      ),
    );
  }

  void _performAction(WidgetRef ref, NavPage page) {
    switch (page) {
      case NavPage.HOME:
        break;
      case NavPage.FILES:
        if (!ref.read(tabsNotEmptyProvider)) {
          ref
              .read(tabsListProvider.notifier)
              .addTab(const AllContextsDiagramPath(), selectIt: true);
        }
        break;
      case NavPage.PRESENTATION:
        break;
      case NavPage.RECENT:
        break;
      case NavPage.ARCHIVE:
        break;
      default:
        break;
    }
  }
}
