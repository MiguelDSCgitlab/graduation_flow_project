import 'package:flutter/material.dart';

import '../../../api_raw_data/infrastructure/api_c4_error/error_dto.dart';

class ErrorFormWidget extends StatelessWidget {
  final ErrorDto? errorDto;

  const ErrorFormWidget({Key? key, required this.errorDto}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return errorDto == null
        ? const SizedBox.shrink()
        : Column(
            children: [
              Row(
                children: [
                  Expanded(
                    child: Text(
                      errorDto?.error ?? 'Technical error',
                      softWrap: true,
                      style: const TextStyle(color: Colors.red, fontSize: 18),
                    ),
                  ),
                  const SizedBox(
                    width: 16,
                  ),
                  const Icon(
                    Icons.error,
                    color: Colors.red,
                  ),
                ],
              ),
              const Padding(
                padding: EdgeInsets.only(top: 16.0),
              ),
            ],
          );
  }
}
