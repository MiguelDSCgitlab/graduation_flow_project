import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class CancelIconButton extends ConsumerWidget {
  final VoidCallback callback;

  const CancelIconButton({
    Key? key,
    required this.callback,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return IconButton(
      icon: const Icon(Icons.cancel),
      onPressed: () {
        callback();
        // Navigator.of(context).pop();
        // ref.read(formProvider.notifier).dismiss();
      },
    );
  }
}
