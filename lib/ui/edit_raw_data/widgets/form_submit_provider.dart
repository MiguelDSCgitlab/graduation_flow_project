import 'package:flutter_riverpod/flutter_riverpod.dart';

final formSubmitProvider = StateNotifierProvider<FormSubmitNotifier, bool>(
    (ref) => FormSubmitNotifier());

class FormSubmitNotifier extends StateNotifier<bool> {
  FormSubmitNotifier() : super(false);

  void submit() {
    state = !state;
  }
}
