import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class LoadingWidget extends ConsumerWidget {
  const LoadingWidget({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return const Row(
      children: [
        Expanded(
            child: Center(
          child: SizedBox(
            width: 75,
            height: 75,
            child: CircularProgressIndicator(),
          ),
        )),
      ],
    );
  }
}
