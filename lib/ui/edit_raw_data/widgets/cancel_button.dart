import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class CancelButton extends ConsumerWidget {
  const CancelButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return ElevatedButton.icon(
      icon: const Icon(Icons.cancel),
      onPressed: () {
        // Navigator.of(context, rootNavigator: false).pop();
        // Navigator.of(context).pop();
        // ref.read(formProvider.notifier).dismiss();
      },
      label: const Padding(
        padding: EdgeInsets.all(8.0),
        child: Text('Cancel'),
      ),
    );
  }
}
