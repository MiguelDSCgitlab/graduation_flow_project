import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../dialog/application/form_provider.dart';
import '../application/container_provider.dart';
import '../application/future_provider_new_container.dart';
import 'container_form.dart';

class ContainerApiPost extends ConsumerStatefulWidget {
  const ContainerApiPost({
    Key? key,
  }) : super(key: key);

  @override
  ConsumerState createState() => _ContainerApiPostState();
}

class _ContainerApiPostState extends ConsumerState<ContainerApiPost> {
  @override
  Widget build(BuildContext context) {
    final apiResult = ref.watch(futureProviderNewContainer);
    final containerDto = ref.read(formProvider.notifier).getContainer();
    var containerInputDto =
        ref.read(containerProvider.notifier).storedContainerInputDto;

    if (containerInputDto == null) {
      return const SizedBox.shrink();
    }

    return apiResult.when(
      data: (data) {
        final errorDto = data.errorDto;

        if (errorDto != null) {
          final contextDto = ref.read(formProvider.notifier).getContext();
          return ContainerForm(
            contextDto: contextDto,
            containerDto: containerDto,
            containerInputDto: containerInputDto,
            errorDto: errorDto,
          );
        } else {
          WidgetsBinding.instance.addPostFrameCallback((_) {
            Navigator.pop(context);
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              backgroundColor: CupertinoColors.activeBlue,
              content: Text(
                "Container ${containerInputDto.name} created.",
                style: const TextStyle(color: CupertinoColors.white),
              ),
            ));
          });
          return const SizedBox.shrink();
        }
      },
      error: (_, __) => const Text('Something went wrong'),
      loading: () => const Text('Loading'),
    );
  }
}
