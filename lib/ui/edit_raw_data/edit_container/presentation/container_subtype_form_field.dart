import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_containers/container_input_dto.dart';

class ContainerSubtypeFormField extends ConsumerStatefulWidget {
  final ContainerInputDto containerInputDto;

  const ContainerSubtypeFormField({
    Key? key,
    required this.containerInputDto,
  }) : super(key: key);

  @override
  ConsumerState createState() => _ContainerSubtypeFormFieldState();
}

class _ContainerSubtypeFormFieldState
    extends ConsumerState<ContainerSubtypeFormField> {
  late Set<String> selection;

  @override
  void initState() {
    selection = <String>{"Server"};
    if (widget.containerInputDto.subType == 'Server') {
      selection = <String>{"Server"};
    }
    if (widget.containerInputDto.subType == 'GUI') {
      selection = <String>{"GUI"};
    }
    if (widget.containerInputDto.subType == 'Database') {
      selection = <String>{"Database"};
    }
    if (widget.containerInputDto.subType == 'Pipe') {
      selection = <String>{"Pipe"};
    }
    if (widget.containerInputDto.subType == null) {
      selection = <String>{"Server"};
      widget.containerInputDto.subType = "Server";
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SegmentedButton<String>(
      segments: const <ButtonSegment<String>>[
        ButtonSegment<String>(value: "GUI", label: Text('GUI')),
        ButtonSegment<String>(value: "Server", label: Text('Server')),
        ButtonSegment<String>(value: "Database", label: Text('Database')),
        ButtonSegment<String>(value: "Pipe", label: Text('Pipe')),
      ],
      selected: selection,
      onSelectionChanged: (Set<String> newSelection) {
        setState(() {
          selection = newSelection;
          if (newSelection.first.startsWith('GUI')) {
            widget.containerInputDto.subType = "GUI";
          }
          if (newSelection.first.startsWith('Server')) {
            widget.containerInputDto.subType = "Server";
          }
          if (newSelection.first.startsWith('Database')) {
            widget.containerInputDto.subType = "Database";
          }
          if (newSelection.first.startsWith('Pipe')) {
            widget.containerInputDto.subType = "Pipe";
          }
        });
      },
      multiSelectionEnabled: false,
    );
  }
}
