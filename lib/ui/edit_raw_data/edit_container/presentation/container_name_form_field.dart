import 'package:c4_frontend_data_entry_flutter/api_raw_data/infrastructure/api_c4_containers/container_input_dto.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class ContainerNameFormField extends ConsumerWidget {
  final ContainerInputDto containerInputDto;

  const ContainerNameFormField({
    Key? key,
    required this.containerInputDto,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return TextFormField(
      decoration: const InputDecoration(
        labelText: "Container name",
        hintText: "Container name",
        prefixIcon: Icon(
          Icons.architecture,
          color: Colors.white,
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(16.0),
          ),
        ),
      ),
      maxLength: 40,
      initialValue: containerInputDto.name,
      validator: (value) {
        if (value != null) {
          if (value.isEmpty) {
            return 'Please enter a name';
          }
        }
        return null;
      },
      onSaved: (value) => containerInputDto.name = value!,
    );
  }
}
