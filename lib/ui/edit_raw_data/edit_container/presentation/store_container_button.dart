import 'package:c4_frontend_data_entry_flutter/ui/edit_raw_data/technology/application/technology_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_containers/container_input_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/container_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import '../../widgets/form_submit_provider.dart';
import '../application/container_provider.dart';

class StoreContainerButton extends ConsumerWidget {
  final GlobalKey<FormState> formKey;
  final ContextDto contextDto;
  final ContainerDto? containerDto;
  final ContainerInputDto containerInputDto;

  const StoreContainerButton({
    Key? key,
    required this.formKey,
    required this.containerInputDto,
    required this.contextDto,
    this.containerDto,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final formState = ref.read(containerProvider);
    return ElevatedButton.icon(
      icon: const Icon(Icons.save),
      onPressed: () {
        if (formKey.currentState!.validate()) {
          formKey.currentState!.save();
          ref.read(formSubmitProvider.notifier).submit();
          if (formState is NewContainerFormState) {
            containerInputDto.technology =
                ref.read(technologyProvider.notifier).selectedTechnologies;
            ref
                .read(containerProvider.notifier)
                .postContainer(containerInputDto);
          }
          if (formState is EditContainerFormState) {
            final container = containerDto;
            if (container != null) {
              ref.read(containerProvider.notifier).setContextDto(contextDto);
              ref.read(containerProvider.notifier).setContainerDto(container);
              ref
                  .read(containerProvider.notifier)
                  .putContainer(containerInputDto);
            }
          }
        }
      },
      label: const Padding(
        padding: EdgeInsets.all(8.0),
        child: Text('Store'),
      ),
    );
  }
}
