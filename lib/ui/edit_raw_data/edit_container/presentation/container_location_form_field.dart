import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_containers/container_input_dto.dart';

class ContainerLocationFormField extends ConsumerStatefulWidget {
  final ContainerInputDto containerInputDto;

  const ContainerLocationFormField({
    Key? key,
    required this.containerInputDto,
  }) : super(key: key);

  @override
  ConsumerState createState() => _LocationFormFieldState();
}

class _LocationFormFieldState
    extends ConsumerState<ContainerLocationFormField> {
  late Set<String> selection;

  @override
  void initState() {
    if (widget.containerInputDto.location == 'External') {
      selection = <String>{"External"};
    } else {
      selection = <String>{"Internal"};
      widget.containerInputDto.location = "Internal";
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SegmentedButton<String>(
      segments: const <ButtonSegment<String>>[
        ButtonSegment<String>(value: "Internal", label: Text('Internal')),
        ButtonSegment<String>(value: "External", label: Text('External')),
      ],
      selected: selection,
      onSelectionChanged: (Set<String> newSelection) {
        setState(() {
          selection = newSelection;
          if (newSelection.first.startsWith('External')) {
            widget.containerInputDto.location = "External";
          } else {
            widget.containerInputDto.location = "Internal";
          }
        });
      },
      multiSelectionEnabled: false,
    );
  }
}
