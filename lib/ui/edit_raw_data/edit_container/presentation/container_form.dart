import 'package:c4_frontend_data_entry_flutter/ui/edit_raw_data/edit_container/application/container_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_containers/container_input_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_error/error_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/container_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import '../../technology/application/technology_provider.dart';
import '../../technology/presentation/technologies_list.dart';
import '../../technology/presentation/technology_button.dart';
import '../../widgets/error_form.dart';
import 'container_description_form_field.dart';
import 'container_location_form_field.dart';
import 'container_name_form_field.dart';
import 'container_subtype_form_field.dart';
import 'container_title.dart';
import 'delete_container_button.dart';
import 'store_container_button.dart';

class ContainerForm extends ConsumerStatefulWidget {
  final ContextDto contextDto;
  final ContainerDto? containerDto;
  final ContainerInputDto containerInputDto;
  final ErrorDto? errorDto;

  const ContainerForm({
    super.key,
    required this.contextDto,
    this.containerDto,
    required this.containerInputDto,
    required this.errorDto,
  });

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _FormNewContextState();
}

class _FormNewContextState extends ConsumerState<ContainerForm> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    String title = "";
    final state = ref.read(containerProvider);
    if (state is NewContainerFormState || state is PostContainerState) {
      title = "New container";
    } else {
      title = "${widget.containerDto?.name}";
    }

    ref.read(technologyProvider.notifier).selectedTechnologies =
        widget.containerInputDto.technology ?? [];
    return Form(
      key: _formKey,
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            const Padding(
              padding: EdgeInsets.all(16.0),
            ),
            ErrorFormWidget(
              errorDto: widget.errorDto,
            ),
            ContainerTitleWidget(name: title),
            ContainerNameFormField(
              containerInputDto: widget.containerInputDto,
            ),
            ContainerDescriptionFormField(
              containerInputDto: widget.containerInputDto,
            ),
            const Padding(
              padding: EdgeInsets.all(8.0),
            ),
            const Text(
              "Type of container",
            ),
            ContainerSubtypeFormField(
              containerInputDto: widget.containerInputDto,
            ),
            const Padding(
              padding: EdgeInsets.all(8.0),
            ),
            const Text(
              "Location?",
            ),
            ContainerLocationFormField(
              containerInputDto: widget.containerInputDto,
            ),
            const Padding(
              padding: EdgeInsets.all(8.0),
            ),
            const Row(
              children: [
                Text('Technologies'),
                Padding(
                  padding: EdgeInsets.only(right: 8),
                ),
                Spacer(),
                TechnologyIconButton(
                  title: 'Supported technologies',
                )
              ],
            ),
            const TechnologiesWidget(),
            const SizedBox(
              height: 16,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                const SizedBox(
                  width: 16,
                ),
                (state is EditContainerFormState)
                    ? DeleteContainerButton(
                        contextDto: widget.contextDto,
                        containerDto: widget.containerDto,
                        containerInputDto: widget.containerInputDto,
                      )
                    : Container(),
                const SizedBox(
                  width: 16,
                ),
                StoreContainerButton(
                  contextDto: widget.contextDto,
                  containerDto: widget.containerDto,
                  containerInputDto: widget.containerInputDto,
                  formKey: _formKey,
                ),
              ],
            ),
            const SizedBox(
              height: 16,
            ),
          ],
        ),
      ),
    );
  }
}
