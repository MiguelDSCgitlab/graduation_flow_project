import 'package:c4_frontend_data_entry_flutter/api_raw_data/infrastructure/api_c4_containers/container_input_dto.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_shared/data/container_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import '../../widgets/loading.dart';
import '../application/container_provider.dart';
import 'container_api_delete.dart';
import 'container_api_post.dart';
import 'container_api_put.dart';
import 'container_form.dart';

class ContainerProviderWidget extends ConsumerWidget {
  final ContextDto contextDto;
  final ContainerDto? containerDto;

  const ContainerProviderWidget({
    Key? key,
    required this.contextDto,
    this.containerDto,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final state = ref.watch(containerProvider);

    if (state is ContainerInitialState) {
      return const LoadingWidget();
    }

    if (state is NewContainerFormState) {
      ContainerInputDto containerInputDto = ContainerInputDto(name: '');
      return ContainerForm(
        contextDto: contextDto,
        containerDto: containerDto,
        containerInputDto: containerInputDto,
        errorDto: null,
      );
    }

    if (state is EditContainerFormState) {
      ContainerDto? tContainerDto = containerDto;
      if (tContainerDto != null) {
        ContainerInputDto containerInputDto = ContainerInputDto(
          name: tContainerDto.name,
          description: tContainerDto.description,
          location: tContainerDto.location,
          subType: tContainerDto.subType,
          technology: tContainerDto.technology,
        );

        return ContainerForm(
          contextDto: contextDto,
          containerDto: containerDto,
          containerInputDto: containerInputDto,
          errorDto: null,
        );
      } else {
        // todo return Error widget
        return const Text('error occurred');
      }
    }

    if (state is PostContainerState) {
      return const ContainerApiPost();
    }

    if (state is PutContainerState) {
      return const ContainerApiPut();
    }
    if (state is DeleteContainerState) {
      return const ContainerApiDelete();
    }

    return Container();
  }
}
