import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_containers/container_input_dto.dart';

class ContainerDescriptionFormField extends ConsumerWidget {
  final ContainerInputDto containerInputDto;

  const ContainerDescriptionFormField({
    Key? key,
    required this.containerInputDto,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return TextFormField(
      decoration: const InputDecoration(
        labelText: "Description of the architecture",
        hintText: "Description of the architecture",
        prefixIcon: Icon(
          Icons.description,
          color: Colors.white,
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(16.0),
          ),
        ),
      ),
      maxLength: 200,
      initialValue: containerInputDto.description,
      validator: (value) {
        if (value != null) {
          if (value.isEmpty) {
            return 'Please enter a description';
          }
        }
        return null;
      },
      onSaved: (value) => containerInputDto.description = value!,
    );
  }
}
