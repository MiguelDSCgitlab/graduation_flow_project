import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_containers/container_input_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/container_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import '../../widgets/form_submit_provider.dart';
import '../application/container_provider.dart';

class DeleteContainerButton extends ConsumerWidget {
  final ContextDto contextDto;
  final ContainerDto? containerDto;
  final ContainerInputDto containerInputDto;

  const DeleteContainerButton({
    Key? key,
    required this.contextDto,
    this.containerDto,
    required this.containerInputDto,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return ElevatedButton.icon(
      icon: const Icon(Icons.delete),
      onPressed: () {
        ref.read(formSubmitProvider.notifier).submit();
        final container = containerDto;
        if (container != null) {
          ref.read(containerProvider.notifier).setContextDto(contextDto);
          ref.read(containerProvider.notifier).setContainerDto(container);
          ref
              .read(containerProvider.notifier)
              .setContainerInputDto(containerInputDto);
          ref.read(containerProvider.notifier).deleteContainer();
        }
      },
      label: const Padding(
        padding: EdgeInsets.all(8.0),
        child: Text('Delete'),
      ),
    );
  }
}
