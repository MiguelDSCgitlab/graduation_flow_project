import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:uuid/uuid.dart';

import '../../../../api_raw_data/infrastructure/api_c4_all_components/dependency_injection.dart';
import '../../../../api_raw_data/infrastructure/api_c4_containers/containers_output_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_containers/containers_repository.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/container_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import '../../../tree_view/menu_context/application/menu_contexts_refresh_provider.dart';
import '../../dialog/application/form_provider.dart';
import '../../widgets/form_submit_provider.dart';

final futureProviderDeleteContainer =
    FutureProvider.autoDispose<ContainersOutputDto>(
  (ref) async {
    ref.watch(formSubmitProvider);
    ContextDto contextDto = ref.read(formProvider.notifier).getContext();
    ContainerDto? containerDto = ref.read(formProvider.notifier).getContainer();
    if (containerDto != null) {
      var result = await getIt<ContainersRepository>().deleteSingleContainer(
        contextId: contextDto.contextId,
        containerId: containerDto.containerId,
      );
      String refreshId = const Uuid().v4();
      ref.read(refreshContextsProvider.notifier).state = refreshId;
      return result;
    } else {
      throw Exception("ContainerDto null");
    }
  },
);
