import 'package:c4_frontend_data_entry_flutter/ui/edit_raw_data/edit_container/application/container_provider.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:uuid/uuid.dart';

import '../../../../api_raw_data/infrastructure/api_c4_all_components/dependency_injection.dart';
import '../../../../api_raw_data/infrastructure/api_c4_containers/container_input_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_containers/containers_output_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_containers/containers_repository.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/container_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import '../../../tree_view/menu_context/application/menu_contexts_refresh_provider.dart';
import '../../widgets/form_submit_provider.dart';

final futureProviderUpdateContainer =
    FutureProvider.autoDispose<ContainersOutputDto>((ref) async {
  try {
    ref.watch(formSubmitProvider);
    ContainerInputDto? containerInputDto =
        ref.read(containerProvider.notifier).storedContainerInputDto;
    ContextDto contextDto =
        ref.read(containerProvider.notifier).getContextDto();
    ContainerDto containerDto =
        ref.read(containerProvider.notifier).getContainerDto();
    if (containerInputDto != null) {
      var result = await getIt<ContainersRepository>().updateSingleContainer(
        contextId: contextDto.contextId,
        containerId: containerDto.containerId,
        containerInputDto: containerInputDto,
      );
      String refreshId = const Uuid().v4();
      ref.read(refreshContextsProvider.notifier).state = refreshId;
      return result;
    } else {
      throw Exception('ContainerInputDto null');
    }
  } catch (exc) {
    rethrow;
  }
});
