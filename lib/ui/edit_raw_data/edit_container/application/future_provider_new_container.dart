import 'package:c4_frontend_data_entry_flutter/api_raw_data/infrastructure/api_c4_containers/containers_output_dto.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:uuid/uuid.dart';

import '../../../../api_raw_data/infrastructure/api_c4_all_components/dependency_injection.dart';
import '../../../../api_raw_data/infrastructure/api_c4_containers/container_input_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_containers/containers_repository.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import '../../../tree_view/menu_context/application/menu_contexts_refresh_provider.dart';
import '../../dialog/application/form_provider.dart';
import '../../widgets/form_submit_provider.dart';
import 'container_provider.dart';

final futureProviderNewContainer =
    FutureProvider.autoDispose<ContainersOutputDto>((ref) async {
  ref.watch(formSubmitProvider);
  ContextDto contextDto = ref.read(formProvider.notifier).getContext();
  ContainerInputDto? containerInputDto =
      ref.read(containerProvider.notifier).storedContainerInputDto;
  if (containerInputDto != null) {
    final result = await getIt<ContainersRepository>().postSingleContainer(
        contextId: contextDto.contextId, containerInputDto: containerInputDto);
    if (result.errorDto == null) {
      String refreshId = const Uuid().v4();
      ref.read(refreshContextsProvider.notifier).state = refreshId;
    }
    return result;
  } else {
    throw Exception('containerInputDto null');
  }
});
