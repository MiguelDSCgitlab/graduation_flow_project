import 'package:c4_frontend_data_entry_flutter/api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_containers/container_input_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/container_dto.dart';
import '../../dialog/application/form_provider.dart';

String getContainerProviderId(
    ContextDto contextDto, ContainerDto containerDto) {
  return "${contextDto.id}___${containerDto.id}";
}

final containerProvider =
    StateNotifierProvider<ContainerNotifier, ContainerHandlerState>((ref) {
  final notifier = ContainerNotifier(ref);
  final state = ref.watch(formProvider);

  if (state is NoFormState) {
    notifier.initialize();
  }
  return notifier;
});

class ContainerNotifier extends StateNotifier<ContainerHandlerState> {
  final Ref ref;
  ContextDto? _contextDto;
  ContainerDto? _containerDto;
  ContainerInputDto? storedContainerInputDto;

  ContainerNotifier(this.ref) : super(ContainerInitialState());

  setContextDto(ContextDto contextDto) {
    _contextDto = contextDto;
  }

  setContainerDto(ContainerDto containerDto) {
    _containerDto = containerDto;
  }

  setContainerInputDto(ContainerInputDto containerInputDto) {
    storedContainerInputDto = containerInputDto;
  }

  ContextDto getContextDto() {
    final ContextDto? contextDto = _contextDto;
    if (contextDto != null) {
      return contextDto;
    } else {
      throw Exception('ContextDto null');
    }
  }

  ContainerDto getContainerDto() {
    final ContainerDto? containerDto = _containerDto;
    if (containerDto != null) {
      return containerDto;
    } else {
      throw Exception('ContainerDto null');
    }
  }

  void newContainerForm() {
    state = NewContainerFormState();
  }

  void editContainerForm() {
    state = EditContainerFormState();
  }

  void deleteContainerForm() {
    state = DeleteContainerFormState();
  }

  void postContainer(ContainerInputDto containerInputDto) {
    storedContainerInputDto = containerInputDto;
    state = PostContainerState();
  }

  void putContainer(ContainerInputDto containerInputDto) {
    storedContainerInputDto = containerInputDto;
    state = PutContainerState();
  }

  void deleteContainer() {
    state = DeleteContainerState();
  }

  void initialize() {
    state = ContainerInitialState();
  }
}

abstract class ContainerHandlerState {}

class ContainerInitialState extends ContainerHandlerState {}

class ContainerLoadingState extends ContainerHandlerState {}

class NewContainerFormState extends ContainerHandlerState {}

class EditContainerFormState extends ContainerHandlerState {}

class DeleteContainerFormState extends ContainerHandlerState {}

class PostContainerState extends ContainerHandlerState {}

class DeleteContainerState extends ContainerHandlerState {}

class PutContainerState extends ContainerHandlerState {}
