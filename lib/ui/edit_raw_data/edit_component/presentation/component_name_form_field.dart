import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_components/component_input_dto.dart';

class ComponentNameFormField extends ConsumerWidget {
  final ComponentInputDto componentInputDto;

  const ComponentNameFormField({
    Key? key,
    required this.componentInputDto,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return TextFormField(
      decoration: const InputDecoration(
        labelText: "Component name",
        hintText: "Component name",
        prefixIcon: Icon(
          Icons.architecture,
          color: Colors.white,
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(16.0),
          ),
        ),
      ),
      maxLength: 40,
      initialValue: componentInputDto.name,
      validator: (value) {
        if (value != null) {
          if (value.isEmpty) {
            return 'Please enter a name';
          }
        }
        return null;
      },
      onSaved: (value) => componentInputDto.name = value!,
    );
  }
}
