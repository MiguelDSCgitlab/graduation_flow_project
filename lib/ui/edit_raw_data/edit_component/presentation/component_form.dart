import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_components/component_input_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_error/error_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/component_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/container_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import '../../technology/application/technology_provider.dart';
import '../../technology/presentation/technologies_list.dart';
import '../../technology/presentation/technology_button.dart';
import '../../widgets/error_form.dart';
import '../application/component_provider.dart';
import 'component_description_form_field.dart';
import 'component_location_form_field.dart';
import 'component_name_form_field.dart';
import 'component_title.dart';
import 'delete_component_button.dart';
import 'store_component_button.dart';

class ComponentForm extends ConsumerStatefulWidget {
  final ContextDto contextDto;
  final ContainerDto containerDto;
  final ComponentDto? componentDto;
  final ComponentInputDto componentInputDto;
  final ErrorDto? errorDto;

  const ComponentForm({
    super.key,
    required this.contextDto,
    required this.containerDto,
    this.componentDto,
    required this.componentInputDto,
    required this.errorDto,
  });

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _FormNewContextState();
}

class _FormNewContextState extends ConsumerState<ComponentForm> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    ref.read(technologyProvider.notifier).selectedTechnologies =
        widget.componentInputDto.technology ?? [];
    final state = ref.read(componentProvider);
    return Form(
      key: _formKey,
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            const Padding(
              padding: EdgeInsets.all(16.0),
            ),
            ErrorFormWidget(
              errorDto: widget.errorDto,
            ),
            ComponentTitleWidget(
              name: widget.componentDto?.name ?? 'New component',
            ),
            ComponentNameFormField(
              componentInputDto: widget.componentInputDto,
            ),
            ComponentDescriptionFormField(
              componentInputDto: widget.componentInputDto,
            ),
            const Padding(
              padding: EdgeInsets.all(8.0),
            ),
            const Text(
              "Location?",
            ),
            ComponentLocationFormField(
              componentInputDto: widget.componentInputDto,
            ),
            const Padding(
              padding: EdgeInsets.all(8.0),
            ),
            const Row(
              children: [
                Text('Technologies'),
                Padding(
                  padding: EdgeInsets.only(right: 8),
                ),
                Spacer(),
                TechnologyIconButton(
                  title: 'Supported technologies',
                )
              ],
            ),
            const TechnologiesWidget(),
            const SizedBox(
              height: 16,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                const SizedBox(
                  width: 16,
                ),
                (state is EditComponentFormState)
                    ? DeleteComponentButton(
                        componentInputDto: widget.componentInputDto)
                    : Container(),
                const SizedBox(
                  width: 16,
                ),
                StoreComponentButton(
                  contextDto: widget.contextDto,
                  containerDto: widget.containerDto,
                  componentDto: widget.componentDto,
                  componentInputDto: widget.componentInputDto,
                  formKey: _formKey,
                ),
              ],
            ),
            const SizedBox(
              height: 16,
            ),
          ],
        ),
      ),
    );
  }
}
