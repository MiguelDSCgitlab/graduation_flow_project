import 'package:c4_frontend_data_entry_flutter/ui/edit_raw_data/edit_component/presentation/component_form.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_components/component_input_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/component_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/container_dto.dart';
import '../../dialog/application/form_provider.dart';
import '../application/component_provider.dart';
import '../application/future_provider_delete_component.dart';

class ComponentApiDelete extends ConsumerStatefulWidget {
  const ComponentApiDelete({
    Key? key,
  }) : super(key: key);

  @override
  ConsumerState createState() => _ComponentApiDeleteState();
}

class _ComponentApiDeleteState extends ConsumerState<ComponentApiDelete> {
  @override
  Widget build(BuildContext context) {
    final apiResult = ref.watch(futureProviderDeleteComponent);
    ContainerDto? containerDto = ref.read(formProvider.notifier).getContainer();
    ComponentDto? componentDto = ref.read(formProvider.notifier).getComponent();
    ComponentInputDto? componentInputDto =
        ref.read(componentProvider.notifier).componentInputDto;
    if (containerDto == null ||
        componentDto == null ||
        componentInputDto == null) {
      return const SizedBox.shrink();
    }

    return apiResult.when(
      data: (data) {
        final errorDto = data.errorDto;

        if (errorDto != null) {
          final contextDto = ref.read(formProvider.notifier).getContext();
          return ComponentForm(
            contextDto: contextDto,
            containerDto: containerDto,
            componentDto: componentDto,
            errorDto: errorDto,
            componentInputDto: componentInputDto,
          );
        } else {
          WidgetsBinding.instance.addPostFrameCallback((_) {
            Navigator.pop(context);
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              backgroundColor: CupertinoColors.activeBlue,
              content: Text(
                "Component ${componentInputDto.name} deleted.",
                style: const TextStyle(color: CupertinoColors.white),
              ),
            ));
          });
          return const SizedBox.shrink();
        }
      },
      error: (_, __) => const Text('Something went wrong'),
      loading: () => const Text('Loading'),
    );
  }
}
