import 'package:c4_frontend_data_entry_flutter/api_raw_data/infrastructure/api_c4_components/component_input_dto.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_shared/data/component_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/container_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import '../../widgets/loading.dart';
import '../application/component_provider.dart';
import 'component_api_delete.dart';
import 'component_api_post.dart';
import 'component_api_put.dart';
import 'component_form.dart';

class ComponentProviderWidget extends ConsumerWidget {
  final ContextDto contextDto;
  final ContainerDto containerDto;
  final ComponentDto? componentDto;

  const ComponentProviderWidget({
    Key? key,
    required this.contextDto,
    required this.containerDto,
    this.componentDto,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final state = ref.watch(componentProvider);

    if (state is ComponentInitialState) {
      return const LoadingWidget();
    }

    if (state is NewComponentFormState) {
      ComponentInputDto componentInputDto = ComponentInputDto(
        name: '',
      );
      return ComponentForm(
        contextDto: contextDto,
        containerDto: containerDto,
        componentInputDto: componentInputDto,
        errorDto: null,
      );
    }

    if (state is EditComponentFormState) {
      ComponentDto? tComponentDto = componentDto;
      if (tComponentDto != null) {
        ComponentInputDto componentInputDto = ComponentInputDto(
          name: tComponentDto.name,
          description: tComponentDto.description,
          location: tComponentDto.location,
          technology: tComponentDto.technology,
        );
        return ComponentForm(
          contextDto: contextDto,
          containerDto: containerDto,
          componentDto: tComponentDto,
          componentInputDto: componentInputDto,
          errorDto: null,
        );
      } else {
        // todo add error widget
        return const Text('error occurred');
      }
    }

    if (state is PostComponentState) {
      return const ComponentApiPost();
    }

    if (state is PutComponentState) {
      return const ComponentApiPut();
    }

    if (state is DeleteComponentState) {
      return const ComponentApiDelete();
    }

    return Container();
  }
}
