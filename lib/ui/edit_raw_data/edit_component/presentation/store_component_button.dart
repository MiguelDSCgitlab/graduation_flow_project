import 'package:c4_frontend_data_entry_flutter/api_raw_data/infrastructure/api_c4_shared/data/component_dto.dart';
import 'package:c4_frontend_data_entry_flutter/api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_components/component_input_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/container_dto.dart';
import '../../technology/application/technology_provider.dart';
import '../../widgets/form_submit_provider.dart';
import '../application/component_provider.dart';

class StoreComponentButton extends ConsumerWidget {
  final GlobalKey<FormState> formKey;
  final ContextDto contextDto;
  final ContainerDto containerDto;
  final ComponentDto? componentDto;
  final ComponentInputDto componentInputDto;

  const StoreComponentButton({
    Key? key,
    required this.formKey,
    required this.contextDto,
    required this.containerDto,
    this.componentDto,
    required this.componentInputDto,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final formState = ref.read(componentProvider);
    return ElevatedButton.icon(
      icon: const Icon(Icons.save),
      onPressed: () {
        if (formKey.currentState!.validate()) {
          formKey.currentState!.save();
          ref.read(formSubmitProvider.notifier).submit();
          if (formState is NewComponentFormState) {
            componentInputDto.technology =
                ref.read(technologyProvider.notifier).selectedTechnologies;
            ref
                .read(componentProvider.notifier)
                .postComponent(componentInputDto);
          }

          if (formState is EditComponentFormState) {
            ref
                .read(componentProvider.notifier)
                .putComponent(componentInputDto);
          }
        }
      },
      label: const Padding(
        padding: EdgeInsets.all(8.0),
        child: Text('Store'),
      ),
    );
  }
}
