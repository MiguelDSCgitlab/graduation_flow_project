import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_components/component_input_dto.dart';

class ComponentLocationFormField extends ConsumerStatefulWidget {
  final ComponentInputDto componentInputDto;

  const ComponentLocationFormField({
    Key? key,
    required this.componentInputDto,
  }) : super(key: key);

  @override
  ConsumerState createState() => _ComponentLocationFormFieldState();
}

class _ComponentLocationFormFieldState
    extends ConsumerState<ComponentLocationFormField> {
  late Set<String> selection;

  @override
  void initState() {
    if (widget.componentInputDto.location == 'External') {
      selection = <String>{"External"};
    } else {
      selection = <String>{"Internal"};
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SegmentedButton<String>(
      segments: const <ButtonSegment<String>>[
        ButtonSegment<String>(value: "Internal", label: Text('Internal')),
        ButtonSegment<String>(value: "External", label: Text('External')),
      ],
      selected: selection,
      onSelectionChanged: (Set<String> newSelection) {
        setState(() {
          selection = newSelection;
          if (newSelection.first.startsWith('External')) {
            widget.componentInputDto.location = "External";
          } else {
            widget.componentInputDto.location = "Internal";
          }
        });
      },
      multiSelectionEnabled: false,
    );
  }
}
