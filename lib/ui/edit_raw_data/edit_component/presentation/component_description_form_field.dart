import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_components/component_input_dto.dart';

class ComponentDescriptionFormField extends ConsumerWidget {
  final ComponentInputDto componentInputDto;

  const ComponentDescriptionFormField({
    Key? key,
    required this.componentInputDto,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return TextFormField(
      decoration: const InputDecoration(
        labelText: "Description of the component",
        hintText: "Description of the component",
        prefixIcon: Icon(
          Icons.description,
          color: Colors.white,
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(16.0),
          ),
        ),
      ),
      maxLength: 200,
      initialValue: componentInputDto.description,
      validator: (value) {
        if (value != null) {
          if (value.isEmpty) {
            return 'Please enter a description';
          }
        }
        return null;
      },
      onSaved: (value) => componentInputDto.description = value!,
    );
  }
}
