import 'package:c4_frontend_data_entry_flutter/api_raw_data/infrastructure/api_c4_components/component_input_dto.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:uuid/uuid.dart';

import '../../../tree_view/menu_context/application/menu_contexts_refresh_provider.dart';
import '../../dialog/application/form_provider.dart';
import '../application/component_provider.dart';
import '../application/future_provider_new_component.dart';
import 'component_form.dart';

class ComponentApiPost extends ConsumerStatefulWidget {
  const ComponentApiPost({
    Key? key,
  }) : super(key: key);

  @override
  ConsumerState createState() => _ComponentApiPostState();
}

class _ComponentApiPostState extends ConsumerState<ComponentApiPost> {
  @override
  Widget build(BuildContext context) {
    final apiResult = ref.watch(futureProviderNewComponent);
    ComponentInputDto? componentInputDto =
        ref.read(componentProvider.notifier).componentInputDto;
    if (componentInputDto == null) {
      return const SizedBox.shrink();
    }

    return apiResult.when(
      data: (data) {
        final errorDto = data.errorDto;
        if (errorDto != null) {
          final contextDto = ref.read(formProvider.notifier).getContext();
          final containerDto = ref.read(formProvider.notifier).getContainer();
          if (containerDto == null) {
            return const SizedBox.shrink();
          }
          return ComponentForm(
              contextDto: contextDto,
              containerDto: containerDto,
              componentInputDto: componentInputDto,
              errorDto: errorDto);
        } else {
          WidgetsBinding.instance.addPostFrameCallback((_) {
            Navigator.pop(context);
            String refreshId = const Uuid().v4();
            ref.read(refreshContextsProvider.notifier).state = refreshId;
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              backgroundColor: CupertinoColors.activeBlue,
              content: Text(
                "Component ${componentInputDto.name} created.",
                style: const TextStyle(color: CupertinoColors.white),
              ),
            ));
          });
          return const SizedBox.shrink();
        }
      },
      error: (_, __) => const Text('Something went wrong'),
      loading: () => const Text('Storing new component'),
    );
  }
}
