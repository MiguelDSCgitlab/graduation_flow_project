import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_components/component_input_dto.dart';
import '../../widgets/form_submit_provider.dart';
import '../application/component_provider.dart';

class DeleteComponentButton extends ConsumerWidget {
  final ComponentInputDto componentInputDto;

  const DeleteComponentButton({
    Key? key,
    required this.componentInputDto,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return ElevatedButton.icon(
      icon: const Icon(Icons.delete),
      onPressed: () {
        ref.read(formSubmitProvider.notifier).submit();
        ref.read(componentProvider.notifier).deleteComponent(componentInputDto);
      },
      label: const Padding(
        padding: EdgeInsets.all(8.0),
        child: Text('Delete'),
      ),
    );
  }
}
