import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:uuid/uuid.dart';

import '../../../../api_raw_data/infrastructure/api_c4_all_components/dependency_injection.dart';
import '../../../../api_raw_data/infrastructure/api_c4_components/component_output_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_components/components_repository.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/component_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/container_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import '../../../tree_view/menu_context/application/menu_contexts_refresh_provider.dart';
import '../../dialog/application/form_provider.dart';
import '../../widgets/form_submit_provider.dart';

final futureProviderDeleteComponent =
    FutureProvider.autoDispose<ComponentsOutputDto>(
  (ref) async {
    ref.watch(formSubmitProvider);
    ContextDto contextDto = ref.read(formProvider.notifier).getContext();
    ContainerDto? containerDto = ref.read(formProvider.notifier).getContainer();
    ComponentDto? componentDto = ref.read(formProvider.notifier).getComponent();
    if (containerDto != null && componentDto != null) {
      var result = await getIt<ComponentsRepository>().deleteSingleComponent(
        contextId: contextDto.contextId,
        containerId: containerDto.containerId,
        componentId: componentDto.componentId,
      );
      String refreshId = const Uuid().v4();
      ref.read(refreshContextsProvider.notifier).state = refreshId;
      return result;
    } else {
      throw Exception('null input received');
    }
  },
);
