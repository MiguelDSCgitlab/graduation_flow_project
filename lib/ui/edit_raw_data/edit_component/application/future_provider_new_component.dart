import 'package:c4_frontend_data_entry_flutter/ui/edit_raw_data/dialog/application/form_provider.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_all_components/dependency_injection.dart';
import '../../../../api_raw_data/infrastructure/api_c4_components/component_input_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_components/component_output_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_components/components_repository.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/container_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import '../../widgets/form_submit_provider.dart';
import 'component_provider.dart';

final futureProviderNewComponent =
    FutureProvider.autoDispose<ComponentsOutputDto>(
  (ref) async {
    ref.watch(formSubmitProvider);
    ContextDto contextDto = ref.read(formProvider.notifier).getContext();
    ContainerDto? containerDto = ref.read(formProvider.notifier).getContainer();
    ComponentInputDto? componentInputDto =
        ref.read(componentProvider.notifier).componentInputDto;
    if (containerDto != null && componentInputDto != null) {
      return await getIt<ComponentsRepository>().postSingleComponent(
        contextId: contextDto.contextId,
        containerId: containerDto.containerId,
        componentInputDto: componentInputDto,
      );
    } else {
      throw Exception('null values passed');
    }
  },
);
