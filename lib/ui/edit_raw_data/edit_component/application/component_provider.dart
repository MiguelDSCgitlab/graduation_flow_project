import 'package:c4_frontend_data_entry_flutter/api_raw_data/infrastructure/api_c4_shared/data/component_dto.dart';
import 'package:c4_frontend_data_entry_flutter/api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_components/component_input_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/container_dto.dart';
import '../../dialog/application/form_provider.dart';

final componentProvider =
    StateNotifierProvider<ComponentNotifier, ComponentHandlerState>((ref) {
  final notifier = ComponentNotifier(ref);
  final state = ref.watch(formProvider);

  if (state is NoFormState) {
    notifier.initialize();
  }
  return notifier;
});

class ComponentNotifier extends StateNotifier<ComponentHandlerState> {
  final Ref ref;
  ContextDto? _contextDto;
  ContainerDto? _containerDto;
  ComponentDto? _componentDto;
  ComponentInputDto? componentInputDto;

  ComponentNotifier(this.ref) : super(ComponentInitialState());

  void newComponentForm() {
    state = NewComponentFormState();
  }

  void editComponentForm() {
    state = EditComponentFormState();
  }

  void deleteComponentForm() {
    state = DeleteComponentFormState();
  }

  void postComponent(ComponentInputDto aComponentInputDto) {
    componentInputDto = aComponentInputDto;
    state = PostComponentState();
  }

  void putComponent(ComponentInputDto aComponentInputDto) {
    componentInputDto = aComponentInputDto;
    state = PutComponentState();
  }

  void deleteComponent(ComponentInputDto aComponentInputDto) {
    componentInputDto = aComponentInputDto;
    state = DeleteComponentState();
  }

  void initialize() {
    state = ComponentInitialState();
  }

  setContextDto(ContextDto contextDto) {
    _contextDto = contextDto;
  }

  setContainerDto(ContainerDto containerDto) {
    _containerDto = containerDto;
  }

  void setComponentDto(ComponentDto componentDto) {
    _componentDto = componentDto;
  }

  ContextDto getContextDto() {
    final ContextDto? contextDto = _contextDto;
    if (contextDto != null) {
      return contextDto;
    } else {
      throw Exception('ContextDto null');
    }
  }

  ContainerDto getContainerDto() {
    final ContainerDto? containerDto = _containerDto;
    if (containerDto != null) {
      return containerDto;
    } else {
      throw Exception('ContainerDto null');
    }
  }

  ComponentDto getComponentDto() {
    final ComponentDto? componentDto = _componentDto;
    if (componentDto != null) {
      return componentDto;
    } else {
      throw Exception('ComponentDto null');
    }
  }
}

abstract class ComponentHandlerState {}

class ComponentInitialState extends ComponentHandlerState {}

class ComponentLoadingState extends ComponentHandlerState {}

class NewComponentFormState extends ComponentHandlerState {}

class EditComponentFormState extends ComponentHandlerState {}

class DeleteComponentFormState extends ComponentHandlerState {}

class PostComponentState extends ComponentHandlerState {}

class DeleteComponentState extends ComponentHandlerState {}

class PutComponentState extends ComponentHandlerState {}
