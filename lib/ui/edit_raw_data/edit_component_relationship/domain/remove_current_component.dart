import 'package:c4_frontend_data_entry_flutter/api_raw_data/infrastructure/api_c4_all_components/component_dto.dart';

/// Use case responsible for filtering components. It should remove all components matching the filter.
class RemoveCurrentComponentUseCase {
  List<MinComponentDto> call(List<MinComponentDto> components, String filter) {
    return components.where((component) => component.name != filter).toList();
  }
}
