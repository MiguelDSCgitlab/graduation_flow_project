import 'package:c4_frontend_data_entry_flutter/api_raw_data/infrastructure/api_c4_all_components/all_components_repository.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_all_components/components_output_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_all_components/dependency_injection.dart';

final futureProviderGetComponents =
    FutureProvider.autoDispose<ComponentsOutputDto>((ref) async {
  return await getIt<AllComponentsRepository>().getComponents();
});
