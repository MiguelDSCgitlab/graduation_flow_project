import 'package:c4_frontend_data_entry_flutter/api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import 'package:c4_frontend_data_entry_flutter/api_raw_data/infrastructure/api_c4_shared/data/relationship_dto.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_relationships/data/relationship_input_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/component_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/container_dto.dart';

final componentRelationshipProvider = StateNotifierProvider<
    ComponentRelationshipNotifier, ComponentRelationshipHandlerState>((ref) {
  final notifier = ComponentRelationshipNotifier(ref);
  return notifier;
});

class ComponentRelationshipNotifier
    extends StateNotifier<ComponentRelationshipHandlerState> {
  final Ref ref;
  late ContextDto contextDto;
  late ContainerDto containerDto;
  late ComponentDto componentDto;
  late RelationshipDto relationshipDto;
  late RelationshipInputDto storedRelationshipInputDto;

  ComponentRelationshipNotifier(this.ref)
      : super(ComponentRelationshipLoadingState());

  void newComponentRelationshipForm() {
    state = NewComponentRelationshipFormState();
  }

  void editComponentRelationshipForm({
    required ContextDto aContextDto,
    required ContainerDto aContainerDto,
    required ComponentDto aComponentDto,
    required RelationshipDto aRelationshipDto,
  }) {
    contextDto = aContextDto;
    containerDto = aContainerDto;
    componentDto = aComponentDto;
    relationshipDto = aRelationshipDto;
    state = EditComponentRelationshipFormState();
  }

  void deleteComponentRelationship(
      {required RelationshipInputDto relationshipInputDto}) {
    storedRelationshipInputDto = relationshipInputDto;
    state = DeleteComponentRelationshipState();
  }

  void putComponentRelationship(
      {required RelationshipInputDto relationshipInputDto}) {
    storedRelationshipInputDto = relationshipInputDto;
    state = PutComponentRelationshipState();
  }

  postComponentRelationship(
      {required RelationshipInputDto relationshipInputDto}) {
    storedRelationshipInputDto = relationshipInputDto;
    state = PostComponentRelationshipState();
  }
}

abstract class ComponentRelationshipHandlerState {}

class ComponentRelationshipLoadingState
    extends ComponentRelationshipHandlerState {}

class NewComponentRelationshipFormState
    extends ComponentRelationshipHandlerState {}

class EditComponentRelationshipFormState
    extends ComponentRelationshipHandlerState {}

class DeleteComponentRelationshipState
    extends ComponentRelationshipHandlerState {}

class PostComponentRelationshipState
    extends ComponentRelationshipHandlerState {}

class PutComponentRelationshipState extends ComponentRelationshipHandlerState {}
