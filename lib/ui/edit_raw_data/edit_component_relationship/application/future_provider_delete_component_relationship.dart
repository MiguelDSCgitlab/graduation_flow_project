import 'package:c4_frontend_data_entry_flutter/api_raw_data/infrastructure/api_c4_relationships/data/relationship_input_dto.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:uuid/uuid.dart';

import '../../../../api_raw_data/infrastructure/api_c4_all_components/dependency_injection.dart';
import '../../../../api_raw_data/infrastructure/api_c4_relationships/data/relationship_output_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_relationships/infrastructure/relationship_repository.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/component_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/container_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/relationship_dto.dart';
import '../../../tree_view/menu_context/application/menu_contexts_refresh_provider.dart';
import '../../dialog/application/form_provider.dart';
import '../../widgets/form_submit_provider.dart';
import 'component_relationship_provider.dart';

final futureProviderDeleteComponentRelationship =
    FutureProvider.autoDispose<RelationshipOutputDto>((ref) async {
  ref.watch(formSubmitProvider);
  ContextDto contextDto = ref.read(formProvider.notifier).getContext();
  ContainerDto? containerDto = ref.read(formProvider.notifier).getContainer();
  ComponentDto? componentDto = ref.read(formProvider.notifier).getComponent();
  RelationshipDto? relationshipDto =
      ref.read(formProvider.notifier).getRelationship();
  RelationshipInputDto relationshipInputDto = ref
      .read(componentRelationshipProvider.notifier)
      .storedRelationshipInputDto;

  if (containerDto != null && componentDto != null && relationshipDto != null) {
    final result =
        await getIt<RelationshipRepository>().deleteComponentRelationship(
      contextDto.contextId,
      containerDto.id,
      componentDto.id,
      relationshipDto.relationshipId,
      relationshipInputDto,
    );
    String refreshId = const Uuid().v4();
    ref.read(refreshContextsProvider.notifier).state = refreshId;
    return result;
  } else {
    throw Exception('input null');
  }
});
