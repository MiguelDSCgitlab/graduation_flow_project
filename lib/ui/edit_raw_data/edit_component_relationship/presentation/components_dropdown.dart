import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_all_components/component_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_relationships/data/relationship_input_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/component_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/container_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';

class ComponentsDropDown extends ConsumerStatefulWidget {
  final List<MinComponentDto> components;
  final ContextDto contextDto;
  final ContainerDto containerDto;
  final ComponentDto componentDto;
  final RelationshipInputDto relationshipInputDto;

  const ComponentsDropDown({
    super.key,
    required this.components,
    required this.contextDto,
    required this.containerDto,
    required this.componentDto,
    required this.relationshipInputDto,
  });

  @override
  ConsumerState createState() => _ComponentsDropDownState();
}

class _ComponentsDropDownState extends ConsumerState<ComponentsDropDown> {
  MinComponentDto? _selectedComponent;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text("Connect component (${widget.componentDto.name}) to:"),
        const Padding(
          padding: EdgeInsets.only(left: 16),
        ),
        Expanded(
          child: DropdownButtonHideUnderline(
            child: DropdownButton(
              itemHeight: 80,
              hint: Text(
                "Select component",
                style: TextStyle(
                  color: Theme.of(context).colorScheme.onSurface,
                ),
              ),
              value: _selectedComponent,
              items: widget.components.map((MinComponentDto minComponentDto) {
                return DropdownMenuItem(
                  key: Key(minComponentDto.containerId),
                  value: minComponentDto,
                  child: ListTile(
                    title: Text(minComponentDto.componentName),
                    subtitle: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Context: ${minComponentDto.contextName}",
                          style: Theme.of(context).textTheme.bodySmall,
                        ),
                        Text(
                          "Container: ${minComponentDto.containerName}",
                          style: Theme.of(context).textTheme.bodySmall,
                        ),
                      ],
                    ),
                  ),
                );
              }).toList(),
              onChanged: (minComponentDto) {
                setState(() {
                  if (minComponentDto != null) {
                    _selectedComponent = minComponentDto;
                    widget.relationshipInputDto.toContext =
                        minComponentDto.contextId;
                    widget.relationshipInputDto.toContainer =
                        minComponentDto.containerId;
                    widget.relationshipInputDto.toComponent =
                        minComponentDto.componentId;
                    widget.relationshipInputDto.toName = minComponentDto.name;
                  }
                });
              },
              isExpanded: true,
            ),
          ),
        ),
      ],
    );
  }
}
