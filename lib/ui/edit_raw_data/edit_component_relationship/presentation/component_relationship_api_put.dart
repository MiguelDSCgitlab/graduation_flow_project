import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_relationships/data/relationship_input_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/component_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/container_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/relationship_dto.dart';
import '../../dialog/application/form_provider.dart';
import '../application/component_relationship_provider.dart';
import '../application/future_provider_edit_component_relationship.dart';
import 'component_relationship_form.dart';

class ComponentRelationshipApiPut extends ConsumerStatefulWidget {
  final ContextDto contextDto;
  final ContainerDto containerDto;
  final ComponentDto componentDto;

  const ComponentRelationshipApiPut({
    Key? key,
    required this.contextDto,
    required this.containerDto,
    required this.componentDto,
  }) : super(key: key);

  @override
  ConsumerState createState() => _ComponentRelationshipApiPutState();
}

class _ComponentRelationshipApiPutState
    extends ConsumerState<ComponentRelationshipApiPut> {
  @override
  Widget build(BuildContext context) {
    final apiResult = ref.watch(futureProviderEditComponentRelationship);
    return apiResult.when(
      data: (data) {
        final errorDto = data.errorDto;
        if (errorDto != null) {
          RelationshipDto? relationshipDto =
              ref.read(formProvider.notifier).getRelationship();
          RelationshipInputDto relationshipInputDto = ref
              .read(componentRelationshipProvider.notifier)
              .storedRelationshipInputDto;

          return ComponentRelationshipForm(
            // contexts: contexts,
            contextDto: widget.contextDto,
            containerDto: widget.containerDto,
            componentDto: widget.componentDto,
            errorDto: errorDto,
            relationshipDto: relationshipDto,
            relationshipInputDto: relationshipInputDto,
          );
        } else {
          WidgetsBinding.instance.addPostFrameCallback((_) {
            Navigator.pop(context);
            ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
              backgroundColor: CupertinoColors.activeBlue,
              content: Text(
                "Component relationship updated.",
                style: TextStyle(color: CupertinoColors.white),
              ),
            ));
          });
          return const SizedBox.shrink();
        }
      },
      error: (_, __) => const Text('Something went wrong'),
      loading: () => const Text('Loading'),
    );
  }
}
