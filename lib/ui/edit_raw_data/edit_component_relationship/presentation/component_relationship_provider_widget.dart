import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_relationships/data/relationship_input_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/component_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/container_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/relationship_dto.dart';
import '../../widgets/loading.dart';
import '../application/component_relationship_provider.dart';
import 'component_relationship_api_delete.dart';
import 'component_relationship_api_post.dart';
import 'component_relationship_api_put.dart';
import 'component_relationship_form.dart';
import 'components_relationships_api_get.dart';

class ComponentRelationshipProviderWidget extends ConsumerWidget {
  final ContextDto contextDto;
  final ContainerDto containerDto;
  final ComponentDto componentDto;
  final RelationshipDto? relationshipDto;

  const ComponentRelationshipProviderWidget({
    Key? key,
    required this.contextDto,
    required this.containerDto,
    required this.componentDto,
    this.relationshipDto,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final state = ref.watch(componentRelationshipProvider);

    if (state is ComponentRelationshipLoadingState) {
      return const LoadingWidget();
    }

    if (state is NewComponentRelationshipFormState) {
      RelationshipInputDto relationshipInputDto = RelationshipInputDto(
        fromContext: contextDto.contextId,
        fromContainer: containerDto.containerId,
        fromComponent: componentDto.componentId,
        toContext: "",
        toContainer: "",
        toComponent: "",
        fromName: containerDto.name,
        toName: "",
        type: "Component",
      )..outgoing = true;

      return ComponentsRelationshipsApiGet(
        contextDto: contextDto,
        containerDto: containerDto,
        componentDto: componentDto,
        relationshipInputDto: relationshipInputDto,
      );
    }

    if (state is EditComponentRelationshipFormState) {
      final tRelationshipDto = relationshipDto;
      if (tRelationshipDto != null) {
        RelationshipInputDto relationshipInputDto = RelationshipInputDto(
            fromContext: tRelationshipDto.fromContext,
            fromContainer: tRelationshipDto.fromContainer,
            fromComponent: tRelationshipDto.fromComponent,
            toContext: tRelationshipDto.toContext,
            toContainer: tRelationshipDto.toContainer,
            toComponent: tRelationshipDto.toComponent,
            fromName: tRelationshipDto.fromName ?? '',
            toName: tRelationshipDto.toName ?? '',
            type: tRelationshipDto.type ?? 'Component',
            description: tRelationshipDto.description)
          ..outgoing = tRelationshipDto.outgoing ?? true;

        return ComponentRelationshipForm(
          contextDto: contextDto,
          containerDto: containerDto,
          componentDto: componentDto,
          relationshipDto: relationshipDto,
          relationshipInputDto: relationshipInputDto,
        );
      } else {
        return const Text('Error occurred');
      }
    }

    if (state is PostComponentRelationshipState) {
      return ComponentRelationshipApiPost(
        contextDto: contextDto,
        containerDto: containerDto,
        componentDto: componentDto,
      );
    }

    if (state is PutComponentRelationshipState) {
      return ComponentRelationshipApiPut(
        contextDto: contextDto,
        containerDto: containerDto,
        componentDto: componentDto,
      );
    }

    if (state is DeleteComponentRelationshipState) {
      return ComponentRelationshipApiDelete(
        contextDto: contextDto,
        containerDto: containerDto,
        componentDto: componentDto,
      );
    }

    return Container();
  }
}
