import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_all_components/component_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_error/error_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_relationships/data/relationship_input_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/component_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/container_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/relationship_dto.dart';
import 'component_relationship_form_incoming.dart';
import 'component_relationship_form_outgoing.dart';

class ComponentRelationshipForm extends ConsumerStatefulWidget {
  final List<MinComponentDto>? components;
  final ContextDto contextDto;
  final ContainerDto containerDto;
  final ComponentDto componentDto;
  final RelationshipDto? relationshipDto;
  final RelationshipInputDto relationshipInputDto;
  final ErrorDto? errorDto;

  const ComponentRelationshipForm({
    Key? key,
    this.components,
    required this.contextDto,
    required this.containerDto,
    required this.componentDto,
    required this.relationshipInputDto,
    this.relationshipDto,
    this.errorDto,
  }) : super(key: key);

  @override
  ConsumerState createState() => _ComponentRelationshipFormState();
}

class _ComponentRelationshipFormState
    extends ConsumerState<ComponentRelationshipForm> {
  @override
  Widget build(BuildContext context) {
    bool outgoing = widget.relationshipInputDto.outgoing;

    return outgoing
        ? ComponentRelationshipFormOutgoing(
            components: widget.components,
            contextDto: widget.contextDto,
            containerDto: widget.containerDto,
            componentDto: widget.componentDto,
            relationshipDto: widget.relationshipDto,
            relationshipInputDto: widget.relationshipInputDto,
            errorDto: widget.errorDto,
          )
        : ComponentRelationshipFormIncoming(
            relationshipInputDto: widget.relationshipInputDto,
          );
  }
}
