import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_relationships/data/relationship_input_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/component_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/container_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import '../application/component_relationship_provider.dart';
import '../application/future_provider_new_component_relationship.dart';
import 'components_relationships_api_get.dart';

class ComponentRelationshipApiPost extends ConsumerStatefulWidget {
  final ContextDto contextDto;
  final ContainerDto containerDto;
  final ComponentDto componentDto;

  const ComponentRelationshipApiPost({
    Key? key,
    required this.contextDto,
    required this.containerDto,
    required this.componentDto,
  }) : super(key: key);

  @override
  ConsumerState createState() => _ComponentRelationshipApiPostState();
}

class _ComponentRelationshipApiPostState
    extends ConsumerState<ComponentRelationshipApiPost> {
  @override
  Widget build(BuildContext context) {
    final apiResult = ref.watch(futureProviderNewComponentRelationship);
    return apiResult.when(
      data: (data) {
        final errorDto = data.errorDto;
        if (errorDto != null) {
          RelationshipInputDto relationshipInputDto = ref
              .read(componentRelationshipProvider.notifier)
              .storedRelationshipInputDto;

          return ComponentsRelationshipsApiGet(
            contextDto: widget.contextDto,
            containerDto: widget.containerDto,
            componentDto: widget.componentDto,
            errorDto: errorDto,
            relationshipInputDto: relationshipInputDto,
          );
        } else {
          WidgetsBinding.instance.addPostFrameCallback((_) {
            Navigator.pop(context);
            ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
              backgroundColor: CupertinoColors.activeBlue,
              content: Text(
                "Component relationship created.",
                style: TextStyle(color: CupertinoColors.white),
              ),
            ));
          });
          return const SizedBox.shrink();
        }
      },
      error: (_, __) => const Text('Something went wrong'),
      loading: () => const Text('Loading'),
    );
  }
}
