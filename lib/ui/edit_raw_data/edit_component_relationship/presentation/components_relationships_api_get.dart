import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_all_components/component_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_error/error_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_relationships/data/relationship_input_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/component_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/container_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import '../application/future_provider_get_components.dart';
import '../domain/remove_current_component.dart';
import 'component_relationship_form.dart';

class ComponentsRelationshipsApiGet extends ConsumerWidget {
  final ContextDto contextDto;
  final ContainerDto containerDto;
  final ComponentDto componentDto;
  final RelationshipInputDto relationshipInputDto;
  final ErrorDto? errorDto;

  const ComponentsRelationshipsApiGet({
    Key? key,
    required this.containerDto,
    required this.relationshipInputDto,
    required this.contextDto,
    required this.componentDto,
    this.errorDto,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final containersList = ref.watch(futureProviderGetComponents);
    return containersList.when(
      data: (data) {
        List<MinComponentDto> components = data.components ?? [];
        List<MinComponentDto> filteredComponents =
            RemoveCurrentComponentUseCase().call(components, componentDto.name);
        if (components.length < 2) {
          return const Text(
              "Unable to create components relationship. No other components exist.");
        } else {
          return ComponentRelationshipForm(
            components: filteredComponents,
            contextDto: contextDto,
            containerDto: containerDto,
            componentDto: componentDto,
            relationshipInputDto: relationshipInputDto,
            errorDto: errorDto,
          );
        }
      },
      error: (_, __) {
        return const Text('Something went wrong');
      },
      loading: () {
        return const Text('Loading');
      },
    );
  }
}
