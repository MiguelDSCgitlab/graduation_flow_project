import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_all_components/component_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_error/error_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_relationships/data/relationship_input_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/component_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/container_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/relationship_dto.dart';
import '../../widgets/error_form.dart';
import '../../widgets/form_submit_provider.dart';
import '../application/component_relationship_provider.dart';
import 'components_dropdown.dart';

class ComponentRelationshipFormOutgoing extends ConsumerStatefulWidget {
  final List<MinComponentDto>? components;
  final ContextDto contextDto;
  final ContainerDto containerDto;
  final ComponentDto componentDto;
  final RelationshipDto? relationshipDto;
  final RelationshipInputDto relationshipInputDto;
  final ErrorDto? errorDto;

  const ComponentRelationshipFormOutgoing({
    Key? key,
    this.components,
    required this.contextDto,
    required this.containerDto,
    required this.componentDto,
    required this.relationshipInputDto,
    this.relationshipDto,
    this.errorDto,
  }) : super(key: key);

  @override
  ConsumerState createState() => _ComponentRelationshipFormOutgoingState();
}

class _ComponentRelationshipFormOutgoingState
    extends ConsumerState<ComponentRelationshipFormOutgoing> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final state = ref.watch(componentRelationshipProvider);

    String titleText = "";
    SizedBox sizedBox;
    Widget dropDown;
    bool showDeleteButton = false;
    bool isEditAction = false;
    String description = "";
    if (state is EditComponentRelationshipFormState ||
        state is PutComponentRelationshipState ||
        state is DeleteComponentRelationshipState) {
      titleText =
          'Edit component relationship between ${widget.relationshipDto?.fromName} and ${widget.relationshipDto?.toName}';
      sizedBox = const SizedBox.shrink();
      dropDown = Container();
      showDeleteButton = true;
      isEditAction = true;
      description = widget.relationshipInputDto.description ?? "";
    } else {
      titleText = "New component relationship";
      sizedBox = const SizedBox(
        height: 16,
      );
      dropDown = ComponentsDropDown(
        contextDto: widget.contextDto,
        containerDto: widget.containerDto,
        componentDto: widget.componentDto,
        components: widget.components ?? [],
        relationshipInputDto: widget.relationshipInputDto,
      );
      if (state is PostComponentRelationshipState) {
        description = widget.relationshipInputDto.description ?? "";
      }
    }

    return SingleChildScrollView(
      child: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            ErrorFormWidget(
              errorDto: widget.errorDto,
            ),
            Text(titleText),
            sizedBox,
            dropDown,
            const SizedBox(
              height: 16,
            ),
            TextFormField(
              maxLength: 40,
              initialValue: description,
              validator: (value) {
                if (value != null) {
                  if (value.isEmpty) {
                    return 'Please enter a description';
                  } else if (value.length < 3) {
                    return 'Minimum length is 3.';
                  }
                }
                return null;
              },
              onSaved: (value) =>
                  widget.relationshipInputDto.description = value!,
              decoration: const InputDecoration(labelText: 'Description'),
            ),
            const SizedBox(
              height: 32,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                showDeleteButton
                    ? ElevatedButton(
                        onPressed: () {
                          ref.read(formSubmitProvider.notifier).submit();
                          ref
                              .read(componentRelationshipProvider.notifier)
                              .deleteComponentRelationship(
                                relationshipInputDto:
                                    widget.relationshipInputDto,
                              );
                        },
                        child: const Padding(
                          padding: EdgeInsets.all(16.0),
                          child: Text('Delete'),
                        ),
                      )
                    : Container(),
                const Padding(
                  padding: EdgeInsets.all(16.0),
                ),
                ElevatedButton(
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      _formKey.currentState!.save();
                      ref.read(formSubmitProvider.notifier).submit();
                      isEditAction
                          ? ref
                              .read(componentRelationshipProvider.notifier)
                              .putComponentRelationship(
                                relationshipInputDto:
                                    widget.relationshipInputDto,
                              )
                          : ref
                              .read(componentRelationshipProvider.notifier)
                              .postComponentRelationship(
                                relationshipInputDto:
                                    widget.relationshipInputDto,
                              );
                    }
                  },
                  child: const Padding(
                    padding: EdgeInsets.all(16.0),
                    child: Text('Store'),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
