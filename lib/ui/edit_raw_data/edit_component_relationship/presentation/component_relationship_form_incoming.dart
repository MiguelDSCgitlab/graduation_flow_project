import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_relationships/data/relationship_input_dto.dart';

class ComponentRelationshipFormIncoming extends ConsumerStatefulWidget {
  final RelationshipInputDto relationshipInputDto;

  const ComponentRelationshipFormIncoming({
    Key? key,
    required this.relationshipInputDto,
  }) : super(key: key);

  @override
  ConsumerState createState() => _ComponentRelationshipFormIncomingState();
}

class _ComponentRelationshipFormIncomingState
    extends ConsumerState<ComponentRelationshipFormIncoming> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 12),
      child: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Padding(
              padding: EdgeInsets.only(bottom: 20.0),
              child: Text('Incoming component relationship'),
            ),
            const SizedBox(height: 32),
            Text("From component: ${widget.relationshipInputDto.fromName}"),
            Text("To component: ${widget.relationshipInputDto.toName}"),
            Text("Description: ${widget.relationshipInputDto.description}"),
          ],
        ),
      ),
    );
  }
}
