import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';

/// Use case responsible for filtering contexts. It should remove all contexts matching the filter.
class RemoveCurrentContextUseCase {
  List<ContextDto> call(List<ContextDto> contexts, String filter) {
    return contexts.where((context) => context.name != filter).toList();
  }
}
