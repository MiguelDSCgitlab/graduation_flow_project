import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../dialog/application/form_provider.dart';
import 'context_relationship_notifier.dart';
import 'context_relationship_state.dart';

final contextRelationshipProvider = StateNotifierProvider<
    ContextRelationshipNotifier, ContextRelationshipHandlerState>((ref) {
  final notifier = ContextRelationshipNotifier(ref);
  final state = ref.watch(formProvider);

  if (state is NoFormState) {
    notifier.initialize();
  }
  return notifier;
});
