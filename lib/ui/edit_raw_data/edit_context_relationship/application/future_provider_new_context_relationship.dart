import 'package:c4_frontend_data_entry_flutter/ui/edit_raw_data/dialog/application/form_provider.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:uuid/uuid.dart';

import '../../../../api_raw_data/infrastructure/api_c4_all_components/dependency_injection.dart';
import '../../../../api_raw_data/infrastructure/api_c4_relationships/data/relationship_input_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_relationships/data/relationship_output_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_relationships/infrastructure/relationship_repository.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import '../../../tree_view/menu_context/application/menu_contexts_refresh_provider.dart';
import '../../widgets/form_submit_provider.dart';
import 'context_relationship_provider.dart';

final futureProviderNewContextRelationship =
    FutureProvider.autoDispose<RelationshipOutputDto>(
  (ref) async {
    ref.watch(formSubmitProvider);
    ContextDto contextDto = ref.read(formProvider.notifier).getContext();
    RelationshipInputDto relationshipInputDto = ref
        .read(contextRelationshipProvider.notifier)
        .storedRelationshipInputDto;
    var result = await getIt<RelationshipRepository>().addContextRelationship(
      contextDto.contextId,
      relationshipInputDto,
    );
    String refreshId = const Uuid().v4();
    ref.read(refreshContextsProvider.notifier).state = refreshId;
    return result;
  },
);
