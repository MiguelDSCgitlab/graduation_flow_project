import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:uuid/uuid.dart';

import '../../../../api_raw_data/infrastructure/api_c4_all_components/dependency_injection.dart';
import '../../../../api_raw_data/infrastructure/api_c4_relationships/data/relationship_input_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_relationships/data/relationship_output_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_relationships/infrastructure/relationship_repository.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/relationship_dto.dart';
import '../../../tree_view/menu_context/application/menu_contexts_refresh_provider.dart';
import '../../dialog/application/form_provider.dart';
import '../../widgets/form_submit_provider.dart';
import 'context_relationship_provider.dart';

final futureProviderDeleteContextRelationship =
    FutureProvider.autoDispose<RelationshipOutputDto>(
  (ref) async {
    ref.watch(formSubmitProvider);
    ContextDto contextDto = ref.read(formProvider.notifier).getContext();
    RelationshipDto? relationshipDto =
        ref.read(formProvider.notifier).getRelationship();
    RelationshipInputDto relationshipInputDto = ref
        .read(contextRelationshipProvider.notifier)
        .storedRelationshipInputDto;

    if (relationshipDto != null) {
      var result = await getIt<RelationshipRepository>()
          .deleteContextRelationship(contextDto.contextId,
              relationshipDto.relationshipId, relationshipInputDto);
      String refreshId = const Uuid().v4();
      ref.read(refreshContextsProvider.notifier).state = refreshId;

      return result;
    } else {
      throw Exception('RelationshipDto null');
    }
  },
);
