import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_relationships/data/relationship_input_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/relationship_dto.dart';
import 'context_relationship_state.dart';

class ContextRelationshipNotifier
    extends StateNotifier<ContextRelationshipHandlerState> {
  final Ref ref;
  late ContextDto contextDto;
  late RelationshipDto relationshipDto;
  late RelationshipInputDto storedRelationshipInputDto;

  ContextRelationshipNotifier(this.ref)
      : super(ContextRelationshipInitialState());

  void newContextRelationshipForm() {
    state = NewContextRelationshipFormState();
  }

  void editContextRelationshipForm() {
    state = EditContextRelationshipFormState();
  }

  void deleteContextRelationship(
      {required RelationshipInputDto relationshipInputDto}) {
    storedRelationshipInputDto = relationshipInputDto;
    state = DeleteContextRelationshipState();
  }

  void putContextRelationship(
      {required RelationshipInputDto relationshipInputDto}) {
    storedRelationshipInputDto = relationshipInputDto;
    state = PutContextRelationshipState();
  }

  void deleteContextRelationshipForm() {
    state = DeleteContextRelationshipFormState();
  }

  void postContextRelationship(
      {required RelationshipInputDto relationshipInputDto}) {
    storedRelationshipInputDto = relationshipInputDto;
    state = PostContextRelationshipState();
  }

  void initialize() {
    state = ContextRelationshipInitialState();
  }

  void createInput({required String description}) {
    if (state is NewContextRelationshipFormState) {
      storedRelationshipInputDto = RelationshipInputDto(
        fromContext: contextDto.contextId,
        fromName: contextDto.name,
        toContext: "",
        toName: "",
        type: "Context",
      );
    } else {
      storedRelationshipInputDto = RelationshipInputDto(
        fromContext: relationshipDto.fromContext,
        fromContainer: null,
        fromComponent: null,
        toContext: relationshipDto.toContext,
        toContainer: null,
        toComponent: null,
        relationshipId: relationshipDto.relationshipId,
        fromName: relationshipDto.fromName ?? '',
        toName: relationshipDto.toName ?? '',
        type: 'Context',
        description: description,
      );
    }
  }
}
