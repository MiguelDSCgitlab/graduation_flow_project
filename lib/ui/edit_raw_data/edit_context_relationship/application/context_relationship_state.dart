abstract class ContextRelationshipHandlerState {}

class ContextRelationshipInitialState extends ContextRelationshipHandlerState {}

// class ContextRelationshipLoadingState extends ContextRelationshipHandlerState {}

class NewContextRelationshipFormState extends ContextRelationshipHandlerState {}

class EditContextRelationshipFormState
    extends ContextRelationshipHandlerState {}

class DeleteContextRelationshipFormState
    extends ContextRelationshipHandlerState {}

class PostContextRelationshipState extends ContextRelationshipHandlerState {}

class DeleteContextRelationshipState extends ContextRelationshipHandlerState {}

class PutContextRelationshipState extends ContextRelationshipHandlerState {}
