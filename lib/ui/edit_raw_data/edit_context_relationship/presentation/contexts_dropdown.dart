import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_relationships/data/relationship_input_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';

class ContextsDropDown extends ConsumerStatefulWidget {
  final List<ContextDto> contexts;
  final ContextDto contextDto;
  final RelationshipInputDto relationshipInputDto;

  const ContextsDropDown({
    super.key,
    required this.contextDto,
    required this.contexts,
    required this.relationshipInputDto,
  });

  @override
  ConsumerState createState() => _ContextsDropDownState();
}

class _ContextsDropDownState extends ConsumerState<ContextsDropDown> {
  ContextDto? _selectedContext;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text("Connect context (${widget.contextDto.name}) to:"),
        const Padding(
          padding: EdgeInsets.only(left: 32),
        ),
        Expanded(
          child: DropdownButton(
            hint: Text(
              "Select context",
              style: TextStyle(
                color: Theme.of(context).colorScheme.onSurface,
              ),
            ),
            value: _selectedContext,
            items: widget.contexts.map((ContextDto contextDto) {
              return DropdownMenuItem(
                key: Key(contextDto.id),
                value: contextDto,
                child: Text(
                  contextDto.name,
                ),
              );
            }).toList(),
            onChanged: (contextDto) {
              setState(() {
                if (contextDto != null) {
                  _selectedContext = contextDto;
                  widget.relationshipInputDto.toContext = contextDto.id;
                  widget.relationshipInputDto.toName = contextDto.name;
                }
              });
            },
            isExpanded: true,
          ),
        ),
      ],
    );
  }
}
