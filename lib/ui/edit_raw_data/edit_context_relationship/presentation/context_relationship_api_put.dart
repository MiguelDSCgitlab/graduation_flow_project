import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_relationships/data/relationship_input_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/relationship_dto.dart';
import '../../../tree_view/menu_context/application/menu_contexts_state_notifier_provider.dart';
import '../../dialog/application/form_provider.dart';
import '../application/context_relationship_provider.dart';
import '../application/future_provider_update_context_relationship.dart';
import '../domain/remove_current_context.dart';
import 'context_relationship_form.dart';

class ContextRelationshipApiPut extends ConsumerStatefulWidget {
  const ContextRelationshipApiPut({
    Key? key,
  }) : super(key: key);

  @override
  ConsumerState createState() => _ApiPutContextRelationshipState();
}

class _ApiPutContextRelationshipState
    extends ConsumerState<ContextRelationshipApiPut> {
  @override
  Widget build(BuildContext context) {
    final apiResult = ref.watch(futureProviderEditContextRelationship);
    return apiResult.when(
      data: (data) {
        final errorDto = data.errorDto;
        if (errorDto != null) {
          final contexts =
              ref.read(menuContextsStateNotifierProvider.notifier).contexts;
          ContextDto contextDto = ref.read(formProvider.notifier).getContext();
          var filteredContexts =
              RemoveCurrentContextUseCase().call(contexts, contextDto.name);
          RelationshipDto? relationshipDto =
              ref.read(formProvider.notifier).getRelationship();
          RelationshipInputDto relationshipInputDto = ref
              .read(contextRelationshipProvider.notifier)
              .storedRelationshipInputDto;

          return ContextRelationshipForm(
            contexts: filteredContexts,
            contextDto: contextDto,
            errorDto: errorDto,
            relationshipDto: relationshipDto,
            relationshipInputDto: relationshipInputDto,
          );
        } else {
          WidgetsBinding.instance.addPostFrameCallback((_) {
            Navigator.pop(context);
            ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
              backgroundColor: CupertinoColors.activeBlue,
              content: Text(
                "Context relationship updated.",
                style: TextStyle(color: CupertinoColors.white),
              ),
            ));
          });
          return const SizedBox.shrink();
        }
      },
      error: (_, __) => const Text('Something went wrong'),
      loading: () => const Text('Loading'),
    );
  }
}
