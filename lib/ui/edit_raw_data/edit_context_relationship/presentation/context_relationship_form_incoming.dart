import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_shared/data/relationship_dto.dart';

class ContextRelationshipFormIncoming extends ConsumerStatefulWidget {
  final RelationshipDto? relationshipDto;

  const ContextRelationshipFormIncoming({
    Key? key,
    required this.relationshipDto,
  }) : super(key: key);

  @override
  ConsumerState createState() => _ViewIncomingContextRelationshipState();
}

class _ViewIncomingContextRelationshipState
    extends ConsumerState<ContextRelationshipFormIncoming> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 12),
      child: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Padding(
              padding: EdgeInsets.only(bottom: 20.0),
              child: Text('Incoming context relationship'),
            ),
            const SizedBox(height: 32),
            Text("From context: ${widget.relationshipDto?.fromName}"),
            Text("To context: ${widget.relationshipDto?.toName}"),
            Text("Description: ${widget.relationshipDto?.description}"),
          ],
        ),
      ),
    );
  }
}
