import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_relationships/data/relationship_input_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/relationship_dto.dart';
import '../../../tree_view/menu_context/application/menu_contexts_state_notifier_provider.dart';
import '../../dialog/application/form_provider.dart';
import '../application/context_relationship_provider.dart';
import '../application/future_provider_delete_context_relationship.dart';
import 'context_relationship_form.dart';

class ContextRelationshipApiDelete extends ConsumerStatefulWidget {
  const ContextRelationshipApiDelete({
    Key? key,
  }) : super(key: key);

  @override
  ConsumerState createState() => _ApiDeleteContextRelationshipState();
}

class _ApiDeleteContextRelationshipState
    extends ConsumerState<ContextRelationshipApiDelete> {
  @override
  Widget build(BuildContext context) {
    final apiResult = ref.watch(futureProviderDeleteContextRelationship);
    return apiResult.when(
      data: (data) {
        final errorDto = data.errorDto;
        if (errorDto != null) {
          final contexts =
              ref.read(menuContextsStateNotifierProvider.notifier).contexts;
          ContextDto contextDto = ref.read(formProvider.notifier).getContext();
          RelationshipDto? relationshipDto =
              ref.read(formProvider.notifier).getRelationship();
          RelationshipInputDto relationshipInputDto = ref
              .read(contextRelationshipProvider.notifier)
              .storedRelationshipInputDto;

          return ContextRelationshipForm(
            contexts: contexts,
            contextDto: contextDto,
            errorDto: errorDto,
            relationshipDto: relationshipDto,
            relationshipInputDto: relationshipInputDto,
          );
        } else {
          WidgetsBinding.instance.addPostFrameCallback((_) {
            Navigator.pop(context);
            ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
              backgroundColor: CupertinoColors.activeBlue,
              content: Text(
                "Context relationship deleted.",
                style: TextStyle(color: CupertinoColors.white),
              ),
            ));
          });
          return const SizedBox.shrink();
        }
      },
      error: (_, __) => const Text('Something went wrong'),
      loading: () => const Text('Loading'),
    );
  }
}
