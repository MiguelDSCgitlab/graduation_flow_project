import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_error/error_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_relationships/data/relationship_input_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/relationship_dto.dart';
import 'context_relationship_form_incoming.dart';
import 'context_relationship_form_outgoing.dart';

class ContextRelationshipForm extends ConsumerStatefulWidget {
  final List<ContextDto> contexts;
  final ContextDto contextDto;
  final RelationshipDto? relationshipDto;
  final RelationshipInputDto relationshipInputDto;
  final ErrorDto? errorDto;

  const ContextRelationshipForm({
    required this.contexts,
    required this.contextDto,
    required this.relationshipInputDto,
    this.relationshipDto,
    this.errorDto,
    Key? key,
  }) : super(key: key);

  @override
  ConsumerState createState() => _ContextRelationshipFormState();
}

class _ContextRelationshipFormState
    extends ConsumerState<ContextRelationshipForm> {
  @override
  Widget build(BuildContext context) {
    bool outgoing = widget.relationshipInputDto.outgoing;
    return outgoing
        ? ContextRelationshipFormOutgoing(
            contexts: widget.contexts,
            contextDto: widget.contextDto,
            relationshipDto: widget.relationshipDto,
            relationshipInputDto: widget.relationshipInputDto,
            errorDto: widget.errorDto,
          )
        : ContextRelationshipFormIncoming(
            relationshipDto: widget.relationshipDto,
          );
  }
}
