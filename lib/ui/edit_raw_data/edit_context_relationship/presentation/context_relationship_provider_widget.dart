import 'package:c4_frontend_data_entry_flutter/api_raw_data/infrastructure/api_c4_relationships/data/relationship_input_dto.dart';
import 'package:c4_frontend_data_entry_flutter/ui/edit_raw_data/edit_context_relationship/application/context_relationship_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/relationship_dto.dart';
import '../../../tree_view/menu_context/application/menu_contexts_state_notifier_provider.dart';
import '../../widgets/loading.dart';
import '../application/context_relationship_state.dart';
import '../domain/remove_current_context.dart';
import 'context_relationship_api_delete.dart';
import 'context_relationship_api_post.dart';
import 'context_relationship_api_put.dart';
import 'context_relationship_form.dart';

class ContextRelationshipProviderWidget extends ConsumerWidget {
  final ContextDto contextDto;
  final RelationshipDto? relationshipDto;

  const ContextRelationshipProviderWidget({
    Key? key,
    required this.contextDto,
    this.relationshipDto,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final state = ref.watch(contextRelationshipProvider);

    if (state is ContextRelationshipInitialState) {
      return const LoadingWidget();
    }

    if (state is NewContextRelationshipFormState) {
      final contexts =
          ref.read(menuContextsStateNotifierProvider.notifier).contexts;
      var filteredContexts =
          RemoveCurrentContextUseCase().call(contexts, contextDto.name);
      RelationshipInputDto relationshipInputDto = RelationshipInputDto(
        fromContext: contextDto.contextId,
        toContext: "",
        fromName: contextDto.name,
        toName: "",
        type: "Context",
      );

      return ContextRelationshipForm(
        contexts: filteredContexts,
        contextDto: contextDto,
        relationshipInputDto: relationshipInputDto,
      );
    }

    if (state is EditContextRelationshipFormState) {
      final contexts =
          ref.read(menuContextsStateNotifierProvider.notifier).contexts;
      final tRelationshipDto = relationshipDto;
      if (tRelationshipDto != null) {
        RelationshipInputDto relationshipInputDto = RelationshipInputDto(
            fromContext: tRelationshipDto.fromContext,
            toContext: tRelationshipDto.toContext,
            fromName: tRelationshipDto.fromName ?? '',
            toName: tRelationshipDto.toName ?? '',
            type: tRelationshipDto.type ?? 'Context',
            description: tRelationshipDto.description)
          ..outgoing = tRelationshipDto.outgoing ?? true;

        return ContextRelationshipForm(
          contexts: contexts,
          contextDto: contextDto,
          relationshipDto: relationshipDto,
          relationshipInputDto: relationshipInputDto,
        );
      } else {
        return const Text('Error occurred');
      }
    }

    if (state is PostContextRelationshipState) {
      return const ContextRelationshipApiPost();
    }

    if (state is PutContextRelationshipState) {
      return const ContextRelationshipApiPut();
    }

    if (state is DeleteContextRelationshipState) {
      return const ContextRelationshipApiDelete();
    }

    return Container();
  }
}
