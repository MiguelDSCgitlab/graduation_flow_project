import 'package:c4_frontend_data_entry_flutter/api_raw_data/infrastructure/api_c4_relationships/data/relationship_input_dto.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_error/error_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/relationship_dto.dart';
import '../../widgets/error_form.dart';
import '../../widgets/form_submit_provider.dart';
import '../application/context_relationship_provider.dart';
import '../application/context_relationship_state.dart';
import 'contexts_dropdown.dart';

class ContextRelationshipFormOutgoing extends ConsumerStatefulWidget {
  final List<ContextDto> contexts;
  final ContextDto contextDto;
  final RelationshipDto? relationshipDto;
  final RelationshipInputDto relationshipInputDto;
  final ErrorDto? errorDto;

  const ContextRelationshipFormOutgoing({
    required this.contexts,
    required this.contextDto,
    this.relationshipDto,
    this.errorDto,
    required this.relationshipInputDto,
    Key? key,
  }) : super(key: key);

  @override
  ConsumerState createState() => _ContextRelationshipFormOutgoingState();
}

class _ContextRelationshipFormOutgoingState
    extends ConsumerState<ContextRelationshipFormOutgoing> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    String titleText = "";
    String description = "";
    bool showDropDown = false;
    bool showEditAndDelete = false;
    final state = ref.watch(contextRelationshipProvider);
    if (state is DeleteContextRelationshipState ||
        state is EditContextRelationshipFormState) {
      titleText =
          'Edit context relationship between ${widget.relationshipDto?.fromName} and ${widget.relationshipDto?.toName}';
      description = widget.relationshipDto?.description ?? "";
      showDropDown = false;
      showEditAndDelete = true;
    } else {
      titleText = "New context relationship";
      description = "";
      showDropDown = true;
    }

    return SingleChildScrollView(
      child: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            ErrorFormWidget(
              errorDto: widget.errorDto,
            ),
            Text(titleText),
            showDropDown
                ? const SizedBox(
                    height: 32,
                  )
                : Container(),
            showDropDown
                ? ContextsDropDown(
                    contextDto: widget.contextDto,
                    contexts: widget.contexts,
                    relationshipInputDto: widget.relationshipInputDto,
                  )
                : Container(),
            const SizedBox(
              height: 16,
            ),
            TextFormField(
              maxLength: 40,
              initialValue: description,
              validator: (value) {
                if (value != null) {
                  if (value.isEmpty) {
                    return 'Please enter a description';
                  } else if (value.length < 3) {
                    return 'Minimum length is 3.';
                  }
                }
                return null;
              },
              onSaved: (value) =>
                  widget.relationshipInputDto.description = value!,
              decoration: const InputDecoration(labelText: 'Description'),
            ),
            const SizedBox(
              height: 32,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                showEditAndDelete
                    ? ElevatedButton(
                        onPressed: () {
                          ref.read(formSubmitProvider.notifier).submit();
                          ref
                              .read(contextRelationshipProvider.notifier)
                              .deleteContextRelationship(
                                  relationshipInputDto:
                                      widget.relationshipInputDto);
                        },
                        child: const Padding(
                          padding: EdgeInsets.all(16.0),
                          child: Text('Delete'),
                        ),
                      )
                    : Container(),
                const Padding(
                  padding: EdgeInsets.all(16.0),
                ),
                ElevatedButton(
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      _formKey.currentState!.save();
                    }
                    ref.read(formSubmitProvider.notifier).submit();
                    showEditAndDelete
                        ? ref
                            .read(contextRelationshipProvider.notifier)
                            .putContextRelationship(
                              relationshipInputDto: widget.relationshipInputDto,
                            )
                        : ref
                            .read(contextRelationshipProvider.notifier)
                            .postContextRelationship(
                              relationshipInputDto: widget.relationshipInputDto,
                            );
                  },
                  child: const Padding(
                    padding: EdgeInsets.all(16.0),
                    child: Text('Store'),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
