import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../dialog/application/form_provider.dart';

class ContextTitleWidget extends ConsumerWidget {
  final String? oldName;

  const ContextTitleWidget({
    Key? key,
    this.oldName,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final formState = ref.read(formProvider);
    if (formState is ShowNewContextFormState) {
      return const Column(
        children: [
          Text(
            'New system architecture',
            style: TextStyle(fontSize: 24),
          ),
          Padding(
            padding: EdgeInsets.only(top: 20.0),
          ),
        ],
      );
    }

    if (formState is ShowEditContextFormState) {
      String name = oldName ?? 'Editing system architecture';
      return Column(
        children: [
          Text(
            name,
            style: const TextStyle(fontSize: 24),
          ),
          const Padding(
            padding: EdgeInsets.only(top: 20.0),
          ),
        ],
      );
    }
    return Container();
  }
}
