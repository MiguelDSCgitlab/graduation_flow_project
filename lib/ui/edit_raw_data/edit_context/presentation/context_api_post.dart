import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_contexts/infrastructure/data/context_input.dto.dart';
import '../application/context_provider.dart';
import '../application/future_provider_new_context.dart';
import 'new_context_form.dart';

class ContextApiPost extends ConsumerStatefulWidget {
  final ContextInputDto contextInputDto;

  const ContextApiPost({
    Key? key,
    required this.contextInputDto,
  }) : super(key: key);

  @override
  ConsumerState createState() => _ApiPostContextState();
}

class _ApiPostContextState extends ConsumerState<ContextApiPost> {
  @override
  Widget build(BuildContext context) {
    final apiResult =
        ref.watch(futureProviderPostContext(widget.contextInputDto));
    return apiResult.when(
      data: (data) {
        final errorDto = data.errorDto;
        ContextInputDto contextInputDto =
            ref.read(contextProvider.notifier).storedContextInputDto;

        if (errorDto != null) {
          return NewContextForm(
              contextInputDto: contextInputDto, errorDto: errorDto);
        } else {
          WidgetsBinding.instance.addPostFrameCallback((_) {
            Navigator.pop(context);
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              backgroundColor: CupertinoColors.activeBlue,
              content: Text(
                "Context ${contextInputDto.name} created.",
                style: const TextStyle(color: CupertinoColors.white),
              ),
            ));
          });
          return const SizedBox.shrink();
        }
      },
      error: (_, __) => const Text('Something went wrong'),
      loading: () => const Text('Storing new context'),
    );
  }
}
