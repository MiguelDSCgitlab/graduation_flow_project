import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_contexts/infrastructure/data/context_input.dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import '../../widgets/form_submit_provider.dart';
import '../application/context_provider.dart';

class DeleteContextButton extends ConsumerWidget {
  final ContextDto contextDto;
  final ContextInputDto contextInputDto;

  const DeleteContextButton({
    Key? key,
    required this.contextInputDto,
    required this.contextDto,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return ElevatedButton.icon(
      icon: const Icon(Icons.delete),
      onPressed: () {
        ref.read(formSubmitProvider.notifier).submit();
        ref.read(contextProvider.notifier).deleteContext(contextInputDto);
      },
      label: const Padding(
        padding: EdgeInsets.all(8.0),
        child: Text('Delete'),
      ),
    );
  }
}
