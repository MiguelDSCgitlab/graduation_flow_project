import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_contexts/infrastructure/data/context_input.dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import '../application/future_provider_update_context.dart';
import 'context_form.dart';

class ContextApiPut extends ConsumerStatefulWidget {
  final ContextInputDto contextInputDto;
  final ContextDto contextDto;

  const ContextApiPut({
    Key? key,
    required this.contextInputDto,
    required this.contextDto,
  }) : super(key: key);

  @override
  ConsumerState createState() => _ContextApiPutState();
}

class _ContextApiPutState extends ConsumerState<ContextApiPut> {
  @override
  Widget build(BuildContext context) {
    final apiResult =
        ref.watch(futureProviderUpdateContext(widget.contextInputDto));
    return apiResult.when(
      data: (data) {
        final errorDto = data.errorDto;
        if (errorDto != null) {
          return ContextForm(
              contextDto: widget.contextDto,
              contextInputDto: widget.contextInputDto,
              errorDto: errorDto);
        } else {
          WidgetsBinding.instance.addPostFrameCallback((_) {
            Navigator.pop(context);
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              backgroundColor: CupertinoColors.activeBlue,
              content: Text(
                "Context ${widget.contextInputDto.name} updated.",
                style: const TextStyle(color: CupertinoColors.white),
              ),
            ));
          });
          return const SizedBox.shrink();
        }
      },
      error: (_, __) => const Text('Something went wrong'),
      loading: () => const Text('Loading'),
    );
  }
}
