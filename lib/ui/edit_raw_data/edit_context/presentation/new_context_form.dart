import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_contexts/infrastructure/data/context_input.dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_error/error_dto.dart';
import '../../widgets/error_form.dart';
import 'context_title.dart';
import 'description_form_field.dart';
import 'location_form_field.dart';
import 'name_form_field.dart';
import 'store_context_button.dart';

class NewContextForm extends ConsumerStatefulWidget {
  final ContextInputDto contextInputDto;
  final ErrorDto? errorDto;

  const NewContextForm({
    super.key,
    required this.contextInputDto,
    required this.errorDto,
  });

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _NewContextFormState();
}

class _NewContextFormState extends ConsumerState<NewContextForm> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            const Padding(
              padding: EdgeInsets.all(16.0),
            ),
            ErrorFormWidget(
              errorDto: widget.errorDto,
            ),
            const ContextTitleWidget(),
            NameFormField(
              contextInputDto: widget.contextInputDto,
            ),
            DescriptionFormField(
              contextInputDto: widget.contextInputDto,
            ),
            const SizedBox(
              height: 8,
            ),
            const Text(
              "Location?",
            ),
            LocationFormField(
              contextInputDto: widget.contextInputDto,
            ),
            const SizedBox(
              height: 16,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                const SizedBox(
                  width: 16,
                ),
                StoreContextButton(
                  contextInputDto: widget.contextInputDto,
                  formKey: _formKey,
                ),
              ],
            ),
            const SizedBox(
              height: 16,
            ),
          ],
        ),
      ),
    );
  }
}
