import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_contexts/infrastructure/data/context_input.dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_error/error_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import '../../widgets/error_form.dart';
import 'context_title.dart';
import 'delete_context_button.dart';
import 'description_form_field.dart';
import 'location_form_field.dart';
import 'name_form_field.dart';
import 'update_context_button.dart';

class ContextForm extends ConsumerStatefulWidget {
  final ContextDto contextDto;
  final ContextInputDto contextInputDto;
  final ErrorDto? errorDto;

  const ContextForm({
    super.key,
    required this.contextInputDto,
    required this.errorDto,
    required this.contextDto,
  });

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _ContextFormState();
}

class _ContextFormState extends ConsumerState<ContextForm> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            const Padding(
              padding: EdgeInsets.all(16.0),
            ),
            ErrorFormWidget(
              errorDto: widget.errorDto,
            ),
            ContextTitleWidget(
              oldName: widget.contextDto.name,
            ),
            NameFormField(
              contextInputDto: widget.contextInputDto,
            ),
            DescriptionFormField(
              contextInputDto: widget.contextInputDto,
            ),
            const SizedBox(
              height: 8,
            ),
            const Text(
              "Location?",
            ),
            LocationFormField(
              contextInputDto: widget.contextInputDto,
            ),
            const SizedBox(
              height: 16,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                const SizedBox(
                  width: 16,
                ),
                DeleteContextButton(
                  contextInputDto: widget.contextInputDto,
                  contextDto: widget.contextDto,
                ),
                const SizedBox(
                  width: 16,
                ),
                UpdateContextButton(
                  contextDto: widget.contextDto,
                  contextInputDto: widget.contextInputDto,
                  formKey: _formKey,
                ),
              ],
            ),
            const SizedBox(
              height: 16,
            ),
          ],
        ),
      ),
    );
  }
}
