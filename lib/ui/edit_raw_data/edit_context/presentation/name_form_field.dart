import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_contexts/infrastructure/data/context_input.dto.dart';

class NameFormField extends ConsumerWidget {
  final ContextInputDto contextInputDto;

  const NameFormField({
    Key? key,
    required this.contextInputDto,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return TextFormField(
      decoration: const InputDecoration(
        labelText: "Architecture name ",
        hintText: "Architecture name",
        prefixIcon: Icon(
          Icons.architecture,
          color: Colors.white,
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(16.0),
          ),
        ),
      ),
      maxLength: 40,
      initialValue: contextInputDto.name,
      validator: (value) {
        if (value != null) {
          if (value.isEmpty) {
            return 'Please enter a name';
          }
        }
        return null;
      },
      onSaved: (value) => contextInputDto.name = value!,
    );
  }
}
