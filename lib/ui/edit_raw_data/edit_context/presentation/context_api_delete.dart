import 'package:c4_frontend_data_entry_flutter/api_raw_data/infrastructure/api_c4_contexts/infrastructure/data/context_input.dto.dart';
import 'package:c4_frontend_data_entry_flutter/api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../application/future_provider_delete_context.dart';
import 'context_form.dart';

class ContextApiDelete extends ConsumerStatefulWidget {
  final ContextInputDto contextInputDto;
  final ContextDto contextDto;

  const ContextApiDelete({
    Key? key,
    required this.contextInputDto,
    required this.contextDto,
  }) : super(key: key);

  @override
  ConsumerState createState() => _ContextApiDeleteState();
}

class _ContextApiDeleteState extends ConsumerState<ContextApiDelete> {
  @override
  Widget build(BuildContext context) {
    final apiResult =
        ref.watch(futureProviderDeleteContext(widget.contextInputDto));
    return apiResult.when(
      data: (data) {
        final errorDto = data.errorDto;
        if (errorDto != null) {
          return ContextForm(
              contextDto: widget.contextDto,
              contextInputDto: widget.contextInputDto,
              errorDto: errorDto);
        } else {
          WidgetsBinding.instance.addPostFrameCallback((_) {
            Navigator.pop(context);
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              backgroundColor: CupertinoColors.activeBlue,
              content: Text(
                "Context ${widget.contextInputDto.name} deleted.",
                style: const TextStyle(color: CupertinoColors.white),
              ),
            ));
          });
          return const SizedBox.shrink();
        }
      },
      error: (_, __) => const Text('Something went wrong'),
      loading: () => const Text('Loading'),
    );
  }
}
