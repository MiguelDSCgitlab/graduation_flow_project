import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_contexts/infrastructure/data/context_input.dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import '../../dialog/application/form_provider.dart';
import '../../widgets/form_submit_provider.dart';
import '../application/context_provider.dart';

class UpdateContextButton extends ConsumerWidget {
  final GlobalKey<FormState> formKey;
  final ContextDto contextDto;
  final ContextInputDto contextInputDto;

  const UpdateContextButton({
    Key? key,
    required this.formKey,
    required this.contextDto,
    required this.contextInputDto,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final formState = ref.read(formProvider);

    return ElevatedButton.icon(
      icon: const Icon(Icons.save),
      onPressed: () {
        if (formKey.currentState!.validate()) {
          ref.read(formSubmitProvider.notifier).submit();
          formKey.currentState!.save();
          if (formState is ShowEditContextFormState) {
            ref.read(contextProvider.notifier).putContext(contextInputDto);
          }
        }
      },
      label: const Padding(
        padding: EdgeInsets.all(8.0),
        child: Text('Store'),
      ),
    );
  }
}
