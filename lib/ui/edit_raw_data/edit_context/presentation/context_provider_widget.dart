import 'package:c4_frontend_data_entry_flutter/api_raw_data/infrastructure/api_c4_contexts/infrastructure/data/context_input.dto.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import '../../widgets/loading.dart';
import '../application/context_provider.dart';
import '../application/context_state.dart';
import 'context_api_delete.dart';
import 'context_api_post.dart';
import 'context_api_put.dart';
import 'context_form.dart';
import 'new_context_form.dart';

class ContextProviderWidget extends ConsumerWidget {
  final ContextDto? contextDto;

  const ContextProviderWidget({
    Key? key,
    this.contextDto,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final state = ref.watch(contextProvider);

    if (state is ContextInitialState) {
      return const LoadingWidget();
    }

    if (state is NewContextFormState) {
      ContextInputDto contextInputDto = ContextInputDto(name: "");
      return NewContextForm(contextInputDto: contextInputDto, errorDto: null);
    }

    if (state is EditContextFormState) {
      final ContextDto? tContextDto = contextDto;
      if (tContextDto != null) {
        ContextInputDto contextInputDto = ContextInputDto(
          name: tContextDto.name,
          description: tContextDto.description,
          location: tContextDto.location,
          id: tContextDto.id,
        );
        return ContextForm(
          contextInputDto: contextInputDto,
          errorDto: null,
          contextDto: tContextDto,
        );
      } else {
        return const Text('something went wrong');
      }
    }

    if (state is PostContextState) {
      ContextInputDto contextInputDto =
          ref.read(contextProvider.notifier).storedContextInputDto;
      return ContextApiPost(contextInputDto: contextInputDto);
    }

    if (state is PutContextState) {
      var inputDto = ref.read(contextProvider.notifier).storedContextInputDto;
      final tContextDto = contextDto;
      if (tContextDto != null) {
        return ContextApiPut(
            contextDto: tContextDto, contextInputDto: inputDto);
      } else {
        return const Text('something went wrong');
      }
    }

    if (state is DeleteContextState) {
      var inputDto = ref.read(contextProvider.notifier).storedContextInputDto;
      final tContextDto = contextDto;
      if (tContextDto != null) {
        return ContextApiDelete(
            contextDto: tContextDto, contextInputDto: inputDto);
      } else {
        return const Text('something went wrong');
      }
    }

    return Container();
  }
}
