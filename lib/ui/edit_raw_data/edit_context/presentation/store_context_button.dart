import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_contexts/infrastructure/data/context_input.dto.dart';
import '../../widgets/form_submit_provider.dart';
import '../application/context_provider.dart';

class StoreContextButton extends ConsumerWidget {
  final GlobalKey<FormState> formKey;
  final ContextInputDto contextInputDto;

  const StoreContextButton({
    Key? key,
    required this.formKey,
    required this.contextInputDto,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return ElevatedButton.icon(
      icon: const Icon(Icons.save),
      onPressed: () {
        ref.read(formSubmitProvider.notifier).submit();
        if (formKey.currentState!.validate()) {
          formKey.currentState!.save();
          ref.read(contextProvider.notifier).postContext(contextInputDto);
        }
      },
      label: const Padding(
        padding: EdgeInsets.all(8.0),
        child: Text('Store'),
      ),
    );
  }
}
