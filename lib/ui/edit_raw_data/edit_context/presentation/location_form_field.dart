import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_contexts/infrastructure/data/context_input.dto.dart';

class LocationFormField extends ConsumerStatefulWidget {
  final ContextInputDto contextInputDto;

  const LocationFormField({
    Key? key,
    required this.contextInputDto,
  }) : super(key: key);

  @override
  ConsumerState createState() => _LocationFormFieldState();
}

class _LocationFormFieldState extends ConsumerState<LocationFormField> {
  late Set<String> selection;

  @override
  void initState() {
    if (widget.contextInputDto.location == 'External') {
      selection = <String>{"External"};
    } else {
      selection = <String>{"Internal"};
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SegmentedButton<String>(
      segments: const <ButtonSegment<String>>[
        ButtonSegment<String>(value: "Internal", label: Text('Internal')),
        ButtonSegment<String>(value: "External", label: Text('External')),
      ],
      selected: selection,
      onSelectionChanged: (Set<String> newSelection) {
        setState(() {
          selection = newSelection;
          if (newSelection.first.startsWith('External')) {
            widget.contextInputDto.location = "External";
          } else {
            widget.contextInputDto.location = "Internal";
          }
        });
      },
      multiSelectionEnabled: false,
    );
  }
}
