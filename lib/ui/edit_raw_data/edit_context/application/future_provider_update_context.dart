import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:uuid/uuid.dart';

import '../../../../api_raw_data/infrastructure/api_c4_all_components/dependency_injection.dart';
import '../../../../api_raw_data/infrastructure/api_c4_contexts/infrastructure/contexts_repository.dart';
import '../../../../api_raw_data/infrastructure/api_c4_contexts/infrastructure/data/context_input.dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_contexts/infrastructure/data/context_output_dto.dart';
import '../../../tree_view/menu_context/application/menu_contexts_refresh_provider.dart';
import '../../widgets/form_submit_provider.dart';

final futureProviderUpdateContext = FutureProvider.autoDispose
    .family<ContextOutputDto, ContextInputDto>((ref, contextInputDto) async {
  ref.watch(formSubmitProvider);
  String? cid = contextInputDto.id;
  if (cid != null) {
    var result = await getIt<ContextsRepository>().updateContext(
      cid,
      contextInputDto.name,
      contextInputDto.description,
      contextInputDto.location,
    );
    String refreshId = const Uuid().v4();
    ref.read(refreshContextsProvider.notifier).state = refreshId;
    return result;
  } else {
    throw Error();
  }
});
