import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:uuid/uuid.dart';

import '../../../../api_raw_data/infrastructure/api_c4_all_components/dependency_injection.dart';
import '../../../../api_raw_data/infrastructure/api_c4_contexts/infrastructure/contexts_repository.dart';
import '../../../../api_raw_data/infrastructure/api_c4_contexts/infrastructure/data/context_input.dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_contexts/infrastructure/data/store_context_output_dto.dart';
import '../../../tree_view/menu_context/application/menu_contexts_refresh_provider.dart';
import '../../widgets/form_submit_provider.dart';

final futureProviderPostContext =
    FutureProvider.autoDispose.family<StoreContextOutputDto, ContextInputDto>(
  (ref, contextInputDto) async {
    ref.watch(formSubmitProvider);
    var result = await getIt<ContextsRepository>().storeContext(
      contextInputDto.name,
      contextInputDto.description,
      contextInputDto.location,
    );
    String refreshId = const Uuid().v4();
    ref.read(refreshContextsProvider.notifier).state = refreshId;
    return result;
  },
);
