import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../dialog/application/form_provider.dart';
import 'context_notifier.dart';
import 'context_state.dart';

final contextProvider =
    StateNotifierProvider<ContextNotifier, ContextHandlerState>((ref) {
  final notifier = ContextNotifier(ref);
  final state = ref.watch(formProvider);

  if (state is NoFormState) {
    notifier.initialize();
  }
  return notifier;
});
