import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_contexts/infrastructure/data/context_input.dto.dart';
import 'context_state.dart';

class ContextNotifier extends StateNotifier<ContextHandlerState> {
  final Ref ref;
  late ContextInputDto storedContextInputDto;

  ContextNotifier(this.ref) : super(ContextInitialState());

  void newContextForm() {
    state = NewContextFormState();
  }

  void editContextForm() {
    state = EditContextFormState();
  }

  void deleteContextForm() {
    state = DeleteContextFormState();
  }

  void postContext(ContextInputDto contextInputDto) {
    storedContextInputDto = contextInputDto;
    state = PostContextState();
  }

  void putContext(ContextInputDto contextInputDto) {
    storedContextInputDto = contextInputDto;
    state = PutContextState();
  }

  void deleteContext(ContextInputDto contextInputDto) {
    storedContextInputDto = contextInputDto;
    state = DeleteContextState();
  }

  void initialize() {
    state = ContextInitialState();
  }
}
