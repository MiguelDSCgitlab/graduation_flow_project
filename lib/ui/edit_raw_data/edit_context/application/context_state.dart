abstract class ContextHandlerState {}

class ContextInitialState extends ContextHandlerState {}

class ContextLoadingState extends ContextHandlerState {}

class NewContextFormState extends ContextHandlerState {}

class EditContextFormState extends ContextHandlerState {}

class DeleteContextFormState extends ContextHandlerState {}

class PostContextState extends ContextHandlerState {}

class DeleteContextState extends ContextHandlerState {}

class PutContextState extends ContextHandlerState {}
