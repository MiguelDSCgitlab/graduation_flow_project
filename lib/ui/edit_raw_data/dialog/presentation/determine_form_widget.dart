import 'package:c4_frontend_data_entry_flutter/ui/edit_raw_data/edit_component/application/component_provider.dart';
import 'package:c4_frontend_data_entry_flutter/ui/edit_raw_data/edit_container/application/container_provider.dart';
import 'package:c4_frontend_data_entry_flutter/ui/edit_raw_data/edit_context_relationship/application/context_relationship_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../edit_component/presentation/component_provider_widget.dart';
import '../../edit_component_relationship/application/component_relationship_provider.dart';
import '../../edit_component_relationship/presentation/component_relationship_provider_widget.dart';
import '../../edit_container/presentation/container_provider_widget.dart';
import '../../edit_container_relationship/application/container_relationship_provider.dart';
import '../../edit_container_relationship/presentation/container_relationship_provider_widget.dart';
import '../../edit_context/application/context_provider.dart';
import '../../edit_context/presentation/context_provider_widget.dart';
import '../../edit_context_relationship/presentation/context_relationship_provider_widget.dart';
import '../application/form_provider.dart';

class DetermineFormWidget extends ConsumerWidget {
  const DetermineFormWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final state = ref.watch(formProvider);

    if (state is ShowNewContextFormState) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        ref.read(contextProvider.notifier).newContextForm();
      });

      return const ContextProviderWidget();
    }

    if (state is ShowEditContextFormState) {
      final contextDto = ref.read(formProvider.notifier).getContext();
      WidgetsBinding.instance.addPostFrameCallback((_) {
        ref.read(contextProvider.notifier).editContextForm();
      });
      return ContextProviderWidget(
        contextDto: contextDto,
      );
    }

    if (state is ShowNewContainerFormState) {
      final contextDto = ref.read(formProvider.notifier).getContext();
      WidgetsBinding.instance.addPostFrameCallback((_) {
        ref.read(containerProvider.notifier).newContainerForm();
      });

      return ContainerProviderWidget(
        contextDto: contextDto,
      );
    }

    if (state is ShowEditContainerFormState) {
      final contextDto = ref.read(formProvider.notifier).getContext();
      final containerDto = ref.read(formProvider.notifier).getContainer();
      WidgetsBinding.instance.addPostFrameCallback(
        (_) {
          ref.read(containerProvider.notifier).editContainerForm();
        },
      );
      if (containerDto != null) {
        return ContainerProviderWidget(
          contextDto: contextDto,
          containerDto: containerDto,
        );
      } else {
        // todo show error widget
        return const Text('error occurred');
      }
    }

    if (state is ShowNewComponentFormState) {
      final contextDto = ref.read(formProvider.notifier).getContext();
      final containerDto = ref.read(formProvider.notifier).getContainer();
      WidgetsBinding.instance.addPostFrameCallback((_) {
        ref.read(componentProvider.notifier).newComponentForm();
      });

      if (containerDto != null) {
        return ComponentProviderWidget(
          contextDto: contextDto,
          containerDto: containerDto,
        );
      }
    }

    if (state is ShowEditComponentFormState) {
      final contextDto = ref.read(formProvider.notifier).getContext();
      final containerDto = ref.read(formProvider.notifier).getContainer();
      final componentDto = ref.read(formProvider.notifier).getComponent();

      WidgetsBinding.instance.addPostFrameCallback(
        (_) {
          if (containerDto != null && componentDto != null) {
            ref.read(componentProvider.notifier).editComponentForm();
          }
        },
      );
      if (containerDto != null && componentDto != null) {
        return ComponentProviderWidget(
          contextDto: contextDto,
          containerDto: containerDto,
          componentDto: componentDto,
        );
      } else {
        // todo create error widget
        return const Text('Error ocurred');
      }
    }

    if (state is ShowNewContextRelationshipFormState) {
      final contextDto = ref.read(formProvider.notifier).getContext();
      WidgetsBinding.instance.addPostFrameCallback((_) {
        ref
            .read(contextRelationshipProvider.notifier)
            .newContextRelationshipForm();
      });

      return ContextRelationshipProviderWidget(
        contextDto: contextDto,
      );
    }

    if (state is ShowEditContextRelationshipFormState) {
      final contextDto = ref.read(formProvider.notifier).getContext();
      final relationshipDto = ref.read(formProvider.notifier).getRelationship();

      WidgetsBinding.instance.addPostFrameCallback((_) {
        if (relationshipDto != null) {
          ref
              .read(contextRelationshipProvider.notifier)
              .editContextRelationshipForm();
        }
      });
      if (relationshipDto != null) {
        return ContextRelationshipProviderWidget(
          contextDto: contextDto,
          relationshipDto: relationshipDto,
        );
      } else {
        // todo create error widget
        return const Text('Error ocurred');
      }
    }

    if (state is ShowNewContainerRelationshipFormState) {
      final contextDto = ref.read(formProvider.notifier).getContext();
      final containerDto = ref.read(formProvider.notifier).getContainer();
      WidgetsBinding.instance.addPostFrameCallback((_) {
        ref
            .read(containerRelationshipProvider.notifier)
            .newContainerRelationshipForm();
      });

      if (containerDto != null) {
        return ContainerRelationshipProviderWidget(
          contextDto: contextDto,
          containerDto: containerDto,
        );
      } else {
        // todo add error widget
        return const Text('error occurred');
      }
    }

    if (state is ShowEditContainerRelationshipFormState) {
      final contextDto = ref.read(formProvider.notifier).getContext();
      final containerDto = ref.read(formProvider.notifier).getContainer();
      final relationshipDto = ref.read(formProvider.notifier).getRelationship();

      WidgetsBinding.instance.addPostFrameCallback((_) {
        if (containerDto != null && relationshipDto != null) {
          ref
              .read(containerRelationshipProvider.notifier)
              .editContainerRelationshipForm();
        }
      });
      if (containerDto != null && relationshipDto != null) {
        return ContainerRelationshipProviderWidget(
          contextDto: contextDto,
          containerDto: containerDto,
          relationshipDto: relationshipDto,
        );
      } else {
        // todo create error widget
        return const Text('Error ocurred');
      }
    }

    if (state is ShowNewComponentRelationshipFormState) {
      final contextDto = ref.read(formProvider.notifier).getContext();
      final containerDto = ref.read(formProvider.notifier).getContainer();
      final componentDto = ref.read(formProvider.notifier).getComponent();
      WidgetsBinding.instance.addPostFrameCallback((_) {
        ref
            .read(componentRelationshipProvider.notifier)
            .newComponentRelationshipForm();
      });

      if (containerDto != null && componentDto != null) {
        return ComponentRelationshipProviderWidget(
          contextDto: contextDto,
          containerDto: containerDto,
          componentDto: componentDto,
        );
      } else {
        // todo add error widget
        return const Text('error occurred');
      }
    }

    if (state is ShowEditComponentRelationshipFormState) {
      final contextDto = ref.read(formProvider.notifier).getContext();
      final containerDto = ref.read(formProvider.notifier).getContainer();
      final componentDto = ref.read(formProvider.notifier).getComponent();
      final relationshipDto = ref.read(formProvider.notifier).getRelationship();

      WidgetsBinding.instance.addPostFrameCallback((_) {
        if (containerDto != null &&
            componentDto != null &&
            relationshipDto != null) {
          ref
              .read(componentRelationshipProvider.notifier)
              .editComponentRelationshipForm(
                aContextDto: contextDto,
                aContainerDto: containerDto,
                aComponentDto: componentDto,
                aRelationshipDto: relationshipDto,
              );
        }
      });
      if (containerDto != null &&
          componentDto != null &&
          relationshipDto != null) {
        return ComponentRelationshipProviderWidget(
          contextDto: contextDto,
          containerDto: containerDto,
          componentDto: componentDto,
          relationshipDto: relationshipDto,
        );
      } else {
        // todo create error widget
        return const Text('Error ocurred');
      }
    }

    return Container();
  }
}
