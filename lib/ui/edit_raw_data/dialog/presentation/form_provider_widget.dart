import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../widgets/cancel_icon_button.dart';
import '../application/form_provider.dart';
import 'determine_form_widget.dart';

class FormProviderWidget extends ConsumerStatefulWidget {
  const FormProviderWidget({
    Key? key,
  }) : super(key: key);

  @override
  ConsumerState createState() => _FormProviderWidgetState();
}

class _FormProviderWidgetState extends ConsumerState<FormProviderWidget> {
  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final state = ref.watch(formProvider);
    if (state is! NoFormState) {
      WidgetsBinding.instance.addPostFrameCallback(
        (_) => _dialogBuilder(context),
        // (_) => Text('hiiii'),
      );
    }
    return Container();
  }

  Future<void> _dialogBuilder(BuildContext context) {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              CancelIconButton(
                callback: () {
                  ref.read(formProvider.notifier).dismiss();
                  Navigator.of(context).pop();
                },
              )
            ],
          ),
          content: Builder(builder: (context) {
            var height = MediaQuery.of(context).size.height;
            var width = MediaQuery.of(context).size.width;
            return SizedBox(
              height: height * 0.5,
              width: width * 0.5,
              child: const DetermineFormWidget(),
            );
          }),
        );
      },
    );
  }
}
