import 'package:c4_frontend_data_entry_flutter/ui/edit_raw_data/edit_container/application/container_provider.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_shared/data/component_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/container_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/relationship_dto.dart';
import '../../edit_component/application/component_provider.dart';

final formProvider =
    StateNotifierProvider<FormNotifier, CustomFormState>((ref) {
  return FormNotifier(ref);
});

class FormNotifier extends StateNotifier<CustomFormState> {
  final Ref ref;
  ContextDto? _contextDto;
  ContainerDto? _containerDto;
  ComponentDto? _componentDto;
  RelationshipDto? _relationshipDto;

  FormNotifier(this.ref) : super(NoFormState());

  ContextDto getContext() {
    ContextDto? contextDto = _contextDto;
    if (contextDto != null) {
      return contextDto;
    } else {
      throw Exception('contextDto null');
    }
  }

  ContainerDto? getContainer() {
    return _containerDto;
  }

  ComponentDto? getComponent() {
    return _componentDto;
  }

  RelationshipDto? getRelationship() {
    return _relationshipDto;
  }

  void dismiss() {
    state = NoFormState();
  }

  void loading() {
    state = LoadingState();
  }

  void newContext() {
    state = ShowNewContextFormState();
  }

  void editContext({required ContextDto contextDto}) {
    _contextDto = contextDto;
    state = ShowEditContextFormState();
  }

  void newContainer({required ContextDto contextDto}) {
    _contextDto = contextDto;
    state = ShowNewContainerFormState();
  }

  void editContainer({
    required ContextDto contextDto,
    required ContainerDto containerDto,
  }) {
    _contextDto = contextDto;
    _containerDto = containerDto;
    ref.read(containerProvider.notifier).setContextDto(contextDto);
    ref.read(containerProvider.notifier).setContainerDto(containerDto);
    state = ShowEditContainerFormState();
  }

  void newComponent({
    required ContextDto contextDto,
    required ContainerDto containerDto,
  }) {
    _contextDto = contextDto;
    _containerDto = containerDto;
    state = ShowNewComponentFormState();
  }

  void editComponent({
    required ContextDto contextDto,
    required ContainerDto containerDto,
    required ComponentDto componentDto,
  }) {
    _contextDto = contextDto;
    _containerDto = containerDto;
    _componentDto = componentDto;
    ref.read(componentProvider.notifier).setContextDto(contextDto);
    ref.read(componentProvider.notifier).setContainerDto(containerDto);
    ref.read(componentProvider.notifier).setComponentDto(componentDto);
    state = ShowEditComponentFormState();
  }

  void newContextRelationship({
    required ContextDto contextDto,
  }) {
    _contextDto = contextDto;
    state = ShowNewContextRelationshipFormState();
  }

  void editContextRelationship(
      {required ContextDto contextDto,
      required RelationshipDto relationshipDto}) {
    _contextDto = contextDto;
    _relationshipDto = relationshipDto;
    state = ShowEditContextRelationshipFormState();
  }

  void newContainerRelationship({
    required ContextDto contextDto,
    required ContainerDto containerDto,
  }) {
    _contextDto = contextDto;
    _containerDto = containerDto;
    state = ShowNewContainerRelationshipFormState();
  }

  void editContainerRelationship(
      {required ContextDto contextDto,
      required ContainerDto containerDto,
      required RelationshipDto relationshipDto}) {
    _contextDto = contextDto;
    _containerDto = containerDto;
    _relationshipDto = relationshipDto;
    state = ShowEditContainerRelationshipFormState();
  }

  void newComponentRelationship({
    required ContextDto contextDto,
    required ContainerDto containerDto,
    required ComponentDto componentDto,
  }) {
    _contextDto = contextDto;
    _containerDto = containerDto;
    _componentDto = componentDto;
    state = ShowNewComponentRelationshipFormState();
  }

  void editComponentRelationship(
      {required ContextDto contextDto,
      required ContainerDto containerDto,
      required ComponentDto componentDto,
      required RelationshipDto relationshipDto}) {
    _contextDto = contextDto;
    _containerDto = containerDto;
    _componentDto = componentDto;
    _relationshipDto = relationshipDto;
    state = ShowEditComponentRelationshipFormState();
  }
}

abstract class CustomFormState {}

class NoFormState extends CustomFormState {}

class LoadingState extends CustomFormState {}

class DismissFormState extends CustomFormState {}

class ShowNewContextFormState extends CustomFormState {}

class ShowEditContextFormState extends CustomFormState {}

class ShowDeleteContextFormState extends CustomFormState {}

class ShowNewContainerFormState extends CustomFormState {}

class ShowEditContainerFormState extends CustomFormState {}

class ShowDeleteContainerFormState extends CustomFormState {}

class ShowNewComponentFormState extends CustomFormState {}

class ShowEditComponentFormState extends CustomFormState {}

class ShowDeleteComponentFormState extends CustomFormState {}

class ShowNewContextRelationshipFormState extends CustomFormState {}

class ShowEditContextRelationshipFormState extends CustomFormState {}

class ShowNewContainerRelationshipFormState extends CustomFormState {}

class ShowEditContainerRelationshipFormState extends CustomFormState {}

class ShowNewComponentRelationshipFormState extends CustomFormState {}

class ShowEditComponentRelationshipFormState extends CustomFormState {}
