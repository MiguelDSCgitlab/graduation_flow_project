import 'package:c4_frontend_data_entry_flutter/api_raw_data/infrastructure/api_c4_shared/data/relationship_dto.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_relationships/data/relationship_input_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/container_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import '../../widgets/loading.dart';
import '../application/container_relationship_provider.dart';
import '../application/container_relationship_state.dart';
import 'container_relationship_api_delete.dart';
import 'container_relationship_api_post.dart';
import 'container_relationship_api_put.dart';
import 'container_relationship_form.dart';
import 'container_relationships_api_get.dart';

class ContainerRelationshipProviderWidget extends ConsumerWidget {
  final ContextDto contextDto;
  final ContainerDto containerDto;
  final RelationshipDto? relationshipDto;

  const ContainerRelationshipProviderWidget({
    Key? key,
    required this.contextDto,
    required this.containerDto,
    this.relationshipDto,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final state = ref.watch(containerRelationshipProvider);
    if (state is ContainerRelationshipInitialState) {
      return const LoadingWidget();
    }

    if (state is NewContainerRelationshipFormState) {
      RelationshipInputDto relationshipInputDto = RelationshipInputDto(
        fromContext: contextDto.contextId,
        fromContainer: containerDto.containerId,
        toContext: "",
        fromName: containerDto.name,
        toName: "",
        type: "Container",
      )..outgoing = true;

      return ContainerRelationshipsApiGet(
        contextDto: contextDto,
        containerDto: containerDto,
        relationshipInputDto: relationshipInputDto,
      );
    }

    if (state is EditContainerRelationshipFormState) {
      final tRelationshipDto = relationshipDto;
      if (tRelationshipDto != null) {
        RelationshipInputDto relationshipInputDto = RelationshipInputDto(
            fromContext: tRelationshipDto.fromContext,
            fromContainer: tRelationshipDto.fromContainer,
            toContext: tRelationshipDto.toContext,
            toContainer: tRelationshipDto.toContainer,
            fromName: tRelationshipDto.fromName ?? '',
            toName: tRelationshipDto.toName ?? '',
            type: tRelationshipDto.type ?? 'Container',
            description: tRelationshipDto.description)
          ..outgoing = tRelationshipDto.outgoing ?? true;

        return ContainerRelationshipForm(
          contextDto: contextDto,
          containerDto: containerDto,
          relationshipDto: relationshipDto,
          relationshipInputDto: relationshipInputDto,
        );
      } else {
        return const Text('Error occurred');
      }
    }

    if (state is PostContainerRelationshipState) {
      return ContainerRelationshipApiPost(
        containerDto: containerDto,
        contextDto: contextDto,
      );
    }

    if (state is PutContainerRelationshipState) {
      return ContainerRelationshipApiPut(
        containerDto: containerDto,
        contextDto: contextDto,
      );
    }

    if (state is DeleteContainerRelationshipState) {
      return ContainerRelationshipApiDelete(
        containerDto: containerDto,
        contextDto: contextDto,
      );
    }

    return Container();
  }
}
