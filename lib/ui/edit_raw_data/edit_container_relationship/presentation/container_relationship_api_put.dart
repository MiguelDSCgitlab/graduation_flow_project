import 'package:c4_frontend_data_entry_flutter/api_raw_data/infrastructure/api_c4_shared/data/container_dto.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_relationships/data/relationship_input_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/relationship_dto.dart';
import '../../dialog/application/form_provider.dart';
import '../application/container_relationship_provider.dart';
import '../application/future_provider_edit_container_relationship.dart';
import 'container_relationship_form.dart';

class ContainerRelationshipApiPut extends ConsumerStatefulWidget {
  final ContextDto contextDto;
  final ContainerDto containerDto;

  const ContainerRelationshipApiPut({
    Key? key,
    required this.containerDto,
    required this.contextDto,
  }) : super(key: key);

  @override
  ConsumerState createState() => _ContainerRelationshipApiPutState();
}

class _ContainerRelationshipApiPutState
    extends ConsumerState<ContainerRelationshipApiPut> {
  @override
  Widget build(BuildContext context) {
    final apiResult = ref.watch(futureProviderEditContainerRelationship);
    return apiResult.when(
      data: (data) {
        final errorDto = data.errorDto;
        if (errorDto != null) {
          RelationshipDto? relationshipDto =
              ref.read(formProvider.notifier).getRelationship();
          RelationshipInputDto relationshipInputDto = ref
              .read(containerRelationshipProvider.notifier)
              .storedRelationshipInputDto;

          return ContainerRelationshipForm(
            contextDto: widget.contextDto,
            containerDto: widget.containerDto,
            errorDto: errorDto,
            relationshipDto: relationshipDto,
            relationshipInputDto: relationshipInputDto,
          );
        } else {
          WidgetsBinding.instance.addPostFrameCallback((_) {
            Navigator.pop(context);
            ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
              backgroundColor: CupertinoColors.activeBlue,
              content: Text(
                "Container relationship updated.",
                style: TextStyle(color: CupertinoColors.white),
              ),
            ));
          });
          return const SizedBox.shrink();
        }
      },
      error: (_, __) => const Text('Something went wrong'),
      loading: () => const Text('Loading'),
    );
  }
}
