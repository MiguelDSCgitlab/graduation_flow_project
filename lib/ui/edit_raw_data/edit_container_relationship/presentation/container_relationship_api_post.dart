import 'package:c4_frontend_data_entry_flutter/api_raw_data/infrastructure/api_c4_shared/data/container_dto.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_relationships/data/relationship_input_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import '../application/container_relationship_provider.dart';
import '../application/future_provider_new_container_relationship.dart';
import 'container_relationships_api_get.dart';

class ContainerRelationshipApiPost extends ConsumerStatefulWidget {
  final ContextDto contextDto;
  final ContainerDto containerDto;

  const ContainerRelationshipApiPost({
    Key? key,
    required this.containerDto,
    required this.contextDto,
  }) : super(key: key);

  @override
  ConsumerState createState() => _ContainerRelationshipApiPutState();
}

class _ContainerRelationshipApiPutState
    extends ConsumerState<ContainerRelationshipApiPost> {
  @override
  Widget build(BuildContext context) {
    final apiResult = ref.watch(futureProviderNewContainerRelationship);
    return apiResult.when(
      data: (data) {
        final errorDto = data.errorDto;
        if (errorDto != null) {
          RelationshipInputDto relationshipInputDto = ref
              .read(containerRelationshipProvider.notifier)
              .storedRelationshipInputDto;

          return ContainerRelationshipsApiGet(
            contextDto: widget.contextDto,
            containerDto: widget.containerDto,
            errorDto: errorDto,
            relationshipInputDto: relationshipInputDto,
          );
        } else {
          WidgetsBinding.instance.addPostFrameCallback((_) {
            Navigator.pop(context);
            ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
              backgroundColor: CupertinoColors.activeBlue,
              content: Text(
                "Container relationship created.",
                style: TextStyle(color: CupertinoColors.white),
              ),
            ));
          });
          return const SizedBox.shrink();
        }
      },
      error: (_, __) => const Text('Something went wrong'),
      loading: () => const Text('Loading'),
    );
  }
}
