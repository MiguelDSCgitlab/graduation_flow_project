import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_all_containers/container_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_relationships/data/relationship_input_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/container_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';

class ContainersDropDown extends ConsumerStatefulWidget {
  final List<MinContainerDto> containers;
  final ContextDto contextDto;
  final ContainerDto containerDto;
  final RelationshipInputDto relationshipInputDto;

  const ContainersDropDown({
    super.key,
    required this.containers,
    required this.contextDto,
    required this.containerDto,
    required this.relationshipInputDto,
  });

  @override
  ConsumerState createState() => _ContextsDropDownState();
}

class _ContextsDropDownState extends ConsumerState<ContainersDropDown> {
  MinContainerDto? _selectedContainer;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text("Connect container (${widget.containerDto.name}) to:"),
        const Padding(
          padding: EdgeInsets.only(left: 16),
        ),
        Expanded(
          child: DropdownButtonHideUnderline(
            child: DropdownButton(
              hint: Text(
                "Select container",
                style: TextStyle(
                  color: Theme.of(context).colorScheme.onSurface,
                ),
              ),
              itemHeight: 80,
              value: _selectedContainer,
              items: widget.containers.map((MinContainerDto minContainerDto) {
                return DropdownMenuItem(
                  key: Key(minContainerDto.containerId),
                  value: minContainerDto,
                  child: ListTile(
                    title: Text(minContainerDto.containerName),
                    subtitle: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Context: ${minContainerDto.contextName}",
                          style: Theme.of(context).textTheme.bodySmall,
                        ),
                      ],
                    ),
                  ),
                );
              }).toList(),
              onChanged: (minContainerDto) {
                setState(() {
                  if (minContainerDto != null) {
                    _selectedContainer = minContainerDto;
                    widget.relationshipInputDto.toContext =
                        minContainerDto.contextId;
                    widget.relationshipInputDto.toContainer =
                        minContainerDto.containerId;
                    widget.relationshipInputDto.toName = minContainerDto.name;
                  }
                });
              },
              isExpanded: true,
            ),
          ),
        ),
      ],
    );
  }
}
