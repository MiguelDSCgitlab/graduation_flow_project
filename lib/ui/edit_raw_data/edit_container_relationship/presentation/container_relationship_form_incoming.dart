import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_relationships/data/relationship_input_dto.dart';

class ContainerRelationshipFormIncoming extends ConsumerStatefulWidget {
  final RelationshipInputDto relationshipInputDto;

  const ContainerRelationshipFormIncoming({
    Key? key,
    required this.relationshipInputDto,
  }) : super(key: key);

  @override
  ConsumerState createState() => _ContainerRelationshipFormIncomingState();
}

class _ContainerRelationshipFormIncomingState
    extends ConsumerState<ContainerRelationshipFormIncoming> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 12),
      child: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Padding(
              padding: EdgeInsets.only(bottom: 20.0),
              child: Text('Incoming container relationship'),
            ),
            const SizedBox(height: 32),
            Text("From container: ${widget.relationshipInputDto.fromName}"),
            Text("To container: ${widget.relationshipInputDto.toName}"),
            Text("Description: ${widget.relationshipInputDto.description}"),
          ],
        ),
      ),
    );
  }
}
