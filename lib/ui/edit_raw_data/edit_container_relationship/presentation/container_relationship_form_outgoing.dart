import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_all_containers/container_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_error/error_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_relationships/data/relationship_input_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/container_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/relationship_dto.dart';
import '../../widgets/error_form.dart';
import '../../widgets/form_submit_provider.dart';
import '../application/container_relationship_provider.dart';
import '../application/container_relationship_state.dart';
import 'containers_dropdown.dart';

class ContainerRelationshipFormOutgoing extends ConsumerStatefulWidget {
  final List<MinContainerDto>? containers;
  final ContextDto contextDto;
  final ContainerDto containerDto;
  final RelationshipDto? relationshipDto;
  final RelationshipInputDto relationshipInputDto;
  final ErrorDto? errorDto;

  const ContainerRelationshipFormOutgoing({
    Key? key,
    required this.containers,
    required this.contextDto,
    required this.containerDto,
    this.relationshipDto,
    required this.relationshipInputDto,
    this.errorDto,
  }) : super(key: key);

  @override
  ConsumerState createState() => _ContainerRelationshipFormOutgoingState();
}

class _ContainerRelationshipFormOutgoingState
    extends ConsumerState<ContainerRelationshipFormOutgoing> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final state = ref.watch(containerRelationshipProvider);

    String titleText = "";
    SizedBox sizedBox;
    Widget dropDown;
    bool showDeleteButton = false;
    bool isEditAction = false;
    String description = "";
    if (state is EditContainerRelationshipFormState ||
        state is PutContainerRelationshipState ||
        state is DeleteContainerRelationshipState) {
      titleText =
          'Edit container relationship between ${widget.relationshipDto?.fromName} and ${widget.relationshipDto?.toName}';
      sizedBox = const SizedBox.shrink();
      dropDown = Container();
      showDeleteButton = true;
      isEditAction = true;
      description = widget.relationshipInputDto.description ?? "";
    } else {
      titleText = "New container relationship";
      sizedBox = const SizedBox(
        height: 16,
      );
      dropDown = ContainersDropDown(
        contextDto: widget.contextDto,
        containerDto: widget.containerDto,
        containers: widget.containers ?? [],
        relationshipInputDto: widget.relationshipInputDto,
      );
      if (state is PostContainerRelationshipState) {
        description = widget.relationshipInputDto.description ?? "";
      }
    }

    return SingleChildScrollView(
      child: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            ErrorFormWidget(
              errorDto: widget.errorDto,
            ),
            Text(titleText),
            sizedBox,
            dropDown,
            const SizedBox(
              height: 16,
            ),
            TextFormField(
              maxLength: 40,
              initialValue: description,
              validator: (value) {
                if (value != null) {
                  if (value.isEmpty) {
                    return 'Please enter a description';
                  } else if (value.length < 3) {
                    return 'Minimum length is 3.';
                  }
                }
                return null;
              },
              onSaved: (value) =>
                  widget.relationshipInputDto.description = value!,
              decoration: const InputDecoration(labelText: 'Description'),
            ),
            const SizedBox(
              height: 32,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                showDeleteButton
                    ? ElevatedButton(
                        onPressed: () {
                          ref.read(formSubmitProvider.notifier).submit();
                          ref
                              .read(containerRelationshipProvider.notifier)
                              .deleteContainerRelationship(
                                relationshipInputDto:
                                    widget.relationshipInputDto,
                              );
                        },
                        child: const Padding(
                          padding: EdgeInsets.all(16.0),
                          child: Text('Delete'),
                        ),
                      )
                    : Container(),
                const Padding(
                  padding: EdgeInsets.all(16.0),
                ),
                ElevatedButton(
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      _formKey.currentState!.save();
                      ref.read(formSubmitProvider.notifier).submit();
                      isEditAction
                          ? ref
                              .read(containerRelationshipProvider.notifier)
                              .putContainerRelationship(
                                relationshipInputDto:
                                    widget.relationshipInputDto,
                              )
                          : ref
                              .read(containerRelationshipProvider.notifier)
                              .postContainerRelationship(
                                relationshipInputDto:
                                    widget.relationshipInputDto,
                              );
                    }
                  },
                  child: const Padding(
                    padding: EdgeInsets.all(16.0),
                    child: Text('Store'),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
