import 'package:c4_frontend_data_entry_flutter/api_raw_data/infrastructure/api_c4_error/error_dto.dart';
import 'package:c4_frontend_data_entry_flutter/api_raw_data/infrastructure/api_c4_relationships/data/relationship_input_dto.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_all_containers/container_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/container_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/relationship_dto.dart';
import 'container_relationship_form_incoming.dart';
import 'container_relationship_form_outgoing.dart';

class ContainerRelationshipForm extends ConsumerStatefulWidget {
  final List<MinContainerDto>? containers;
  final ContextDto contextDto;
  final ContainerDto containerDto;
  final RelationshipDto? relationshipDto;
  final RelationshipInputDto relationshipInputDto;
  final ErrorDto? errorDto;

  const ContainerRelationshipForm({
    Key? key,
    this.containers,
    required this.contextDto,
    required this.containerDto,
    this.relationshipDto,
    required this.relationshipInputDto,
    this.errorDto,
  }) : super(key: key);

  @override
  ConsumerState createState() => _ContainerRelationshipFormState();
}

class _ContainerRelationshipFormState
    extends ConsumerState<ContainerRelationshipForm> {
  @override
  Widget build(BuildContext context) {
    return widget.relationshipInputDto.outgoing
        ? ContainerRelationshipFormOutgoing(
            contextDto: widget.contextDto,
            containerDto: widget.containerDto,
            relationshipDto: widget.relationshipDto,
            relationshipInputDto: widget.relationshipInputDto,
            containers: widget.containers,
            errorDto: widget.errorDto,
          )
        : ContainerRelationshipFormIncoming(
            relationshipInputDto: widget.relationshipInputDto,
          );
  }
}
