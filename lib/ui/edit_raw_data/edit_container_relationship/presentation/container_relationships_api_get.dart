import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_all_containers/container_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_error/error_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_relationships/data/relationship_input_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/container_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import '../application/future_provider_get_containers.dart';
import '../domain/remove_current_container.dart';
import 'container_relationship_form.dart';

class ContainerRelationshipsApiGet extends ConsumerWidget {
  final ContextDto contextDto;
  final ContainerDto containerDto;
  final RelationshipInputDto relationshipInputDto;
  final ErrorDto? errorDto;

  const ContainerRelationshipsApiGet({
    Key? key,
    required this.contextDto,
    required this.containerDto,
    required this.relationshipInputDto,
    this.errorDto,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final containersList = ref.watch(futureProviderGetContainers);
    return containersList.when(
      data: (data) {
        List<MinContainerDto> containers = data.containers ?? [];
        List<MinContainerDto> filteredContainers =
            RemoveCurrentContainerUseCase().call(containers, containerDto.name);

        if (containers.length < 2) {
          return const Text(
              "Unable to create container relationship. No other containers exist.");
        } else {
          return ContainerRelationshipForm(
            containers: filteredContainers,
            contextDto: contextDto,
            containerDto: containerDto,
            relationshipInputDto: relationshipInputDto,
            errorDto: errorDto,
          );
        }
      },
      error: (_, __) {
        return const Text('Something went wrong');
      },
      loading: () {
        return const Text('Loading');
      },
    );
  }
}
