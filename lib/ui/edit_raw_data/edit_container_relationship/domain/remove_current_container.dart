import '../../../../api_raw_data/infrastructure/api_c4_all_containers/container_dto.dart';

/// Use case responsible for filtering contexts. It should remove all contexts matching the filter.
class RemoveCurrentContainerUseCase {
  List<MinContainerDto> call(List<MinContainerDto> containers, String filter) {
    return containers.where((container) => container.name != filter).toList();
  }
}
