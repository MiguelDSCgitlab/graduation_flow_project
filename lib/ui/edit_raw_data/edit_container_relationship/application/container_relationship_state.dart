abstract class ContainerRelationshipHandlerState {}

class ContainerRelationshipInitialState
    extends ContainerRelationshipHandlerState {}

class NewContainerRelationshipFormState
    extends ContainerRelationshipHandlerState {}

class EditContainerRelationshipFormState
    extends ContainerRelationshipHandlerState {}

class DeleteContainerRelationshipFormState
    extends ContainerRelationshipHandlerState {}

class PostContainerRelationshipState
    extends ContainerRelationshipHandlerState {}

class DeleteContainerRelationshipState
    extends ContainerRelationshipHandlerState {}

class PutContainerRelationshipState extends ContainerRelationshipHandlerState {}
