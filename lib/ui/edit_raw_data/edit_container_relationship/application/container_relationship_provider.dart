import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../dialog/application/form_provider.dart';
import 'container_relationship_notifier.dart';
import 'container_relationship_state.dart';

final containerRelationshipProvider = StateNotifierProvider<
    ContainerRelationshipNotifier, ContainerRelationshipHandlerState>((ref) {
  final notifier = ContainerRelationshipNotifier(ref);
  final state = ref.watch(formProvider);

  if (state is NoFormState) {
    notifier.initialize();
  }
  return notifier;
});
