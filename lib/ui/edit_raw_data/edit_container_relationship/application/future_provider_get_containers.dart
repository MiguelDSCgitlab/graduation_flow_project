import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_all_components/dependency_injection.dart';
import '../../../../api_raw_data/infrastructure/api_c4_all_containers/all_containers_repository.dart';
import '../../../../api_raw_data/infrastructure/api_c4_all_containers/containers_output_dto.dart';

final futureProviderGetContainers =
    FutureProvider.autoDispose<ContainersOutputDto>((ref) async {
  return await getIt<AllContainersRepository>().getContainers();
});
