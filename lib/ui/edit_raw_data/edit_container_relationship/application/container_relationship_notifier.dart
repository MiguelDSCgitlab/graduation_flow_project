import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_relationships/data/relationship_input_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/container_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/relationship_dto.dart';
import 'container_relationship_state.dart';

class ContainerRelationshipNotifier
    extends StateNotifier<ContainerRelationshipHandlerState> {
  final Ref ref;
  late ContextDto contextDto;
  late ContainerDto containerDto;
  late RelationshipDto relationshipDto;
  late RelationshipInputDto storedRelationshipInputDto;

  ContainerRelationshipNotifier(this.ref)
      : super(ContainerRelationshipInitialState());

  void newContainerRelationshipForm() {
    state = NewContainerRelationshipFormState();
  }

  void editContainerRelationshipForm() {
    state = EditContainerRelationshipFormState();
  }

  void deleteContainerRelationship(
      {required RelationshipInputDto relationshipInputDto}) {
    storedRelationshipInputDto = relationshipInputDto;
    state = DeleteContainerRelationshipState();
  }

  void putContainerRelationship(
      {required RelationshipInputDto relationshipInputDto}) {
    storedRelationshipInputDto = relationshipInputDto;
    state = PutContainerRelationshipState();
  }

  void initialize() {
    state = ContainerRelationshipInitialState();
  }

  void createInput(
      {required String description, required List<String> technology}) {
    storedRelationshipInputDto = RelationshipInputDto(
        fromContext: relationshipDto.fromContext,
        fromContainer: relationshipDto.fromContainer,
        fromComponent: null,
        toContext: relationshipDto.toContext,
        toContainer: relationshipDto.toContainer,
        toComponent: null,
        relationshipId: relationshipDto.relationshipId,
        fromName: relationshipDto.fromName ?? '',
        toName: relationshipDto.toName ?? '',
        type: relationshipDto.type ?? 'Container',
        description: description,
        technology: technology);
  }

  postContainerRelationship(
      {required RelationshipInputDto relationshipInputDto}) {
    storedRelationshipInputDto = relationshipInputDto;
    state = PostContainerRelationshipState();
  }
}
