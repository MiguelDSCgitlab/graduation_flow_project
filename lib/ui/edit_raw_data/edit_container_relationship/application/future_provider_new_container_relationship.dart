import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:uuid/uuid.dart';

import '../../../../api_raw_data/infrastructure/api_c4_all_components/dependency_injection.dart';
import '../../../../api_raw_data/infrastructure/api_c4_relationships/data/relationship_input_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_relationships/data/relationship_output_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_relationships/infrastructure/relationship_repository.dart';
import '../../../tree_view/menu_context/application/menu_contexts_refresh_provider.dart';
import '../../widgets/form_submit_provider.dart';
import 'container_relationship_provider.dart';

final futureProviderNewContainerRelationship =
    FutureProvider.autoDispose<RelationshipOutputDto>((ref) async {
  ref.watch(formSubmitProvider);
  RelationshipInputDto relationshipInputDto = ref
      .read(containerRelationshipProvider.notifier)
      .storedRelationshipInputDto;

  String? fromContainer = relationshipInputDto.fromContainer;
  if (fromContainer != null) {
    var result = await getIt<RelationshipRepository>().addContainerRelationship(
      relationshipInputDto.fromContext,
      fromContainer,
      relationshipInputDto,
    );
    String refreshId = const Uuid().v4();
    ref.read(refreshContextsProvider.notifier).state = refreshId;

    return result;
  } else {
    throw Exception('null input');
  }
});
