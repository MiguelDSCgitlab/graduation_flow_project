import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_cdn/domain/get_supported_technologies_usecase.dart';
import '../../../../api_cdn/infrastructure/output_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_all_components/dependency_injection.dart';

final futureProviderSupportedTechnologies =
    FutureProvider.autoDispose<TechnologiesOutputDto>((ref) {
  return getIt<GetSupportedTechnologiesUseCase>().call();
});
