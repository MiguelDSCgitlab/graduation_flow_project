import 'package:flutter_riverpod/flutter_riverpod.dart';

final technologyProvider =
    StateNotifierProvider<TechnologyNotifier, TechnologyState>((ref) {
  final notifier = TechnologyNotifier(ref);
  return notifier;
});

class TechnologyNotifier extends StateNotifier<TechnologyState> {
  final Ref ref;
  List<String> technologiesFromServer = [];
  List<String> searchTechnologies = [];
  List<String> selectedTechnologies = [];
  String searchInput = '';

  TechnologyNotifier(this.ref) : super(TechnologyAState());

  List<String> getDisplayTechnologies() {
    return List.from(selectedTechnologies)..addAll(searchTechnologies);
  }

  void technologyClicked(String technology) {
    if (!(selectedTechnologies.contains(technology))) {
      selectedTechnologies.add(technology);
    }
    _changeState();
  }

  void doneClicked() {
    state = DoneState();
  }

  bool isSelected(String technology) {
    return selectedTechnologies.contains(technology) ? true : false;
  }

  void searchTechnology(String text) {
    searchInput = text;
    searchTechnologies = technologiesFromServer
        .where((element) => element.startsWith(searchInput))
        .toList();
    _changeState();
  }

  void clear() {
    searchInput = '';
    searchTechnologies = technologiesFromServer;
    _changeState();
  }

  void setTechnologies(List<String> list) {
    technologiesFromServer = list;
    searchTechnologies = list;
  }

  void _changeState() {
    if (state is TechnologyAState) {
      state = TechnologyBState();
    } else {
      state = TechnologyAState();
    }
  }

  void removeTechnology(String technology) {
    if (selectedTechnologies.contains(technology)) {
      selectedTechnologies.remove(technology);
      _changeState();
    }
  }
}

abstract class TechnologyState {}

class TechnologyAState extends TechnologyState {}

class TechnologyBState extends TechnologyState {}

class DoneState extends TechnologyState {}
