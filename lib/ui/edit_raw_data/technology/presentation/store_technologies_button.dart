import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class StoreTechnologiesButton extends ConsumerWidget {
  const StoreTechnologiesButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return ElevatedButton.icon(
      icon: const Icon(Icons.save),
      onPressed: () {
        Navigator.pop(context);
      },
      label: const Padding(
        padding: EdgeInsets.all(8.0),
        child: Text('Done'),
      ),
    );
  }
}
