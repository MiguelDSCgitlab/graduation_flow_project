import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../providers.dart';
import '../application/technology_provider.dart';

class SearchBarInput extends ConsumerStatefulWidget {
  final List<String> technologies;

  const SearchBarInput({
    super.key,
    required this.technologies,
  });

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _SearchBarInputState();
}

class _SearchBarInputState extends ConsumerState<SearchBarInput> {
  final TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    ref.watch(technologyProvider);
    controller.text = ref.read(technologyProvider.notifier).searchInput;
    return TextField(
      focusNode: focusNode,
      controller: controller,
      decoration: InputDecoration(
        contentPadding:
            const EdgeInsets.symmetric(horizontal: 12, vertical: 16),
        prefixIcon: const Icon(
          Icons.search,
          color: Colors.white,
        ),
        hintText: 'Search...',
        suffixIcon: IconButton(
          icon: const Icon(
            Icons.cancel,
            color: Colors.white,
          ),
          onPressed: () {
            ref.read(technologyProvider.notifier).clear();
            setState(() {});
          },
        ),
      ),
      onChanged: (value) {
        ref.read(technologyProvider.notifier).searchTechnology(controller.text);
      },
    );
  }
}
