import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class CancelIconButton extends ConsumerWidget {
  const CancelIconButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return IconButton(
      icon: const Icon(Icons.cancel),
      onPressed: () {
        Navigator.pop(context);
      },
    );
  }
}
