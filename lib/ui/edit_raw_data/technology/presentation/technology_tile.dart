import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../application/technology_provider.dart';

class TechnologyTile extends ConsumerStatefulWidget {
  final String content;

  const TechnologyTile({
    required this.content,
    super.key,
  });

  @override
  ConsumerState createState() => _TechnologyTileState();
}

class _TechnologyTileState extends ConsumerState<TechnologyTile> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        ref.read(technologyProvider.notifier).technologyClicked(widget.content);
      },
      child: Column(
        children: [
          Row(
            children: [
              Text(
                widget.content,
                style: TextStyle(color: Colors.grey[700]),
              ),
              const Spacer(),
            ],
          ),
        ],
      ),
    );
  }
}
