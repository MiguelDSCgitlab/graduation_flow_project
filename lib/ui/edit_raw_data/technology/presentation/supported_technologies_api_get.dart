import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../application/future_provider_supported_technologies.dart';
import '../application/technology_provider.dart';
import 'search_technologies_widget.dart';

class SupportedTechnologiesApiGet extends ConsumerStatefulWidget {
  const SupportedTechnologiesApiGet({
    Key? key,
  }) : super(key: key);

  @override
  ConsumerState createState() => _SupportedTechnologiesApiGetState();
}

class _SupportedTechnologiesApiGetState
    extends ConsumerState<SupportedTechnologiesApiGet> {
  @override
  Widget build(BuildContext context) {
    final apiResult = ref.watch(futureProviderSupportedTechnologies);
    return apiResult.when(
      data: (data) {
        List<String>? technologies = data.technologies;
        ref
            .read(technologyProvider.notifier)
            .setTechnologies(technologies ?? []);
        return const SearchTechnologiesWidget();
      },
      error: (_, __) => const Text('Something went wrong'),
      loading: () => const Text('Loading'),
    );
  }
}
