import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../application/technology_provider.dart';
import 'selected_technology_tile.dart';

class TechnologiesWidget extends ConsumerStatefulWidget {
  const TechnologiesWidget({
    super.key,
  });

  @override
  ConsumerState createState() => _TechnologiesWidgetState();
}

class _TechnologiesWidgetState extends ConsumerState<TechnologiesWidget> {
  @override
  Widget build(BuildContext context) {
    ref.watch(technologyProvider);
    List<String> selectedTechnologies =
        ref.read(technologyProvider.notifier).selectedTechnologies;

    return ListView.builder(
      itemCount: selectedTechnologies.length,
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemBuilder: (BuildContext context, int index) {
        return SelectedTechnologyTile(content: selectedTechnologies[index]);
      },
    );
  }
}
