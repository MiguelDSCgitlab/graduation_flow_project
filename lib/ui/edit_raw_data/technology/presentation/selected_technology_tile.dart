import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../application/technology_provider.dart';

class SelectedTechnologyTile extends ConsumerStatefulWidget {
  final String content;

  const SelectedTechnologyTile({
    required this.content,
    super.key,
  });

  @override
  ConsumerState createState() => _SelectedTechnologyTileState();
}

class _SelectedTechnologyTileState
    extends ConsumerState<SelectedTechnologyTile> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Text(
              widget.content,
              style: const TextStyle(color: Colors.white),
            ),
            const Spacer(),
            IconButton(
              icon: const Icon(Icons.delete),
              color: Colors.white,
              padding: const EdgeInsets.all(4),
              onPressed: () {
                ref
                    .read(technologyProvider.notifier)
                    .removeTechnology(widget.content);
              },
            )
          ],
        ),
      ],
    );
  }
}
