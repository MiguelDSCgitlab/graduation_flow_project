import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../application/technology_provider.dart';
import 'cancel_icon_button.dart';
import 'search_bar_input.dart';
import 'selected_technologies_list.dart';
import 'store_technologies_button.dart';

class SearchTechnologiesWidget extends ConsumerStatefulWidget {
  const SearchTechnologiesWidget({
    super.key,
  });

  @override
  ConsumerState createState() => _SearchTechnologiesWidgetState();
}

class _SearchTechnologiesWidgetState
    extends ConsumerState<SearchTechnologiesWidget> {
  @override
  Widget build(BuildContext context) {
    ref.watch(technologyProvider);
    List<String> technologies =
        ref.read(technologyProvider.notifier).searchTechnologies;
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            CancelIconButton(),
          ],
        ),
        const Padding(
          padding: EdgeInsets.only(top: 16),
        ),
        const Text('Technologies'),
        const Padding(
          padding: EdgeInsets.only(top: 16),
        ),
        SearchBarInput(technologies: technologies),
        const Padding(
          padding: EdgeInsets.only(top: 8.0),
        ),
        const Flexible(fit: FlexFit.tight, child: SelectedTechnologiesWidget()),
        const Padding(
          padding: EdgeInsets.only(top: 8),
        ),
        // const Spacer(),
        const Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            SizedBox(
              width: 16,
            ),
            StoreTechnologiesButton(),
          ],
        ),
      ],
    );
  }
}
