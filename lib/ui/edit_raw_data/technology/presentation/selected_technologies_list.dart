import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../application/technology_provider.dart';
import 'selected_technology_tile.dart';
import 'technology_tile.dart';

class SelectedTechnologiesWidget extends ConsumerStatefulWidget {
  const SelectedTechnologiesWidget({
    super.key,
  });

  @override
  ConsumerState createState() => _TechnologiesWidgetState();
}

class _TechnologiesWidgetState
    extends ConsumerState<SelectedTechnologiesWidget> {
  @override
  Widget build(BuildContext context) {
    ref.watch(technologyProvider);
    List<String> selectedTechnologies =
        ref.read(technologyProvider.notifier).selectedTechnologies;
    List<String> searchTechnologies =
        ref.read(technologyProvider.notifier).searchTechnologies;
    List<String> displayTechnologies =
        ref.read(technologyProvider.notifier).getDisplayTechnologies();

    return ListView.builder(
      itemCount: displayTechnologies.length,
      shrinkWrap: true,
      itemBuilder: (BuildContext context, int index) {
        if (index < selectedTechnologies.length) {
          return SelectedTechnologyTile(content: selectedTechnologies[index]);
        } else {
          if (index == selectedTechnologies.length &&
              selectedTechnologies.isNotEmpty) {
            return Column(
              children: [
                const Divider(
                  height: 2,
                  color: Colors.white60,
                ),
                TechnologyTile(
                    content:
                        searchTechnologies[index - selectedTechnologies.length])
              ],
            );
          } else {
            return TechnologyTile(
                content:
                    searchTechnologies[index - selectedTechnologies.length]);
          }
        }
      },
    );
  }
}
