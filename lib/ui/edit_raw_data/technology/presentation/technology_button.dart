import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'search_dialog.dart';

class TechnologyIconButton extends ConsumerWidget {
  final String title;

  const TechnologyIconButton({required this.title, super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return IconButton(
      onPressed: () {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return const SupportedTechnologiesDialog();
          },
        );
      },
      icon: const Icon(Icons.add),
    );
  }
}
