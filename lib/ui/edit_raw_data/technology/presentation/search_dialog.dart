import 'package:flutter/material.dart';

import 'supported_technologies_api_get.dart';

class SupportedTechnologiesDialog extends StatelessWidget {
  const SupportedTechnologiesDialog({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: Builder(builder: (context) {
        var height = MediaQuery.of(context).size.height;
        var width = MediaQuery.of(context).size.width;
        return SizedBox(
          height: height * 0.5,
          width: width * 0.5,
          child: const SupportedTechnologiesApiGet(),
        );
      }),
    );
  }
}
