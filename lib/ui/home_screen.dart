import 'package:c4_frontend_data_entry_flutter/ui/browser/browser_widget.dart';
import 'package:c4_frontend_data_entry_flutter/ui/explorer/explorer_widget.dart';
import 'package:c4_frontend_data_entry_flutter/ui/nav_bar/nav_bar_widget.dart';
import 'package:c4_frontend_data_entry_flutter/ui/providers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'edit_raw_data/dialog/presentation/form_provider_widget.dart';
import 'nav_bar/application/providers.dart';

class HomeScreen extends ConsumerWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    NavPage currentPage = ref.watch(currentPageProvider);
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        focusNode.unfocus();
      },
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: GestureDetector(
          child: Row(
            children: [
              const NavBarWidget(),
              currentPage == NavPage.FILES ? ExplorerWidget() : SizedBox(),
              const Expanded(
                child: Column(
                  children: [
                    FormProviderWidget(),
                    Expanded(child: BrowserWidget()),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
