import 'dart:ui';

const darkColor = Color(0xFF333333);
const blackColor = Color(0xFF252526);
const greyColor = Color(0xFF474747);
const whiteTextColor = Color(0xFFC5C5C5);
const lightBlackColor = Color(0xFF2D2D2D);
const lightGreyColor = Color(0xFF3C3C3C);
