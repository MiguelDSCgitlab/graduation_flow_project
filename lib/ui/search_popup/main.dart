import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'presentation/search_overlay.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ProviderScope(
      child: MaterialApp(
        home: Scaffold(
          appBar: AppBar(
            title: const Text('Main App'),
          ),
          body: Center(
            child: Builder(
              builder: (context) {
                return ElevatedButton(
                  onPressed: () {
                    showGeneralDialog(
                      context: context,
                      barrierDismissible: true,
                      barrierLabel: 'Search...',
                      pageBuilder: (context, animation, secondaryAnimation) {
                        return const SearchOverlay(height: 400, width: 600);
                      },
                    );
                  },
                  child: const Text('Search'),
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}
