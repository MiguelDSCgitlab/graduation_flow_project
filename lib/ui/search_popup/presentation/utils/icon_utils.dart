import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class IconUtils {
  static Widget getIcon(String type) {
    String assetName = '';
    switch (type) {
      case 'Component':
        assetName = 'assets/icons/ui/c3.svg';
        break;
      case 'Container':
        assetName = 'assets/icons/ui/c2.svg';
        break;
      default:
        assetName = 'assets/icons/ui/c1.svg';
    }
    return SvgPicture.asset(assetName, width: 24, height: 24);
  }
}
