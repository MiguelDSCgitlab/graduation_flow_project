import 'package:c4_frontend_data_entry_flutter/ui/search_popup/presentation/search_popup.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class SearchOverlay extends ConsumerWidget {
  final double width;
  final double height;

  const SearchOverlay({super.key, this.height = 400, this.width = 600});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return GestureDetector(
      onTap: () => Navigator.pop(context),
      child: Material(
        color: Colors.transparent,
        child: Center(
          child: GestureDetector(
            onTap: () {},
            child: Container(
              color: Colors.white,
              width: width,
              height: height,
              child: SearchPopup(darkMode: false),
            ),
          ),
        ),
      ),
    );
  }

  Future<Object?> handleOnSearchTap(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;

    final popupHeight = screenHeight * 0.6;
    final popupWidth = screenWidth * 0.4;

    return showGeneralDialog(
      context: context,
      barrierDismissible: true,
      barrierLabel: 'Search...',
      pageBuilder: (context, animation, secondaryAnimation) {
        return SearchOverlay(
          height: popupHeight > 400 ? popupHeight : 400,
          width: popupWidth > 300 ? popupWidth : 300,
        );
      },
    );
  }
}
