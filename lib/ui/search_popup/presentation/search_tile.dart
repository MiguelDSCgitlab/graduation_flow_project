import 'package:c4_frontend_data_entry_flutter/ui/search_popup/model/search_result.dart';
import 'package:c4_frontend_data_entry_flutter/ui/search_popup/presentation/utils/icon_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../application/providers.dart';

class SearchTile extends ConsumerWidget {
  final SearchResult result;
  final bool darkMode;

  const SearchTile(this.result, this.darkMode, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final provider = ref.watch(searchProvider);

    Color textColor = darkMode ? Colors.white : Colors.black;
    Color iconColor = darkMode ? Colors.white : Colors.black;

    return ListTile(
      textColor: textColor,
      title: Text(result.name),
      leading: IconUtils.getIcon(result.type),
      trailing: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          IconButton(
            icon: Icon(Icons.zoom_in_map, color: iconColor),
            onPressed: () => provider.navigateTo(result, ref),
          ),
          if (provider.selectedContainer == null || result.type != 'Component')
            IconButton(
              icon: Icon(Icons.add, color: iconColor),
              onPressed: () => provider.selectResult(result),
            ),
        ],
      ),
    );
  }
}
