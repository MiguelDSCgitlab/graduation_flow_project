import 'package:c4_frontend_data_entry_flutter/ui/search_popup/presentation/utils/icon_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../application/providers.dart';

class SelectedItems extends ConsumerWidget {
  final bool darkMode;

  const SelectedItems(this.darkMode, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    Color textColor = darkMode ? Colors.white : Colors.black;
    Color iconColor = darkMode ? Colors.white : Colors.black;

    final provider = ref.watch(searchProvider);
    return Column(
      children: [
        if (provider.selectedContext != null)
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              IconUtils.getIcon(provider.selectedContext!.type),
              Text(provider.selectedContext!.name,
                  style: TextStyle(color: textColor)),
              IconButton(
                icon: Icon(Icons.remove, color: iconColor),
                onPressed: () {
                  provider.selectContext(null);
                },
              ),
            ],
          ),
        if (provider.selectedContainer != null)
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              IconUtils.getIcon(provider.selectedContainer!.type),
              Text(provider.selectedContainer!.name,
                  style: TextStyle(color: textColor)),
              IconButton(
                icon: Icon(Icons.remove, color: iconColor),
                onPressed: () {
                  provider.selectContainer(null);
                },
              ),
            ],
          ),
      ],
    );
  }
}
