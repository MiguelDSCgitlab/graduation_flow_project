import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../providers.dart';

class SearchBarInput extends ConsumerWidget {
  final bool isLoading;
  final Function(String) onSearch;
  final bool darkMode;

  const SearchBarInput(
      {super.key,
      required this.isLoading,
      required this.onSearch,
      required this.darkMode});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    Color textColor = darkMode ? Colors.white : Colors.black;

    return TextField(
      style: TextStyle(color: textColor),
      focusNode: focusNode,
      onChanged: onSearch,
      decoration: InputDecoration(
        contentPadding:
            const EdgeInsets.symmetric(horizontal: 12, vertical: 16),
        prefixIcon: const Icon(Icons.search),
        hintText: 'Search...',
        hintStyle: TextStyle(color: textColor),
        suffixIcon: isLoading
            ? const Padding(
                padding: EdgeInsets.all(12.0),
                child: SizedBox(
                  width: 20,
                  height: 20,
                  child: CircularProgressIndicator(strokeWidth: 2.0),
                ),
              )
            : null,
      ),
    );
  }
}
