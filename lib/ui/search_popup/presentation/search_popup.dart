import 'package:c4_frontend_data_entry_flutter/ui/search_popup/application/providers.dart';
import 'package:c4_frontend_data_entry_flutter/ui/search_popup/model/search_result.dart';
import 'package:c4_frontend_data_entry_flutter/ui/search_popup/presentation/search_bar_input.dart';
import 'package:c4_frontend_data_entry_flutter/ui/search_popup/presentation/search_tile.dart';
import 'package:c4_frontend_data_entry_flutter/ui/search_popup/presentation/selected_items.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class SearchPopup extends ConsumerWidget {
  final bool darkMode;
  bool initialSearchDone = false;

  SearchPopup({Key? key, required this.darkMode}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final provider = ref.watch(searchProvider);

    WidgetsBinding.instance!.addPostFrameCallback((_) {
      if (!initialSearchDone) {
        provider.search("");
        initialSearchDone = true;
      }
    });

    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        children: [
          SearchBarInput(
            isLoading: provider.isLoading,
            onSearch: (value) => provider.search(value),
            darkMode: darkMode,
          ),
          SelectedItems(darkMode),
          if (provider.errorMessage.isNotEmpty) Text(provider.errorMessage),
          if (!provider.isLoading && provider.errorMessage.isEmpty)
            Expanded(
              child: ListView.builder(
                itemCount: provider.results.length,
                itemBuilder: (context, index) {
                  SearchResult result = provider.results[index];
                  return SearchTile(result, darkMode);
                },
              ),
            ),
        ],
      ),
    );
  }
}
