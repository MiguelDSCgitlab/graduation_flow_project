import 'dart:convert';

import 'package:http/http.dart' as http;

import '../model/search_result.dart';

class SearchApi {
  // Todo: Change this to Environment variable or something like that
  // static const baseUrl = 'http://10.0.2.2:8085';
  static const baseUrl = 'https://c4enhanced.nl/orchestrator';

  static Future<List<SearchResult>> searchContext(String query) async {
    return search('$baseUrl/search/contexts?query=$query');
  }

  static Future<List<SearchResult>> searchContainer(
      String query, String selectedContext) async {
    return search(
        "$baseUrl/search/contexts/$selectedContext/containers?query=$query");
  }

  static Future<List<SearchResult>> searchComponent(
      String query, String selectedContext, String selectedContainer) async {
    return search(
        '$baseUrl/search/contexts/$selectedContext/containers/$selectedContainer/components?query=$query');
  }

  static Future<List<SearchResult>> search(String url) async {
    try {
      final response = await http.get(Uri.parse(url));

      if (response.statusCode == 200) {
        Iterable jsonResponse = json.decode(response.body);
        return jsonResponse.map((item) => SearchResult.fromJson(item)).toList();
      } else {
        throw Exception('Failed to load search results');
      }
    } catch (e) {
      throw Exception('Network error');
    }
  }
}
