import 'dart:convert';

import 'package:c4_frontend_data_entry_flutter/ui/browser/browser_body/diagram/application/diagram_path.dart';
import 'package:c4_frontend_data_entry_flutter/ui/browser/browser_tabs/application/providers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../nav_bar/application/providers.dart';
import '../api/search_api.dart';
import '../model/search_result.dart';

class SearchProvider with ChangeNotifier {
  List<SearchResult> _results = [];
  bool _isLoading = false;
  String _errorMessage = '';

  String _lastQuery = '';

  String get lastQuery => _lastQuery;

  List<SearchResult> get results => _results;

  bool get isLoading => _isLoading;

  String get errorMessage => _errorMessage;

  SearchResult? _selectedContext;

  SearchResult? get selectedContext => _selectedContext;

  SearchResult? _selectedContainer;

  SearchResult? get selectedContainer => _selectedContainer;

  SearchProvider() {
    loadSelectedContext();
    loadSelectedContainer();
  }

  void search(String query) async {
    loading();
    final searchQuery = query.isEmpty ? _lastQuery : query;
    _lastQuery = searchQuery;

    if (selectedContext == null) {
      searchContext(query);
      return;
    } else if (selectedContainer == null && selectedContext != null) {
      searchContainer(query, selectedContext!.id);
      return;
    } else {
      searchComponent(query, selectedContext!.id, selectedContainer!.id);
    }
  }

  void navigateTo(SearchResult result, WidgetRef ref) {
    DiagramPath? path;

    switch (result.type) {
      case "Software System":
        path = ContextDiagramPath(result.id);
        break;
      case "Container":
        if (_selectedContext != null) {
          path = ContainerDiagramPath(_selectedContext!.id, result.id);
        }
        break;
      default:
        if (_selectedContext != null && _selectedContainer != null) {
          path = ComponentDiagramPath(
              _selectedContext!.id, _selectedContainer!.id, result.id);
        }
        break;
    }

    if (path != null) {
      ref.read(currentPageProvider.notifier).state = NavPage.FILES;
      ref.read(tabsListProvider.notifier).addTab(path, selectIt: true);
    } else {
      print('Failed to generate path for result type: ${result.type}');
    }
  }

  void searchContext(String query) async {
    try {
      _results = await SearchApi.searchContext(query);
      finishLoading();
    } catch (e) {
      _errorMessage = 'Network error';
    }
  }

  void searchContainer(String query, String selectedContext) async {
    try {
      _results = await SearchApi.searchContainer(query, selectedContext);
      finishLoading();
    } catch (e) {
      _errorMessage = 'Network error';
    }
  }

  void searchComponent(
      String query, String selectedContext, String selectedContainer) async {
    try {
      _results = await SearchApi.searchComponent(
          query, selectedContext, selectedContainer);
      finishLoading();
    } catch (e) {
      _errorMessage = 'Network error';
    }
  }

  void selectResult(SearchResult result) {
    if (result.type == "Software System") {
      selectContext(result);
    } else if (result.type == "Container") {
      selectContainer(result);
    }
  }

  void selectContext(SearchResult? result) {
    _selectedContext = result;
    saveSelectedContext(result);
    if (result == null) {
      searchContext("");
      return;
    }
    try {
      searchContainer("", selectedContext!.id);
    } catch (e) {
      print("Error searching for containers: $e");
    }
    notifyListeners();
  }

  void selectContainer(SearchResult? result) {
    _selectedContainer = result;
    saveSelectedContainer(result);
    if (result == null) {
      searchContainer("", selectedContext!.id);
      return;
    }
    try {
      searchComponent("", selectedContext!.id, selectedContainer!.id);
    } catch (e) {
      print("Error searching for components: $e");
    }
    notifyListeners();
  }

  void saveSelectedContext(SearchResult? context) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(
        'selectedContext', context != null ? jsonEncode(context.toJson()) : '');
    _selectedContext = context;
    notifyListeners();
  }

  void saveSelectedContainer(SearchResult? container) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('selectedContainer',
        container != null ? jsonEncode(container.toJson()) : '');
    _selectedContainer = container;
    notifyListeners();
  }

  Future<void> loadSelectedContext() async {
    final prefs = await SharedPreferences.getInstance();
    final contextString = prefs.getString('selectedContext');
    if (contextString != null && contextString.isNotEmpty) {
      final contextJson = jsonDecode(contextString);
      _selectedContext =
          SearchResult.fromJson(contextJson as Map<String, dynamic>);
      notifyListeners();
    }
  }

  Future<void> loadSelectedContainer() async {
    final prefs = await SharedPreferences.getInstance();
    final containerString = prefs.getString('selectedContainer');
    if (containerString != null && containerString.isNotEmpty) {
      final containerJson = jsonDecode(containerString);
      _selectedContainer =
          SearchResult.fromJson(containerJson as Map<String, dynamic>);
    }
    notifyListeners();
  }

  void loading() {
    _isLoading = true;
    _errorMessage = '';
    notifyListeners();
  }

  void finishLoading() {
    _isLoading = false;
    notifyListeners();
  }

  void deSelectAll() {
    selectContext(null);
    selectContainer(null);
    search(_lastQuery);
    notifyListeners();
  }

  void deselectContainer() {
    selectContainer(null);
    search(_lastQuery);
    notifyListeners();
  }
}
