import 'package:c4_frontend_data_entry_flutter/ui/search_popup/application/search_provider.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final searchProvider =
    ChangeNotifierProvider<SearchProvider>((ref) => SearchProvider());
