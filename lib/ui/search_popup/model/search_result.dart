class SearchResult {
  final String name;
  final String id;
  final String type;

  SearchResult({required this.name, required this.id, required this.type});

  factory SearchResult.fromJson(Map<String, dynamic> json) {
    return SearchResult(
      name: json['name'],
      id: json['id'],
      type: json['type'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'id': id,
      'type': type,
    };
  }
}
