import 'package:c4_frontend_data_entry_flutter/ui/browser/browser_tabs/application/providers.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../color_scheme/colors.dart';

class CurrentLocationBar extends ConsumerWidget {
  const CurrentLocationBar({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    var currentPath = ref.watch(currentPathProvider);
    return Container(
      height: 30,
      width: double.infinity,
      color: darkColor,
      padding: const EdgeInsets.symmetric(horizontal: 10),
      alignment: Alignment.centerLeft,
      child: Text((currentPath ?? "").toString(),
          style: const TextStyle(
            fontSize: 12,
            fontWeight: FontWeight.bold,
            color: whiteTextColor,
          )),
    );
  }
}
