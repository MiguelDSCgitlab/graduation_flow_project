import 'package:c4_frontend_data_entry_flutter/ui/color_scheme/colors.dart';
import 'package:c4_frontend_data_entry_flutter/ui/explorer/explorer_body/presentation/current_location_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../tree_view/menu_context/presentation/menu_contexts_api.dart';

class ExplorerBodyWidget extends StatelessWidget {
  const ExplorerBodyWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const CurrentLocationBar(),
        Expanded(
          child: Container(
            color: blackColor,
            child: const GetContextsApi(),
          ),
        ),
      ],
    );
  }
}
