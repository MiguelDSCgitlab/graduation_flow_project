import 'package:c4_frontend_data_entry_flutter/ui/explorer/explorer_body/explorer_body_widget.dart';
import 'package:c4_frontend_data_entry_flutter/ui/explorer/explorer_finder/explorer_finder_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ExplorerWidget extends StatelessWidget {
  const ExplorerWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const SizedBox(
        width: 250,
        child: Column(
          children: [
            ExplorerFinderWidget(),
            Expanded(child: ExplorerBodyWidget()),
          ],
        ));
  }
}
