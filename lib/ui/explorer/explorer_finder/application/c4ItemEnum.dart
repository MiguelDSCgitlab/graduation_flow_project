enum C4Item {
  contexts,
  context,
  container,
  component,
  contextRelationship,
  containerRelationship,
  componentRelationship
}
