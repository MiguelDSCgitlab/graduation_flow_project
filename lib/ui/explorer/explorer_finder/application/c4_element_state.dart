abstract class C4ElementState {}

class ContextsState extends C4ElementState {}

class ContextState extends C4ElementState {}

class ContainerState extends C4ElementState {}

class ComponentState extends C4ElementState {}

class ContextRelationshipState extends C4ElementState {}

class ContainerRelationshipState extends C4ElementState {}

class ComponentRelationshipState extends C4ElementState {}
