import 'package:c4_frontend_data_entry_flutter/api_raw_data/infrastructure/api_c4_shared/data/component_dto.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../api_raw_data/infrastructure/api_c4_shared/data/container_dto.dart';
import '../../../../api_raw_data/infrastructure/api_c4_shared/data/context_dto.dart';
import '../presentation/popup_menu_text.dart';
import 'c4ItemEnum.dart';
import 'c4_element_state.dart';

class C4ElementNotifier extends StateNotifier<C4ElementState> {
  final Ref ref;
  String contextName = '';
  String containerName = '';
  String componentName = '';
  String addText = 'Context';
  ContextDto? _contextDto;
  ContainerDto? _containerDto;
  ComponentDto? _componentDto;

  C4ElementNotifier(this.ref) : super(ContextsState());

  List<PopupMenuEntry<C4Item>> getOptions() {
    if (state is ContextsState) {
      return <PopupMenuEntry<C4Item>>[
        const PopupMenuItem<C4Item>(
          value: C4Item.context,
          child: Text(
            "New context",
          ),
        ),
      ];
    }

    if (state is ContextState) {
      return <PopupMenuEntry<C4Item>>[
        const PopupMenuItem<C4Item>(
          value: C4Item.context,
          child: Text(
            "New context",
          ),
        ),
        const PopupMenuDivider(),
        PopupMenuText(content: contextName),
        const PopupMenuItem<C4Item>(
            value: C4Item.container,
            child: Text(
              'New container',
            )),
        const PopupMenuItem<C4Item>(
            value: C4Item.contextRelationship,
            child: Text('New context relation')),
      ];
    }

    if (state is ContainerState) {
      return <PopupMenuEntry<C4Item>>[
        const PopupMenuItem<C4Item>(
          value: C4Item.context,
          child: Text(
            "New context",
          ),
        ),
        const PopupMenuDivider(),
        PopupMenuText(content: containerName),
        const PopupMenuItem<C4Item>(
            value: C4Item.component,
            child: Text(
              'New component',
            )),
        const PopupMenuItem<C4Item>(
            value: C4Item.containerRelationship,
            child: Text('New container relation')),
      ];
    }

    if (state is ComponentState) {
      return <PopupMenuEntry<C4Item>>[
        const PopupMenuItem<C4Item>(
          value: C4Item.context,
          child: Text(
            "New context",
          ),
        ),
        const PopupMenuDivider(),
        PopupMenuText(content: componentName),
        const PopupMenuItem<C4Item>(
            value: C4Item.componentRelationship,
            child: Text('New component relation')),
      ];
    }

    return <PopupMenuEntry<C4Item>>[
      const PopupMenuItem<C4Item>(
        value: C4Item.context,
        child: Text(
          "New context",
        ),
      ),
    ];
  }

  void selectItem(C4Item item) {
    print(item);
    if (item == C4Item.contexts) {
      state = ContextsState();
      addText = 'Contexts';
    }
    if (item == C4Item.context) {
      state = ContextState();
      addText = 'Context';
    }
    if (item == C4Item.container) {
      state = ContainerState();
      addText = 'Container';
    }
    if (item == C4Item.component) {
      state = ComponentState();
      addText = 'Component';
    }
    if (item == C4Item.contextRelationship) {
      state = ContextRelationshipState();
      addText = 'Relationship';
    }
    if (item == C4Item.containerRelationship) {
      state = ContainerRelationshipState();
      addText = 'Relationship';
    }
    if (item == C4Item.componentRelationship) {
      state = ComponentRelationshipState();
      addText = 'Relationship';
    }
  }

  void contextsState() {
    contextName = 'Context';
    containerName = '';
    componentName = '';
    state = ContextsState();
  }

  void contextState(ContextDto contextDto) {
    contextName = contextDto.name;
    _contextDto = contextDto;
    state = ContextState();
  }

  void containerState(ContextDto contextDto, ContainerDto containerDto) {
    _contextDto = contextDto;
    _containerDto = containerDto;
    contextName = contextDto.name;
    containerName = containerDto.name;
    state = ContainerState();
  }

  void componentState(ContextDto contextDto, ContainerDto containerDto,
      ComponentDto componentDto) {
    _contextDto = contextDto;
    _containerDto = containerDto;
    _componentDto = componentDto;
    contextName = contextDto.name;
    containerName = containerDto.name;
    componentName = componentDto.name;
    state = ComponentState();
  }

  ContextDto getContext() {
    final context = _contextDto;
    if (context != null) {
      return context;
    } else {
      throw Exception('ContextDto null');
    }
  }

  ContainerDto getContainer() {
    final container = _containerDto;
    if (container != null) {
      return container;
    } else {
      throw Exception('ContainerDto null');
    }
  }

  ComponentDto getComponent() {
    final component = _componentDto;
    if (component != null) {
      return component;
    } else {
      throw Exception('ComponentDto null');
    }
  }
}
