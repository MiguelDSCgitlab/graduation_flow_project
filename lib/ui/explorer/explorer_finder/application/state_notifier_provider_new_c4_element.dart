import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'c4_element_notifier.dart';
import 'c4_element_state.dart';

final newC4ElementProvider =
    StateNotifierProvider<C4ElementNotifier, C4ElementState>((ref) {
  final notifier = C4ElementNotifier(ref);
  return notifier;
});
