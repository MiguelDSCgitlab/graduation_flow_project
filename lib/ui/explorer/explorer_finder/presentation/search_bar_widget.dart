import 'package:c4_frontend_data_entry_flutter/ui/color_scheme/colors.dart';
import 'package:c4_frontend_data_entry_flutter/ui/explorer/explorer_finder/presentation/add_button.dart';
import 'package:c4_frontend_data_entry_flutter/ui/search_popup/presentation/search_overlay.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../application/state_notifier_provider_new_c4_element.dart';

class SearchBarWidget extends ConsumerWidget {
  const SearchBarWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    ref.watch(newC4ElementProvider);
    // var options = ref.watch(newC4ElementProvider.notifier).getOptions();

    return Container(
      color: blackColor,
      padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 12),
      child: Row(
        children: [
          const AddButton(),
          const Spacer(),
          IconButton(
            onPressed: () => const SearchOverlay().handleOnSearchTap(context),
            icon: const Icon(
              CupertinoIcons.search,
              color: whiteTextColor,
            ),
          ),
        ],
      ),
    );
  }
}
