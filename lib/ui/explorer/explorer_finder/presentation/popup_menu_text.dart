import 'package:flutter/material.dart';

class PopupMenuText extends PopupMenuEntry<Never> {
  final String content;

  const PopupMenuText({super.key, this.height = 16.0, required this.content});

  @override
  final double height;

  @override
  bool represents(void value) => false;

  @override
  State<PopupMenuText> createState() => _PopupMenuTextState();
}

class _PopupMenuTextState extends State<PopupMenuText> {
  @override
  Widget build(BuildContext context) => Padding(
        padding: const EdgeInsets.only(left: 8),
        child: Text(widget.content),
      );
}
