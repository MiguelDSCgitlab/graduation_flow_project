import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../application/c4_element_state.dart';
import '../application/state_notifier_provider_new_c4_element.dart';

class ActionText extends ConsumerStatefulWidget {
  const ActionText({super.key});

  @override
  ConsumerState createState() => _ActionTextState();
}

class _ActionTextState extends ConsumerState<ActionText> {
  double fontSize = 8.0;

  @override
  Widget build(BuildContext context) {
    ref.watch(newC4ElementProvider);
    var state = ref.read(newC4ElementProvider);
    if (state is ContextsState) {
      return Text(
        "Context",
        style: TextStyle(fontSize: fontSize),
      );
    }
    if (state is ContextState) {
      return Text(
        "Context",
        style: TextStyle(fontSize: fontSize),
      );
    }
    if (state is ContainerState) {
      return Text(
        "Container",
        style: TextStyle(fontSize: fontSize),
      );
    }
    if (state is ComponentState) {
      return Text(
        "Component",
        style: TextStyle(fontSize: fontSize),
      );
    }
    return Text(
      'Relationship',
      style: TextStyle(fontSize: fontSize),
    );
  }
}
