import 'package:c4_frontend_data_entry_flutter/ui/edit_raw_data/dialog/application/form_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class AddButton extends ConsumerStatefulWidget {
  const AddButton({
    Key? key,
  }) : super(key: key);

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _AddButtonState();
}

class _AddButtonState extends ConsumerState<AddButton> {
  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: () {
        ref.read(formProvider.notifier).newContext();
      },
      icon: const Icon(Icons.add),
    );
  }
}
