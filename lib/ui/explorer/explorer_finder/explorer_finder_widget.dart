import 'package:c4_frontend_data_entry_flutter/ui/color_scheme/colors.dart';
import 'package:c4_frontend_data_entry_flutter/ui/explorer/explorer_finder/presentation/search_bar_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ExplorerFinderWidget extends StatelessWidget {
  const ExplorerFinderWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      color: darkColor,
      child: const Column(
        children: [
          Expanded(child: SearchBarWidget()),
        ],
      ),
    );
  }
}
