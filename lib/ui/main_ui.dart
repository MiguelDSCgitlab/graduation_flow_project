import 'package:c4_frontend_data_entry_flutter/ui/color_scheme/colors.dart';
import 'package:c4_frontend_data_entry_flutter/ui/home_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

void main() {
  runApp(
    const ProviderScope(
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'C4 CRUD App',
      themeMode: ThemeMode.system,
      theme: ThemeData(
        scaffoldBackgroundColor: greyColor,
        // colorScheme: ColorScheme.fromSwatch(primarySwatch: Colors.blue),
        // colorSchemeSeed: Colors.green,
        useMaterial3: true,
        // inputDecorationTheme: InputDecorationTheme(
        //   filled: true,
        //   fillColor: Theme.of(context).colorScheme.onPrimary,
        // ),
      ),
      home: const SafeArea(child: HomeScreen()),
    );
  }
}
