import 'package:c4_frontend_data_entry_flutter/ui/browser/browser_body/browser_body_widget.dart';
import 'package:c4_frontend_data_entry_flutter/ui/browser/browser_tabs/browser_tabs_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BrowserWidget extends StatelessWidget {
  const BrowserWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Column(
      children: [
        BrowserTabsWidget(),
        Expanded(child: BrowserBodyWidget()),
      ],
    );
  }
}
