import 'package:c4_frontend_data_entry_flutter/ui/browser/browser_tabs/presentation/browser_nav_buttons.dart';
import 'package:c4_frontend_data_entry_flutter/ui/browser/browser_tabs/presentation/browser_tabs_list.dart';
import 'package:c4_frontend_data_entry_flutter/ui/color_scheme/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../nav_bar/application/providers.dart';

class BrowserTabsWidget extends ConsumerWidget {
  const BrowserTabsWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    var page = ref.watch(currentPageProvider);
    if (page != NavPage.FILES) {
      return const SizedBox();
    }
    return Container(
      color: darkColor,
      height: 50,
      child: const Column(
        children: [
          Spacer(),
          Row(
            children: [
              BrowserNavButtons(),
              Expanded(child: BrowserTabList()),
            ],
          )
        ],
      ),
    );
  }
}
