import 'package:flame/components.dart';
import 'package:flame/experimental.dart';

import '../../browser_body/diagram/domain/graph/custom_graph.dart';

class FlameTabInformation {
  Vector2 cameraPosition = Vector2.zero();
  double cameraZoom = 0.5;
  Rectangle? cameraBounds;
  CustomGraph graph;

  FlameTabInformation(this.graph) {
    cameraZoom = 0.5;
    cameraBounds = Rectangle.fromPoints(
        Vector2(graph.boundingBox![2], graph.boundingBox![0]),
        Vector2(graph.boundingBox![3], graph.boundingBox![1]));
    cameraPosition = cameraBounds!.center;
  }
}
