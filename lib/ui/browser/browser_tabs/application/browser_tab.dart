import 'dart:async';
import 'dart:ui';

import 'package:c4_frontend_data_entry_flutter/ui/browser/browser_body/diagram/application/browser_uri_api.dart';
import 'package:c4_frontend_data_entry_flutter/ui/browser/browser_body/diagram/domain/graph/custom_graph.dart';

import '../../browser_body/diagram/application/diagram_path.dart';
import 'flame_information.dart';

class BrowserTab {
  List<DiagramPath> _pathHistory = [];
  int _currentIndex = 0;
  StreamSubscription<CustomGraph>? dataSub;
  DiagramPath path;
  bool isSelected;
  FlameTabInformation? info;

  BrowserTab({
    required this.path,
    this.isSelected = false,
  }) {
    _pathHistory.add(path);
    _updatePath();
  }

  void navigate(DiagramPath newPath) {
    info = null;
    _pathHistory = _pathHistory.sublist(0, _currentIndex + 1);
    _pathHistory.add(newPath);
    _currentIndex++;
    _updatePath();
  }

  void _updatePath() {
    path = _pathHistory[_currentIndex];
  }

  void goBack() {
    if (!canGoBack()) {
      throw Exception("Cannot go back");
    }
    info = null;
    _currentIndex--;
    _updatePath();
  }

  void goForward() {
    if (!canGoForward()) {
      throw Exception("Cannot go forward");
    }
    info = null;
    _currentIndex++;
    _updatePath();
  }

  bool canGoBack() {
    return _currentIndex > 0;
  }

  bool canGoForward() {
    return _currentIndex < _pathHistory.length - 1;
  }

  void loadTab(VoidCallback callback) {
    dataSub?.cancel();
    dataSub = BrowserUriApi().loadDiagram(path).asStream().listen((graph) {
      info = FlameTabInformation(graph);
      callback();
    });
  }
}
