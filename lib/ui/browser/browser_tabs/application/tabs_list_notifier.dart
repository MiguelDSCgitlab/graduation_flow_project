import 'package:c4_frontend_data_entry_flutter/ui/browser/browser_body/diagram/application/diagram_path.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'browser_tab.dart';

class BrowserTabsListStateNotifier extends StateNotifier<List<BrowserTab>> {
  BrowserTabsListStateNotifier() : super([]);

  Future<void> addTab(DiagramPath path, {bool selectIt = false}) async {
    var tab = BrowserTab(path: path);
    state = [...state, tab];
    if (state.isEmpty || selectIt) {
      selectTab(tab);
    }
    tab.loadTab(() {
      state = [...state];
    });
  }

  void removeTab(BrowserTab tab) {
    var i = state.indexOf(tab);
    state = state.where((e) => e != tab).toList();
    if (state.isNotEmpty && tab.isSelected) {
      if (i >= state.length) {
        i = state.length - 1;
      }
      selectTab(state[i]);
    }
  }

  void selectTab(BrowserTab tab) {
    if (tab.isSelected) {
      return;
    }
    state = state.map((e) => e..isSelected = e == tab).toList();
  }

  BrowserTab? currentTab() {
    if (state.isEmpty) {
      return null;
    }
    return state.firstWhere((e) => e.isSelected);
  }

  Future<void> navigate(DiagramPath newPath) async {
    if (currentTab() == null) {
      addTab(newPath, selectIt: true);
      return;
    }
    currentTab()!.navigate(newPath);
    state = [...state];
    currentTab()!.loadTab(() {
      state = [...state];
    });
  }

  Future<void> goBack() async {
    currentTab()?.goBack();
    state = [...state];
    currentTab()!.loadTab(() {
      state = [...state];
    });
  }

  Future<void> goForward() async {
    currentTab()?.goForward();
    state = [...state];
    currentTab()!.loadTab(() {
      state = [...state];
    });
  }
}
