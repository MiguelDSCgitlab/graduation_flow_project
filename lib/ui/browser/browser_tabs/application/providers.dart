import 'package:c4_frontend_data_entry_flutter/ui/browser/browser_tabs/application/flame_information.dart';
import 'package:c4_frontend_data_entry_flutter/ui/browser/browser_tabs/application/tabs_list_notifier.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../browser_body/diagram/application/diagram_path.dart';
import 'browser_tab.dart';

final tabsListProvider =
    StateNotifierProvider<BrowserTabsListStateNotifier, List<BrowserTab>>(
        (ref) => BrowserTabsListStateNotifier());

var currentTabProvider = Provider<BrowserTab?>((ref) {
  ref.watch(tabsListProvider);
  return ref.read(tabsListProvider.notifier).currentTab();
});

var currentFlameInfoProvider = Provider<FlameTabInformation?>((ref) {
  ref.watch(tabsListProvider);
  return ref.read(tabsListProvider.notifier).currentTab()?.info;
});

var currentPathProvider = Provider<DiagramPath?>((ref) {
  ref.watch(tabsListProvider);
  return ref.read(tabsListProvider.notifier).currentTab()?.path;
});

var addTabProvider = StateProvider<bool>((ref) => false);
