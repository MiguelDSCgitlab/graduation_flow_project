import 'dart:math';

import 'package:c4_frontend_data_entry_flutter/ui/browser/browser_tabs/application/providers.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../application/browser_tab.dart';
import 'browser_tab_widget.dart';

class BrowserTabList extends ConsumerWidget {
  const BrowserTabList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    List<BrowserTab> tabs = ref.watch(tabsListProvider);
    if (tabs.isEmpty) {
      return Container();
    }
    return LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
      return SizedBox(
          height: 35,
          child: ListView.builder(
            itemCount: tabs.length,
            scrollDirection: Axis.horizontal,
            itemBuilder: (context, index) {
              return Container(
                width: _calculateWidth(constraints, tabs.length),
                child: BrowserTabWidget(tab: tabs[index]),
              );
            },
          ));
    });
  }

  double _calculateWidth(BoxConstraints constraints, int length) {
    double width = constraints.maxWidth;
    return min(width / length, 200);
  }
}
