import 'package:c4_frontend_data_entry_flutter/ui/browser/browser_body/home/favorite/application/providers.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../application/providers.dart';

class BrowserNavButtons extends ConsumerWidget {
  const BrowserNavButtons({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    var notifier = ref.read(tabsListProvider.notifier);
    var currentTab = ref.watch(currentTabProvider);
    ref.watch(currentPathProvider);
    ref.watch(favoriteDiagramsProvider);
    var addTabNotifier = ref.read(addTabProvider.notifier);
    var addTabIsToggled = ref.watch(addTabProvider);
    var favoriteNotifier = ref.read(favoriteDiagramsProvider.notifier);
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        _browserButton(
            (currentTab != null && currentTab.canGoBack()),
            (currentTab == null || !currentTab.canGoBack())
                ? null
                : CupertinoColors.inactiveGray, () {
          notifier.goBack();
        }, CupertinoIcons.arrow_left),
        const SizedBox(
          width: 2,
        ),
        _browserButton(
            (currentTab != null && currentTab.canGoForward()),
            (currentTab == null || !currentTab.canGoForward())
                ? null
                : CupertinoColors.inactiveGray, () {
          notifier.goForward();
        }, CupertinoIcons.arrow_right),
        const SizedBox(
          width: 2,
        ),
        _browserButton(
            true,
            (addTabIsToggled
                ? CupertinoColors.activeOrange
                : CupertinoColors.inactiveGray), () {
          addTabNotifier.state = !addTabIsToggled;
        }, CupertinoIcons.add),
        const SizedBox(
          width: 2,
        ),
        _browserButton(
            currentTab != null,
            (currentTab != null && favoriteNotifier.isFavorite(currentTab.path)
                ? CupertinoColors.activeOrange
                : CupertinoColors.inactiveGray), () {
          if (favoriteNotifier.isFavorite(currentTab!.path)) {
            favoriteNotifier.removeFavorite(currentTab.path);
          } else {
            favoriteNotifier.addFavorite(currentTab.path);
          }
        }, CupertinoIcons.bookmark),
        const SizedBox(
          width: 2,
        ),
      ],
    );
  }
}

Widget _browserButton(
  bool enabled,
  CupertinoDynamicColor? color,
  VoidCallback action,
  IconData icon,
) {
  return GestureDetector(
      onTap: enabled ? action : null,
      child: Container(
          color: color,
          alignment: Alignment.center,
          height: 30,
          width: 30,
          padding: const EdgeInsets.symmetric(horizontal: 5),
          child: Icon(icon)));
}
