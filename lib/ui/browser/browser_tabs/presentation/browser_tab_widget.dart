import 'package:c4_frontend_data_entry_flutter/ui/color_scheme/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_svg/svg.dart';

import '../application/browser_tab.dart';
import '../application/providers.dart';

class BrowserTabWidget extends ConsumerWidget {
  const BrowserTabWidget({Key? key, required this.tab}) : super(key: key);

  final BrowserTab tab;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    var notifier = ref.read(tabsListProvider.notifier);
    return GestureDetector(
      onTap: () {
        notifier.selectTab(tab);
      },
      behavior: HitTestBehavior.translucent,
      child: Container(
          color: tab.isSelected ? greyColor : lightBlackColor,
          alignment: Alignment.centerLeft,
          height: 30,
          padding: const EdgeInsets.symmetric(horizontal: 5),
          child: Row(
            children: [
              SizedBox(
                width: 25,
                height: 25,
                child: tab.info != null
                    ? SvgPicture.asset(
                        "assets/icons/ui/nav_files.svg",
                        width: 22,
                        height: 22,
                      )
                    : const Padding(
                        padding: EdgeInsets.all(4.0),
                        child: CircularProgressIndicator(
                          strokeWidth: 2,
                          color: whiteTextColor,
                        ),
                      ),
              ),
              const SizedBox(width: 3),
              Expanded(
                child: Text(
                  tab.path.toString(),
                  style: const TextStyle(color: whiteTextColor, fontSize: 10),
                  softWrap: false,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              GestureDetector(
                onTap: () {
                  notifier.removeTab(tab);
                },
                child: const Icon(
                  CupertinoIcons.xmark,
                  color: whiteTextColor,
                  size: 25,
                ),
              ),
            ],
          )),
    );
  }
}
