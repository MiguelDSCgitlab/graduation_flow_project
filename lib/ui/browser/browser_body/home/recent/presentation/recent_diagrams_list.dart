import 'package:c4_frontend_data_entry_flutter/ui/color_scheme/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../application/providers.dart';

class RecentDiagramsList extends ConsumerWidget {
  const RecentDiagramsList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    var diagramPathHistory = ref.watch(diagramPathHistoryProvider);
    return ListView.builder(
      shrinkWrap: true,
      itemCount: diagramPathHistory.length,
      itemBuilder: (context, index) {
        var diagramPath = diagramPathHistory[index];
        return Padding(
          padding: const EdgeInsets.all(3.0),
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              border: Border.all(color: blackColor),
              color: lightGreyColor,
            ),
            child: ListTile(
              tileColor: blackColor,
              visualDensity: VisualDensity.compact,
              title: Text(diagramPath.toString(),
                  overflow: TextOverflow.ellipsis,
                  style: const TextStyle(color: CupertinoColors.link)),
              onTap: () {
                ref
                    .read(diagramPathHistoryProvider.notifier)
                    .onClick(diagramPath);
              },
            ),
          ),
        );
      },
    );
  }
}
