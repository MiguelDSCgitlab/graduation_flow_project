import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../diagram/application/diagram_path.dart';
import 'diagram_path_history_notifier.dart';

final diagramPathHistoryProvider =
    StateNotifierProvider<DiagramPathHistoryNotifier, List<DiagramPath>>((ref) {
  return DiagramPathHistoryNotifier(ref);
});
