import 'package:c4_frontend_data_entry_flutter/ui/browser/browser_body/diagram/application/diagram_path.dart';
import 'package:c4_frontend_data_entry_flutter/ui/browser/browser_tabs/application/providers.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../../nav_bar/application/providers.dart';

class DiagramPathHistoryNotifier extends StateNotifier<List<DiagramPath>> {
  final Ref _ref;

  DiagramPathHistoryNotifier(this._ref) : super([]) {
    _ref.listen<DiagramPath?>(currentPathProvider, (_, path) {
      if (path == null) return;
      if (state.contains(path)) {
        state.remove(path);
      }
      state = [path, ...state];
    });
  }

  void onClick(DiagramPath path) {
    _ref.read(currentPageProvider.notifier).state = NavPage.FILES;
    _ref.read(tabsListProvider.notifier).addTab(path, selectIt: true);
  }
}
