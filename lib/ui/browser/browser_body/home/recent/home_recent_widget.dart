import 'package:c4_frontend_data_entry_flutter/ui/browser/browser_body/home/recent/presentation/recent_diagrams_list.dart';
import 'package:c4_frontend_data_entry_flutter/ui/color_scheme/colors.dart';
import 'package:flutter/material.dart';

class HomeRecentWidget extends StatelessWidget {
  const HomeRecentWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(height: 5),
        Center(
          child: Text('Recent',
              style: Theme.of(context).textTheme.titleLarge?.copyWith(
                    color: whiteTextColor,
                  )),
        ),
        const SizedBox(height: 5),
        const Expanded(child: RecentDiagramsList()),
      ],
    );
  }
}
