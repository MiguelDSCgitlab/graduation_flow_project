import 'package:c4_frontend_data_entry_flutter/ui/search_popup/presentation/search_popup.dart';
import 'package:flutter/material.dart';

class HomeSearchWidget extends StatelessWidget {
  const HomeSearchWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SearchPopup(darkMode: true),
    );
  }
}
