import 'package:c4_frontend_data_entry_flutter/ui/browser/browser_body/home/favorite/presentation/favorite_diagrams_list.dart';
import 'package:flutter/material.dart';

import '../../../../color_scheme/colors.dart';

class HomeFavoriteWidget extends StatelessWidget {
  const HomeFavoriteWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(height: 5),
        Center(
          child: Text('Favorite',
              style: Theme.of(context).textTheme.titleLarge?.copyWith(
                    color: whiteTextColor,
                  )),
        ),
        const SizedBox(height: 5),
        const Expanded(child: FavoriteDiagramsList()),
      ],
    );
  }
}
