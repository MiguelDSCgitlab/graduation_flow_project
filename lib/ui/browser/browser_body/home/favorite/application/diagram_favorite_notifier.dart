import 'package:c4_frontend_data_entry_flutter/ui/browser/browser_body/diagram/application/diagram_path.dart';
import 'package:c4_frontend_data_entry_flutter/ui/browser/browser_tabs/application/providers.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../../nav_bar/application/providers.dart';

class DiagramFavoriteNotifier extends StateNotifier<List<DiagramPath>> {
  final Ref _ref;

  DiagramFavoriteNotifier(this._ref) : super([]);

  void onClick(DiagramPath path) {
    _ref.read(currentPageProvider.notifier).state = NavPage.FILES;
    _ref.read(tabsListProvider.notifier).addTab(path, selectIt: true);
  }

  void addFavorite(DiagramPath path) {
    if (!state.contains(path)) {
      state = [...state, path];
    }
  }

  void removeFavorite(DiagramPath path) {
    if (state.contains(path)) {
      state = state.where((e) => e != path).toList();
    }
  }

  bool isFavorite(DiagramPath path) {
    return state.contains(path);
  }
}
