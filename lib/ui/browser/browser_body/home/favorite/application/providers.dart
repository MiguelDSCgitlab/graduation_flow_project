import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../diagram/application/diagram_path.dart';
import 'diagram_favorite_notifier.dart';

final favoriteDiagramsProvider =
    StateNotifierProvider<DiagramFavoriteNotifier, List<DiagramPath>>((ref) {
  return DiagramFavoriteNotifier(ref);
});
