import 'package:c4_frontend_data_entry_flutter/ui/browser/browser_body/home/recent/home_recent_widget.dart';
import 'package:c4_frontend_data_entry_flutter/ui/browser/browser_body/home/search/home_search_widget.dart';
import 'package:c4_frontend_data_entry_flutter/ui/color_scheme/colors.dart';
import 'package:flutter/material.dart';

import 'favorite/home_favorite_widget.dart';

class BrowserHomeWidget extends StatelessWidget {
  const BrowserHomeWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: greyColor,
      child: Row(
        children: [
          Flexible(
            flex: 1,
            child: Column(
              children: [
                Flexible(
                  flex: 1,
                  child: _buildBorderedWidget(const HomeFavoriteWidget()),
                ),
                Flexible(
                  flex: 2,
                  child: _buildBorderedWidget(const HomeRecentWidget()),
                ),
              ],
            ),
          ),
          Flexible(
            flex: 3,
            child: _buildBorderedWidget(const HomeSearchWidget()),
          ),
        ],
      ),
    );
  }

  Widget _buildBorderedWidget(Widget child) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(
          color: Colors.black,
          width: 2.0, // Adjust the width as needed
        ),
        borderRadius:
            BorderRadius.circular(10.0), // Adjust the border radius as needed
      ),
      margin: const EdgeInsets.all(5.0), // Adjust the margin as needed
      child: child,
    );
  }
}
