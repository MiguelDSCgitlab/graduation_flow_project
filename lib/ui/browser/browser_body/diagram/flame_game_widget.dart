import 'dart:async';

import 'package:c4_frontend_data_entry_flutter/ui/browser/browser_tabs/application/flame_information.dart';
import 'package:flame/camera.dart';
import 'package:flame/components.dart';
import 'package:flame/events.dart';
import 'package:flame/game.dart';
import 'package:flame/palette.dart';
import 'package:flame_riverpod/flame_riverpod.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../browser_tabs/application/providers.dart';
import 'presentation/flame_objects/c4_cylinder.dart';
import 'presentation/flame_objects/c4_edge.dart';
import 'presentation/flame_objects/c4_outline.dart';
import 'presentation/flame_objects/c4_rectangle.dart';

class C4DiagramGame extends FlameGame
    with ScrollDetector, ScaleDetector, SingleGameInstance, RiverpodGameMixin {
  final ProviderRef _ref;
  @override
  final C4World world;

  C4DiagramGame(this._ref, this.world)
      : super(world: world, camera: CameraComponent());

  @override
  Future<void> onLoad() async {
    camera.viewport = FixedResolutionViewport(resolution: size);
    super.onLoad();
  }

  @override
  void onMount() {
    _ref.listen(currentFlameInfoProvider, (_, info) {
      if (info != null) {
        world.renderDiagram(info);
      } else {
        world.removeAll(world.children);
      }
    });
    super.onMount();
  }

  @override
  Color backgroundColor() => const Color(0xffffffff);

  @override
  void onGameResize(Vector2 size) {
    if (isMounted) {
      camera.viewport = FixedResolutionViewport(resolution: size);
    }
    super.onGameResize(size);
  }

  void clampZoom() {
    camera.viewfinder.zoom = camera.viewfinder.zoom.clamp(0.2, 1.0);
    _saveCamera();
  }

  static const zoomPerScrollUnit = 0.02;

  @override
  void onScroll(PointerScrollInfo info) {
    camera.viewfinder.zoom -=
        info.scrollDelta.global.y.sign * zoomPerScrollUnit;
    clampZoom();
  }

  late double startZoom;

  @override
  void onScaleStart(info) {
    startZoom = camera.viewfinder.zoom;
  }

  @override
  void onScaleUpdate(ScaleUpdateInfo info) {
    final currentScale = info.scale.global;
    if (!currentScale.isIdentity()) {
      camera.viewfinder.zoom = startZoom * currentScale.y;
      clampZoom();
    } else {
      final delta = info.delta.global;
      camera.viewfinder.position -= (delta / camera.viewfinder.zoom);
      _saveCamera();
    }
  }

  void _saveCamera() {
    _ref.read(currentFlameInfoProvider)?.cameraPosition =
        camera.viewfinder.position;
    _ref.read(currentFlameInfoProvider)?.cameraZoom = camera.viewfinder.zoom;
  }

  void setCamera(FlameTabInformation info) {
    camera.moveTo(info.cameraPosition);
    camera.viewfinder.zoom = info.cameraZoom;
    camera.setBounds(info.cameraBounds);
  }
}

class C4World extends World with HasGameRef<C4DiagramGame> {
  renderDiagram(FlameTabInformation info) {
    removeAll(children);
    var objects = <Component>[];
    for (final e in info.graph.objects) {
      switch (e.shape) {
        case 'rect':
          objects.add(C4Rectangle.fromModel(e));
          break;
        case 'cylinder':
          objects.add(C4Cylinder.fromModel(e));
          break;
        case '':
          objects.add(C4Outline.fromModel(e));
          break;
        default:
          break;
      }
      for (final e in info.graph.edges) {
        objects.add(C4Edge(e));
      }
    }
    addAll(objects).asStream().listen((_) {
      game.setCamera(info);
    });
  }
}
