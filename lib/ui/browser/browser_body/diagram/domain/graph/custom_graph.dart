import '../../infrastructure/get_edges.dart';
import '../../infrastructure/get_objects.dart';
import '../edges/custom_edge.dart';
import '../object/custom_object.dart';
import 'graph_draw.dart';

class CustomGraph {
  String name;
  bool? directed;
  bool? strict;
  GraphDraw draw;
  List<double>? boundingBox;
  String compound;
  String? fontName;
  String? label;
  double? minNodeSeparation;
  String? rankDir;
  String? rankSep;
  List<CustomObject> objects;
  List<CustomEdge> edges;

  CustomGraph(
      {required this.name,
      this.directed,
      this.strict,
      required this.draw,
      this.boundingBox,
      required this.compound,
      this.fontName,
      this.label,
      this.minNodeSeparation,
      this.rankDir,
      this.rankSep,
      required this.objects,
      required this.edges});

  factory CustomGraph.fromJson(Map<String, dynamic> data) {
    List<double> parseBoundingBox(String boundingBoxString) {
      List<String> values = boundingBoxString.split(',');
      return values.map((value) => double.parse(value)).toList();
    }

    List<double> boundingBox = parseBoundingBox(data['bb']);

    // Check if 'draw' key exists, otherwise, use '_draw_' key
    List<dynamic> drawData = data['draw'] ?? data['_draw_'];

    return CustomGraph(
      name: data['name'] is String ? data['name'] as String : '',
      directed: data['directed'] as bool?,
      strict: data['strict'] as bool?,
      draw: GraphDraw.fromJson(drawData),
      boundingBox: boundingBox,
      compound: data['compound'] is String ? data['compound'] as String : '',
      fontName: data['fontname'] as String?,
      label: data['label'] as String?,
      minNodeSeparation: data['nodesep'] != null
          ? double.parse(data['nodesep'].toString())
          : 1.0,
      rankDir: data['rankdir'] as String?,
      rankSep: data['ranksep'] as String?,
      objects: getObjects(data),
      edges: getEdges(data),
    );
  }
}
