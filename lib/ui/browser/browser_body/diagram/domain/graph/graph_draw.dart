import 'dart:math';

import '../../application/utils.dart';

class GraphDraw {
  String? gradient;
  String? color;
  List<Point<double>> points;

  GraphDraw({
    this.gradient,
    this.color,
    required this.points,
  });

  factory GraphDraw.fromJson(List<dynamic> data) {
    String? gradient = data[0]['grad'] as String?;
    String? color = data[0]['color'] as String?;
    List<Point<double>> points = parsePoints(data[2]['points']);
    return GraphDraw(gradient: gradient, color: color, points: points);
  }
}
