import 'dart:math';

import '../application/utils.dart';

class LabelDraw {
  String? fontName;
  double? fontSize;
  String? gradient;
  String? color;
  Point<double> point;
  String? align;
  double? width;
  String? text;

  LabelDraw({
    this.fontName,
    this.fontSize,
    this.gradient,
    this.color,
    required this.point,
    this.align,
    this.width,
    this.text,
  });

  factory LabelDraw.fromJson(List<dynamic> data) {
    // Extract style from the data
    String? fontName = data[0]['face'] as String?;
    double? fontSize = data[0]['size'] as double?;
    String? gradient = data[1]['grad'] as String?;
    String? color = data[1]['color'] as String?;
    String? align = data[2]['align'] as String?;
    double? width = data[2]['width'] as double?;
    String? labelText = data[2]['text'] as String?;
    Point<double> labelPoints = parsePoints([data[2]['pt']])[0];
    return LabelDraw(
        fontName: fontName,
        fontSize: fontSize,
        gradient: gradient,
        color: color,
        point: labelPoints,
        align: align,
        width: width,
        text: labelText);
  }
}
