import 'dart:math';

class ObjectDraw {
  String gradient;
  String color;
  List<Point<double>> points;

  ObjectDraw([this.gradient = "", this.color = "", this.points = const []]);
}
