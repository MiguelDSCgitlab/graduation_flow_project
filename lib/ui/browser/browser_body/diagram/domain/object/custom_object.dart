import '../../application/utils.dart';
import '../label_draw.dart';
import 'object_draw.dart';

class CustomObject {
  int gvid;
  String name;
  String? url;
  List<ObjectDraw> draw;
  List<LabelDraw> text;
  String? color;
  String? fillColor;
  String? fontColor;
  String? fontName;
  double? height;
  int? id;
  String label;
  String? margin;
  String? pos;
  String? shape;
  String? style;
  String? width;
  String? boundingBox;
  String? compound;
  double? labelHeight;
  String? labelPos;
  double? labelWidth;
  List<int>? nodes;
  List<int>? edges;

  CustomObject(
      {required this.gvid,
      required this.name,
      this.url,
      required this.draw,
      required this.text,
      this.color = "#0b4884",
      this.fillColor = "#1168bd",
      this.fontColor = "ffffff",
      this.fontName = "Arial",
      this.height,
      this.id,
      required this.label,
      this.margin,
      this.pos,
      this.shape,
      this.style = "filled",
      this.width,
      this.boundingBox,
      this.compound,
      this.labelHeight,
      this.labelPos,
      this.labelWidth,
      this.nodes,
      this.edges});

  //gvid name label

  factory CustomObject.fromJson(Map<String, dynamic> data) {
    int gvid = -1;

    if (data['_gvid'] is int) {
      gvid = data['_gvid'] as int;
    }

    // Check if 'draw' key exists, otherwise, use '_draw_' key
    List<dynamic> drawData = data['draw'] ?? data['_draw_'];
    List<dynamic> labelDrawData = data['ldraw'] ?? data['_ldraw_'];
    List<ObjectDraw> draw = [];
    late ObjectDraw currDraw;
    for (int i = 0; i < drawData.length; i++) {
      if (drawData[i]["op"] == "c") {
        currDraw = ObjectDraw();
        currDraw.color = drawData[i]["color"];
        currDraw.gradient = drawData[i]["grad"];
      }
      if (drawData[i].containsKey("points")) {
        currDraw.points = parsePoints(drawData[i]["points"]);
        draw.add(currDraw);
      }
    }
    List<LabelDraw> text = [];
    for (int i = 0; i < labelDrawData.length - 2; i += 3) {
      text.add(LabelDraw.fromJson(labelDrawData.sublist(i, i + 3)));
    }
    CustomObject customObject = CustomObject(
      gvid: gvid,
      name: data['name'] is String ? data['name'] as String : '',
      url: data['URL'] as String?,
      draw: draw,
      text: text,
      color: data['color'] as String? ?? "#0b4884",
      fillColor: data['fillcolor'] as String? ?? "#1168bd",
      fontColor: data['fontcolor'] as String? ?? "ffffff",
      fontName: data['fontName'] as String? ?? "Arial",
      height: data['height'] != null
          ? double.parse(data['height'].toString())
          : 0.0,
      id: data['id'] != null ? int.parse(data['id'].toString()) : -1,
      label: data['label'] is String ? data['label'] as String : '',
      margin: data['margin'] as String?,
      pos: data['pos'] as String?,
      shape: data['shape'] as String? ?? "",
      style: data['style'] as String? ?? "",
      width: data['width'] as String?,
      boundingBox: data['boundingBox'] as String?,
      compound: data['compound'] as String?,
      labelHeight: data['labelHeight'] != null
          ? double.parse(data['labelHeight'].toString())
          : null,
      labelPos: data['labelPos'] as String?,
      labelWidth: data['labelWidth'] != null
          ? double.parse(data['labelWidth'].toString())
          : null,
      nodes: data['nodes'] is List<int> ? data['nodes'] : null,
      edges: data['edges'] is List<int> ? data['edges'] : null,
    );

    return customObject;
  }
}
