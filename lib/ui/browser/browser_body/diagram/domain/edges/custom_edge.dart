import '../label_draw.dart';
import 'arrow.dart';
import 'edge_draw.dart';

class CustomEdge {
  int gvid;
  int tail;
  int head;
  EdgeDraw draw;
  Arrow arrow;
  LabelDraw labelDraw;
  String? color;
  String? fontColor;
  String? fontName;
  String? label;
  String? labelPos;
  String? pos;
  String? style;

  CustomEdge({
    required this.gvid,
    required this.tail,
    required this.head,
    required this.draw,
    required this.arrow,
    required this.labelDraw,
    this.label,
    this.color = "#707070",
    this.fontColor = "#707070",
    this.fontName = "Arial",
    this.labelPos,
    this.pos,
    this.style = "solid",
  });

  factory CustomEdge.fromJson(Map<String, dynamic> data) {
    int gvid;
    int tail;
    int head;

    if (data['_gvid'] is! int || data['tail'] is! int || data['head'] is! int) {
      gvid = -1;
      tail = -1;
      head = -1;
    } else {
      gvid = data['_gvid'] as int;
      tail = data['tail'] as int;
      head = data['head'] as int;
    }

    // Check if 'draw' key exists, otherwise, use '_draw_' key
    List<dynamic> drawData = data['draw'] ?? data['_draw_'];
    List<dynamic> labelDrawData = data['ldraw'] ?? data['_ldraw_'];
    List<dynamic> arrowDrawData = data['hdraw'] ?? data['_hdraw_'];

    EdgeDraw edgeDraw = EdgeDraw.fromJson(drawData);
    Arrow arrow = Arrow.fromJson(arrowDrawData);
    LabelDraw labelDraw = LabelDraw.fromJson(labelDrawData);

    String? label = data['label'] as String?;
    String? color = data['color'] as String?;
    String? fontColor = data['fontcolor'] as String?;
    String? fontName = data['fontname'] as String?;
    String? labelPos = data['lp'] as String?;
    String? pos = data['pos'] as String?;
    String? style = data['style'] as String?;

    return CustomEdge(
      gvid: gvid,
      tail: tail,
      head: head,
      draw: edgeDraw,
      arrow: arrow,
      labelDraw: labelDraw,
      color: color,
      fontColor: fontColor,
      fontName: fontName,
      label: label,
      labelPos: labelPos,
      pos: pos,
      style: style,
    );
  }
}
