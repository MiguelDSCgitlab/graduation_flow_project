import 'dart:math';

import '../../application/utils.dart';

class Arrow {
  String? style;
  String? gradient;
  String? color;
  List<Point<double>> points;

  Arrow({this.style, this.gradient, this.color, required this.points});

  factory Arrow.fromJson(List<dynamic> data) {
    String? style = data[0]['style'] as String?;
    String? gradient = data[1]['grad'] as String?;
    String? color = data[1]['color'] as String?;
    List<Point<double>> points = parsePoints(data[3]['points']);

    return Arrow(
        style: style, gradient: gradient, color: color, points: points);
  }
}
