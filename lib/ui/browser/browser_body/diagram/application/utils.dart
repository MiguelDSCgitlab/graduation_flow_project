import 'dart:math';
import 'dart:ui';

import 'package:flame/components.dart';

Color hexToColor(String color) {
  return Color(int.parse(color.substring(1), radix: 16)).withOpacity(1);
}

Path drawCurvePath(List<Point<double>> points) {
  var path = Path();
  path.moveTo(points[0].x, points[0].y);
  for (var i = 1; i < points.length - 1; i++) {
    var xc = (points[i].x + points[i + 1].x) / 2;
    var yc = (points[i].y + points[i + 1].y) / 2;
    path.quadraticBezierTo(points[i].x, points[i].y, xc, yc);
  }
  path.quadraticBezierTo(
    points[points.length - 2].x,
    points[points.length - 2].y,
    points[points.length - 1].x,
    points[points.length - 1].y,
  );
  return path;
}

Path drawStraightPath(List<Point<double>> points) {
  var path = Path();
  path.moveTo(points[0].x, points[0].y);
  for (var i = 1; i < points.length; i++) {
    path.lineTo(points[i].x, points[i].y);
  }
  path.lineTo(points[0].x, points[0].y);
  return path;
}

Vector2 pointToVector2(Point point) {
  return Vector2(point.x.toDouble(), point.y.toDouble());
}

List<Point<double>> parsePoints(List<dynamic> points) {
  List<Point<double>> result = [];
  for (dynamic p in points) {
    assert(p.length == 2);
    result.add(Point<double>(p[0], p[1]));
  }
  return result;
}
