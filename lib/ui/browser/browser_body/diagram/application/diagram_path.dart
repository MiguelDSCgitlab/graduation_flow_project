abstract class DiagramPath {
  const DiagramPath();

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
    if (other is DiagramPath) {
      return _isEqual(other);
    }
    return false;
  }

  @override
  int get hashCode => _getHashCode();

  @override
  String toString() {
    return _toString();
  }

  String _toString();

  bool _isEqual(DiagramPath other);

  int _getHashCode();
}

class AllContextsDiagramPath extends DiagramPath {
  const AllContextsDiagramPath();

  @override
  bool _isEqual(Object other) {
    return other is AllContextsDiagramPath;
  }

  @override
  int _getHashCode() => runtimeType.hashCode;

  @override
  String _toString() {
    return "Contexts";
  }
}

class ContextDiagramPath extends DiagramPath {
  String contextId;

  ContextDiagramPath(this.contextId);

  @override
  bool _isEqual(Object other) {
    return other is ContextDiagramPath && other.contextId == contextId;
  }

  @override
  int _getHashCode() => contextId.hashCode ^ runtimeType.hashCode;

  @override
  String _toString() {
    return "Context: $contextId";
  }
}

class AllContainersDiagramPath extends DiagramPath {
  String contextId;

  AllContainersDiagramPath(this.contextId);

  @override
  bool _isEqual(Object other) {
    return other is AllContainersDiagramPath && other.contextId == contextId;
  }

  @override
  int _getHashCode() => contextId.hashCode ^ runtimeType.hashCode;

  @override
  String _toString() {
    return "Containers";
  }
}

class ContainerDiagramPath extends DiagramPath {
  String contextId;
  String containerId;

  ContainerDiagramPath(this.contextId, this.containerId);

  @override
  bool _isEqual(Object other) {
    return other is ContainerDiagramPath &&
        other.contextId == contextId &&
        other.containerId == containerId;
  }

  @override
  int _getHashCode() =>
      Object.hash(contextId, containerId) ^ runtimeType.hashCode;

  @override
  String _toString() {
    return "Container: $containerId";
  }
}

class AllComponentsDiagramPath extends DiagramPath {
  String contextId;
  String containerId;

  AllComponentsDiagramPath(this.contextId, this.containerId);

  @override
  bool _isEqual(Object other) {
    return other is AllComponentsDiagramPath &&
        other.contextId == contextId &&
        other.containerId == containerId;
  }

  @override
  int _getHashCode() =>
      Object.hash(contextId, containerId) ^ runtimeType.hashCode;

  @override
  String _toString() {
    return "Components";
  }
}

class ComponentDiagramPath extends DiagramPath {
  String contextId;
  String containerId;
  String componentId;

  ComponentDiagramPath(this.contextId, this.containerId, this.componentId);

  @override
  bool _isEqual(Object other) {
    return other is ComponentDiagramPath &&
        other.contextId == contextId &&
        other.containerId == containerId &&
        other.componentId == componentId;
  }

  @override
  int _getHashCode() =>
      Object.hash(contextId, containerId, componentId) ^ runtimeType.hashCode;

  @override
  String _toString() {
    return "Component: $componentId";
  }
}
