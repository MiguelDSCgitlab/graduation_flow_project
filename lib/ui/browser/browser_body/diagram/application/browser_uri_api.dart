import 'dart:convert';

import 'package:c4_frontend_data_entry_flutter/ui/browser/browser_body/diagram/domain/graph/graph_draw.dart';
import 'package:http/http.dart' as http;

import '../domain/graph/custom_graph.dart';
import 'diagram_path.dart';

class BrowserUriApi {
  static const String _c4Scheme = "https";
  static const String _c4BaseUrl = "c4enhanced.nl";

  static const String _contextsEndpoint = "orchestrator/svg/contexts";
  static const String _containersEndpoint = "containers";
  static const String _componentsEndpoint = "components";

  Uri getUri(DiagramPath path, {Map<String, String> queryParams = const {}}) {
    Uri uri = Uri(
        scheme: _c4Scheme,
        host: _c4BaseUrl,
        // port: _c4Port,
        path: _pathToString(path),
        queryParameters: queryParams);
    return uri;
  }

  String _pathToString(DiagramPath path) {
    var baseUrl = "/$_contextsEndpoint";
    switch (path.runtimeType) {
      case AllContextsDiagramPath:
        return baseUrl;
      case ContextDiagramPath:
        path as ContextDiagramPath;
        return "$baseUrl/${path.contextId}";
      case AllContainersDiagramPath:
        path as AllContainersDiagramPath;
        return "$baseUrl/${path.contextId}/$_containersEndpoint";
      case ContainerDiagramPath:
        path as ContainerDiagramPath;
        return "$baseUrl/${path.contextId}/$_containersEndpoint/${path.containerId}";
      case AllComponentsDiagramPath:
        path as AllComponentsDiagramPath;
        return "$baseUrl/${path.contextId}/$_containersEndpoint/${path.containerId}/$_componentsEndpoint";
      case ComponentDiagramPath:
        path as ComponentDiagramPath;
        return "$baseUrl/${path.contextId}/$_containersEndpoint/${path.containerId}/$_componentsEndpoint/${path.componentId}";
      default:
        throw Exception("Unknown path type");
    }
  }

  DiagramPath getPath(String uri) {
    var path = [...Uri.parse(uri).pathSegments];
    path.removeRange(0, 3); // Remove /svg/contexts
    switch (path.length) {
      case 0:
        return const AllContextsDiagramPath();
      case 1:
        return ContextDiagramPath(path[0]);
      case 2:
        return AllContainersDiagramPath(path[0]);
      case 3:
        return ContainerDiagramPath(path[0], path[2]);
      case 4:
        return AllComponentsDiagramPath(path[0], path[2]);
      default:
        return ComponentDiagramPath(path[0], path[2], path[4]);
    }
  }

  Future<CustomGraph> loadDiagram(DiagramPath diagramPath) async {
    var url = getUri(diagramPath, queryParams: {"engine": "graphviz-json"});
    var response = await http.get(url);
    late var data;
    if (response.statusCode == 200) {
      data = json.decode(response.body);
    } else {
      // TODO: properly handle error
      print(
          'Request failed with status: ${response.statusCode}. Error: ${response.body}');
      data = {};
      return CustomGraph(
          name: 'Error',
          draw: GraphDraw(points: []),
          compound: '',
          objects: [],
          edges: []);
    }
    return CustomGraph.fromJson(data);
  }
}
