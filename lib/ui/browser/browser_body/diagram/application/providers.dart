import 'package:flame_riverpod/flame_riverpod.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../browser_tabs/application/providers.dart';
import '../flame_game_widget.dart';

final gameWidgetKey = GlobalKey<RiverpodAwareGameWidgetState<C4DiagramGame>>();
final gameWidgetProvider =
    Provider<RiverpodAwareGameWidget<C4DiagramGame>>((ref) {
  return RiverpodAwareGameWidget(
      key: gameWidgetKey,
      game: C4DiagramGame(ref, C4World()),
      loadingBuilder: (BuildContext context) {
        return const Center(
          child: CircularProgressIndicator(),
        );
      },
      errorBuilder: (BuildContext context, Object error) {
        return Center(
          child: Text('Error: $error'),
        );
      });
});

var tabsNotEmptyProvider = Provider<bool>((ref) {
  return ref.watch(tabsListProvider).isNotEmpty;
});
