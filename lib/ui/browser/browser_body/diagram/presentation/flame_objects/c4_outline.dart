import 'dart:ui';

import 'package:flame/components.dart';
import 'package:flutter/material.dart';

import '../../application/utils.dart';
import '../../domain/label_draw.dart';
import '../../domain/object/custom_object.dart';
import 'c4_text.dart';

class C4Outline extends RectangleComponent {
  final CustomObject _obj;

  C4Outline.fromModel(this._obj)
      : super(
            position: pointToVector2(_obj.draw[0].points[1]),
            size: pointToVector2(_obj.draw[0].points[3]) -
                pointToVector2(_obj.draw[0].points[1]),
            anchor: Anchor.topLeft,
            paintLayers: [
              if (_obj.color != null)
                (Paint()
                  ..color = hexToColor(_obj.color!)
                  ..style = PaintingStyle.stroke
                  ..strokeWidth = double.parse(_obj.width ?? '1'))
            ]);

  @override
  Future<void> onLoad() async {
    for (LabelDraw label in _obj.text) {
      var text = C4Text.fromModel(label);
      text.position = text.position - position;
      add(text);
    }
    super.onLoad();
  }
}
