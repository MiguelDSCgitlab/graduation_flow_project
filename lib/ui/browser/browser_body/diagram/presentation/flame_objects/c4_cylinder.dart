import 'dart:ui';

import 'package:flame/components.dart';

import '../../application/utils.dart';
import '../../domain/label_draw.dart';
import '../../domain/object/custom_object.dart';
import 'c4_text.dart';

class C4Cylinder extends PositionComponent {
  final CustomObject _obj;

  C4Cylinder.fromModel(this._obj)
      : super(
          position: Vector2.all(0),
          size: Vector2.all(1),
          anchor: Anchor.center,
        );

  @override
  void render(Canvas canvas) {
    var outerLine = drawCurvePath(_obj.draw[0].points);
    canvas.drawPath(
        outerLine,
        Paint()
          ..style = PaintingStyle.fill
          ..color = hexToColor(_obj.fillColor!));
    canvas.drawPath(
        outerLine,
        Paint()
          ..style = PaintingStyle.stroke
          ..color = hexToColor(_obj.color!)
          ..strokeWidth = 1);
    var innerLine = drawCurvePath(_obj.draw[1].points);
    canvas.drawPath(
        innerLine,
        Paint()
          ..style = PaintingStyle.stroke
          ..color = hexToColor(_obj.color!)
          ..strokeWidth = 1);
    super.render(canvas);
  }

  @override
  void onLoad() {
    for (LabelDraw label in _obj.text) {
      var text = C4Text.fromModel(label);
      text.position = text.position - position;
      add(text);
    }
    super.onLoad();
  }
}
