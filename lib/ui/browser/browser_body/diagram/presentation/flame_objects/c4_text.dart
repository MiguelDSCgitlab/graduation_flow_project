import 'package:flame/components.dart';
import 'package:flame/text.dart';

import '../../application/utils.dart';
import '../../domain/label_draw.dart';

class C4Text extends TextComponent {
  C4Text.fromModel(LabelDraw label)
      : super(
          text: label.text!,
          position: pointToVector2(label.point),
          anchor: Anchor.centerLeft,
          textRenderer: TextPaint(
            style: TextStyle(
              fontSize: label.fontSize,
              fontFamily: label.fontName,
              color: hexToColor(label.color!),
            ),
          ),
        );
}
