import 'package:c4_frontend_data_entry_flutter/ui/browser/browser_body/diagram/application/browser_uri_api.dart';
import 'package:c4_frontend_data_entry_flutter/ui/browser/browser_body/diagram/flame_game_widget.dart';
import 'package:c4_frontend_data_entry_flutter/ui/browser/browser_tabs/application/providers.dart';
import 'package:flame/components.dart';
import 'package:flame/events.dart';
import 'package:flame_riverpod/flame_riverpod.dart';
import 'package:flutter/material.dart';

import '../../application/utils.dart';
import '../../domain/label_draw.dart';
import '../../domain/object/custom_object.dart';
import 'c4_text.dart';

class C4Rectangle extends RectangleComponent
    with
        TapCallbacks,
        HoverCallbacks,
        HasGameRef<C4DiagramGame>,
        RiverpodComponentMixin {
  final CustomObject _obj;

  C4Rectangle.fromModel(this._obj)
      : super(
            position: pointToVector2(_obj.draw[0].points[1]),
            size: pointToVector2(_obj.draw[0].points[3]) -
                pointToVector2(_obj.draw[0].points[1]),
            anchor: Anchor.topLeft,
            paintLayers: [
              if (_obj.fillColor != null)
                (Paint()
                  ..color = hexToColor(_obj.fillColor!)
                  ..style = PaintingStyle.fill),
              if (_obj.color != null)
                (Paint()
                  ..color = hexToColor(_obj.color!)
                  ..style = PaintingStyle.stroke
                  ..strokeWidth = double.parse('2'))
            ]);

  @override
  Future<void> onLoad() async {
    for (LabelDraw label in _obj.text) {
      var text = C4Text.fromModel(label);
      text.position = text.position - position;
      add(text);
    }
    super.onLoad();
  }

  @override
  void onTapUp(TapUpEvent event) {
    if (_obj.url == null) {
      return;
    }
    var path = BrowserUriApi().getPath(_obj.url!);
    if (!ref.watch(addTabProvider)) {
      ref.read(tabsListProvider.notifier).navigate(path);
    } else {
      ref.read(tabsListProvider.notifier).addTab(path);
    }
    super.onTapUp(event);
  }

  @override
  void onHoverEnter() {
    if (_obj.url == null) {
      return;
    }
    game.mouseCursor = SystemMouseCursors.click;
    super.onHoverEnter();
  }

  @override
  void onHoverExit() {
    game.mouseCursor = SystemMouseCursors.basic;
    super.onHoverExit();
  }
}
