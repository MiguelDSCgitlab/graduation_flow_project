import 'dart:ui';

import 'package:flame/components.dart';

import '../../application/utils.dart';
import '../../domain/edges/custom_edge.dart';
import 'c4_text.dart';

class C4Edge extends PositionComponent {
  final CustomEdge edge;
  late C4Text label;

  C4Edge(this.edge)
      : super(
          position: Vector2.all(0),
          size: Vector2.all(1),
          anchor: Anchor.center,
        ) {
    label = C4Text.fromModel(edge.labelDraw);
  }

  @override
  void render(Canvas canvas) {
    var edgeLine = drawCurvePath(edge.draw.points);
    canvas.drawPath(
        edgeLine,
        Paint()
          ..style = PaintingStyle.stroke
          ..color = hexToColor(edge.draw.color!)
          ..strokeWidth = 1);
    var arrowLine = drawStraightPath(edge.arrow.points);
    canvas.drawPath(
        arrowLine,
        Paint()
          ..style = PaintingStyle.fill
          ..strokeWidth = 3
          ..color = hexToColor(edge.draw.color!));
    super.render(canvas);
  }

  @override
  void onLoad() {
    label.position = label.position - position;
    add(label);
    super.onLoad();
  }
}
