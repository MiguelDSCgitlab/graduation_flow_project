import '../domain/object/custom_object.dart';

// extract object info from given json
List<CustomObject> getObjects(Map<String, dynamic> data) {
  List<CustomObject> objectList = [];

  for (var object in data['objects']) {
    CustomObject customObject = CustomObject.fromJson(object);

    objectList.add(customObject);
  }

  return objectList;
}
