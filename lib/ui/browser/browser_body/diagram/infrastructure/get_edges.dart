import '../domain/edges/custom_edge.dart';

// extract edge info from given json
List<CustomEdge> getEdges(Map<String, dynamic> data) {
  List<CustomEdge> edgesList = [];

  if (data['edges'] != null) {
    for (var dataEdge in data['edges']) {
      CustomEdge customEdge = CustomEdge.fromJson(dataEdge);

      edgesList.add(customEdge);
    }
  }

  return edgesList;
}
