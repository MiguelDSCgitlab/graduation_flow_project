import 'package:c4_frontend_data_entry_flutter/ui/browser/browser_body/home/browser_home_widget.dart';
import 'package:c4_frontend_data_entry_flutter/ui/nav_bar/application/providers.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'diagram/application/providers.dart';

class BrowserBodyWidget extends ConsumerWidget {
  const BrowserBodyWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    var game = ref.watch(gameWidgetProvider);
    switch (ref.watch(currentPageProvider)) {
      case NavPage.FILES:
        return game;
      case NavPage.HOME:
        return const BrowserHomeWidget();
      default:
        return const SizedBox();
    }
  }
}
