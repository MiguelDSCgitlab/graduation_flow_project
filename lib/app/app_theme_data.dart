import 'package:c4_frontend_data_entry_flutter/app/app_drop_down_menu_item_scheme.dart';
import 'package:flutter/material.dart';

import 'app_color_scheme.dart';
import 'app_divider_theme.dart';
import 'app_elevated_button_theme_data.dart';
import 'app_icon_theme_data.dart';
import 'app_input_decoration.dart';
import 'app_segmented_button_theme_data.dart';
import 'app_text_theme.dart';

var customThemeData = ThemeData(
  useMaterial3: true,
  iconTheme: appIconThemeData,
  textTheme: appTextTheme,
  colorScheme: appColorScheme,
  inputDecorationTheme: appInputDecoration,
  elevatedButtonTheme: appElevatedButtonThemeData,
  segmentedButtonTheme: appSegmentedButtonTheme,
  dividerTheme: appDividerTheme,
  dropdownMenuTheme: appDropDownMenuScheme,
);
