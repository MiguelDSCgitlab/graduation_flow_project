import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

var appDividerTheme =
    const DividerThemeData(color: CupertinoColors.activeOrange);
