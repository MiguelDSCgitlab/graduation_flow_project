import 'package:flutter/material.dart';

var appIconThemeData = const IconThemeData(
  size: 20,
  color: Color(0xFFC5C5C5),
);
