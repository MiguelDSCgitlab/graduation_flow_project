import 'package:flutter/material.dart';

import 'app_color_scheme.dart';
import 'app_text_theme.dart';

var popupMenuTheme = PopupMenuThemeData(
  textStyle: appTextTheme.bodySmall?..copyWith(color: appColorScheme.onSurface),
);
