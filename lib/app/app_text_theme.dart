import 'package:flutter/material.dart';

var appTextTheme = const TextTheme(
  bodyLarge: TextStyle(color: Color(0xFFC5C5C5)),
  bodyMedium: TextStyle(color: Color(0xFFC5C5C5)),
  bodySmall: TextStyle(color: Color(0xFFFFFFFF)),
  displayLarge: TextStyle(color: Color(0xFFC5C5C5)),
  displayMedium: TextStyle(color: Color(0xFFC5C5C5)),
  displaySmall: TextStyle(color: Color(0xFFC5C5C5)),
  headlineLarge: TextStyle(color: Color(0xFFC5C5C5)),
  headlineMedium: TextStyle(color: Color(0xFFC5C5C5)),
  headlineSmall: TextStyle(color: Color(0xFFC5C5C5)),
  titleLarge: TextStyle(color: Color(0xFFC5C5C5)),
  titleMedium: TextStyle(color: Color(0xFFC5C5C5)),
  titleSmall: TextStyle(color: Color(0xFFC5C5C5)),
  labelLarge: TextStyle(color: Color(0xFFC5C5C5)),
  labelMedium: TextStyle(color: Color(0xFFC5C5C5)),
  labelSmall: TextStyle(color: Color(0xFFC5C5C5)),
);
