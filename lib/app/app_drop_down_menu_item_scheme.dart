import 'package:flutter/material.dart';

import 'app_color_scheme.dart';

var appDropDownMenuScheme = DropdownMenuThemeData(
  textStyle: TextStyle(color: appColorScheme.onSurface),
);
