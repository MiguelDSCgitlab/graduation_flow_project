import 'package:flutter/material.dart';

var appColorScheme = const ColorScheme(
  brightness: Brightness.light,
  primary: Color(0xFF1E1E1E),
  onPrimary: Color(0xFFFFFFFF),
  secondary: Color(0xFF1E1E1E),
  onSecondary: Color(0xFF1E1E1E),
  error: Color(0xFF1E1E1E),
  onError: Color(0xFF1E1E1E),
  background: Color(0xFF1E1E1E),
  onBackground: Color(0xFF1E1E1E),
  surface: Color(0xFF373737),
  onSurface: Color(0xFFC5C5C5),
);
