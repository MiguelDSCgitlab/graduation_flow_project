import 'package:flutter/material.dart';

var appSegmentedButtonTheme = const SegmentedButtonThemeData(
  style: ButtonStyle(
    iconColor: MaterialStatePropertyAll(Colors.white),
    foregroundColor: MaterialStatePropertyAll(Colors.white),
    backgroundColor: MaterialStatePropertyAll(Colors.grey),
  ),
);
