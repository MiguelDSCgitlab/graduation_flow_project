import 'package:flutter/cupertino.dart';

class AppContainerSelected extends StatelessWidget {
  final Widget child;

  const AppContainerSelected({
    super.key,
    required this.child,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          // color: CupertinoColors.activeOrange,
          border: Border.all(color: CupertinoColors.activeOrange),
          borderRadius: const BorderRadius.all(Radius.circular(2))),
      child: child,
    );
  }
}
