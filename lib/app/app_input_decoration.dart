import 'package:flutter/material.dart';

OutlineInputBorder _buildOutlineInputBorder(Color color) {
  return OutlineInputBorder(
    borderRadius: const BorderRadius.all(Radius.circular(4)),
    borderSide: BorderSide(
      color: color,
      width: 2,
    ),
  );
}

TextStyle _buildTextStyle(Color color, {double size = 16.0}) {
  return TextStyle(
    color: color,
    fontSize: size,
  );
}

var appInputDecoration = InputDecorationTheme(
  contentPadding: const EdgeInsets.all(16),
  floatingLabelBehavior: FloatingLabelBehavior.always,

  /*
      borders
       */
  enabledBorder: _buildOutlineInputBorder(Colors.grey),
  errorBorder: _buildOutlineInputBorder(Colors.red),
  focusedErrorBorder: _buildOutlineInputBorder(Colors.red),
  border: _buildOutlineInputBorder(Colors.white),
  focusedBorder: _buildOutlineInputBorder(Colors.blue),
  disabledBorder: _buildOutlineInputBorder(Colors.grey),

  /*
    text styles
   */
  suffixStyle: _buildTextStyle(const Color(0xFFC5C5C5)),
  counterStyle: _buildTextStyle(Colors.white, size: 12.0),
  floatingLabelStyle: _buildTextStyle(const Color(0xFFC5C5C5), size: 12.0),
  labelStyle: _buildTextStyle(const Color(0xFFC5C5C5), size: 12.0),
  errorStyle: _buildTextStyle(Colors.red, size: 12.0),
  helperStyle: _buildTextStyle(const Color(0xFFC5C5C5), size: 12.0),
  hintStyle: _buildTextStyle(const Color(0xFFC5C5C5), size: 12.0),
  prefixStyle: _buildTextStyle(const Color(0xFFC5C5C5)),
);
