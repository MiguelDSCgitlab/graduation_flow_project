import 'package:flutter/material.dart';

var appElevatedButtonThemeData = const ElevatedButtonThemeData(
  style: ButtonStyle(
    iconColor: MaterialStatePropertyAll(Colors.white),
    foregroundColor: MaterialStatePropertyAll(Colors.white),
    backgroundColor: MaterialStatePropertyAll(Colors.pink),
  ),
);
