import '../dependency_injection.dart';
import '../infrastructure/cdn_repository.dart';
import '../infrastructure/output_dto.dart';

class GetSupportedTechnologiesUseCase {
  late ContentDeliveryRepository contentDeliveryRepository;

  GetSupportedTechnologiesUseCase(
      {ContentDeliveryRepository? aContentDeliveryRepository}) {
    contentDeliveryRepository =
        aContentDeliveryRepository ?? apiCDN<ContentDeliveryRepository>();
  }

  Future<TechnologiesOutputDto> call() async {
    TechnologiesOutputDto technologiesOutputDto =
        await contentDeliveryRepository.getSupportedTechnologies();
    List<String>? technologies = technologiesOutputDto.technologies;
    if (technologies != null) {
      technologies.sort((a, b) => a.toLowerCase().compareTo(
            b.toLowerCase(),
          ));
      return TechnologiesOutputDto()..technologies = technologies;
    }
    return technologiesOutputDto;
  }
}
