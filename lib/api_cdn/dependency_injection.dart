import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;

import 'domain/get_supported_technologies_usecase.dart';
import 'infrastructure/api_config.dart';
import 'infrastructure/cdn_repository.dart';
import 'infrastructure/supported_technologies_get_dao.dart';

final apiCDN = GetIt.instance;

void setupApiCDN() {
  if (!GetIt.I.isRegistered<http.Client>()) {
    apiCDN.registerSingleton<http.Client>(http.Client());
  }

  if (!GetIt.I.isRegistered<CDNConfig>()) {
    apiCDN.registerSingleton<CDNConfig>(CDNConfig());
  }

  if (!GetIt.I.isRegistered<SupportedTechnologiesDao>()) {
    apiCDN.registerSingleton<SupportedTechnologiesDao>(
        SupportedTechnologiesDaoServer());
  }

  if (!GetIt.I.isRegistered<ContentDeliveryRepository>()) {
    apiCDN.registerSingleton<ContentDeliveryRepository>(
        ContentDeliveryRepository());
  }

  if (!GetIt.I.isRegistered<GetSupportedTechnologiesUseCase>()) {
    apiCDN.registerSingleton<GetSupportedTechnologiesUseCase>(
        GetSupportedTechnologiesUseCase());
  }
}
