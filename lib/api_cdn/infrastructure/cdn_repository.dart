import '../dependency_injection.dart';
import 'output_dto.dart';
import 'supported_technologies_get_dao.dart';

class ContentDeliveryRepository {
  late SupportedTechnologiesDao supportedTechnologiesDao;

  ContentDeliveryRepository({
    SupportedTechnologiesDao? aSupportedTechnologiesDao,
  }) {
    supportedTechnologiesDao =
        aSupportedTechnologiesDao ?? apiCDN<SupportedTechnologiesDao>();
  }

  Future<TechnologiesOutputDto> getSupportedTechnologies() async {
    return await supportedTechnologiesDao.getSupportedTechnologies();
  }
}
