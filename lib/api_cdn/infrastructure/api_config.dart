/*
  APIConfig is used to get endpoint for the api server.
  When running the app from terminal (using 'flutter run'), Flutter provides the
  option to pass environment properties. This can be used to inject compile/ build
  time variables. This is done by the 'dart-define' command, followed by a certain
  key/value pair. To change the default url settings use the following command when
  running the app:
  flutter run --dart-define=API_CONFIG=PROD
 */
class CDNConfig {
  String apiConfig = "";

  // String scheme = "http";
  // String c4BaseUrl = "136.144.189.154";
  String scheme = "https";
  String c4BaseUrl = "c4enhanced.nl/raw";

  // int port = 8086;
  final String supportedTechnologiesEndpoint = "png/file-names";

  CDNConfig({String? anApiConfig}) {
    apiConfig = anApiConfig ?? const String.fromEnvironment("API_CONFIG");
  }

  Uri constructSupportedTechnologiesUri() {
    Uri uri = Uri(
      scheme: scheme,
      host: c4BaseUrl,
      // port: port,
      path: supportedTechnologiesEndpoint,
    );
    return uri;
  }
}
