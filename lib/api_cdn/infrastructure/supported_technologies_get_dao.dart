import 'dart:convert';

import 'package:http/http.dart' as http;

import '../../api_raw_data/infrastructure/api_c4_error/api_exception.dart';
import '../../api_raw_data/infrastructure/api_c4_error/error_dto.dart';
import '../dependency_injection.dart';
import 'api_config.dart';
import 'output_dto.dart';

/*
Responsible for api call GET to /contexts
 */
abstract class SupportedTechnologiesDao {
  Future<TechnologiesOutputDto> getSupportedTechnologies();
}

/*
Retrieves contexts from API service.
Endpoint: /contexts?contextProperties=name,description
*/
class SupportedTechnologiesDaoServer implements SupportedTechnologiesDao {
  late CDNConfig apiConfig;
  late http.Client client;

  SupportedTechnologiesDaoServer({
    http.Client? aClient,
    CDNConfig? anApiConfig,
  }) {
    client = aClient ?? apiCDN<http.Client>();
    apiConfig = anApiConfig ?? apiCDN<CDNConfig>();
  }

  @override
  Future<TechnologiesOutputDto> getSupportedTechnologies() async {
    try {
      TechnologiesOutputDto outputDto = TechnologiesOutputDto();
      Uri uri = apiConfig.constructSupportedTechnologiesUri();

      var response = await client.get(uri);

      if (response.statusCode == 200) {
        var data = (jsonDecode(response.body) as List<dynamic>).cast<String>();
        outputDto.technologies = data;
        return outputDto;
      }

      if (response.statusCode == 400 || response.statusCode == 500) {
        var errorDto = ErrorDto.fromJson(json.decode(response.body));
        outputDto.errorDto = errorDto;
        return outputDto;
      }
      throw ApiException();
    } catch (err) {
      throw ApiException();
    }
  }
}
