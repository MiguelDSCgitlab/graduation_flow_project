# c4_frontend_data_entry_flutter

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://docs.flutter.dev/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials, samples, guidance on
mobile development, and a full API reference.

1) AttributeListTile Attribute onTap()
   ref.read(stateProviderAttributesState.notifier).state = attributeStateDto;
   ref.read(stateNotifierProviderSelectFormToDisplay.notifier).setIdle();

When done building:
ref.refresh(stateNotifierProviderSelectFormToDisplay.notifier)
.editAttributePressed()

2) stateNotifierProviderSelectFormToDisplay
   editAttributePressed()
   ref.read(stateNotifierProviderAttributes.notifier).loadEditAttributesForm();
   state = EditAttributeState();